<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->delete();

        $languages = [
            ['id' => 1, 'name' => 'Srpski', 'code' => 'sr', 'active' => 1],
            ['id' => 2, 'name' => 'Engleski', 'code' => 'en', 'active' => 1]
        ];

        foreach ($languages as $language) {
            App\Language::create($language);
        }
    }
}
