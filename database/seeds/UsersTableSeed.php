<?php

use Illuminate\Database\Seeder;

class UsersTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $users = [
            ['name' => 'Admin', 'email' => 'admin@imlek.rs', 'password'=> 'imlekadmin', 'active' =>1, 'role'  => 0],
            ['name' => 'Borislav Jagodić', 'email' => 'office@krooya.com', 'password'=> 'krooyadev021', 'active' =>1, 'role'  => 0]
        ];

        foreach ($users as $user) {
            App\User::create($user);
        }
    }
}
