<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryYearTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_year_translations', function (Blueprint $table) {
            $table->unsignedInteger('history_year_id');
            $table->unsignedInteger('language_id');
            $table->string('title', 200);
            $table->text('text');
            $table->unsignedTinyInteger('active');

            $table->primary(['history_year_id', 'language_id']);
            $table->foreign('history_year_id')->references('id')->on('history_years')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('history_year_translations');
    }
}
