<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedTinyInteger('category_id');
            $table->unsignedTinyInteger('preparation_time');
            $table->unsignedInteger('share_count')->default(0);
            $table->unsignedSmallInteger('order');
            $table->unsignedSmallInteger('active');
            $table->timestamps();
            $table->string('grid_image', 40)->nullable();
            $table->string('image', 40)->nullable();
            $table->string('video', 40)->nullable();
            $table->string('yt-id', 40)->nullable();

            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recipes');
    }
}
