<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeCategoryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_category_translations', function (Blueprint $table) {
            $table->unsignedInteger('language_id');
            $table->unsignedInteger('recipe_category_id');
            $table->string('title', 50);
            $table->string('slug', 50);

            $table->primary(['language_id', 'recipe_category_id'], 'recipe_category_translations_pk');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recipe_category_translations');
    }
}
