<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename', 30);
            $table->unsignedTinyInteger('type');
            $table->unsignedTinyInteger('active');
            $table->unsignedTinyInteger('order');
            $table->string('fallback_image', 40)->nullable();
            $table->unsignedTinyInteger('text_color');
            $table->unsignedTinyInteger('url_type')->nullable();
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slides');
    }
}
