<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackshotTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packshot_translations', function (Blueprint $table) {
            $table->unsignedInteger('packshot_id');
            $table->unsignedInteger('language_id');
            
            $table->string('label', 100);
            $table->string('text', 100);
            $table->string('slug', 100);


            $table->primary(['packshot_id', 'language_id']);
            $table->foreign('packshot_id')->references('id')->on('packshots')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('packshot_translations');
    }
}
