<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id')->unsigned();

            $table->string('featured_image', 40);
            $table->TinyInteger('image_orientation');
            $table->string('video', 40);
            $table->string('poster', 40);
            $table->text('web_address');
            $table->unsignedTinyInteger('active');
            $table->unsignedTinyInteger('order');
            $table->timestamps();
            $table->text('fb_link');
            $table->text('in_link');
            $table->text('yt_link');
            $table->text('ms_link');

            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade')->onUpdate('cascade');

            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
