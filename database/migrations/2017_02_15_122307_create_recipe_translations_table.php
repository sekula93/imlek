<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_translations', function (Blueprint $table) {
            $table->unsignedInteger('recipe_id');
            $table->unsignedInteger('language_id');
            $table->string('title', 150);
            $table->string('slug', 150);
            $table->text('excerpt');
            $table->text('preparation');
            $table->text('ingredients');
            
            $table->primary(['recipe_id', 'language_id']);
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recipe_translations');
    }
}
