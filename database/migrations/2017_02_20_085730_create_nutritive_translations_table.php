<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNutritiveTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nutritive_translations', function (Blueprint $table) {
            $table->unsignedInteger('nutritive_id');
            $table->unsignedInteger('language_id');
            $table->text('table');
            $table->text('legend');

            $table->primary(['nutritive_id', 'language_id']);

            $table->foreign('nutritive_id')->references('id')->on('nutritives')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nutritive_translations');
    }
}
