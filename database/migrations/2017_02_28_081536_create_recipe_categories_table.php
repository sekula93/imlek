<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('active');
        });

        // Schema::table('recipes', function (Blueprint $table) {
        //     $table->foreign('category_id')->references('id')->on('recipe_categories')->onDelete(null)->onUpdate('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recipe_categories');
    }
}
