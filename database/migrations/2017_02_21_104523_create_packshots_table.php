<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackshotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packshots', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('nutritive_id');
            $table->string('filename', 40);
            $table->unsignedTinyInteger('order');
            $table->unsignedTinyInteger('active');
            $table->timestamps();
            $table->unsignedTinyInteger('orientation');
            $table->string('label', 100);
            $table->text('text');
            $table->text('link');

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('nutritive_id')->references('id')->on('nutritives')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('packshots');
    }
}
