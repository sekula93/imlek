<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_years', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('year');
            $table->string('image_left', 40);
            $table->string('file_right', 40);
            $table->unsignedTinyInteger('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('history_years');
    }
}
