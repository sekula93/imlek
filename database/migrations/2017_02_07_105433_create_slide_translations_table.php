<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slide_translations', function (Blueprint $table) {
            $table->unsignedInteger('slide_id');
            $table->unsignedInteger('language_id');
            $table->string('title', 100);
            $table->string('subtitle', 150);
            $table->string('button_label', 30);
            $table->text('url');

            $table->primary(['slide_id', 'language_id']);
            $table->foreign('slide_id')->references('id')->on('slides')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slide_translations');
    }
}
