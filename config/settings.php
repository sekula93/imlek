<?php

    return [
        'application_name' => "Imlek",
        'token_size' => 30,
        'pagination' => [
            'items' => 15
        ],
        'image' => [
            'upload_path' => public_path().'/uploads/',
            'slide' => [
                'quality' => 95,
                'upload_dir' => '/uploads/slides/images/',
                'full_destination_filename' => false,
                'random_name' => true,
                'resize_original' => true,
                'resize_dimensions' => [1920,1080],
                'create_thumbs' => false,
                'thumb_dimensions' => []
            ],
            'certificate' => [
                'quality' => 95,
                'upload_dir' => '/uploads/certificate/images/',
                'full_destination_filename' => false,
                'random_name' => true,
                'resize_original' => true,
                'resize_dimensions' => [377,265],
                'create_thumbs' => false,
                'thumb_dimensions' => []
            ],
            'post' => [
                'featured' => [
                    'quality' => 100,
                    'upload_dir' => '/uploads/posts/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => true,
                    'resize_dimensions' => [520, 589, true],
                    'create_thumbs' => true,
                    'thumb_dimensions' => [
                        [520, 345, true, 95, 'grid']
                    ]
                ],
                'landscape' => [
                    'quality' => 100,
                    'upload_dir' => '/uploads/posts/landscape/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => false,
                    'resize_dimensions' => [520, 589, true],
                    'create_thumbs' => false,
                    'thumb_dimensions' => [
                        [520, 345, true, 95, 'grid']
                    ]
                ],
                'slider' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/posts/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => true,
                    'resize_dimensions' => [1200, 450],
                    'create_thumbs' => false,
                    'thumb_dimensions' => []
                ]
            ],
            'author' => [
                'quality' => 95,
                'upload_dir' => '/uploads/authors/',
                'full_destination_filename' => false,
                'random_name' => true,
                'resize_original' => true,
                'resize_dimensions' => [400, 400],
                'create_thumbs' => false,
                'thumb_dimensions' => []
            ],
            'recipe' => [
                'hero' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/recipes/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => true,
                    'resize_dimensions' => [1200, 450, true],
                    'create_thumbs' => true,
                    'thumb_dimensions' => []
                ],
                'grid' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/recipes/grid/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => true,
                    'resize_dimensions' => [286, 265, true],
                    'create_thumbs' => false,
                    'thumb_dimensions' => []
                ],
                'slider' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/recipes/slides/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => true,
                    'resize_dimensions' => [1200, 450],
                    'create_thumbs' => false,
                    'thumb_dimensions' => []
                ]
            ],

            'brand' => [
                'featured' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/brands/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => false,
                    'create_thumbs' => false,
                ]
            ],

            'product' => [
                'featured' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/products/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => true,
                    'resize_dimensions' => [600, 400],
                    'create_thumbs' => true,
                    'thumb_dimensions' => [
                        [370, 240, false, 80, 'thumbnails']
                    ]
                ],
                'poster' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/products/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => false,
                    'resize_dimensions' => [600, 400, true],
                    'create_thumbs' => false,
                    'thumb_dimensions' => []
                ],
                'packshot' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/products/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => true,
                    'resize_dimensions' => [600, 400],
                    'create_thumbs' => false,
                    'thumb_dimensions' => []
                ],
            ],
            'package' => [
                'featured' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/packages/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => false,
                    // 'resize_dimensions' => [600, 400],
                    'create_thumbs' => false,
                    // 'thumb_dimensions' => [
                    //     [370, 240, false, 80, 'thumbnails']
                    // ]
                ],
            ],
            'company' => [
                'featured' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/company/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => false,
                    // 'resize_dimensions' => [600, 400],
                    'create_thumbs' => false,
                    // 'thumb_dimensions' => [
                    //     [370, 240, false, 80, 'thumbnails']
                    // ]
                ],
                'slider' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/company/images/slides/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => false,
                    // 'resize_dimensions' => [600, 400],
                    'create_thumbs' => false,
                    // 'thumb_dimensions' => [
                    //     [370, 240, false, 80, 'thumbnails']
                    // ]
                ],
            ],
            'history' => [
                'left' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/history/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => true,
                    'resize_dimensions' => [745,466],
                    'create_thumbs' => false,
                    'thumb_dimensions' => []
                ],
                'right' => [
                    'quality' => 95,
                    'upload_dir' => '/uploads/history/images/',
                    'full_destination_filename' => false,
                    'random_name' => true,
                    'resize_original' => true,
                    'resize_dimensions' => [615,410],
                    'create_thumbs' => false,
                    'thumb_dimensions' => []
                ]
            ],
        ],
        'file' => [
            'post' => [
                'upload_dir' => '/uploads/posts/documents/',
                'random_name' => true
            ],
            'report' => [
                'upload_dir' => '/uploads/reports/documents/',
                'random_name' => true
            ],
            'certificate' => [
                'upload_dir' => '/uploads/certificates/documents/',
                'random_name' => true
            ],
            'catalogue' => [
                'upload_dir' => '/uploads/catalogues/',
                'random_name' => false
            ],
            'email' => [
                'upload_dir' => '/uploads/emails/',
                'random_name' => true
            ],
            
        ],
        'video' => [
            'slide' => [
                'upload_dir' => '/uploads/slides/videos/',
                'random_name' => true
            ],
            'product' => [
                'upload_dir' => '/uploads/products/videos/',
                'random_name' => true
            ]
        ]
    ];
