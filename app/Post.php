<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = ['category_id','attachment', 'image', 'landscape_image', 'active', 'order', 'type', 'featured', 'created_at'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function category()
    {
        return $this->belongsTo(PostCategory::class);
    }

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'post_translations')
            ->withPivot('title', 'slug', 'excerpt', 'text', 'attachment_label');
    }

    public function photos()
    {
        return $this->hasMany(PostPhoto::class);
    }

    public function social()
    {
        return $this->hasMany(SocialPost::class);
    }

    public function frontTranslations()
    {
        return $this->belongsToMany(Language::class, 'post_translations')
            ->where('language_id', languageId(\App::getLocale()))
            ->withPivot('title', 'slug', 'excerpt', 'text', 'attachment_label');
    }
}
