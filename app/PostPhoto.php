<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostPhoto extends Model
{
    protected $table = 'post_photos';

    protected $fillable = ['post_id', 'filename', 'active', 'order'];

    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
