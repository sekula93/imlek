<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table = 'slides';

    protected $fillable = ['filename', 'type', 'active', 'order', 'fallback_image', 'text_color', 'url_type'];

    public $timestamps = true;

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'slide_translations')
            ->withPivot('title', 'subtitle', 'button_label', 'url');
    }
}
