<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['featured_image', 'video', 'poster', 'web_address', 'order', 'active', 'image_orientation', 'brand_id', 'yt_link', 'fb_link', 'in_link', 'ms_link'];

    public $timestamps = true;

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'product_translations')
            ->withPivot('title', 'slug', 'text', 'excerpt');
    }

    public function packshots()
    {
        return $this->hasMany(Packshot::class)->orderBy('order');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
