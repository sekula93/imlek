<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'jobs';

    protected $fillable = ['active'];

	public $timestamps = true;

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'job_translations')
            ->withPivot('job_label');
    }
}
