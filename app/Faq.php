<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faqs';

    protected $fillable = ['active', 'order'];

    public $timestamps = false;


    public function translations()
    {
        return $this->belongsToMany(Language::class, 'faq_translations')
            ->withPivot('title', 'text');
    }
}
