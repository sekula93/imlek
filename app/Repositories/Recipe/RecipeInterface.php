<?php

namespace App\Repositories\Recipe;

interface RecipeInterface
{

    public function all();

    public function find($id);

    public function findBySlug($slug);
    
    public function findBySlugAdmin($slug);

    public function create($input);

    public function update($id, $input);

    public function delete($id);

    public function lists($value, $key = null);

    public function paginate();

    public function paginateForFront($items = null, $category = null);

    public function slugify($title);

    public function setOrder($id, $index);
}
