<?php

namespace App\Repositories\Recipe;

use App\Recipe;

class RecipeRepository implements RecipeInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $recipe;

    public function __construct(Recipe $recipe)
    {
        $this->recipe = $recipe;
    }

    public function all()
    {
        return $this->recipe->all();
    }

    public function find($id)
    {
        return $this->recipe->findOrFail($id);
    }

    public function findBySlug($slug)
    {
        $recipe = $this->recipe
            ->leftJoin('recipe_translations', 'recipe_translations.recipe_id', '=', 'recipes.id')->where('slug', $slug)
            ->leftJoin('languages', 'languages.id', '=', 'recipe_translations.language_id')->where('code', \App::getLocale())
            // ->leftJoin('users', 'users.id', '=', 'recipes.user_id')
            // ->leftJoin('recipe_author_profiles', 'recipe_author_profiles.user_id', '=', 'users.id')
            ->select(['recipe_translations.*', 'recipes.*'])
            ->first();
        return $recipe;
    }

    public function findBySlugAdmin($slug)
    {
        $recipe = $this->recipe->with('photos')
            ->leftJoin('recipe_translations', 'recipe_translations.recipe_id', '=', 'recipes.id')->where('slug', $slug)
            ->leftJoin('languages', 'languages.id', '=', 'recipe_translations.language_id')->where('code', \App::getLocale())
            ->select(['recipe_translations.*', 'recipes.*'])
            ->first();
        return $recipe;
    }

    public function recent($items)
    {
        $recipe = $this->recipe
            ->join('recipe_translations', 'recipe_translations.recipe_id', '=', 'recipes.id')
            ->join('languages', 'languages.id', '=', 'recipe_translations.language_id')->where('code', \App::getLocale())
            ->orderBy('recipes.id', 'DESC')
            ->simplePaginate($items);
        return $recipe;
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $recipe = $this->recipe->create([
            // 'user_id'           => $input['user_id'],
            'category_id'       => $input['category_id'],
            'preparation_time'  => $input['preparation_time'],
            'video'             => $input['video'],
            'image'             => $input['image'],
            'grid_image'        => $input['grid_image'],
            'active'            => $input['active'],
            'youtube'             => $input['yt-id'],
            'created_at'     => $input['created_at']
        ]);

        foreach ($input['trans'] as $language => $item) {
            $recipe->translations()->attach([$language => [
                'title'     => $item['title'],
                'slug'      => $item['slug'],
                'excerpt'   => $item['excerpt'],
                'preparation' => $item['preparation'],
                'ingredients' => json_encode($item['step']),
            ]]);
        }
        \DB::commit();
    }

    public function update($id, $input)
    {
        $recipe = $this->find($id);

        if (!$input['image']) {
            $input['image'] = $recipe->image;
        }
        if (!$input['grid_image']) {
            $input['grid_image'] = $recipe->grid_image;
        }
        if (!$input['video']) {
            $input['video'] = $recipe->video;
        }

        \DB::beginTransaction();
        $recipe->update([
            // 'user_id' => $input['user_id'],
            'category_id' => $input['category_id'],
            'preparation_time' => $input['preparation_time'],
            'video' => $input['video'],
            'image' => $input['image'],
            'grid_image' => $input['grid_image'],
            'active' => $input['active'],
            'youtube' => $input['yt-id'],
            'created_at'     => $input['created_at']
        ]);
        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'title' => $item['title'],
                'slug' => $item['slug'],
                'excerpt' => $item['excerpt'],
                'preparation' => $item['preparation'],
                'ingredients' => json_encode($item['step'])
            ];
        }
        $recipe->translations()->sync($syncArray);
        \DB::commit();
    }

    public function delete($id)
    {
        $recipe = $this->find($id);
        if (!$recipe) {
            return false;
        }

        return $recipe->delete();
    }

    public function lists($value, $key = null)
    {
        return $this->recipe->lists($value, $key);
    }

    public function paginate()
    {
        return $this->recipe->with('translations')
        ->leftJoin('recipe_categories', 'recipe_categories.id', '=', 'recipes.category_id')
        ->leftJoin('recipe_category_translations', 'recipe_category_translations.recipe_category_id', '=', 'recipe_categories.id')
         ->select('recipes.*', 'recipe_category_translations.*', 'recipe_category_translations.title as cat_title')
         ->where('language_id', 1)
        ->orderBy('order')->paginate(100);
    }

    public function paginateForFront($items = null, $keywords = null, $excludeId = null, $category = null)
    {
        $query = $this->recipe
            // ->with(['translations', function($query) {
            //     $query->where('language_id', 1);
            // }])
            ->with('frontTranslations')
            // ->leftJoin('recipe_translations', 'recipe_translations.recipe_id', '=', 'recipes.id')            
            ->leftJoin('recipe_categories', 'recipe_categories.id', '=', 'recipes.category_id')
            ->leftJoin('recipe_category_translations', 'recipe_category_translations.recipe_category_id', '=', 'recipe_categories.id')
            ->leftJoin('languages', 'recipe_category_translations.language_id', '=', 'languages.id')
            ->select('recipes.*', 'recipe_category_translations.*', 'languages.code', 'recipe_category_translations.slug as cat_slug')
            ->where('languages.code', \App::getLocale())
            ->where('recipes.active', 1);

            // 'recipe_category_translations.slug as cat_slug'
        // if ($keywords) {
        //     $query->where('recipe_translations.title', 'LIKE', '%'.$keywords.'%');
        // }
        if ($excludeId) {
            $query->where('recipes.id', '<>', $excludeId);
        }
        if ($category) {
            if (is_int($category)) {
                $query->where('recipes.category_id', $category);
            } else {
                $query->where('recipe_category_translations.slug', $category);
            }

        }

        $recipes = $query->orderBy('order')->paginate($items ?: config('settings.pagination.items', 15));

        return $recipes;
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $recipe = $this->recipe->find($input['id']);
            if ($recipe->slug == str_slug($input['title'])) {
                $slug = $recipe->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->recipe->translations()->where('slug', $slug)->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }

    public function decodeSteps($recipe)
    {
        $steps = [];
        foreach ($recipe->translations as $translation) {
            $steps[$translation->id] = json_decode($translation->pivot->ingredients);
        }
        return $steps;
    }

    public function setOrder($id, $index)
    {
        $recipe = $this->recipe->find($id);
        $recipe->update(['order' => $index]);
    }
}
