<?php

namespace App\Repositories\Brand;

use App\Brand;

class BrandRepository implements BrandInterface
{
	use \App\Repositories\Traits\StatusTrait;

	protected $brand;

	public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    public function all()
    {
    	return $this->brand->all();
    }

    public function getActive()
    {
        return $this->brand->where('active', 1)->get();
    }
    
    public function find($id)
    {
        return $this->brand->findOrFail($id);
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $brand = $this->brand->create([
            'name'=> $input['name'],
            'active' => $input['active'],
            'featured_image'=> $input['featured_image'],
        ]);

        \DB::commit();
        return $brand;
    }

    public function update($id, $input)
    {
        $brand = $this->find($id);

        if (!$input['featured_image']) {
            $input['featured_image'] = $brand->featured_image;
        }

        \DB::beginTransaction();
        $brand->update([
            'name'=> $input['name'],
            'active' => $input['active'],
            'featured_image' => $input['featured_image'],
        ]);
        
        \DB::commit();
    }

    public function delete($id)
    {
        $brand = $this->find($id);
        if (!$brand) {
            return false;
        }

        return $brand->delete();
    }

    public function paginate()
    {
        return $this->brand->orderBy('order')->paginate(config('settings.pagination.items', 15));
    }

    public function setOrder($id, $index)
    {
        $brand = $this->brand->find($id);
        $brand->update(['order' => $index]);
    }
}