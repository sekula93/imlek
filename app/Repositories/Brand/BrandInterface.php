<?php

namespace App\Repositories\Brand;

interface BrandInterface
{

    public function all();

    public function find($id);

    public function create($input);

    public function update($id, $input);

    public function paginate();

    public function setOrder($id, $index);
    
}