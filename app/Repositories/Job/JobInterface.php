<?php

namespace App\Repositories\Job;

interface JobInterface
{

    public function all();

    public function getActiveJobs();

    public function find($id);

    public function create($input);

    public function update($id, $input);

    public function createSocial($input);

    public function delete($id);

    public function lists($value, $key = null);

    public function recent($items, $except = null);

    public function paginate();

    public function paginateSocial($type);

    public function slugify($title);
}
