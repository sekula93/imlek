<?php

namespace App\Repositories\Job;

use App\Job;

class JobRepository implements JobInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $job;

    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    public function all()
    {
        return $this->job->all();
    }

    public function getActiveJobs()
    {
        return $this->job->with('translations')->where('active', 1)->get();
    }

    public function find($id)
    {
        return $this->job->findOrFail($id);
    }


    public function create($input)
    {
        \DB::beginTransaction();
        $job = $this->job->create([
            'active'    => $input['active'],
        ]);

        foreach ($input['trans'] as $language => $item) {
            $job->translations()->attach([$language => [
                'job_label' => $item['job_label'],
            ]]);
        }
        \DB::commit();
        return $job;
    }

    public function update($id, $input)
    {
        $job = $this->find($id);

        \DB::beginTransaction();
        $job->update([
            'active' => $input['active']
        ]);

        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'job_label' => $item['job_label'],
            ];
        }
        $job->translations()->sync($syncArray);
        \DB::commit();
    }

    public function createSocial($input)
    {
        \DB::beginTransaction();
        $job = $this->job->create([
            'type'     => $input['type'],
            'image'     => $input['image'],
            'active'    => 0
        ]);

        $job->social()->create([
            'link'     => $input['link'],
            'message'     => $input['message'],
            'origin_id'=> $input['origin_id'],
        ]);
        \DB::commit();
        return $job;
    }

    public function delete($id)
    {
        $job = $this->find($id);
        if (!$job) {
            return false;
        }

        return $job->delete();
    }

    public function lists($value, $key = null)
    {
        return $this->job->lists($value, $key);
    }

    public function recent($items, $type = 1, $except = null)
    {
        $query = $this->job
            ->join('job_translations', 'job_translations.job_id', '=', 'jobs.id')
            ->join('languages', 'job_translations.language_id', '=', 'languages.id')
            ->where('jobs.active', 1)->where('jobs.type', $type);
        $query->select('job_translations.*', 'jobs.*');
        if ($except) {
            $query->whereNotIn('jobs.id', $except);
        }

        return $query->orderBy('jobs.created_at', 'DESC')->simplePaginate($items);
    }

    public function paginate()
    {
        return $this->job->with('translations')->orderBy('created_at', 'DESC')->paginate(config('settings.pagination.items', 15));
    }

    public function paginateSocial($type)
    {
        return $this->job->with('social')->where('type', $type)->orderBy('created_at', 'DESC')->paginate(config('settings.pagination.items', 15));
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $job = $this->job->find($input['id']);
            if ($job->slug == str_slug($input['title'])) {
                $slug = $job->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->job->translations()->where('slug', $slug)->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }

    public function latestByType($items, $type, $active = 1)
    {
        return $this->job->with('social')->where('active', $active)->where('type', $type)->orderBy('created_at', 'DESC')->simplePaginate($items);
    }

    public function categoriesRecent()
    {
        $news = $this->recent(5, 1);
        $facebook = $this->latestByType(3, 2);
        $instagram = $this->latestByType(3, 3);
        return compact('news', 'instagram', 'facebook');
    }

    public function mixedRecent($items, $exclude = null)
    {
        $query = $this->job->with('translations', 'social')
            ->leftJoin('job_translations', 'job_translations.job_id', '=', 'jobs.id')
            ->leftJoin('social_jobs', 'social_jobs.job_id', '=', 'jobs.id')
            ->leftJoin('languages', 'job_translations.language_id', '=', 'languages.id')
            ->where('jobs.active', 1);
        $query->select('social_jobs.*', 'job_translations.*', 'jobs.*');
        if ($exclude) {
            $query->whereNotIn('jobs.id', $exclude);
        }
        return $query->orderBy('jobs.id', 'DESC')->get();
        // return $query->orderBy('jobs.id', 'DESC')->simplePaginate($items);
    }

}
