<?php

namespace App\Repositories\Post;

interface PostInterface
{

    public function all();

    public function find($id);

    public function findBySlug($slug);

    public function create($input);

    public function update($id, $input);

    public function createSocial($input);

    public function delete($id);

    public function lists($value, $key = null);

    public function recent($items, $except = null);

    public function paginate();

    public function paginateAdmin();

    public function getBySlug($slug);

    public function paginateSocial($type);

    public function slugify($title);

    public function setOrder($id, $index);

}
