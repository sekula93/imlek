<?php

namespace App\Repositories\Post;

use App\Post;

class PostRepository implements PostInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function all()
    {
        return $this->post->all();
    }

    public function find($id)
    {
        return $this->post
            ->with(['category' => function ($query) {
                $query->with('translations');
            }])
        ->findOrFail($id);
    }

    public function findBySlug($slug)
    {
        return $this->post
            ->with(['photos' => function ($query) {
                $query->where('post_photos.active', 1)->orderBy('order');
            }])
            ->leftJoin('post_translations', 'post_translations.post_id', '=', 'posts.id')->where('slug', $slug)
            // ->leftJoin('languages', 'post_translations.language_id', '=', 'languages.id')->where('languages.code', \App::getLocale())
            ->where('posts.active', 1)->first();
    }

    public function create($input)
    {
        if (isset($input['checkbox'])) {
            $input['checkbox'] = 1;
        } else {
            $input['checkbox'] = 0;
        }

        \DB::beginTransaction();
        $post = $this->post->create([
            'type'      => 1,
            'attachment'=> $input['attachment'],
            'image'     => $input['image'],
            'landscape_image'     => $input['landscape_image'],
            'active'    => $input['active'],
            'featured'  => $input['checkbox'],
            'created_at' => $input['created_at'],
            'category_id' => $input['category_id']
        ]);

        foreach ($input['trans'] as $language => $item) {
            $post->translations()->attach([$language => [
                'title'     => $item['title'],
                'slug'      => $item['slug'],
                'excerpt'   => $item['excerpt'],
                'text'      => $item['text'],
                'attachment_label' => $item['attachment_label'],
            ]]);
        }
        \DB::commit();
        return $post;
    }

    public function update($id, $input)
    {
        $post = $this->find($id);

        if (!$input['image']) {
            $input['image'] = $post->image;
        }
        if (!$input['landscape_image']) {
            $input['landscape_image'] = $post->landscape_image;
        }
        if (!$input['attachment']) {
            $input['attachment'] = $post->attachment;
        }

        if (isset($input['checkbox'])) {
            $input['checkbox'] = 1;
        } else {
            $input['checkbox'] = 0;
        }

        \DB::beginTransaction();
        $post->update([
            'attachment' => $input['attachment'],
            'image' => $input['image'],
            'landscape_image' => $input['landscape_image'],
            'active' => $input['active'],
            'featured'  => $input['checkbox'],
            'created_at' => $input['created_at'],
            'category_id' => $input['category_id']
        ]);
        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'title' => $item['title'],
                'slug' => $item['slug'],
                'excerpt' => $item['excerpt'],
                'text' => $item['text'],
                'attachment_label' => $item['attachment_label'],
            ];
        }
        $post->translations()->sync($syncArray);
        \DB::commit();
    }

    public function createSocial($input)
    {
        \DB::beginTransaction();
        $post = $this->post->create([
            'type'     => $input['type'],
            'image'     => $input['image'],
            'active'    => 0
        ]);

        $post->social()->create([
            'link'     => $input['link'],
            'message'     => $input['message'],
            'origin_id'=> $input['origin_id'],
        ]);
        \DB::commit();
        return $post;
    }

    public function delete($id)
    {
        $post = $this->find($id);
        if (!$post) {
            return false;
        }

        return $post->delete();
    }

    public function lists($value, $key = null)
    {
        return $this->post->lists($value, $key);
    }

    public function recent($items, $except = null)
    {
        $query = $this->post
            ->with(['category' => function ($query) {
                $query->with('translations');
            }])
            ->join('post_translations', 'post_translations.post_id', '=', 'posts.id')
            ->join('languages', 'post_translations.language_id', '=', 'languages.id')
            // ->join('post_categories', 'post_categories.id', '=', 'posts.category_id')
            // ->join('post_category_translations', 'post_category_translations.post_category_id', '=', 'post_categories.id')
            ->where('posts.active', 1)
            ->where('languages.code', \App::getLocale());
        // $query->select('post_translations.*', 'posts.*', 'post_category_translations.slug as cat_slug');
        if ($except) {
            $query->whereNotIn('posts.id', $except);
        }
        return $query->orderBy('posts.order')->simplePaginate($items);
    }

    public function paginate()
    {
        return $this->post
            ->with('translations')
            // ->join('post_translations', 'post_translations.post_id', '=', 'posts.id')
            ->leftJoin('post_categories', 'post_categories.id', '=', 'posts.category_id')
            ->leftJoin('post_category_translations', 'post_category_translations.post_category_id', '=', 'post_categories.id')
            ->join('languages', 'post_category_translations.language_id', '=', 'languages.id')
            ->select('posts.*', 'post_category_translations.slug as cat_slug', 'languages.code')
            ->where('languages.code', \App::getLocale())
            ->where('posts.active', 1)
            ->where('posts.category_id', 1)
            ->orderBy('posts.order', 'ASC')->paginate(8);
    }

    public function paginateAdmin()
    {
        return $this->post
            ->join('post_translations', 'post_translations.post_id', '=', 'posts.id')
            ->join('languages', 'post_translations.language_id', '=', 'languages.id')
            ->select('posts.*', 'post_translations.*', 'languages.code')
            ->where('languages.code', 'sr')
            ->orderBy('order')->paginate(150);
    }

    public function getBySlug($slug)
    {
        $query = $this->post
            // ->with(['category' => function ($query) {
                // $query->with('translations');
            // }])
            ->with('translations')

            // *
            // ->join('post_translations', 'post_translations.post_id', '=', 'posts.id')
            
            ->leftJoin('post_categories', 'post_categories.id', '=', 'posts.category_id')
            ->leftJoin('post_category_translations', 'post_category_translations.post_category_id', '=', 'post_categories.id')
            
            // // *
            ->leftJoin('languages', 'post_category_translations.language_id', '=', 'languages.id')

            ->select('posts.*', 'post_category_translations.slug as cat_slug', 'languages.code')
            // // *
            ->where('languages.code', \App::getLocale())
            ->where('posts.active', 1);

            if ($slug != 'sve-vesti' && $slug != 'all-news') {

                $query->where('post_category_translations.slug', $slug);
            } else {

            }


            return $query->orderBy('posts.order')->paginate(6);
            

    }

    public function paginateSocial($type)
    {
        return $this->post->with('social')->where('type', $type)->orderBy('created_at', 'DESC')->paginate(config('settings.pagination.items', 15));
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $post = $this->post->find($input['id']);
            if ($post->slug == str_slug($input['title'])) {
                $slug = $post->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->post->leftJoin('post_translations', 'post_translations.post_id', '=', 'posts.id')->where('slug', str_slug($slug))->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }

    public function latestByType($items, $type, $active = 1)
    {
        return $this->post->with('social')->where('active', $active)->where('type', $type)->orderBy('created_at', 'DESC')->simplePaginate($items);
    }

    public function categoriesRecent()
    {
        $news = $this->recent(5, 1);
        $facebook = $this->latestByType(3, 2);
        $instagram = $this->latestByType(3, 3);
        return compact('news', 'instagram', 'facebook');
    }

    public function mixedRecent($items, $exclude = null)
    {
        $query = $this->post->with('translations', 'social')
            ->leftJoin('post_translations', 'post_translations.post_id', '=', 'posts.id')
            ->leftJoin('social_posts', 'social_posts.post_id', '=', 'posts.id')
            ->leftJoin('languages', 'post_translations.language_id', '=', 'languages.id')
            ->where('posts.active', 1);
        $query->select('social_posts.*', 'post_translations.*', 'posts.*');
        if ($exclude) {
            $query->whereNotIn('posts.id', $exclude);
        }
        return $query->orderBy('posts.id', 'DESC')->get();
        // return $query->orderBy('posts.id', 'DESC')->simplePaginate($items);
    }

    public function setOrder($id, $index)
    {
        $post = $this->post->find($id);
        $post->update(['order' => $index]);
    }

}
