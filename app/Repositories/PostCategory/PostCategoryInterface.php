<?php

namespace App\Repositories\PostCategory;

interface PostCategoryInterface
{

    public function all();

    public function find($id);

    public function create($input);

    public function update($id, $input);

    public function delete($id);

    public function listCategories();

    public function listCategoriesForAdmin();

    public function paginate();
}
