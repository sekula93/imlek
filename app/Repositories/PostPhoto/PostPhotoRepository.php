<?php

namespace App\Repositories\PostPhoto;

use App\PostPhoto;
use App\Services\File\Image as ImageService;

class PostPhotoRepository implements PostPhotoInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $photo;
    protected $image;

    public function __construct(PostPhoto $photo, ImageService $image)
    {
        $this->photo = $photo;
        $this->image = $image;
    }

    public function all()
    {
        return $this->photo->all();
    }

    public function find($id)
    {
        return $this->photo->findOrFail($id);
    }

    public function create($id, $fileName)
    {
        return $this->photo->create([
            'post_id' => $id,
            'filename' => $fileName,
            // 'cover' => $this->_isCover($id),
            'order' => 0,
            'active' => 1
        ]);
    }

    public function delete($id)
    {
        $photo = $this->find($id);

        $this->image->delete($photo->filename, config('settings.image.post'));

        return $photo->delete();
    }

    // public function order($id)
    // {
    //     return $this->photo->where('active', 1)->orderBy('title')->pluck('title', 'id')->toArray();
    // }

    public function setCover($id)
    {
        $photo = $this->find($id);
        $this->photo->where('post_id', $photo->post_id)->update(['cover' => 0]);
        return $photo->update(['cover' => 1, 'active' => 1]);
    }

    public function setOrder($id, $index)
    {
        $file = $this->find($id);
        return $file->update(['order' => $index]);
    }

    // private function _isCover($id)
    // {
    //     if ($this->photo->where('post_id', $id)->count() < 1) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}
