<?php

namespace App\Repositories\Certificate;

use App\Certificate;

class CertificateRepository implements CertificateInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $certificate;

    public function __construct(Certificate $certificate)
    {
        $this->certificate = $certificate;
    }

    public function all()
    {
        return $this->certificate->all();
    }

    public function find($id)
    {
        return $this->certificate->findOrFail($id);
    }


    public function create($input)
    {
        \DB::beginTransaction();
        $certificate = $this->certificate->create([
            'attachment'=> $input['attachment'],
            'active'    => $input['active'],
            'featured_image'=> $input['featured_image'],
        ]);

        foreach ($input['trans'] as $language => $item) {
            $certificate->translations()->attach([$language => [
                'attachment_label' => $item['attachment_label'],
            ]]);
        }
        \DB::commit();
        return $certificate;
    }

    public function update($id, $input)
    {
        $certificate = $this->find($id);

        if (!$input['attachment']) {
            $input['attachment'] = $certificate->attachment;
        }
        if (!$input['featured_image']) {
            $input['featured_image'] = $certificate->featured_image;
        }

        \DB::beginTransaction();
        $certificate->update([
            'attachment' => $input['attachment'],
            'active' => $input['active'],
            'featured_image' => $input['featured_image']
        ]);

        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'attachment_label' => $item['attachment_label'],
            ];
        }
        $certificate->translations()->sync($syncArray);
        \DB::commit();
    }

    public function createSocial($input)
    {
        \DB::beginTransaction();
        $certificate = $this->certificate->create([
            'type'     => $input['type'],
            'image'     => $input['image'],
            'active'    => 0
        ]);

        $certificate->social()->create([
            'link'     => $input['link'],
            'message'     => $input['message'],
            'origin_id'=> $input['origin_id'],
        ]);
        \DB::commit();
        return $certificate;
    }

    public function delete($id)
    {
        $certificate = $this->find($id);
        if (!$certificate) {
            return false;
        }

        return $certificate->delete();
    }

    public function lists($value, $key = null)
    {
        return $this->certificate->lists($value, $key);
    }

    public function recent($items, $type = 1, $except = null)
    {
        $query = $this->certificate
            ->join('certificate_translations', 'certificate_translations.certificate_id', '=', 'certificates.id')
            ->join('languages', 'certificate_translations.language_id', '=', 'languages.id')
            ->where('certificates.active', 1)->where('certificates.type', $type);
        $query->select('certificate_translations.*', 'certificates.*');
        if ($except) {
            $query->whereNotIn('certificates.id', $except);
        }

        return $query->orderBy('certificates.created_at', 'DESC')->simplePaginate($items);
    }

    public function paginate()
    {
        return $this->certificate->with('translations')->orderBy('order')->paginate(config('settings.pagination.items', 15));
    }

    public function paginateForFront($items)
    {
        $query = $this->certificate
            ->join('certificate_translations', 'certificate_translations.certificate_id', '=', 'certificates.id')
            ->join('languages', 'certificate_translations.language_id', '=', 'languages.id')
            ->where('certificates.active', 1)->where('languages.code', \App::getLocale());
        $query->select('certificate_translations.*', 'certificates.*', 'languages.code');
        
        return $query->orderBy('certificates.order')->paginate($items);
    }

    public function paginateSocial($type)
    {
        return $this->certificate->with('social')->where('type', $type)->orderBy('created_at', 'DESC')->paginate(config('settings.pagination.items', 15));
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $certificate = $this->certificate->find($input['id']);
            if ($certificate->slug == str_slug($input['title'])) {
                $slug = $certificate->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->certificate->translations()->where('slug', $slug)->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }

    public function latestByType($items, $type, $active = 1)
    {
        return $this->certificate->with('social')->where('active', $active)->where('type', $type)->orderBy('created_at', 'DESC')->simplePaginate($items);
    }

    public function categoriesRecent()
    {
        $news = $this->recent(5, 1);
        $facebook = $this->latestByType(3, 2);
        $instagram = $this->latestByType(3, 3);
        return compact('news', 'instagram', 'facebook');
    }

    public function mixedRecent($items, $exclude = null)
    {
        $query = $this->certificate->with('translations', 'social')
            ->leftJoin('certificate_translations', 'certificate_translations.certificate_id', '=', 'certificates.id')
            ->leftJoin('social_certificates', 'social_certificates.certificate_id', '=', 'certificates.id')
            ->leftJoin('languages', 'certificate_translations.language_id', '=', 'languages.id')
            ->where('certificates.active', 1);
        $query->select('social_certificates.*', 'certificate_translations.*', 'certificates.*');
        if ($exclude) {
            $query->whereNotIn('certificates.id', $exclude);
        }
        return $query->orderBy('certificates.id', 'DESC')->get();
        // return $query->orderBy('certificates.id', 'DESC')->simplePaginate($items);
    }

    public function setOrder($id, $index)
    {
        $certificate = $this->certificate->find($id);
        $certificate->update(['order' => $index]);
    }

}
