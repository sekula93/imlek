<?php

namespace App\Repositories\Certificate;

interface CertificateInterface
{

    public function all();

    public function find($id);

    public function create($input);

    public function update($id, $input);

    public function createSocial($input);

    public function delete($id);

    public function lists($value, $key = null);

    public function recent($items, $except = null);

    public function paginate();

    public function paginateForFront($items);

    public function paginateSocial($type);

    public function slugify($title);
    
    public function setOrder($id, $index);
}
