<?php

namespace App\Repositories\User;

use App\User;

class UserRepository implements UserInterface
{

    use \App\Repositories\Traits\StatusTrait;

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function all()
    {
        return $this->user->all();
    }

    public function bloggers()
    {
        return $this->user->where('role', 2)->where('active', 1)->get();
    }

    public function count($role)
    {
        return $this->user->where('role', $role)->count();
    }

    public function countKeyValue($key, $value)
    {
        return $this->user->where($key, $value)->count();
    }

    public function find($id)
    {
        return $this->user->findOrFail($id);
    }

    public function findByEmail($email)
    {
        return $this->user->where('email', '=', $email)->where('active', '=', 1)->first();
    }

    public function create(Type\User $userType)
    {
        \DB::beginTransaction();

        $userTypeData = $userType->prepareData();
        $user = $this->user->create($userTypeData);

        if ($userType->userRelation) {
            $user->{$userType->userRelation}()->create($userType->prepareLinkedData());
        }

        $user->plainPassword = isset($memberData['password']) ? $memberData['password'] : null;

        \DB::commit();

        return $user;
    }

    public function update($id, Type\User $userType)
    {
        \DB::beginTransaction();

        $user = $this->find($id);
        $user->update($userType->prepareData());

        if ($userType->userRelation && $updateData = $userType->prepareLinkedData()) {
            $user->{$userType->userRelation}()->update($updateData);
        }

        \DB::commit();
        return true;
    }

    public function delete($id)
    {
        $user = $this->find($id);

        return $user->delete();
    }

    public function lists($value, $key = null)
    {
        return $this->user->lists($value, $key);
    }

    public function listsRecipeAuthors()
    {
        return $this->user->where('role', 5)->get();
    }

    public function paginateAll()
    {

        return $this->user->paginate(\Config::get('settings.pagination.items', 15));
    }

    public function paginateUsers($role)
    {
        return $this->user
            ->where('role', $role)
            ->orderBy('created_at', 'desc')
            ->paginate(\Config::get('settings.pagination.items', 15));
    }

    public function paginateKeyValue($key, $value)
    {
        return $this->user
            ->where($key, $value)
            ->orderBy('created_at', 'desc')
            ->paginate(\Config::get('settings.pagination.items', 15));
    }

    public function setPassword($id, $password)
    {
        $user = $this->find($id);

        return $user->update(['password' => $password, 'active' => 1]);
    }

    public function loggedInUpdate($id)
    {
        $user = $this->find($id);

        return $user->update(['logged_at' => \DB::raw('CURRENT_TIMESTAMP')]);
    }
}
