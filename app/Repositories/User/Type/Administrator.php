<?php

namespace App\Repositories\User\Type;

class Administrator extends User
{
    /**
     * Relation with table defined in User model
     *
     * @var string
     */
    public $userRelation = null;
    // public $userRelation = 'adminData';

    /**
     * Default active flag state
     *
     * @var integer
     */
    public $defaultActive = 0;

    /**
     * Return client role
     *
     * @return int
     */
    public function getRole()
    {
        return 0;
    }

    public function prepareData()
    {
        if (isset($this->input['_method']) && $this->input['_method'] == 'PUT') {
            $mergeArray = [];
            if ($this->input['password'] != '') {
                $mergeArray['password'] = $this->input['password'];
            }
        } else {
            $mergeArray = [
                'password' => isset($this->input['password']) ? $this->input['password'] : str_random(12)
            ];
        }
        return array_merge($this->memberArray, $mergeArray);
    }

    public function prepareLinkedData()
    {
        return [
            'phone' => $this->input['phone']
        ];
    }
}
