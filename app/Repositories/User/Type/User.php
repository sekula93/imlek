<?php

namespace App\Repositories\User\Type;

abstract class User
{
    /**
     * User properties
     *
     * @var array
     */
    protected $memberArray;

    /**
     * Input data
     *
     * @var array
     */
    protected $input;

    /**
     * Linked model
     *
     * @var string
     */
    public $linkedModel;

    /**
     * Return role id
     *
     * @return int
     */
    abstract public function getRole();

    abstract public function prepareData();

    abstract public function prepareLinkedData();

    /**
     * User constructor
     *
     * @param array $input
     */
    public function __construct($input)
    {
        $this->memberArray = [
            'name' => $input['name'],
            // 'last_name' => $input['last_name'],
            'email' => $input['email'],
            'role' => $this->getRole(),
            'active' => $input['active']
        ];

        $this->input = $input;
    }
}
