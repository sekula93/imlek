<?php

namespace App\Repositories\User\Type;

class RecipeAuthor extends User
{
    const USER_ROLE = 5;

    /**
     * Relation with table defined in User model
     *
     * @var string
     */
    public $userRelation = 'recipeAuthorProfile';

    /**
     * Default active flag state
     *
     * @var integer
     */
    public $defaultActive = 0;

    /**
     * Return client role
     *
     * @return int
     */
    public function getRole()
    {
        return 5;
    }

    public function prepareData()
    {
        if (isset($this->input['_method']) && $this->input['_method'] == strtolower('PUT')) {
            $mergeArray = [];
            if (isset($this->input['password']) && $this->input['password'] != '') {
                $mergeArray['password'] = $this->input['password'];
            }
        } else {
            $mergeArray = [
                'password' => isset($this->input['password']) ? $this->input['password'] : str_random(12)
            ];
        }
        return array_merge($this->memberArray, $mergeArray);
    }

    public function prepareLinkedData()
    {
        if ($this->input['image']) {
            $data['image'] = $this->input['image'];
            return $data;
        } else {
            return null;
        }
    }
}
