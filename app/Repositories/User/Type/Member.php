<?php

namespace App\Repositories\User\Type;

class Member extends User
{
    /**
     * Relation with table defined in User model
     *
     * @var string
     */
    public $userRelation = 'memberProfile';

    /**
     * Default active flag state
     *
     * @var integer
     */
    public $defaultActive = 0;

    /**
     * Return client role
     *
     * @return int
     */
    public function getRole()
    {
        return 3;
    }

    public function prepareData()
    {
        if (isset($this->input['_method']) && $this->input['_method'] == 'PUT') {
            $mergeArray = [];
            if ($this->input['password'] != '') {
                $mergeArray['password'] = $this->input['password'];
            }
        } else {
            $mergeArray = [
                'password' => isset($this->input['password']) ? $this->input['password'] : str_random(12)
            ];
        }
        return array_merge($this->memberArray, $mergeArray);
    }

    public function prepareLinkedData()
    {
        $data = [
            'phone' => $this->input['phone'],
            'address' => $this->input['address'],
            'city' => $this->input['city']
        ];

        if ($this->input['image']) {
            $data['image'] = $this->input['image'];
            // if ($this->input['admin']) {
            //     $data['image_status'] = 3;
            // } else {
            //     $data['image_status'] = 1;
            // }
        } else {
            // $this->input['image_status'] ? $data['image_status'] = $this->input['image_status'] : null;
        }

        // if ($this->input['image_status'] == 2) {
        //     $data['image'] = '';
        // }

        return $data;
    }
}
