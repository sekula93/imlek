<?php

namespace App\Repositories\User;

interface UserInterface
{

    public function all();

    public function count($role);

    public function find($id);

    public function findByEmail($email);

    public function create(Type\User $userType);

    public function update($id, Type\User $userType);

    public function delete($id);

    public function lists($value, $key = null);

    public function paginateAll();

    public function paginateUsers($role);

    public function paginateKeyValue($key, $value);

    public function setPassword($id, $password);

    public function changeStatus($id);

    public function loggedInUpdate($id);
}
