<?php

namespace App\Repositories\Slide;

use App\Slide;

class SlideRepository implements SlideInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $slide;

    public function __construct(Slide $slide)
    {
        $this->slide = $slide;
    }

    public function all()
    {
        return $this->slide->all();
    }

    public function getImages($limit = null)
    {
        return $this->slide->where('type', 1)->orderBy('order')->orderBy('id', 'DESC')->get();
    }

    public function getVideos()
    {
        return $this->slide->where('type', 2)->orderBy('order')->orderBy('id', 'DESC')->get();
    }

    public function getImagesAndVideos() {
        return $this->slide->orderBy('order')->orderBy('id', 'DESC')->get();
    }

    private function _makeArrayItem($slide)
    {
        $translation = $slide->translations->where('code', \App::getLocale())->first();
        // switch ($slide->text_color) {
        //     case 1:
        //         $textColorClass = 'white-text';
        //         break;
        //     case 2:
        //         $textColorClass = 'dark-text';
        //         break;
        //     default:
        //         $textColorClass = 'no-shadding';
        //         break;
        // }
        return [
            'type' => $slide->type == 1 ? 'image' : 'video',
            'filename' => $slide->filename,
            'fallback_image' => $slide->fallback_image,
            'title' => $translation->pivot->title,
            'subtitle' => $translation->pivot->subtitle,
            // 'button_label' => $translation->pivot->button_label,
            // 'text_color_class' => $textColorClass,
            // 'url_type' => $slide->url_type,
            'url' => $translation->pivot->url,
            'order' =>$slide->order
        ];
    }

    public function getForFront($limit = null)
    {
        $videos = $this->slide->with('translations')->where('type', 2)->where('active', 1)->orderBy('order')->orderBy('id', 'DESC')->get();
        $images = $this->slide->where('type', 1)->where('active', 1)->orderBy('order')->orderBy('id', 'DESC')->limit($limit ?: 5)->get();

        $sliderArray = [];
        foreach ($videos as $video) {
            array_push($sliderArray, $this->_makeArrayItem($video));
        }
        foreach ($images as $image) {
            array_push($sliderArray, $this->_makeArrayItem($image));
        }

        $order = [];
        foreach($sliderArray as $key => $item) {
            $order[$key] = $item['order'];
        }
        // $sliderArray = $this->slide->with('translations')->where('active', 1)->orderBy('order')->get();

        array_multisort($order, SORT_ASC , $sliderArray);
        return $sliderArray;
    }



    public function find($id)
    {
        return $this->slide->with('translations')->find($id);
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $slide = $this->slide->create([
            'filename'  => $input['filename'],
            'fallback_image'  => $input['fallback_image'],
            'type'      => $input['type'],
            // 'text_color'=> $input['text_color'],
            // 'url_type'  => $input['url_type'],
            'active'    => $input['active']
        ]);

        foreach ($input['trans'] as $language => $item) {
            $slide->translations()->attach([$language => [
                'title'         => $item['title'],
                'subtitle'      => $item['subtitle'],
                // 'button_label'  => $item['button_label'],
                'url'           => $item['url']
            ]]);
        }
        \DB::commit();
    }

    public function update($id, $input)
    {
        $slide = $this->find($id);

        if (!isset($input['filename'])) {
            $input['filename'] = $slide->filename;
            $input['type'] = $slide->type;
        }

        // if (!$input['fallback_image']) {
        //     $input['fallback_image'] = $slide->fallback_image;
        // }

        \DB::beginTransaction();
        $slide->update([
            'filename'  => $input['filename'],
            'fallback_image' => $input['fallback_image'],
            'type'      => $input['type'],
            // 'text_color'=> $input['text_color'],
            // 'url_type'  => $input['url_type'],
            'active'    => $input['active']
        ]);
        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'title'     => $item['title'],
                'subtitle'  => $item['subtitle'],
                // 'button_label' => $item['button_label'],
                'url'       => $item['url'],
            ];
        }
        $slide->translations()->sync($syncArray);
        \DB::commit();
    }

    public function paginate()
    {
        return $this->slide->orderBy('created_at', 'DESC')->paginate(config('settings.pagination.items', 15));
    }

    public function delete($id)
    {
        $slide = $this->find($id);
        if (!$slide) {
            return false;
        }

        return $slide->delete();
    }

    public function setOrder($id, $index)
    {
        $slide = $this->find($id);
        $slide->update(['order' => $index]);
    }
}
