<?php

namespace App\Repositories\Slide;

interface SlideInterface
{

    public function all();

    public function getForFront($limit = null);

    public function find($id);

    public function create($input);

    public function update($id, $input);

    public function paginate();

    public function delete($id);
}
