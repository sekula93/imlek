<?php

namespace App\Repositories\CompanyPackshot;

use App\CompanyPackshot;
use App\Services\File\Image as ImageService;

class CompanyPackshotRepository implements CompanyPackshotInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $photo;
    protected $image;

    public function __construct(CompanyPackshot $photo, ImageService $image)
    {
        $this->photo = $photo;
        $this->image = $image;
    }

    public function all()
    {
        return $this->photo->all();
    }

    public function find($id)
    {
        return $this->photo->findOrFail($id);
    }

    public function create($id, $fileName)
    {
        return $this->photo->create([
            'company_id' => $id,
            'filename' => $fileName,
            // 'cover' => $this->_isCover($id),
            'order' => 0,
            'active' => 1
        ]);
    }

    public function delete($id)
    {
        $photo = $this->find($id);

        $this->image->delete($photo->filename, config('settings.image.company.slider'));

        return $photo->delete();
    }

    // public function order($id)
    // {
    //     return $this->photo->where('active', 1)->orderBy('title')->pluck('title', 'id')->toArray();
    // }

    public function setCover($id)
    {
        $photo = $this->find($id);
        $this->photo->where('company_id', $photo->company_id)->update(['cover' => 0]);
        return $photo->update(['cover' => 1, 'active' => 1]);
    }

    public function setOrder($id, $index)
    {
        $file = $this->find($id);
        return $file->update(['order' => $index]);
    }

    // private function _isCover($id)
    // {
    //     if ($this->photo->where('company_id', $id)->count() < 1) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}
