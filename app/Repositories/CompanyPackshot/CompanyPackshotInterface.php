<?php

namespace App\Repositories\CompanyPackshot;

interface CompanyPackshotInterface
{
    public function all();

    public function find($id);

    public function create($id, $data);

    public function delete($id);

    // public function order($id);

    public function changeStatus($id);
}
