<?php

namespace App\Repositories\History;

interface HistoryInterface
{

    public function all();

    public function find($id);

    public function create($input);

    public function update($id, $input);

    public function delete($id);

    public function lists($value, $key = null);
}
