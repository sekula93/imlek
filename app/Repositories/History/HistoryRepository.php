<?php

namespace App\Repositories\History;

use App\HistoryYear;

class HistoryRepository implements HistoryInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $history;

    public function __construct(HistoryYear $history)
    {
        $this->history = $history;
    }

    public function all()
    {
        return $this->history->all();
    }

    public function find($id)
    {
        return $this->history->findOrFail($id);
    }

    public function getForFront()
    {
        return $this->history
            ->join('history_year_translations', 'history_years.id', '=', 'history_year_translations.history_year_id')
            ->join('languages', 'history_year_translations.language_id', '=', 'languages.id')
            ->select('history_year_translations.*', 'history_years.*', 'languages.code')
            ->where('history_years.active', 1)
            ->where('languages.code', \App::getLocale())
            ->orderBy('year')->get();
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $history = $this->history->create([
            'year'          => $input['year'],
            'image_left'    => $input['image_left'],
            // 'file_right'    => $input['file_right'],
            // 'template_id'    => $input['template_id'],
            'active'        => $input['active']
        ]);

        foreach ($input['trans'] as $language => $item) {
            $history->translations()->attach([$language => [
                // 'title'     => $item['title'],
                'text'      => $item['text'],
            ]]);
        }
        \DB::commit();
    }

    public function update($id, $input)
    {
        $history = $this->find($id);

        if (!$input['image_left']) {
            $input['image_left'] = $history->image_left;
        }
        // if (!$input['file_right']) {
        //     $input['file_right'] = $history->file_right;
        // }

        \DB::beginTransaction();
        $history->update([
            'year' => $input['year'],
            'image_left' => $input['image_left'],
            // 'file_right' => $input['file_right'],
            // 'template_id' => $input['template_id'],
            'active'     => $input['active']
        ]);
        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                // 'title' => $item['title'],
                'text'  => $item['text']
            ];
        }
        $history->translations()->sync($syncArray);
        \DB::commit();
    }

    public function delete($id)
    {
        $history = $this->find($id);
        if (!$history) {
            return false;
        }

        return $history->delete();
    }

    public function lists($value, $key = null)
    {
        return $this->history->lists($value, $key);
    }
}
