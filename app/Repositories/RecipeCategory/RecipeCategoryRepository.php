<?php

namespace App\Repositories\RecipeCategory;

use App\RecipeCategory;

class RecipeCategoryRepository implements RecipeCategoryInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $category;

    public function __construct(RecipeCategory $category)
    {
        $this->category = $category;
    }

    public function all()
    {
        // return $this->category->with('translations')->where('active', 1)->get();
        return $this->category
            ->leftJoin('recipe_category_translations', 'recipe_category_translations.recipe_category_id', '=', 'recipe_categories.id')
            ->leftJoin('languages', 'languages.id', '=', 'recipe_category_translations.language_id')
            ->where('code', \App::getLocale())
            ->where('recipe_categories.active', 1)
            ->get();
    }   

    public function find($id)
    {
        return $this->category->findOrFail($id);
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $category = $this->category->create([
            'active'            => $input['active']
        ]);

        foreach ($input['trans'] as $language => $item) {
            $category->translations()->attach([$language => [
                'title' => $item['title'],
                'slug'  => str_slug($item['title']),
            ]]);
        }
        \DB::commit();
    }

    public function update($id, $input)
    {
        $category = $this->find($id);

        \DB::beginTransaction();
        $category->update([
            'active' => $input['active']
        ]);
        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'title' => $item['title'],
                'slug'  => str_slug($item['title']),
            ];
        }
        $category->translations()->sync($syncArray);
        \DB::commit();
    }

    public function delete($id)
    {
        $category = $this->find($id);
        if (!$category) {
            return false;
        }

        return $category->delete();
    }

    public function listCategories()
    {
        return $this->category->with(['translations' => function ($query) {
            $query->where('language_id', 1);
        }])->get();
    }

    public function paginate()
    {
        return $this->category->with('translations')->paginate(config('settings.pagination.items', 15));
    }
}
