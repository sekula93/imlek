<?php

namespace App\Repositories\RecipeCategory;

interface RecipeCategoryInterface
{

    public function all();

    public function find($id);

    public function create($input);

    public function update($id, $input);

    public function delete($id);

    public function listCategories();

    public function paginate();
}
