<?php

namespace App\Repositories\Report;

use App\Report;

class ReportRepository implements ReportInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $report;

    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    public function all()
    {
        return $this->report->all();
    }

    public function find($id)
    {
        return $this->report->findOrFail($id);
    }


    public function create($input)
    {
        \DB::beginTransaction();
        $report = $this->report->create([
            'attachment'=> $input['attachment'],
            'active'    => $input['active'],
            'created_at'    => $input['created_at'],
        ]);

        foreach ($input['trans'] as $language => $item) {
            $report->translations()->attach([$language => [
                'attachment_label' => $item['attachment_label'],
            ]]);
        }
        \DB::commit();
        return $report;
    }

    public function update($id, $input)
    {
        $report = $this->find($id);

        if (!$input['attachment']) {
            $input['attachment'] = $report->attachment;
        }

        \DB::beginTransaction();
        $report->update([
            'attachment' => $input['attachment'],
            'active' => $input['active'],
            'created_at' => $input['created_at']
        ]);

        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'attachment_label' => $item['attachment_label'],
            ];
        }
        $report->translations()->sync($syncArray);
        \DB::commit();
    }

    public function createSocial($input)
    {
        \DB::beginTransaction();
        $report = $this->report->create([
            'type'     => $input['type'],
            'image'     => $input['image'],
            'active'    => 0
        ]);

        $report->social()->create([
            'link'     => $input['link'],
            'message'     => $input['message'],
            'origin_id'=> $input['origin_id'],
        ]);
        \DB::commit();
        return $report;
    }

    public function delete($id)
    {
        $report = $this->find($id);
        if (!$report) {
            return false;
        }

        return $report->delete();
    }

    public function lists($value, $key = null)
    {
        return $this->report->lists($value, $key);
    }

    public function recent($items, $type = 1, $except = null)
    {
        $query = $this->report
            ->join('report_translations', 'report_translations.report_id', '=', 'reports.id')
            ->join('languages', 'report_translations.language_id', '=', 'languages.id')
            ->where('reports.active', 1)->where('reports.type', $type);
        $query->select('report_translations.*', 'reports.*');
        if ($except) {
            $query->whereNotIn('reports.id', $except);
        }

        return $query->orderBy('reports.created_at', 'DESC')->simplePaginate($items);
    }

    public function paginate($items = 100)
    {
        return $this->report->with('translations')->orderBy('order')->paginate($items);
    }

    public function paginateForFront($items)
    {
        $query = $this->report
            ->join('report_translations', 'report_translations.report_id', '=', 'reports.id')
            ->join('languages', 'report_translations.language_id', '=', 'languages.id')
            ->where('reports.active', 1)
            ->where('languages.code', \App::getLocale())
            ->orderBy('order');
        $query->select('report_translations.*', 'reports.*', 'languages.code');
        
        return $query->orderBy('reports.order')->paginate($items);
    }

    public function paginateSocial($type)
    {
        return $this->report->with('social')->where('type', $type)->orderBy('created_at', 'DESC')->paginate(config('settings.pagination.items', 15));
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $report = $this->report->find($input['id']);
            if ($report->slug == str_slug($input['title'])) {
                $slug = $report->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->report->translations()->where('slug', $slug)->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }

    public function latestByType($items, $type, $active = 1)
    {
        return $this->report->with('social')->where('active', $active)->where('type', $type)->orderBy('created_at', 'DESC')->simplePaginate($items);
    }

    public function categoriesRecent()
    {
        $news = $this->recent(5, 1);
        $facebook = $this->latestByType(3, 2);
        $instagram = $this->latestByType(3, 3);
        return compact('news', 'instagram', 'facebook');
    }

    public function mixedRecent($items, $exclude = null)
    {
        $query = $this->report->with('translations', 'social')
            ->leftJoin('report_translations', 'report_translations.report_id', '=', 'reports.id')
            ->leftJoin('social_reports', 'social_reports.report_id', '=', 'reports.id')
            ->leftJoin('languages', 'report_translations.language_id', '=', 'languages.id')
            ->where('reports.active', 1);
        $query->select('social_reports.*', 'report_translations.*', 'reports.*');
        if ($exclude) {
            $query->whereNotIn('reports.id', $exclude);
        }
        return $query->orderBy('reports.id', 'DESC')->get();
        // return $query->orderBy('reports.id', 'DESC')->simplePaginate($items);
    }

    public function setOrder($id, $index)
    {
        $report = $this->report->find($id);
        $report->update(['order' => $index]);
    }

}
