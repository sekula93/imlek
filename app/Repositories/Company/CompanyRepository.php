<?php

namespace App\Repositories\Company;

use App\Company;

class CompanyRepository implements CompanyInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    public function all()
    {
        return $this->company->all();
    }

    public function find($id)
    {
        return $this->company->findOrFail($id);
    }
    

    public function findBySlug($slug)
    {
        $query = $this->company
            // ->with(['packshots' => function ($query) {
            //     $query->where('active', 1);
            // }])
            ->leftJoin('company_translations', 'company_translations.company_id', '=', 'companies.id')->where('slug', $slug)
            ->where('companies.active', 1)
            ->first();

        return $query;
    }

    public function recent($items, $type = 1, $except = null)
    {
        $query = $this->company
            ->join('company_translations', 'company_translations.company_id', '=', 'companies.id')
            ->join('languages', 'company_translations.language_id', '=', 'languages.id')
            ->where('companies.active', 1);
        $query->select('company_translations.*', 'companies.*', 'languages.code');
        $query->where('languages.code', \App::getLocale());
        if ($except) {
            $query->whereNotIn('companies.id', $except);
        }

        return $query->orderBy('companies.order')->simplePaginate($items);
    }

    public function publicFamilies()
    {
        $companies = $this->company->with('translations')->where('active', 1)->orderBy('order')->get();
        $familiesArray = [];
        foreach ($companies as $company) {
            $translation = $company->translations->where('code', \App::getLocale())->first();
            array_push($familiesArray, [
                'title' => $translation->pivot->title,
                'slug'  => $translation->pivot->slug,
                'excerpt' => $translation->pivot->excerpt,
                'poster' => $company->poster,
                'featured_image' => $company->featured_image,
                'image_orientation' => $company->image_orientation,
                'video'  => $company->video,
                'poster' => $company->poster,
            ]);
        }
        return $familiesArray;
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $company = $this->company->create([
            'featured_image'=> $input['featured_image'],
            'image_orientation' => $input['image_orientation'],
            // 'video'         => $input['video'],
            // 'poster'        => $input['poster'],
            // 'web_address'   => $input['web_address'],
            'active'        => 1,
            // 'brand_id'      => $input['brand_id'],
        ]);

        foreach ($input['trans'] as $language => $item) {
            $company->translations()->attach([$language => [
                'title'     => $item['title'],
                // 'slug'      => $item['slug'],
                'excerpt'   => $item['excerpt'],
                'text'      => $item['text'],
            ]]);
        }
        \DB::commit();
        return $company;
    }

    public function update($id, $input)
    {
        $company = $this->find($id);

        if (!$input['featured_image']) {
            $input['featured_image'] = $company->featured_image;
        }
        
        if (!isset($input['image_orientation'])) {
            $input['image_orientation'] = $company->image_orientation;
        }

        \DB::beginTransaction();
        $company->update([
            'featured_image' => $input['featured_image'],
            'image_orientation' => $input['image_orientation'],

            // 'web_address' => $input['web_address'],
            // 'active' => $input['active']
        ]);
        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'title' => $item['title'],
                'text' => $item['text'],
                'excerpt' => $item['excerpt']
            ];
        }
        $company->translations()->sync($syncArray);
        \DB::commit();
    }

    public function delete($id)
    {
        $company = $this->find($id);
        if (!$company) {
            return false;
        }

        return $company->delete();
    }

    public function lists($value, $key = null)
    {
        return $this->company->lists($value, $key);
    }

    public function paginate()
    {
        return $this->company->with('translations')->paginate(config('settings.pagination.items', 15));
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $company = $this->company->find($input['id']);
            if ($company->slug == str_slug($input['title'])) {
                $slug = $company->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    public function setOrder($id, $index)
    {
        $company = $this->company->find($id);
        $company->update(['order' => $index]);
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->company->translations()->where('slug', $slug)->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }
}
