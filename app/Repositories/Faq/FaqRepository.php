<?php

namespace App\Repositories\Faq;

use App\Faq;

class FaqRepository implements FaqInterface
{
	use \App\Repositories\Traits\StatusTrait;

	protected $faq;

	public function __construct(Faq $faq)
    {
        $this->faq = $faq;
    }

    public function all()
    {
    	return $this->faq->all();
    }

    public function getActive()
    {
        return $this->faq->where('active', 1)->get();
    }

    public function getOrdered()
    {
        return $this->faq
            ->leftJoin('faq_translations', 'faq_translations.faq_id', '=', 'faqs.id')
            ->leftJoin('languages', 'faq_translations.language_id', '=', 'languages.id')
            ->where('faqs.active', 1)
            ->where('languages.code', \App::getLocale())
            ->select('faq_translations.*', 'faqs.*', 'languages.code')
            ->orderBy('faqs.order')->get();
    }
    
    public function find($id)
    {
        return $this->faq->findOrFail($id);
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $faq = $this->faq->create([
            'active'        => $input['active'],
            'order'         => count($this->all())+1
        ]);

        foreach ($input['trans'] as $language => $item) {
            $faq->translations()->attach([$language => [
                'title'     => $item['title'],
                'text'      => $item['text'],
            ]]);
        }
        \DB::commit();
    }

    public function update($id, $input)
    {
        $faq = $this->find($id);

        \DB::beginTransaction();
        $faq->update([
            'active'     => $input['active']
        ]);
        
        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'title' => $item['title'],
                'text'  => $item['text']
            ];
        }
        $faq->translations()->sync($syncArray);
        \DB::commit();
    }

    public function delete($id)
    {
        $faq = $this->find($id);
        if (!$faq) {
            return false;
        }

        return $faq->delete();
    }

    public function paginate()
    {
        return $this->faq->orderBy('order')->paginate(config('settings.pagination.items', 15));
    }

    public function setOrder($id, $index)
    {
        $faq = $this->faq->find($id);
        $faq->update(['order' => $index]);
    }
}