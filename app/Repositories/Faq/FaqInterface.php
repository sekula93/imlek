<?php

namespace App\Repositories\Faq;

interface FaqInterface
{

    public function all();

    public function getActive();

    public function getOrdered();

    public function find($id);

    public function create($input);

    public function update($id, $input);

    public function delete($id);

    public function paginate();

    public function setOrder($id, $index);
    
}