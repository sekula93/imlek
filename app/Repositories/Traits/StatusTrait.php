<?php

namespace App\Repositories\Traits;

trait StatusTrait
{

    public function changeStatus($id)
    {
        $object = $this->find($id);

        if ($object->active == 1) {
            $active = 0;
        } else {
            $active = 1;
        }

        if ($object->update(['active' => $active])) {
            return $active;

        } else {
            return false;
        }
    }
}
