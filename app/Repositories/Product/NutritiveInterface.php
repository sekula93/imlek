<?php

namespace App\Repositories\Product;

interface NutritiveInterface
{

    public function all();

    public function find($id);

    public function create($input);

    public function update($id, $input);

    public function delete($id);

    public function lists($value, $key = null);

    public function paginate();

    public function decodeTable($table);
}
