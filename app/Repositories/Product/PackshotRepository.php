<?php

namespace App\Repositories\Product;

use App\Packshot;
use App\Services\File\Image as ImageService;

class PackshotRepository implements PackshotInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $packshot;
    protected $image;

    public function __construct(Packshot $packshot, ImageService $image)
    {
        $this->packshot = $packshot;
        $this->image = $image;
    }

    public function all()
    {
        return $this->packshot->all();
    }

    public function find($id)
    {
        return $this->packshot->findOrFail($id);
    }


    public function featuredPackshots() {
        return $this->packshot->where('flag', 1)->get();
    }

    public function create($id, $input)
    {
        $packshot = $this->packshot->create([
            'product_id' => $id,
            'nutritive_id' => $input['nutritive_id'],
            'filename' => $input['filename'],
            'link' => $input['link'],
            // 'label' => $input['label'],
            // 'text' => $input['text'],
            'orientation' => $input['orientation'],
            'order' => 0,
            'active' => 0
        ]);

        // if ($input['trans']) {

        foreach ($input['trans'] as $language => $item) {
            $packshot->translations()->attach([$language+1 => [
                'label'     => $item['label'],
                'text'      => $item['text'],
                'slug'      => $item['slug'],
            ]]);
        }
        // }

        return $packshot;
    }

    public function update($id, $input)
    {
        $packshot = $this->packshot->find($id);
        $packshot->update([
            'nutritive_id' => $input['nutritive_id'],
            'link' => $input['link'],
            // 'label' => $input['label'],
            // 'text' => $input['text'],
        ]);
        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language+1] = [
                'label' => $item['label'],
                'text' => $item['text'],
                'slug' => $item['slug']
            ];
        }
        $packshot->translations()->sync($syncArray);
    }

    public function delete($id)
    {
        $packshot = $this->find($id);

        $this->image->delete($packshot->filename, config('settings.image.product.packshot'));

        return $packshot->delete();
    }

    public function setOrder($id, $index)
    {
        $file = $this->find($id);
        return $file->update(['order' => $index]);
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $packshot = $this->packshot->find($input['id']);
            if ($packshot->slug == str_slug($input['title'])) {
                $slug = $packshot->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->packshot->leftJoin('packshot_translations', 'packshot_translations.packshot_id', '=', 'packshots.id')->where('slug', str_slug($slug))->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }
}
