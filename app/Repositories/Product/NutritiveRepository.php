<?php

namespace App\Repositories\Product;

use App\Nutritive;

class NutritiveRepository implements NutritiveInterface
{
    protected $nutritive;

    public function __construct(Nutritive $nutritive)
    {
        $this->nutritive = $nutritive;
    }

    public function all()
    {
        return $this->nutritive->all();
    }

    public function find($id)
    {
        return $this->nutritive->findOrFail($id);
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $nutritive = $this->nutritive->create([
            'name' => $input['name'],
        ]);

        foreach ($input['trans'] as $language => $item) {
            $nutritive->translations()->attach([$language => [
                'table'  => json_encode($item['table']),
                // 'legend' => $item['legend'],
            ]]);
        }
        \DB::commit();
    }

    public function update($id, $input)
    {
        $nutritive = $this->find($id);

        \DB::beginTransaction();
        $nutritive->update([
            'name' => $input['name']
        ]);
        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'table'  => json_encode($item['table']),
                // 'legend' => $item['legend']
            ];
        }
        $nutritive->translations()->sync($syncArray);
        \DB::commit();
    }

    public function delete($id)
    {
        $nutritive = $this->find($id);
        if (!$nutritive) {
            return false;
        }

        return $nutritive->delete();
    }

    public function lists($value, $key = null)
    {
        return $this->nutritive->lists($value, $key);
    }

    public function paginate()
    {
        return $this->nutritive->with('translations')->paginate(25);
    }

    public function decodeTable($table)
    {
        $tableTranslations = [];
        foreach ($table->translations as $translation) {
            $tableTranslations[$translation->id] = json_decode($translation->pivot->table);
        }
        return $tableTranslations;
    }
}
