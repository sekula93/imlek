<?php

namespace App\Repositories\Product;

interface ProductInterface
{

    public function all();

    public function find($id);

    public function findForAdmin($id);

    public function findBySlug($slug);

    public function create($input);

    public function update($id, $input);

    public function delete($id);

    public function lists($value, $key = null);

    public function paginate();

    public function slugify($title);
}
