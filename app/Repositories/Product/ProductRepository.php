<?php

namespace App\Repositories\Product;

use App\Product;

class ProductRepository implements ProductInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function all()
    {
        return $this->product->all();
    }

    public function find($id)
    {
        return $this->product
            ->with('brand')
            ->with(['packshots' => function($query) {
                $query->leftJoin('packshot_translations', 'packshot_translations.packshot_id', '=','packshots.id')
                    ->where('packshot_translations.language_id', languageId(\App::getLocale()));
            }])
            ->findOrFail($id);

    }
    
    public function findForAdmin($id) {
        return $this->product
        ->with('brand')
            ->with(['packshots' => function($query) {
                $query->with('translations');
            }])
            ->findOrFail($id);
    }

    public function findBySlug($slug)
    {
        $query = $this->product
            ->with(['packshots' => function($query) {
                $query->leftJoin('nutritives', 'packshots.nutritive_id', '=', 'nutritives.id')
                    ->leftJoin('nutritive_translations', function ($join) {
                        $join->on('nutritive_translations.nutritive_id', '=', 'nutritives.id')
                            ->where('nutritive_translations.language_id', '=', languageId(\App::getLocale()));
                            // ->where('nutritive_translations.language_id', '=', 1);
                    })
                    ->leftJoin('packshot_translations', 'packshot_translations.packshot_id', '=','packshots.id')
                    ->where('packshot_translations.language_id', languageId(\App::getLocale()))
                    // ->where('packshot_translations.language_id', 1)
                    ->where('packshots.active', 1);
            }])
            // ->with('packshots')
            // ->with(['packshots' => function ($query) {
            //     // $query->with('translations');
            //     $query->where('packshots.active', 1);
            //     $query->leftJoin('nutritives', 'packshots.nutritive_id', '=', 'nutritives.id');
            //     $query->leftJoin('nutritive_translations', 'nutritive_translations.nutritive_id', '=', 'nutritives.id');
            //     $query->Join('languages', 'nutritive_translations.language_id', '=', 'languages.id');
            //     $query->where('languages.code', \App::getLocale());
            // }])
            ->leftJoin('product_translations', 'product_translations.product_id', '=', 'products.id')->where('slug', $slug)
            ->where('product_translations.language_id', languageId(\App::getLocale()))
            ->where('products.active', 1)
            ->first();

        return $query;
    }

    public function publicFamilies()
    {
        $products = $this->product->with('translations')->with('brand')->where('active', 1)->orderBy('order')->get();

        $familiesArray = [];
        foreach ($products as $product) {
            $translation = $product->translations->where('code', \App::getLocale())->first();

            array_push($familiesArray, [
                'title' => $translation->pivot->title,
                'slug'  => $translation->pivot->slug,
                'excerpt' => $translation->pivot->excerpt,
                'poster' => $product->poster,
                'featured_image' => $product->featured_image,
                'image_orientation' => $product->image_orientation,
                'video'  => $product->video,
                'poster' => $product->poster,
                'brand_name' => $product->brand->name,
                'brand_img' => $product->brand->featured_image
            ]);
        }

        return $familiesArray;
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $product = $this->product->create([
            'featured_image'=> $input['featured_image'],
            'image_orientation' => $input['image_orientation'],
            // 'video'         => $input['video'],
            'poster'        => $input['poster'],
            // 'web_address'   => $input['web_address'],
            'active'        => $input['active'],
            'brand_id'      => $input['brand_id'],
            'fb_link'       => $input['fb-link'],
            'in_link'       => $input['in-link'],
            'yt_link'       => $input['yt-link'],
            'ms_link'       => $input['ms-link'],
        ]);

        foreach ($input['trans'] as $language => $item) {
            $product->translations()->attach([$language => [
                'title'     => $item['title'],
                'slug'      => $item['slug'],
                // 'excerpt'   => $item['excerpt'],
                'text'      => $item['text'],
            ]]);
        }
        \DB::commit();
        return $product;
    }

    public function update($id, $input)
    {
        $product = $this->find($id);

        if (!$input['featured_image']) {
            $input['featured_image'] = $product->featured_image;
        }
        // if (!$input['poster']) {
        //     $input['poster'] = $product->poster;
        // }
        if (!$input['video']) {
            $input['video'] = $product->video;
        }
        if (!isset($input['image_orientation'])) {
            $input['image_orientation'] = $product->image_orientation;
        }

        \DB::beginTransaction();
        $product->update([
            'featured_image' => $input['featured_image'],
            'image_orientation' => $input['image_orientation'],
            'poster' => $input['poster'],
            // 'video' => $input['video'],
            // 'web_address' => $input['web_address'],
            'active' => $input['active'],
            'brand_id'      => $input['brand_id'],
            
            'fb_link'       => $input['fb-link'],
            'in_link'       => $input['in-link'],
            'yt_link'       => $input['yt-link'],
            'ms_link'       => $input['ms-link'],
        ]);
        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'title' => $item['title'],
                'slug' => $item['slug'],
                // 'excerpt' => $item['excerpt'],
                'text' => $item['text']
            ];
        }
        $product->translations()->sync($syncArray);
        \DB::commit();
    }

    public function delete($id)
    {
        $product = $this->find($id);
        if (!$product) {
            return false;
        }

        return $product->delete();
    }

    public function lists($value, $key = null)
    {
        return $this->product->lists($value, $key);
    }

    public function paginate()
    {
        return $this->product->with('translations')->with('brand')->orderBy('order')->paginate(100);
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $product = $this->product->find($input['id']);
            if ($product->slug == str_slug($input['title'])) {
                $slug = $product->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    public function setOrder($id, $index)
    {
        $product = $this->product->find($id);
        $product->update(['order' => $index]);
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->product->leftJoin('product_translations', 'product_translations.product_id', '=', 'products.id')->where('slug', $slug)->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }
}
