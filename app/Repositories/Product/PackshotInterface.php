<?php

namespace App\Repositories\Product;

interface PackshotInterface
{
    public function all();

    public function find($id);

    public function featuredPackshots();

    public function create($id, $data);

    public function delete($id);

    public function changeStatus($id);
}
