<?php

namespace App\Repositories\Package;

use App\Package;

class PackageRepository implements PackageInterface
{
    use \App\Repositories\Traits\StatusTrait;

    protected $package;

    public function __construct(Package $package)
    {
        $this->package = $package;
    }

    public function all()
    {
        return $this->package->all();
    }

    public function find($id)
    {
        return $this->package->findOrFail($id);
    }
    

    public function findBySlug($slug)
    {
        $query = $this->package
            ->with(['packshots' => function ($query) {
                $query->where('active', 1);
            }])
            ->leftJoin('package_translations', 'package_translations.package_id', '=', 'packages.id')->where('slug', $slug)
            ->where('packages.active', 1)
            ->first();

        return $query;
    }

    public function recent($items, $type = 1, $except = null)
    {
        $query = $this->package
            ->join('package_translations', 'package_translations.package_id', '=', 'packages.id')
            ->join('languages', 'package_translations.language_id', '=', 'languages.id')
            ->where('packages.active', 1);
        $query->select('package_translations.*', 'packages.*', 'languages.code');
        $query->where('languages.code', \App::getLocale());
        if ($except) {
            $query->whereNotIn('packages.id', $except);
        }

        return $query->orderBy('packages.order')->simplePaginate($items);
    }

    public function publicFamilies()
    {
        $packages = $this->package->with('translations')->where('active', 1)->orderBy('order')->get();
        $familiesArray = [];
        foreach ($packages as $package) {
            $translation = $package->translations->where('code', \App::getLocale())->first();
            array_push($familiesArray, [
                'title' => $translation->pivot->title,
                'slug'  => $translation->pivot->slug,
                'excerpt' => $translation->pivot->excerpt,
                'poster' => $package->poster,
                'featured_image' => $package->featured_image,
                'image_orientation' => $package->image_orientation,
                'video'  => $package->video,
                'poster' => $package->poster,
            ]);
        }
        return $familiesArray;
    }

    public function create($input)
    {
        \DB::beginTransaction();
        $package = $this->package->create([
            'featured_image'=> $input['featured_image'],
            'image_orientation' => $input['image_orientation'],
            // 'video'         => $input['video'],
            // 'poster'        => $input['poster'],
            // 'web_address'   => $input['web_address'],
            'active'        => $input['active'],
            // 'brand_id'      => $input['brand_id'],
        ]);

        foreach ($input['trans'] as $language => $item) {
            $package->translations()->attach([$language => [
                'title'     => $item['title'],
                // 'slug'      => $item['slug'],
                // 'excerpt'   => $item['excerpt'],
                'text'      => $item['text'],
                'link'  => $item['link']

            ]]);
        }
        \DB::commit();
        return $package;
    }

    public function update($id, $input)
    {
        $package = $this->find($id);

        if (!$input['featured_image']) {
            $input['featured_image'] = $package->featured_image;
        }
        
        if (!isset($input['image_orientation'])) {
            $input['image_orientation'] = $package->image_orientation;
        }

        \DB::beginTransaction();
        $package->update([
            'featured_image' => $input['featured_image'],
            'image_orientation' => $input['image_orientation'],

            // 'web_address' => $input['web_address'],
            'active' => $input['active']
        ]);
        $syncArray = [];
        foreach ($input['trans'] as $language => $item) {
            $syncArray[$language] = [
                'title' => $item['title'],
                'text' => $item['text'],
                'link' => $item['link']
            ];
        }
        $package->translations()->sync($syncArray);
        \DB::commit();
    }

    public function delete($id)
    {
        $package = $this->find($id);
        if (!$package) {
            return false;
        }

        return $package->delete();
    }

    public function lists($value, $key = null)
    {
        return $this->package->lists($value, $key);
    }

    public function paginate()
    {
        return $this->package->with('translations')->orderBy('order')->paginate(config('settings.pagination.items', 15));
    }

    public function slugify($input)
    {
        if (isset($input['id']) && $input['id'] != 0) {
            $package = $this->package->find($input['id']);
            if ($package->slug == str_slug($input['title'])) {
                $slug = $package->slug;
            } else {
                $slug = $this->_generateSlug($input['title']);
            }
        } else {
            $slug = $this->_generateSlug($input['title']);
        }

        return $slug;
    }

    public function setOrder($id, $index)
    {
        $package = $this->package->find($id);
        $package->update(['order' => $index]);
    }

    private function _generateSlug($title)
    {
        $i = 1;
        $notUnique = true;
        $forSlug = $title;
        do {
            $slug = str_slug($forSlug);
            if ($this->package->translations()->where('slug', $slug)->count()) {
                $forSlug = $title.'-'.$i;
            } else {
                $notUnique = false;
            }
            $i++;
        } while ($notUnique);

        return $slug;
    }
}
