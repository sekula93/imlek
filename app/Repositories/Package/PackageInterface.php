<?php

namespace App\Repositories\Package;

interface PackageInterface
{

    public function all();

    public function find($id);

    public function findBySlug($slug);

    public function create($input);

    public function update($id, $input);

    public function delete($id);

    public function lists($value, $key = null);

    public function paginate();

    public function slugify($title);
}
