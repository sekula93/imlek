<?php

namespace App\Repositories\StaticPage;

interface StaticPageInterface
{

    public function all();

    public function find($id);

    public function update($id, $input);
}
