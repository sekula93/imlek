<?php

namespace App\Repositories\StaticPage;

use App\StaticPage;

class StaticPageRepository implements StaticPageInterface
{

    protected $text;

    public function __construct(StaticPage $text)
    {
        $this->text = $text;
    }

    public function all()
    {
        return $this->text->all();
    }

    public function find($id)
    {
        return $this->text->findOrFail($id);
    }

    public function findBySlug($slug)
    {
        return $this->text->where('slug', $slug)->firstOrFail();
    }

    public function update($id, $input)
    {
        $text = $this->find($id);
        return $text->update([
            'text'  => $input['text'],
        ]);
    }
}
