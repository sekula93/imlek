<?php

namespace App\Repositories\UserToken;

interface UserTokenInterface
{

    public function find($userId, $tokenType);

    public function findByToken($tokenString, $tokenType);

    public function create($input);

    public function getToken($input);

    public function delete($id);
}
