<?php

namespace App\Repositories\UserToken;

use App\UserToken;

class UserTokenRepository implements UserTokenInterface
{

    protected $token;

    public function __construct(UserToken $token)
    {
        $this->token = $token;
    }

    public function find($userId, $tokenType)
    {
        return $this->token->where('user_id', '=', $userId)->where('token_type', '=', $tokenType)->firstOrFail();
    }

    public function findByToken($tokenString, $tokenType)
    {
        return $this->token->where('token', '=', $tokenString)->where('token_type', '=', $tokenType)->first();
    }

    public function create($input)
    {
        do {
            $tokenString = str_random(\Config::get('settings.token_size', 30));
        } while ($this->findByToken($tokenString, $input['token_type']));

        return  $this->token->create(array(
            'user_id'       => $input['user_id'],
            'token'         => $tokenString,
            'token_type'    => $input['token_type']
            ));
    }

    public function getToken($input)
    {
        $token = $this->find($input['user_id'], $input['token_type']);
        if ($token) {
            do {
                $tokenString = str_random(\Config::get('settings.token_size', 30));
            } while ($this->findByToken($tokenString, $input['token_type']));

            $token = $token->update(array(
                'token'     => $tokenString,
            ));
        } else {
            $token = $this->create($input);
            $tokenString = $token->token;
        }

        return $tokenString;
    }

    public function delete($token, $tokenType = 1)
    {
        $token = $this->findByToken($token, $tokenType);
        if (!$token) {
            return false;
        }

        return $token->delete();
    }
}
