<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeCategory extends Model
{
    protected $table = 'recipe_categories';

    protected $fillable = ['active'];

    public $timestamps = false;

    public function recipes()
    {
        return $this->hasMany(Recipe::class);
    }

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'recipe_category_translations')
            ->withPivot('title', 'slug');
    }
}
