<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryYear extends Model
{
    protected $table = 'history_years';

    protected $fillable = ['year', 'image_left', 'file_right', 'active', 'template_id'];

    public $timestamps = false;


    public function translations()
    {
        return $this->belongsToMany(Language::class, 'history_year_translations')
            ->withPivot('title', 'text');
    }
}
