<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticContent extends Model
{
    protected $table = 'static_contents';

    protected $fillable = ['key', 'value'];

    public $timestamps = false;


    public function getCatalogue()
    {
        $catalogue = \Cache::rememberForever('catalogue', function () {
            return $this->where('key', 'catalogue')->first();
        });

        return $catalogue;
    }

    public function updateCatalogue($filename)
    {
        $catalogue = $this->getCatalogue();
        $catalogue->update(['value' => $filename]);

        \Cache::forget('catalogue');
    }
}
