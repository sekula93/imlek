<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    protected $table = 'post_categories';

    protected $fillable = ['active'];

    public $timestamps = false;

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'post_category_translations')->where('language_id', languageId(\App::getLocale()))
            ->withPivot('title', 'slug');
    }
}
