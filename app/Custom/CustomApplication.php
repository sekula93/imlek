<?php namespace App\Custom;
use Illuminate\Foundation\Application;

class CustomApplication extends Application {

    public function publicPath()
    {
        return $this->basePath.'/public_html';
    }
}