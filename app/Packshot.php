<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packshot extends Model
{
    protected $table = 'packshots';

    protected $fillable = ['product_id', 'nutritive_id', 'filename', 'label', 'text', 'active', 'order', 'orientation', 'link'];

    public function getOrientationAttribute($value)
    {
        switch ($value) {
            case 0:
                return 'square';
                break;
            case 1:
                return 'portrait';
                break;
            case 2:
                return 'landscape';
                break;
            default:
                return '';
                break;
        }
        return ucfirst($value);
    }

    public function nutritiveTable()
    {
        return $this->hasOne(Nutritive::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'packshot_translations')
            ->withPivot('label', 'text', 'slug');
    }
}
