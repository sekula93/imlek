<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeAuthorProfile extends Model
{
    protected $table = 'recipe_author_profiles';

    protected $fillable = ['image'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
