<?php

namespace App\Services\File;

class Document extends File
{

    /**
     * Upload an file to the public storage
     *
     * @param  File $file
     * @param  array $config
     * @param  string $itemFolder
     * @return boolean
     */
    public function upload($file, $config, $itemFolder = null)
    {
        if ($file) {
            $destinationDir = $this->getDestinationDir($config, $itemFolder);

            $fileName = self::generateName($file, $destinationDir, $config);

            if ($file->move(public_path().$destinationDir, $fileName)) {
                return $fileName;
            }
        }
        return null;
    }

    /**
     * Delete file from the filesystem according to config
     *
     * @param  string   $fileName
     * @param  array    $config     data from settings config
     * @param  string   $itemFolder
     * @return boolean
     */
    public function delete($fileName, $config, $itemFolder = null)
    {
        $uploadDir = public_path().$config['upload_dir'];
        if ($itemFolder) {
            $uploadDir .= $itemFolder.'/';
        }
        if (\File::exists($uploadDir.$fileName)) {
            return \File::delete($uploadDir.$fileName);
        }
        return false;
    }
}
