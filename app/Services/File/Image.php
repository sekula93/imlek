<?php

namespace App\Services\File;

class Image extends File
{
    /**
     * Injected Image Manipulation Library
     * @var [type]
     */
    protected $imageLibrary;

    /**
     * Always force overwriting of files
     * @var boolean
     */
    public $overwrite = true;

    /**
     * Quality of compression
     * @var integer
     */
    public $quality = 85;


    /**
     * Upload an image to the public storage
     * @param  File $file
     * @param  array $config
     * @return string
     */
    public function upload($file, $config = [], $itemFolder = null)
    {
        if ($file && in_array($file->guessExtension(), ['jpeg', 'png'])) {
            $destinationDir = $this->getDestinationDir($config, $itemFolder);
            $fileName = $this->generateName($file, $destinationDir, $config);
            $path = $destinationDir.'/'.$fileName;

            if ($file->move(public_path().$destinationDir, $fileName)) {
                if (isset($config['resize_original']) && $config['resize_original']) {
                    $dimension = isset($config['resize_dimensions']) ? $config['resize_dimensions'] : [];
                    $width   = (int) $dimension[0];
                    $height  = isset($dimension[1]) ? (int) $dimension[1] : $width;
                    $crop    = isset($dimension[2]) ? (bool) $dimension[2] : false;
                    $quality = isset($dimension[3]) ? (int) $dimension[3] : $config['quality'];
                    $this->resize($path, true, $width, $height, $crop, $quality);
                }

                if ($config['create_thumbs']) {
                    $this->createDimensions($destinationDir.$fileName, $config['thumb_dimensions']);
                }

                return $fileName;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Resize an image
     * @param  string  $url
     * @param  integer $width
     * @param  integer $height
     * @param  boolean $crop
     * @return string
     */
    public function resize($url, $overwrite = false, $width = 100, $height = null, $crop = false, $quality = null, $folderName = null)
    {
        // URL info
        $info = pathinfo($url);

        // The size
        if (!$height) {
            $height = $width;
        }

        // Directories and file names
        $fileName       = $info['basename'];
        $sourceDirPath  = public_path() . $info['dirname'];
        $sourceFilePath = $sourceDirPath . '/' . $fileName;

        if (!$overwrite) {
            if ($folderName) {
                $destinationDirName  = $folderName;
            } else {
                $destinationDirName  = $width . 'x' . $height . ($crop ? '_crop' : '');
            }

            $destinationDirPath  = $sourceDirPath . '/' . $destinationDirName . '/';
            $destinationFilePath = $destinationDirPath . $fileName;
            $destinationUrl      = asset($info['dirname'] . '/' . $destinationDirName . '/' . $fileName);
        } else {
            $destinationDirPath  = $sourceDirPath;
            $destinationFilePath = $sourceFilePath;
            $destinationUrl      = $url;
        }

        // Create directory if missing
        try {
            // Create dir if missing
            if (! \File::isDirectory($destinationDirPath) && $destinationDirPath) {
                @\File::makeDirectory($destinationDirPath);
            }

            $image = \Image::make(public_path().$url);
            if ($crop) {
                $image->fit($width, $height);
                // $image->fit($width, $height)->resize($width, $height, function ($constraint) use ($crop) {
                //     $constraint->aspectRatio();
                //     $constraint->upsize();
                // });
            }
            $image->resize($width, $height, function ($constraint) use ($crop) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            // if ($overwrite || !\File::exists($destinationFilePath) || (\File::lastModified($destinationFilePath) < \File::lastModified($sourceFilePath))) {
                $image->save($destinationFilePath, $quality);
            // }
        } catch (\Exception $e) {
            \Log::error('[IMAGE SERVICE] Failed to resize image "' . $url . '" [' . $e->getMessage() . ']');
        }

        return $destinationUrl;
    }

    /**
     * Creates image dimensions based on a configuration
     *
     * @param  string $url
     * @param  array  $dimensions
     * @param  string $itemFolder   folder name if files are grouped into separated folders
     * @return void
     */
    public function createDimensions($url, array $dimensions, $itemFolder = null)
    {
        foreach ($dimensions as $dimension) {
            // Get dimmensions and quality
            $width   = (int) $dimension[0];
            $height  = isset($dimension[1]) ?  (int) $dimension[1] : $width;
            $crop    = isset($dimension[2]) ? (bool) $dimension[2] : false;
            $quality = isset($dimension[3]) ?  (int) $dimension[3] : $this->quality;
            $folderName = isset($dimension[4]) ? $dimension[4] : null;

            if ($itemFolder) {
                $folderName .= $itemFolder.'/'.$folderName;
            }

            // Run resizer
            $img = $this->resize($url, false, $width, $height, $crop, $quality, $folderName);
        }
    }

    public function getOrientation($file)
    {
        $image = \Image::make($file);
        $ratio = $image->getSize()->width / $image->getSize()->height;
        if ($ratio < 1.2 && $ratio > 0.8) {
            return 0;
        } else if ($ratio < 0.8) {
            return 1;
        }
        return 2;
    }

    /**
     * Takes information about image sizes and folders and deletes each image dimension according to config setup
     *
     * @param string $fileName
     * @param array $config
     * @param string $itemFolder
     * @return boolean
     */
    public function delete($fileName, $config, $itemFolder = null)
    {
        if (isset($config['upload_dir'])) {
            $uploadDir = public_path().$config['upload_dir'];
            if ($itemFolder) {
                $uploadDir .= $itemFolder.'/';
            }

            if (\File::exists($uploadDir.$fileName)) {
                \File::delete($uploadDir.$fileName);
                if (isset($config['create_thumbs']) && $config['create_thumbs']) {
                    foreach ($config['thumb_dimensions'] as $dimension) {
                        if (\File::exists($uploadDir.$dimension[4].'/'.$fileName)) {
                            \File::delete($uploadDir.$dimension[4].'/'.$fileName);
                        }
                    }
                }
            }
            return false;
        }

        return false;
    }
}