<?php

namespace App\Services\File;

abstract class File
{
    const IMAGE_TYPE = 1;
    const VIDEO_TYPE = 2;

    abstract public function upload($file, $config, $itemFolder = null);

    abstract public function delete($fileName, $config, $itemFolder = null);


    public function getDestinationDir($config, $itemFolder)
    {
        // Generate random dir
        if (isset($config['upload_dir'])) {
            $destinationDir = $config['upload_dir'];
        } else {
            $destinationDir = str_random(8).'/';
        }

        if ($itemFolder) {
            $destinationDir .= $itemFolder.'/';
        }

        return $destinationDir;
    }


    /**
     * Generate filename according to config
     * @param  file $file
     * @param  array $config
     * @return string
     */
    public function generateName($file, $destinationDir, $config)
    {
        // Get file info and try to move
        if (isset($config['random_name']) && $config['random_name']) {
            $extension = $file->getClientOriginalExtension();
            do {
                $fileName = str_random(16).".{$extension}";
            } while (\File::exists(public_path().$destinationDir.$fileName));
        } else {
            $fileName = $this->cleanFileName($file->getClientOriginalName());
            $fileNameParts = explode('.', $fileName);

            if (strlen($fileName) > 200) {
                $fileNameParts[0] = substr($fileNameParts[0], 0, 180);
            }

            $i = 1;

            while (\File::exists(public_path().$destinationDir.$fileName)) {
                $fileRename = $fileNameParts[0].'_'.$i;
                $fileName = implode('.', [$fileRename, $fileNameParts[count($fileNameParts)-1]]);
                $i++;
            }
        }
        return $fileName;
    }

    protected function cleanFileName($string)
    {
        // Remove special accented characters - ie. sí.
        $clean_name = strtr($string, array('Š' => 'S','Ž' => 'Z','š' => 's','ž' => 'z','Ÿ' => 'Y','À' => 'A','Á' => 'A','Â' => 'A','Ã' => 'A','Ä' => 'A','Å' => 'A','Ç' => 'C','È' => 'E','É' => 'E','Ê' => 'E','Ë' => 'E','Ì' => 'I','Í' => 'I','Î' => 'I','Ï' => 'I','Ñ' => 'N','Ò' => 'O','Ó' => 'O','Ô' => 'O','Õ' => 'O','Ö' => 'O','Ø' => 'O','Ù' => 'U','Ú' => 'U','Û' => 'U','Ü' => 'U','Ý' => 'Y','à' => 'a','á' => 'a','â' => 'a','ã' => 'a','ä' => 'a','å' => 'a','ç' => 'c','è' => 'e','é' => 'e','ê' => 'e','ë' => 'e','ì' => 'i','í' => 'i','î' => 'i','ï' => 'i','ñ' => 'n','ò' => 'o','ó' => 'o','ô' => 'o','õ' => 'o','ö' => 'o','ø' => 'o','ù' => 'u','ú' => 'u','û' => 'u','ü' => 'u','ý' => 'y','ÿ' => 'y'));
        $clean_name = strtr($clean_name, array('Þ' => 'TH', 'þ' => 'th', 'Ð' => 'Dj', 'ð' => 'dj','đ' => 'dj', 'ß' => 'ss', 'Œ' => 'OE', 'œ' => 'oe', 'Æ' => 'AE', 'æ' => 'ae', 'µ' => 'u'));

        $clean_name = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), $clean_name);
        return $clean_name;
    }
}
