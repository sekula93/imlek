<?php

namespace App\Services\Mailers;

class ContactMailer extends Mailer
{

    public function sendContactMail($input)
    {
        $data = [
            'email' => $input['email'],
            'text' => $input['message'],
            'phone' => $input['phone'],
            'name' => $input['name'], 
        ];

        if (isset($input['path'])) {
	        $path = $input['path'];
        } else {
	        $path = null;
        }

        $this->sendToAdmin(env('MAIL_ADDRESS'), 'Komentar sa sajta: imlek.rs', 'emails.contact', $data, $path);
    }
}
