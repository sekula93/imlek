<?php

namespace App\Services\Mailers;

abstract class Mailer
{

    public function sendTo(\App\User $user, $subject, $view, $data = [])
    {
        return \Mail::send($view, $data, function ($message) use ($user, $subject) {
            $message->to($user->email, $user->first_name.' '.$user->last_name)->subject($subject);
        });
    }

    public function sendToAdmin($email, $subject, $view, $data = [], $path = null)
    {

        return \Mail::send($view, $data, function ($message) use ($email, $subject, $path) {
            $message->to($email)->subject($subject);
            if ($path) {
                $message->attach($path);
            }

        });
    }
}
