<?php

namespace App\Services\Mailers;

use App\Repositories\UserToken\UserTokenInterface;
// use App\Repositories\Settings\SettingsInterface;

use \App\User;

class UserMailer extends Mailer
{

    protected $token;
    protected $settings;
    protected $user;

    public function __construct(UserTokenInterface $token)
    {
        $this->token = $token;
    }

    public function sendVerificationMail(User $user, $token)
    {
        $subject = \Lang::get('front/mail.subject_verification');
        $view = 'emails.auth.verification';
        $data = ['token' => $userToken];
        $this->sendTo($user, $subject, $view, $data);
    }

    public function sendPasswordResetMail(User $user, $token)
    {
        $subject = \Lang::get('front/mail.subject_password_reset');
        $view = 'emails.auth.password-reset';
        $data = ['token' => $token];

        $this->sendTo($user, $subject, $view, $data);
    }

    public function sendWelcomeMail(User $user)
    {
        $subject = \Lang::get('front/mail.subject_welcome');
        $view = 'emails.user.welcome';

        $this->sendTo($user, $subject, $view, compact('user'));
    }

    public function sendActivationMail(User $user, $token)
    {
        $subject = \Lang::get('front/mail.subject_activation');
        $view = 'emails.auth.activation';
        $data = [
            'name' => $user->first_name,
            'token' => $token
        ];

        $this->sendTo($user, $subject, $view, $data);
    }
}
