<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $table = 'certificates';

    protected $fillable = ['attachment', 'active', 'order', 'featured_image'];

	public $timestamps = true;

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'certificate_translations')
            ->withPivot('attachment_label');
    }
}
