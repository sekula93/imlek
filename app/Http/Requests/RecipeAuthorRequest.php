<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RecipeAuthorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'    => 'required',
        ];

        if ($this->method == 'PUT') {
            $rules['email'] = 'required|email';
        } else {
            $rules['email'] = 'required|email';
        }

        return $rules;
    }
}
