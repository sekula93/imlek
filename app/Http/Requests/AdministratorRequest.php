<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdministratorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'    => 'required',
            // 'last_name'     => 'required',
            // 'password'      => ['confirmed', 'regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})']
        ];
        if ($this->method == 'PUT') {
            $rules['email'] = 'required|email';
        } else {
            $rules['email'] = 'required|email|unique:users';
        }

        return $rules;
    }
}
