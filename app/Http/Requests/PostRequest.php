<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            // 'trans.*.title' => 'required',
            // 'trans.*.slug'  => 'required',
            // 'trans.*.text'  => 'required',
            'active'        => 'required',
            'attachment'    => 'mimes:pdf,doc,docx,zip'
        ];

        if (strtolower($this->method) == 'put') {
            $rules['image'] = 'mimes:jpeg,png,mp4';
            $rules['landscape_image'] = 'mimes:jpeg,png,mp4';
        } else {
            $rules['image'] = 'required|mimes:jpeg,png,mp4';
            $rules['landscape_image'] = 'required|mimes:jpeg,png,mp4';
        }

        return $rules;
    }
}
