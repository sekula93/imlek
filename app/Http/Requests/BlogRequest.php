<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BlogRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title'     => 'required',
            'excerpt'   => 'required',
            'text'      => 'required',
            'file'      => 'required|media',
        ];

        if ($this->method == 'PUT') {
            // $rules['email'] = 'required|email';
        } else {
            // $rules['password'] = 'required|confirmed';
            // $rules['email'] = 'required|email|unique:users';
            // $rules['rules'] = 'accepted';
        }

        return $rules;
    }
}
