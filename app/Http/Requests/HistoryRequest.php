<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HistoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'year'  => 'required',
            // 'template_id' => 'required|in:1,2,3',
            // 'trans.*.title' => 'required',
            // 'trans.*.text'  => 'required',
            'active'        => 'required',
        ];

        if (strtolower($this->method) == 'put') {
            $rules['image_left'] = 'mimes:jpeg,png';
            // $rules['file_right'] = 'mimes:jpeg,png,mp4';
        } else {
            $rules['image_left'] = 'required|mimes:jpeg,png';
            // $rules['file_right'] = 'required|mimes:jpeg,png,mp4';
        }

        return $rules;
    }
}
