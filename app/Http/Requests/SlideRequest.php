<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SlideRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            // 'trans.*.title'     => 'required',/
            // 'trans.*.subtitle'  => 'required',
            'active'            => 'required'
        ];


        if (strtolower($this->method) == 'put') {
            $rules['file'] = 'mimes:jpeg,png,mp4';
        } else {
            $rules['file'] = 'required|mimes:jpeg,png,mp4';
        }

        return $rules;
    }
}
