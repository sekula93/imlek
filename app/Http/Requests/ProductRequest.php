<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            // 'trans.*.title' => 'required',
            // 'trans.*.slug'  => 'required',
            // 'trans.*.text'  => 'required',
            'active'        => 'required',
        ];

        if (strtolower($this->method) == 'put') {
            // $rules['video'] = 'mimes:mp4';
            // $rules['poster'] = 'mimes:jpeg,png';
            $rules['featured_image'] = 'mimes:jpeg,png';
        } else {
            // $rules['video'] = 'required|mimes:mp4';
            // $rules['poster'] = 'required|mimes:jpeg,png';
            $rules['featured_image'] = 'required|mimes:jpeg,png';
        }

        return $rules;
    }
}
