<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RecipeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'category_id'       => 'required',
            // 'trans.*.title'     => 'required',
            // 'trans.*.slug'      => 'required',
            // 'trans.*.excerpt'   => 'required',
            'active'            => 'required',
            'video'             => 'mimes:mp4',
            // 'trans.*.preparation' => 'required',
            // 'trans.*.step.0.title' => 'required',
            // 'trans.*.step.0.ingredient' => 'required\'
        ];

        if (strtolower($this->method) == 'put') {
            $rules['image'] = 'mimes:jpeg,png';
            $rules['grid_image'] = 'mimes:jpeg,png';
        } else {
            $rules['image'] = 'required|mimes:jpeg,png';
            $rules['grid_image'] = 'required|mimes:jpeg,png';
        }

        return $rules;
    }
}
