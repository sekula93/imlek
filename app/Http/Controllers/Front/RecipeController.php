<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\Recipe\RecipeInterface;
use App\Repositories\RecipeCategory\RecipeCategoryInterface;

class RecipeController extends Controller
{
    private $recipe;

    public function __construct(RecipeInterface $recipe)
    {
        $this->recipe = $recipe;
    }

    /**
     * Display recipe listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RecipeCategoryInterface $recipeCategory, $cat = null)
    {
        // $keywords = $request->get('keywords');
        $categoriesArray = $recipeCategory->all();
        // $filterClasses = array_map(function ($item) {
        //     return 'recipe-cat-'.$item['id'];
        // }, $categoriesArray->toArray());

        $paginatedItems = 12;
        // if ($filteredCategory) {
        //     $paginatedItems = 30;
        // }

        $recipesArray = $this->recipe->paginateForFront($paginatedItems, null, null, $cat);
        // dd($recipesArray);
        // if ($request->ajax()) {
        //     $content = '';
        //     foreach ($recipesArray as $recipe) {
        //         $content .= view('front.recipe._partials.recipe', compact('recipe'))->render();
        //     }
        //     $next = $recipesArray->appends(['keywords' => $keywords])->nextPageUrl();
        //     return compact('content', 'next');
        // }
        
        return view('front.recipe.index', compact('recipesArray', 'categoriesArray'));
    }

    /**
     * Display recipe details
     *
     * @return \Illuminate\Http\Response
     */
    public function show($cat, $slug)
    {

        $recipe = $this->recipe->findBySlug($slug);
        $components = $recipe->preparation;

        $componentsArray = preg_split('/\n|\r\n?/', $components);

        $similarArray = $this->recipe->paginateForFront(4, null, $recipe->recipe_id, $recipe->category_id);


        return view('front.recipe.show', compact('recipe', 'similarArray', 'componentsArray'));
    }
}
