<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ContactRequest;
use App\Http\Controllers\Controller;
use App\Repositories\Post\PostInterface;
use App\Repositories\PostCategory\PostCategoryInterface;
use App\Repositories\Slide\SlideInterface;
use App\Repositories\Recipe\RecipeInterface;
use App\Repositories\History\HistoryInterface;
use App\Repositories\Product\PackshotInterface;
use App\Repositories\Package\PackageInterface;
use App\Repositories\Company\CompanyInterface;
use App\Repositories\Report\ReportInterface;
use App\Repositories\Faq\FaqInterface;
use App\Repositories\Job\JobInterface;
use App\Repositories\Certificate\CertificateInterface;
// use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use App\Services\Mailers\ContactMailer;
use App\StaticContent;
use App\Services\File\Document as DocumentService;

class PageController extends Controller
{
    protected $post;
    protected $instagram;

    public function __construct(PostInterface $post)
    {
        $this->post = $post;
    }
    /**
     * Display front page
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SlideInterface $slide, PackageInterface $package, StaticContent $content)
    {
        $slides = $slide->getForFront();
        // $recentPosts = $this->post->categoriesRecent();
        // $recentRecipes = $recipe->recent(5);

        $recentPosts = $this->post->recent(5);

        $packages = $package->recent(5);
        // dd($packages);
        $bodyClass = 'index';

        return view('front.page.index', compact('slides', 'packages','recentPosts', 'bodyClass'));
    }

    public function pavlaka()
    {
        $bodyClass = 'pavlaka-bg';
        return view('front.pavlaka.index', compact('bodyClass'));
    }

    public function history(HistoryInterface $history, CompanyInterface $company)
    {
        $yearsArray = $history->getForFront();
        $listArray = $company->recent(4);
        return view('front.page.history', compact('yearsArray', 'listArray'));
    }


    public function news(PostCategoryInterface $category, $cat = null)
    {

        $posts = $this->post->getBySlug($cat);

        if(count($posts) < 1) {
            $posts = $this->post->paginate();
        }

        $categories = $category->listCategories();
        return view('front.page.news.index', compact('posts', 'categories'));
    }

    public function showPost($cat, $slug)
    {
        $post = $this->post->findBySlug($slug);

        $id = ['id' => $post->id];
        $recentPosts = $this->post->recent(5, $id);
        return view('front.page.news.show', compact('post','recentPosts'));
    }

    public function paginatePosts()
    {
        $recentPosts = $this->post->recent(4);
        $content = '';
        foreach ($recentPosts as $post) {
            $content .= view('front.layout._partials.post.news', compact('post'))->render();
        }
        $next = $recentPosts->setPath(route('front.page.paginate-posts'))->nextPageUrl();
        return compact('content', 'next');
    }

    public function paginateHomeGrid(RecipeInterface $recipe)
    {
        $recentPosts = $this->post->categoriesRecent();
        $recentRecipes = $recipe->recent(5);
        $content = '';
        foreach ($recentPosts as $post) {
            $content .= view('front.layout._partials.post-grid', compact('recentPosts', 'recentRecipes'))->render();
        }
        $next = $recentPosts['news']->setPath(route('front.page.paginate-home-grid'))->nextPageUrl();
        return compact('content', 'next');
    }

    /**
     * Display privacy page
     *
     * @return \Illuminate\Http\Response
     */
    public function staticText(StaticPageInterface $page, $slug)
    {
        // $content = $page->findBySlug($slug);
        // $recent = $this->post->recent(4);

        return view('front.page.static-text', compact('content', 'recent'));
    }

    // get contact form
    public function showContact(JobInterface $job)
    {
        $jobs = $job->getActiveJobs();
        return view('front.page.contact', compact('jobs'));
    }


    // post contact form
    public function contactUs(Request $request, ContactMailer $mailer, DocumentService $document)
    {

        // try {
            
            $attachment = $request->file('file');
            $data = $request->all();
            $filename = null;
            if ($attachment) {
                $filename = $document->upload($attachment, config('settings.file.email'));
                $path = public_path().config('settings.file.email.upload_dir').$filename;
                $data['path'] = $path;
            }
            
            $mailer->sendContactMail($data);
            if ($filename) {
                $document->delete($filename, config('settings.file.email'));
            }

            return ['status' => 'success'];
        // } catch (\Exception $e) {
            dd($e);
            return ['status' => 'error'];
        // }
    }

    public function reports(ReportInterface $report)
    {
        $reports = $report->paginateForFront(30);
        return view('front.page.reports', compact('reports'));
    }

    public function faq(FaqInterface $faq)
    {
        $faqs = $faq->getOrdered();

        return view('front.page.faqs', compact('faqs'));
    }

    public function certificates(CertificateInterface $certificate)
    {
        $certificates = $certificate->paginateForFront(9);
        return view('front.page.certificates', compact('certificates'));
    }

    public function showCompany($slug, CompanyInterface $company)
    {
        $post = $company->findBySlug($slug);
        return view('front.page.company.show', compact('post'));
    }

    public function junior()
    {
        $bodyClass = 'junior-parallax';
        $footerClass = 'junior-footer';
        return view('front.junior.index', compact('bodyClass', 'footerClass'));
    }

    public function maslac()
    {
        $bodyClass = 'maslac-parallax';
        $footerClass = 'maslac-footer';
        return view('front.maslac.index', compact('bodyClass', 'footerClass'));
    }

    public function jogood() 
    {
        $bodyClass = 'jogood-parallax';
        $footerClass = 'jogood-footer';
        return view ('front.jogood.index', compact('bodyClass', 'footerClass'));
    }

    public function error404() {
        return view('errors.404');
    }


}
