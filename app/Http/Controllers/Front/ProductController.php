<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\Product\ProductInterface;
use App\Repositories\Brand\BrandInterface;
use App\Repositories\Product\NutritiveInterface;
use App\Repositories\Post\PostInterface;

class ProductController extends Controller
{
    protected $product;

    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }

    /**
     * Display product listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BrandInterface $brand)
    {
        $brands = $brand->all();
        $productsArray = $this->product->publicFamilies();
        return view('front.product.index', compact('productsArray', 'brands'));
    }

    /**
     * Display product details
     *
     * @return \Illuminate\Http\Response
     */
    public function show(PostInterface $post, $slug)
    {
        $product = $this->product->findBySLug($slug);
        // dd($product);
        // dd($product);
        $recentPosts = $post->recent(4);

        return view('front.product.show', compact('product', 'recentPosts'));
    }

    public function showNutritive(NutritiveInterface $nutritive, $id)
    {
        $nutritive = $nutritive->find($id)->translations()->where('code', \App::getLocale())->first();
        $table = json_decode($nutritive->pivot->table);
        $legend = $nutritive->pivot->legend;

        return view('front.product._partials.table', compact('legend', 'table'));
    }
}
