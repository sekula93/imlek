<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Repositories\Product\ProductInterface;
use App\Repositories\Brand\BrandInterface;
use App\Repositories\Product\NutritiveInterface;
use App\Services\File\Image as ImageService;
use App\Services\File\Document as DocumentService;

class ProductController extends BaseController
{
    protected $product;

    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }

    /**
     * Display a product listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->product->paginate();
        return view('admin.product.index', compact('listArray'));
    }

    /**
     * Display product create form
     *
     * @return \Illuminate\Http\Response
     */
    public function create(BrandInterface $brand)
    {
        $brands = $brand->getActive();
        return view('admin.product.create', compact('brands'));
    }

    /**
     * Store product
     *
     * @return
     */
    public function store(ProductRequest $request, ImageService $image, DocumentService $document)
    {
        // try {
            $data = $request->all();
            $data['image_orientation'] = $image->getOrientation($request->file('featured_image'));
            // $data['video'] = $document->upload($request->file('video'), config('settings.video.product'));
            $data['poster'] = $image->upload($request->file('poster'), config('settings.image.product.poster'));
            $data['featured_image'] = $image->upload($request->file('featured_image'), config('settings.image.product.featured'));
            $product = $this->product->create($data);

            \Notification::success('Proizvod je uspešno kreiran');
            return redirect(route('admin.product.edit', $product->id));
        // } catch (\Exception $e) {
        //     \Notification::error('Dogodila se greška prilikom kreiranja proizvoda');
        //     return redirect(route('admin.product.index'));
        // }
    }

    /**
     * Display product create form
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(NutritiveInterface $nutritive, BrandInterface $brand, $id)
    {

        $brands = $brand->getActive();
        $product = $this->product->findForAdmin($id);
        $nutritiveTables = $nutritive->all();


        return view('admin.product.edit', compact('product', 'nutritiveTables', 'brands'));
    }

    public function update($id, ProductRequest $request, ImageService $image, DocumentService $document)
    {
        try {
            $product = $this->product->find($id);
            $data = $request->all();
        

            $data['featured_image'] = $image->upload($request->file('featured_image'), config('settings.image.product.featured'));
            $data['poster'] = $image->upload($request->file('poster'), config('settings.image.product.poster'));
            $data['video'] = $document->upload($request->file('video'), config('settings.video.product'));
            if ($data['featured_image']) {
                $imagePath = public_path().config('settings.image.product.featured.upload_dir').$data['featured_image'];
                $data['image_orientation'] = $image->getOrientation($imagePath);
                $image->delete($this->product->find($id)->featured_image, config('settings.image.product.featured'));
            }
            if ($data['poster']) {
                $image->delete($this->product->find($id)->poster, config('settings.image.product.poster'));
            }
            if ($data['video']) {
                $document->delete($this->product->find($id)->video, config('settings.video.product'));
            }
            $this->product->update($id, $data);

            \Notification::success('Proizvod je uspešno izmenjen');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene proizvoda');
        }

        return back();
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->product->slugify($request->all())];
    }

    /**
     * Remove the specified product
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $product = $this->product->find($id);
        return $this->deleteItem($id, $this->product);
    }

    /**
     * Method is changing active state of product
     *
     * @return json     information about changed state
     */
    public function changeState(Request $request)
    {
        $product = $this->product->find($request->get('id'));
        // if (\Auth::user()->cannot('update-product', $product)) {
        //     abort(403, "Pristup nije dozvoljen");
        // }

        return $this->changeStatus($this->product, $request);
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->product->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
