<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Request;

class AuthController extends Controller
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Constructor for AuthController class
     *
     * @param Guard $auth [description]
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Show login view
     *
     * @return View
     */
    public function login()
    {
        return view('admin.auth.login');
    }

    /**
     * Proceeed login form
     *
     * @param  \Illuminate\Http\Request $request [description]
     * @return [type]                            [description]
     */
    public function postLogin(\Illuminate\Http\Request $request)
    {
        $this->validate($request, ['email' => 'required|email', 'password' => 'required']);
        $credentials = $request->only('email', 'password');
        if ($this->auth->attempt($credentials, $request->has('remember'))) {
            \Auth::user()->update(['logged_at' => \Carbon\Carbon::now()]);
            if ($this->auth->user()->role == 0) {
                return redirect(route('admin.dashboard.index'));
            } else {
                return redirect(route('admin.post.index'));
            }
        }

        return redirect(route('admin.auth.login'))
                    ->withInput($request->only('email', 'remember'))
                    ->withErrors([
                        'email' => trans('admin/auth.errors.login_failed'),
                    ]);
    }

    public function logout()
    {
        $this->auth->logout();

        return redirect(route('admin.auth.login'));
    }



    /**
     * Show password forgot form
     *
     * @return View
     */
    public function passwordForgot()
    {
        return view('admin.auth.password-forgot');
    }

    /**
     * Process password reset form
     *
     * @param  \Illuminate\Http\Request $request [description]
     * @return [type]                            [description]
     */
    public function passwordReset(\Illuminate\Http\Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        return $request->all();
    }
}
