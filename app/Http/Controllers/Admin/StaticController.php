<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\StaticContent;
use App\Http\Requests\CatalogueRequest;
use App\Services\File\Document as DocumentService;

class StaticController extends Controller
{
    public function __construct()
    {
    }

    public function catalogue(StaticContent $content)
    {
        $catalogue = $content->getCatalogue();
        return view('admin.static.catalogue', compact('catalogue'));
    }

    /**
     * Update the catalogue file
     *
     */
    public function updateCatalogue(CatalogueRequest $request, StaticContent $content, DocumentService $document)
    {
        try {
            $filename = $document->upload($request->file('file'), config('settings.file.catalogue'));
            $content->updateCatalogue($filename);
            \Notification::success('Katalog je uspešno izmenjen');
        } catch (\Exception $e) {
            dd($e);
            \Notification::error('Dogodila se greška prilikom izmene kataloga');
        }

        return redirect(route('admin.static.catalogue'));
    }
}
