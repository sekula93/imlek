<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\Post\PostInterface;
use App\Services\File\Image as ImageService;

class SocialPostController extends BaseController
{
    protected $post;

    public function __construct(PostInterface $post)
    {
        $this->post = $post;
    }

    /**
     * Display a post listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        $postsArray = $this->post->paginateSocial($type);

        return view('admin.social-post.index', compact('postsArray'));
    }

    /**
     * Remove the specified post
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $post = $this->post->find($id);
        return $this->deleteItem($id, $this->post);
    }

    /**
     * Method is changing active state of post
     *
     * @return json     information about changed state
     */
    public function changeState(Request $request)
    {
        $post = $this->post->find($request->get('id'));
        // if (\Auth::user()->cannot('update-post', $post)) {
        //     abort(403, "Pristup nije dozvoljen");
        // }

        return $this->changeStatus($this->post, $request);
    }
}
