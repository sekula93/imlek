<?php

namespace App\Http\Controllers\Admin\Product;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\BaseController;
use App\Services\File\Image as ImageService;
use App\Repositories\Product\PackshotInterface;

class PackshotController extends BaseController
{
    protected $packshot;

    public function __construct(PackshotInterface $packshot)
    {
        $this->packshot = $packshot;
    }

    public function store(Request $request, ImageService $image, $id)
    {
        try {
            $file = $request->file('file');
            $data = $request->all();
            $data['trans'] = json_decode($data['trans'], true);



            $data['orientation'] = $image->getOrientation($file);
            $data['filename'] = $image->upload($file, config('settings.image.product.packshot'));
            $packshot = $this->packshot->create($id, $data);

            return ['status' => 'success'];
        } catch (Exception $e) {
            dd($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            $data['trans'] = json_decode($data['trans'], true);
            $packshot = $this->packshot->update($id, $data);

            return ['status' => 'success'];
        } catch (Exception $e) {
            dd($e);
        }
    }

    public function destroy($id)
    {
        return $this->deleteItem($id, $this->packshot);
    }

    public function reorder(Request $request)
    {
        if ($request->get('file')) {
            foreach ($request->get('file') as $index => $fileId) {
                $this->packshot->setOrder($fileId, $index);
            }
            return ['status' => 'success'];
        }
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->packshot, $request);
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->packshot->slugify($request->all())];
    }
}
