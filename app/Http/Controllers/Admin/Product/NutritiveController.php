<?php

namespace App\Http\Controllers\Admin\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NutritiveRequest;
use App\Repositories\Product\NutritiveInterface;
use App\Services\File\Image as ImageService;
use App\Services\File\Document as DocumentService;

class NutritiveController extends BaseController
{
    protected $nutritive;

    public function __construct(NutritiveInterface $nutritive)
    {
        $this->nutritive = $nutritive;
    }

    /**
     * Display a nutritive listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->nutritive->paginate();
        return view('admin.product.nutritive.index', compact('listArray'));
    }

    /**
     * Display nutritive create form
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.nutritive.create');
    }

    /**
     * Store nutritive
     *
     * @return
     */
    public function store(NutritiveRequest $request, ImageService $image, DocumentService $document)
    {
        try {
            $data = $request->all();
            $this->nutritive->create($data);
            \Notification::success('Tabela je uspešno kreirana');
        } catch (\Exception $e) {
            dd($e);
            \Notification::error('Dogodila se greška prilikom kreiranja tabele');
        }
        return redirect(route('admin.product.nutritive.index'));
    }

    /**
     * Display nutritive create form
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nutritive = $this->nutritive->find($id);
        $nutritiveTable = $this->nutritive->decodeTable($nutritive);
        return view('admin.product.nutritive.edit', compact('nutritive', 'nutritiveTable'));
    }

    public function update($id, NutritiveRequest $request, ImageService $image, DocumentService $document)
    {
        try {
            $data = $request->all();
            $this->nutritive->update($id, $data);
            \Notification::success('Tabela je uspešno izmenjena');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene tabele');
        }

        return back();
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->nutritive->slugify($request->all())];
    }

    /**
     * Remove the specified nutritive
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $nutritive = $this->nutritive->find($id);
        return $this->deleteItem($id, $this->nutritive);
    }

    /**
     * Method is changing active state of nutritive
     *
     * @return json     information about changed state
     */
    public function changeState(Request $request)
    {
        $nutritive = $this->nutritive->find($request->get('id'));
        return $this->changeStatus($this->nutritive, $request);
    }
}
