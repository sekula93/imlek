<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\RecipeRequest;
use App\Repositories\Recipe\RecipeInterface;
use App\Repositories\RecipeCategory\RecipeCategoryInterface;
use App\Repositories\User\UserInterface;
use App\Services\File\Image as ImageService;
use App\Services\File\Document as DocumentService;

class RecipeController extends BaseController
{
    protected $recipe;

    public function __construct(RecipeInterface $recipe)
    {
        $this->recipe = $recipe;
    }

    /**
     * Display a recipe listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->recipe->paginate();
        return view('admin.recipe.index', compact('listArray'));
    }

    /**
     * Display recipe create form
     *
     * @return \Illuminate\Http\Response
     */
    public function create(UserInterface $user, RecipeCategoryInterface $category)
    {
        $categoriesArray = $category->listCategories();
        $authorsArray = $user->listsRecipeAuthors();
        return view('admin.recipe.create', compact('authorsArray', 'categoriesArray'));
    }

    /**
     * Store recipe
     *
     * @return
     */
    public function store(RecipeRequest $request, ImageService $image, DocumentService $document)
    {
        try {
            $data = $request->all();
            // dd($data);
            $data['video'] = $document->upload($request->file('video'), config('settings.video.recipe'));
            $data['image'] = $image->upload($request->file('image'), config('settings.image.recipe.hero'));
            $data['grid_image'] = $image->upload($request->file('grid_image'), config('settings.image.recipe.grid'));
            $this->recipe->create($data);
            \Notification::success('Recept je uspešno kreiran');
        } catch (\Exception $e) {
            dd($e);
            \Notification::error('Dogodila se greška prilikom kreiranja recepta');
        }
        return redirect(route('admin.recipe.index'));
    }

    /**
     * Display recipe create form
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(UserInterface $user, RecipeCategoryInterface $category, $id)
    {
        $categoriesArray = $category->listCategories();
        $authorsArray = $user->listsRecipeAuthors();
        $recipe = $this->recipe->find($id);
        $stepsArray = $this->recipe->decodeSteps($recipe);

        return view('admin.recipe.edit', compact(['recipe', 'authorsArray', 'stepsArray', 'categoriesArray']));
    }

    public function update($id, RecipeRequest $request, ImageService $image, DocumentService $document)
    {
        try {
            $recipe = $this->recipe->find($id);
            $data = $request->all();
            $data['video'] = $document->upload($request->file('video'), config('settings.video.recipe'));
            $data['image'] = $image->upload($request->file('image'), config('settings.image.recipe.hero'));
            $data['grid_image'] = $image->upload($request->file('grid_image'), config('settings.image.recipe.grid'));
            if ($data['image']) {
                $image->delete($this->recipe->find($id)->image, config('settings.image.recipe.hero'));
            }
            if ($data['grid_image']) {
                $image->delete($this->recipe->find($id)->grid_image, config('settings.image.recipe.grid'));
            }
            if ($data['video']) {
                $document->delete($this->recipe->find($id)->video, config('settings.video.recipe'));
            }
            $this->recipe->update($id, $data);
            \Notification::success('Recept je uspešno izmenjen');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene recepta');
        }
        return back();
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->recipe->slugify($request->all())];
    }

    /**
     * Remove the specified recipe
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $recipe = $this->recipe->find($id);
        return $this->deleteItem($id, $this->recipe);
    }

    /**
     * Method is changing active state of recipe
     *
     * @return json     information about changed state
     */
    public function changeState(Request $request)
    {
        $recipe = $this->recipe->find($request->get('id'));
        return $this->changeStatus($this->recipe, $request);
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->recipe->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
