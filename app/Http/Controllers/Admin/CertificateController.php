<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\Certificate\CertificateInterface;
use App\Services\File\Image as ImageService;
use App\Services\File\Document as DocumentService;

class CertificateController extends BaseController
{

    protected $certificate;

    public function __construct(CertificateInterface $certificate)
    {
        $this->certificate = $certificate;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->certificate->paginate();
        return view('admin.certificate.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.certificate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ImageService $image, DocumentService $document)
    {
        try {
            $data = $request->all();
            $data['attachment'] = $document->upload($request->file('attachment'), config('settings.file.certificate'));
            $data['featured_image'] = $image->upload($request->file('featured_image'), config('settings.image.certificate'));
            $certificate = $this->certificate->create($data);

            \Notification::success('Sertifikat je uspešno kreiran');
            return redirect(route('admin.certificate.index'));

        } catch (\Exception $e) {
            \Notification::error('Dogodila se greška prilikom kreiranja sertifikata');
            return redirect(route('admin.certificate.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $certificate = $this->certificate->find($id);

        return view('admin.certificate.edit', compact('certificate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, DocumentService $document, ImageService $image)
    {
        try {
            $certificate = $this->certificate->find($id);
            $data = $request->all();

            $data['attachment'] = $document->upload($request->file('attachment'), config('settings.file.certificate'));
            $data['featured_image'] = $image->upload($request->file('featured_image'), config('settings.image.certificate'));
            
            $this->certificate->update($id, $data);

            \Notification::success('Posao je uspešno izmenjen');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene Posla');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $certificate = $this->certificate->find($id);
        return $this->deleteItem($id, $this->certificate);
    }

    public function changeState(Request $request)
    {
        $certificate = $this->certificate->find($request->get('id'));
    
        return $this->changeStatus($this->certificate, $request);
    }

     public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->certificate->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
