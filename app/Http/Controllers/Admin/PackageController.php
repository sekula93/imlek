<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\Package\PackageInterface;
use App\Services\File\Image as ImageService;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PackageController extends BaseController
{
    protected $package;

    public function __construct(PackageInterface $package)
    {
        $this->package = $package;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->package->paginate();
        return view('admin.package.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ImageService $image)
    {
        // try {
            $data = $request->all();

            $data['image_orientation'] = $image->getOrientation($request->file('featured_image'));
            // $data['video'] = $document->upload($request->file('video'), config('settings.video.package'));
            // $data['poster'] = $image->upload($request->file('poster'), config('settings.image.package.poster'));
            $data['featured_image'] = $image->upload($request->file('featured_image'), config('settings.image.package.featured'));
            $package = $this->package->create($data);

            \Notification::success('Package3d je uspešno kreiran');
            return redirect(route('admin.package.edit', $package->id));
        // } catch (\Exception $e) {
        //     \Notification::error('Dogodila se greška prilikom kreiranja proizvoda');
        //     return redirect(route('admin.package.index'));
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = $this->package->find($id);
        return view('admin.package.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, ImageService $image)
    {
        try {
            $package = $this->package->find($id);
            $data = $request->all();

            $data['featured_image'] = $image->upload($request->file('featured_image'), config('settings.image.package.featured'));

            if ($data['featured_image']) {
                $imagePath = public_path().config('settings.image.package.featured.upload_dir').$data['featured_image'];
                $data['image_orientation'] = $image->getOrientation($imagePath);
                $image->delete($this->package->find($id)->featured_image, config('settings.image.package.featured'));
            }
            
            $this->package->update($id, $data);

            \Notification::success('Proizvod je uspešno izmenjen');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene proizvoda');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = $this->package->find($id);
        return $this->deleteItem($id, $this->package);
    }

    /**j
     * Method is changing active state of package
     *
     * @return json     information about changed state
     */
    public function changeState(Request $request)
    {
        $package = $this->package->find($request->get('id'));
        return $this->changeStatus($this->package, $request);
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->package->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        } 
    }
}
