<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Repositories\Post\PostInterface;
use App\Repositories\PostCategory\PostCategoryInterface;
use App\Services\File\Image as ImageService;
use App\Services\File\Document as DocumentService;

class PostController extends BaseController
{
    protected $post;

    public function __construct(PostInterface $post)
    {
        $this->post = $post;
    }

    /**
     * Display a post listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->post->paginateAdmin();
        return view('admin.post.index', compact('listArray'));
    }

    /**
     * Display post create form
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PostCategoryInterface $category)
    {
        $categories = $category->listCategoriesForAdmin();
        return view('admin.post.create', compact('categories'));
    }

    /**
     * Store post
     *
     * @return
     */
    public function store(PostRequest $request, ImageService $image, DocumentService $document)
    {
        // try {
            $data = $request->all();

            $data['attachment'] = $document->upload($request->file('attachment'), config('settings.file.post'));
            $data['image'] = $image->upload($request->file('image'), config('settings.image.post.featured'));
            $data['landscape_image'] = $image->upload($request->file('landscape_image'), config('settings.image.post.landscape'));
            $post = $this->post->create($data);

            \Notification::success('Post je uspešno kreiran');
            return redirect(route('admin.post.edit', $post->id));
        // } catch (\Exception $e) {
            // \Notification::error('Dogodila se greška prilikom kreiranja posta');
            // return redirect(route('admin.post.index'));
        // }

    }

    /**
     * Display post create form
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id, PostCategoryInterface $category)
    {
        
        $post = $this->post->find($id);

        $categories = $category->listCategoriesForAdmin();

        if (\Auth::user()->cannot('update-post', $post)) {
            abort(403, "Pristup nije dozvoljen");
        }

        return view('admin.post.edit', compact('post', 'categories'));

    }

    public function update($id, PostRequest $request, ImageService $image, DocumentService $document)
    {
        try {

            $post = $this->post->find($id);
            $data = $request->all();

            $data['attachment'] = $document->upload($request->file('attachment'), config('settings.file.post'));
            $data['image'] = $image->upload($request->file('image'), config('settings.image.post.featured'));
            $data['landscape_image'] = $image->upload($request->file('landscape_image'), config('settings.image.post.landscape'));
            if ($data['attachment']) {
                $image->delete($this->post->find($id)->attachment, config('settings.file.post'));
            }
            if ($data['image']) {
                $image->delete($this->post->find($id)->image, config('settings.image.post.featured'));
            }
            if ($data['landscape_image']) {
                $image->delete($this->post->find($id)->landscape_image, config('settings.image.post.landscape'));
            }
            $this->post->update($id, $data);

            \Notification::success('Post je uspešno izmenjen');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene posta');
        }

        return back();
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->post->slugify($request->all())];
    }

    /**
     * Remove the specified post
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $post = $this->post->find($id);
        return $this->deleteItem($id, $this->post);
    }

    /**
     * Method is changing active state of post
     *
     * @return json     information about changed state
     */
    public function changeState(Request $request)
    {
        $post = $this->post->find($request->get('id'));
        // if (\Auth::user()->cannot('update-post', $post)) {
        //     abort(403, "Pristup nije dozvoljen");
        // }

        return $this->changeStatus($this->post, $request);
    }

    public function uploadGallery(Request $request, \App\PostPhoto $postPhoto, ImageService $image, $id)
    {
        try {
            foreach ($request->file('files') as $file) {
                $fileName = $image->upload($file, config('settings.image.post'));

                $postPhoto->create(['post_id' => $id, 'filename' => $fileName]);

                return ['status' => 'success'];
            }
        } catch (Exception $e) {
            dd($e);
        }
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->post->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
