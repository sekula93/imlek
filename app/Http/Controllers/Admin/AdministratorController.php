<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserInterface;
use App\Repositories\User\Type\Administrator;
use App\Http\Requests\AdministratorRequest;
use App\Services\File\Image as ImageService;

class AdministratorController extends BaseController
{
    private $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;

        // if (\Auth::user()->cannot('access')) {
        //     abort(403, "Pristup nije dozvoljen");
        // }

        // $this->authorize('access', \Auth::user());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::user()->cannot('access')) {
            abort(403, "Pristup nije dozvoljen");
        }

        $listArray = $this->user->paginateUsers(0);

        return view('admin.administrator.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (\Auth::user()->cannot('access')) {
            abort(403, "Pristup nije dozvoljen");
        }

        return view('admin.administrator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdministratorRequest $request)
    {
        if (\Auth::user()->cannot('access')) {
            abort(403, "Pristup nije dozvoljen");
        }

        try {
            $user = $this->user->create(new Administrator($request->all()));
            \Notification::success('Novi administrator je uspešno kreiran');
        } catch (\Exception $e) {
            dd($e);
            \Notification::error('Dogodila se greška prilikom kreiranja administratora');
        }

        return redirect(route('admin.administrator.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (\Auth::user()->cannot('access')) {
            abort(403, "Pristup nije dozvoljen");
        }

        $user = $this->user->find($id);
        return view('admin.administrator.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdministratorRequest $request, $id)
    {
        if (\Auth::user()->cannot('access')) {
            abort(403, "Pristup nije dozvoljen");
        }

        try {
            $this->user->update($id, new Administrator($request->all()));
            \Notification::success('Podaci administratora su uspešno izmenjeni');
        } catch (\Exception $e) {
            dd($e);
            \Notification::error('Dogodila se greška prilikom kreiranja administratora');
        }

        return redirect(route('admin.administrator.index'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        if (\Auth::user()->cannot('access')) {
            abort(403, "Pristup nije dozvoljen");
        }

        return $this->deleteItem($id, $this->user);
    }

    /**
     * Method is changing active state of user
     *
     * @return json     information about changed state
     */
    public function changeState(Request $request)
    {
        if (\Auth::user()->cannot('access')) {
            abort(403, "Pristup nije dozvoljen");
        }

        return $this->changeStatus($this->user, $request);
    }

    public function editProfile()
    {
        $user = \Auth::user();
        $profile = true;

        if ($user->role == 0) {
            return view('admin.administrator.edit', compact('user'));
        } else {
            return view('admin.blogger.edit', compact('user', 'profile'));
        }
    }
}
