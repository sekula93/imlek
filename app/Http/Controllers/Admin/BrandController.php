<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\Brand\BrandInterface;
use App\Http\Requests;
use App\Services\File\Image as ImageService;
use App\Http\Controllers\Controller;

class BrandController extends BaseController
{
    protected $brand;

    public function __construct(BrandInterface $brand)
    {
        $this->brand = $brand;
    }

    public function index()
    {
    	$listArray = $this->brand->paginate();

    	return view('admin.brand.index', compact('listArray'));
    }

    public function create()
    {
        return view('admin.brand.create');
    }

    public function store(Request $request, ImageService $image)
    {
        try {
            $data = $request->all();
            $data['featured_image'] = $image->upload($request->file('featured_image'), config('settings.image.brand.featured'));
            $brand = $this->brand->create($data);

            \Notification::success('Brend je uspešno kreiran');
            return redirect(route('admin.brand.index'));
        } catch (\Exception $e) {
            \Notification::error('Dogodila se greška prilikom kreiranja brenda');
            return redirect(route('admin.brand.index'));
        }
    }

    public function edit($id)
    {
        $brand = $this->brand->find($id);
        return view('admin.brand.edit', compact('brand'));
    }

    public function update($id, ImageService $image, Request $request)
    {
        try {
            $brand = $this->brand->find($id);
            $data = $request->all();

            $data['featured_image'] = $image->upload($request->file('featured_image'), config('settings.image.brand.featured'));
            
            $this->brand->update($id, $data);

            \Notification::success('Brend je uspešno izmenjen');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene brenda');
        }

        return back();
    }

     public function destroy($id)
    {
        $brand = $this->brand->find($id);
        return $this->deleteItem($id, $this->brand);
    }

    public function changeState(Request $request)
    {
        $brand = $this->brand->find($request->get('id'));
     	
     	return $this->changeStatus($this->brand, $request);
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->brand->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }
}
