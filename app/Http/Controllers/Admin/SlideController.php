<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SlideRequest;
use App\Repositories\Slide\SlideInterface;
use App\Services\File\Image as ImageService;

class SlideController extends BaseController
{
    protected $slide;

    public function __construct(SlideInterface $slide)
    {
        $this->slide = $slide;
    }

    /**
     * Display a slide listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videoArray = $this->slide->getVideos();
        $imageArray = $this->slide->getImages();
        $listArray = $this->slide->getImagesAndVideos();

        return view('admin.slide.index', compact('videoArray', 'imageArray', 'listArray'));
    }

    /**
     * Display slide create form
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slide.create');
    }

    /**
     * Store slide
     *
     * @return
     */
    public function store(SlideRequest $request, ImageService $media)
    {
        try {
            $data = $request->all();
            $file = $request->file('file');
            if (in_array($file->guessExtension(), ['jpeg', 'png'])) {
                $data['filename'] = $media->upload($file, config('settings.image.slide'));
                $data['type'] = ImageService::IMAGE_TYPE;
            } else {
                $uploadPath = public_path().config('settings.video.slide.upload_dir');
                $data['filename'] = $media->generateName($file, $uploadPath, ['random_name' => true]);
                $request->file->move($uploadPath, $data['filename']);
                $data['type'] = ImageService::VIDEO_TYPE;
            }
            $data['fallback_image'] = $media->upload($request->file('fallback_image'), config('settings.image.slide'));
            $this->slide->create($data);

            \Notification::success('Slajd je uspešno kreiran');
        } catch (\Exception $e) {
            dd($e);
            \Notification::error('Dogodila se greška prilikom kreiranja slajda');
        }
        return redirect(route('admin.slide.index'));
    }

    /**
     * Display slide create form
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = $this->slide->find($id);
        if ($slide->type == ImageService::IMAGE_TYPE) {
            $uploadPath = config('settings.image.slide.upload_dir');
        } else {
            $uploadPath = config('settings.video.slide.upload_dir');
        }
        return view('admin.slide.edit', compact('slide', 'uploadPath'));
    }

    public function update($id, SlideRequest $request, ImageService $media)
    {
        try {
            $file = $request->file('file');
            $data = $request->all();
            $data['filename'] = null;
            if ($file) {
                // Main file check file type and upload
                if ($file && in_array($file->guessExtension(), ['jpeg', 'png'])) {
                    $data['filename'] = $media->upload($file, config('settings.image.slide'));
                    $data['type'] = ImageService::IMAGE_TYPE;
                } else {
                    $uploadPath = public_path().config('settings.video.slide.upload_dir');
                    $data['filename'] = $media->generateName($file, $uploadPath, ['random_name' => true]);
                    $request->file->move($uploadPath, $data['filename']);
                    $data['type'] = ImageService::VIDEO_TYPE;
                }
                // Main file delete old if new is present
                if ($data['filename']) {
                    $slide = $this->slide->find($id);
                    if ($slide->type == ImageService::IMAGE_TYPE) {
                        $media->delete($slide->filename, config('settings.image.slide'));
                    } else {
                        unlink(public_path().config('settings.video.slide.upload_dir').$slide->filename);
                    }
                }
            }
            // Falback image check existance and upload image
            $data['fallback_image'] = $media->upload($request->file('fallback_image'), config('settings.image.slide'));
            if ($data['fallback_image']) {
                $media->delete($this->slide->find($id)->fallback_image, config('settings.image.slide'));
            }

            $this->slide->update($id, $data);
            \Notification::success('Slide post je uspešno izmenjen');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene slide posta');
        }

        return back();
    }

    public function destroy($id)
    {
        return $this->deleteItem($id, $this->slide);
    }

    /**
     * Method is changing active state of post
     *
     * @return json     information about changed state
     */
    public function changeState(Request $request)
    {
        return $this->changeStatus($this->slide, $request);
    }

    public function reorder(Request $request)
    {
        try {
            $sortArray = $request->get('sort_item');
            foreach ($sortArray as $index => $id) {
                $this->slide->setOrder($id, $index);
            }
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
