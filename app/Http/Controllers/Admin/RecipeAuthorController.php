<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserInterface;
use App\Repositories\User\Type\RecipeAuthor;
use App\Http\Requests\RecipeAuthorRequest;
use App\Services\File\Image as ImageService;

class RecipeAuthorController extends BaseController
{
    private $user;
    private $imageService;

    public function __construct(UserInterface $user, ImageService $imageService)
    {
        $this->user = $user;
        $this->imageService = $imageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->user->paginateUsers(5);
        return view('admin.recipe-author.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.recipe-author.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecipeAuthorRequest $request)
    {
        try {
            $data = $request->all();
            $data['admin'] = false;
            $data['image'] = $this->imageService->upload($request->file('image'), config('settings.image.author'));
            $user = $this->user->create(new RecipeAuthor($data));
            \Notification::success('Novi autor recepata je uspešno kreiran');
        } catch (\Exception $e) {
            dd($e);
            \Notification::error('Dogodila se greška prilikom kreiranja novog autora recepata');
        }

        return redirect(route('admin.recipe-author.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user->find($id);
        $profile = false;

        return view('admin.recipe-author.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecipeAuthorRequest $request, $id)
    {
        try {
            $data = $request->all();
            $data['image'] = $this->imageService->upload($request->file('image'), config('settings.image.author'));

            if ($data['image']) {
                $this->_deleteImage($id);
            }
            $this->user->update($id, new RecipeAuthor($data));

            \Notification::success('Autorovi podaci su uspešno izmenjeni');
        } catch (\Exception $e) {
            dd($e);
            // todo Log error
            \Notification::error('Dogodila se greška prilikom izmene autorovih podataka');
        }
        return back();
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $this->_deleteImage($id);
        return $this->deleteItem($id, $this->user);
    }

    /**
     * Method is changing active state of user
     *
     * @return json     information about changed state
     */
    public function changeState(Request $request)
    {
        return $this->changeStatus($this->user, $request);
    }

    private function _deleteImage($id)
    {
        $user = $this->user->find($id);
        $this->imageService->delete($user->recipeAuthorProfile->image, config('settings.image.author'));
    }
}
