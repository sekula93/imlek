<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class NewsletterController extends Controller
{
    /**
     * Show the list of newsletter subscribers
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\App\Newsletter $newsletter)
    {
        $listArray = $newsletter->paginate();

        return view('admin.newsletter.index', compact('listArray'));
    }

    /**
     * Export the list in excel format
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
    }
}
