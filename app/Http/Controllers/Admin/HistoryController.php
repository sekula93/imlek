<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\HistoryRequest;
use App\Repositories\History\HistoryInterface;
use App\Services\File\Image as ImageService;
use App\Services\File\Document as DocumentService;

class HistoryController extends BaseController
{
    protected $history;

    public function __construct(HistoryInterface $history)
    {
        $this->history = $history;
    }

    /**
     * Display a history listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->history->all();
        return view('admin.history.index', compact('listArray'));
    }

    /**
     * Display history create form
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.history.create');
    }

    /**
     * Store history
     *
     * @return
     */
    public function store(HistoryRequest $request, ImageService $image)
    {
        try {
            $data = $request->all();
            $data['image_left'] = $image->upload($request->file('image_left'), config('settings.image.history.left'));
            // $data['file_right'] = $image->upload($request->file('file_right'), config('settings.image.history.right'));
            $this->history->create($data);
            \Notification::success('Godina je uspešno kreirana');
        } catch (\Exception $e) {
            dd($e);
            \Notification::error('Dogodila se greška prilikom kreiranja godine');
        }
        return redirect(route('admin.history.index'));
    }

    /**
     * Display history create form
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $history = $this->history->find($id);
        return view('admin.history.edit', compact('history'));
    }

    public function update($id, HistoryRequest $request, ImageService $image)
    {
        try {
            $history = $this->history->find($id);
            $data = $request->all();

            $data['image_left'] = $image->upload($request->file('image_left'), config('settings.image.history.left'));
            // $data['file_right'] = $image->upload($request->file('file_right'), config('settings.image.history.right'));
            if ($data['image_left']) {
                $image->delete($this->history->find($id)->image_left, config('settings.image.history.left'));
            }
            // if ($data['file_right']) {
            //     $image->delete($this->history->find($id)->file_right, config('settings.image.history.file_right'));
            // }
            $this->history->update($id, $data);
            \Notification::success('Godina je uspešno izmenjena');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene godine');
        }

        return back();
    }

    /**
     * Remove the specified history
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $history = $this->history->find($id);
        return $this->deleteItem($id, $this->history);
    }

    /**
     * Method is changing active state of history
     *
     * @return json     information about changed state
     */
    public function changeState(Request $request)
    {
        $history = $this->history->find($request->get('id'));
        return $this->changeStatus($this->history, $request);
    }
}
