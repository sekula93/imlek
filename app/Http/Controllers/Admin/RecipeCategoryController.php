<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\RecipeCategoryRequest;
use App\Repositories\RecipeCategory\RecipeCategoryInterface;

class RecipeCategoryController extends BaseController
{
    protected $category;

    public function __construct(RecipeCategoryInterface $category)
    {
        $this->category = $category;
    }

    /**
     * Display a category listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->category->paginate();
        return view('admin.recipe-category.index', compact('listArray'));
    }

    /**
     * Display category create form
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.recipe-category.create');
    }

    /**
     * Store category
     *
     * @return
     */
    public function store(RecipeCategoryRequest $request)
    {
        try {
            $this->category->create($request->all());
            \Notification::success('Kategorija je uspešno kreirana');
        } catch (\Exception $e) {
            dd($e);
            \Notification::error('Dogodila se greška prilikom kreiranja kategorije');
        }
        return redirect(route('admin.recipe-category.index'));
    }

    /**
     * Display category create form
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->find($id);
        return view('admin.recipe-category.edit', compact(['category']));
    }

    public function update(RecipeCategoryRequest $request, $id)
    {
        try {
            $this->category->update($id, $request->all());
            \Notification::success('Kategorija je uspešno izmenjena');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene kategorije');
        }
        return back();
    }

    public function slugify(Request $request)
    {
        return ['slug' => $this->category->slugify($request->all())];
    }

    /**
     * Remove the specified category
     *
     * @param  int  $id
     * @return json
     */
    public function destroy($id)
    {
        $category = $this->category->find($id);
        return $this->deleteItem($id, $this->category);
    }

    /**
     * Method is changing active state of category
     *
     * @return json     information about changed state
     */
    public function changeState(Request $request)
    {
        $category = $this->category->find($request->get('id'));
        return $this->changeStatus($this->category, $request);
    }
}
