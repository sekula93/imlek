<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\Report\ReportInterface;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\File\Document as DocumentService;

class ReportController extends BaseController
{

    protected $report;

    public function __construct(ReportInterface $report)
    {
        $this->report = $report;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->report->paginate();
        return view('admin.report.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.report.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, DocumentService $document)
    {
        // try {
            $data = $request->all();
            // dd($data);
            $data['attachment'] = $document->upload($request->file('attachment'), config('settings.file.report'));
            $report = $this->report->create($data);
            \Notification::success('Izveštaj je uspešno kreiran');
            return redirect(route('admin.report.edit', $report->id));
        // } catch (\Exception $e) {
            // \Notification::error('Dogodila se greška prilikom kreiranja izveštaja');
            // return redirect(route('admin.report.index'));
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = $this->report->find($id);

        return view('admin.report.edit', compact('report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, DocumentService $document)
    {
        try {

            $report = $this->report->find($id);
            $data = $request->all();

            $data['attachment'] = $document->upload($request->file('attachment'), config('settings.file.report'));
            $this->report->update($id, $data);

            \Notification::success('Izveštaj je uspešno izmenjen');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene izveštaja');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $report = $this->report->find($id);
        return $this->deleteItem($id, $this->report);
    }

    public function changeState(Request $request)
    {
        $report = $this->report->find($request->get('id'));
    
        return $this->changeStatus($this->report, $request);
    }

    public function reorder(Request $request)
    {
        if ($request->get('sort_item')) {
            foreach ($request->get('sort_item') as $index => $itemId) {
                $this->report->setOrder($itemId, $index);
            }
            return ['status' => 'success'];
        }
    }

}
