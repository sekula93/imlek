<?php

namespace App\Http\Controllers\Admin;

class BaseController extends \App\Http\Controllers\Controller
{
    public function changeStatus($model, $request)
    {
        $result = $model->changeStatus($request->get('id'));
        
        if ($result === false) {
            $response = ['status' => 'error'];
        } else {
            $response = ['status' => 'success', 'state' => $result];
        }

        return $response;
    }

    public function deleteItem($id, $model)
    {
        if ($model->delete($id)) {
            $response = ['status' => 'success'];
        } else {
            $response = ['status' => 'error'];
        }

        return $response;
    }
}
