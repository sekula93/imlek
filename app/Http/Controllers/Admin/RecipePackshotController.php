<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\File\Image as ImageService;
use App\Repositories\RecipePackshot\RecipePackshotInterface;

class RecipePackshotController extends BaseController
{
    protected $photo;

    public function __construct(RecipePackshotInterface $photo)
    {
        $this->photo = $photo;
    }

    public function store(Request $request, ImageService $image, $id)
    {
        try {
            foreach ($request->file('files') as $file) {
                $fileName = $image->upload($file, config('settings.image.recipe.slider'));
                $photo = $this->photo->create($id, $fileName);

                return ['status' => 'success'];
            }
        } catch (Exception $e) {
            dd($e);
        }
    }

    public function destroy($id)
    {
        return $this->deleteItem($id, $this->photo);
    }

    public function reorder(Request $request)
    {
        if ($request->get('file')) {
            foreach ($request->get('file') as $index => $fileId) {
                $this->photo->setOrder($fileId, $index);
            }
            return ['status' => 'success'];
        }
    }

    public function changeState(Request $request)
    {
        return $this->changeStatus($this->photo, $request);
    }

}
