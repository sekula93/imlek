<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\Company\CompanyInterface;
use App\Services\File\Image as ImageService;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CompanyController extends BaseController
{
    protected $company;

    public function __construct(CompanyInterface $company)
    {
        $this->company = $company;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $listArray = $this->company->paginate();

        return view('admin.company.index', compact('listArray'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ImageService $image)
    {
         $data = $request->all();

            $data['image_orientation'] = $image->getOrientation($request->file('featured_image'));
            // $data['video'] = $document->upload($request->file('video'), config('settings.video.company'));
            // $data['poster'] = $image->upload($request->file('poster'), config('settings.image.company.poster'));
            $data['featured_image'] = $image->upload($request->file('featured_image'), config('settings.image.company.featured'));
            $company = $this->company->create($data);

            \Notification::success('Item je uspešno kreiran');
            return redirect(route('admin.company.edit', $company->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = $this->company->find($id);
        return view('admin.company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImageService $image, $id)
    {
        try {
            $company = $this->company->find($id);
            $data = $request->all();

            $data['featured_image'] = $image->upload($request->file('featured_image'), config('settings.image.company.featured'));

            if ($data['featured_image']) {
                $imagePath = public_path().config('settings.image.company.featured.upload_dir').$data['featured_image'];
                $data['image_orientation'] = $image->getOrientation($imagePath);
                $image->delete($this->company->find($id)->featured_image, config('settings.image.company.featured'));
            }
            
            $this->company->update($id, $data);

            \Notification::success('Uspeša izmena');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
