<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\Job\JobInterface;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class JobController extends BaseController
{


    protected $job;

    public function __construct(JobInterface $job)
    {
        $this->job = $job;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listArray = $this->job->paginate();
        return view('admin.job.index', compact('listArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.job.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $job = $this->job->create($data);

            \Notification::success('Izvestaj je uspešno kreiran');
            return redirect(route('admin.job.edit', $job->id));
        } catch (\Exception $e) {
            \Notification::error('Dogodila se greška prilikom kreiranja izvestaja');
            return redirect(route('admin.job.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = $this->job->find($id);

        return view('admin.job.edit', compact('job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $data = $request->all();

            $this->job->update($id, $data);

            \Notification::success('Izvestaj je uspešno izmenjen');
        } catch (\Exception $e) {
            dd($e);
            \Notification::success('Dogodila se greška prilikom izmene izvestaja');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = $this->job->find($id);
        return $this->deleteItem($id, $this->job);
    }


    public function changeState(Request $request)
    {
        $job = $this->job->find($request->get('id'));
    
        return $this->changeStatus($this->job, $request);
    }
}
