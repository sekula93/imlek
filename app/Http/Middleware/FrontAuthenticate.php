<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class FrontAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest() || Auth::guard($guard)->user()->role != 3) {
            if ($request->ajax() || $request->wantsJson()) {
                return response(['status' => 'unauthorized'], 401);
            } else {
                return redirect()->guest(route('front.user.login'));
            }
        }

        return $next($request);
    }
}
