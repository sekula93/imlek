<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Repositories\Product\ProductRepository;

class ProductsMenuComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $product = new ProductRepository(new \App\Product());
        $view->with('productFamilies', $product->publicFamilies());
    }
}
