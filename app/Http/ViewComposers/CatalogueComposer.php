<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\StaticContent;

class CatalogueComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $content = new StaticContent();
        $catalogue = $content->getCatalogue()->value;
        $view->with(compact('catalogue'));
    }
}
