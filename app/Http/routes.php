<?php

$langSegment = Request::segment(1);
$locale = '';
if ($langSegment == 'en') {
    \App::setLocale($langSegment);
    $locale = $langSegment;
}

Route::pattern('id', '[0-9]+');
Route::pattern('post', '[0-9]+');
Route::pattern('product', '[0-9]+');
Route::pattern('nutritive', '[0-9]+');


Route::group(['middleware' => 'web'], function () use ($locale) {
    Route::group(['prefix' => $locale], function ()  {
        Route::group(['namespace' => 'Front', 'as' => 'front.'], function () {
            // Route::get('/facebook/login', ['uses' => 'PageController@fbLogin', 'as' => 'page.fb-login']);
            // Route::get('/facebook/callback', ['uses' => 'PageController@fb', 'as' => 'page.fb']);
            Route::get('/', ['uses' => 'PageController@index', 'as' => 'page.index']);
            Route::get('/pavlaka', ['uses' => 'PageController@pavlaka', 'as' => 'page.pavlaka']);
            Route::get(trans('front/interface.menu.company.url'), ['uses' => 'PageController@history', 'as' => 'page.history']);



            Route::get(trans('front/interface.menu.news.url').'/{cat?}', ['uses' => 'PageController@news', 'as' => 'page.news']);
            Route::get(trans('front/interface.menu.news.url').'/{cat}/{slug}', ['uses' => 'PageController@showPost', 'as' => 'page.show-post']);
            // Route::get('/vesti', ['uses' => 'PageController@news', 'as' => 'page.paginate-posts']);
            // Route::get('/postovi', ['uses' => 'PageController@paginateHomeGrid', 'as' => 'page.paginate-home-grid']);

            Route::get(trans('front/interface.menu.products.url'), ['uses' => 'ProductController@index', 'as' => 'product.index']);
            Route::get(trans('front/interface.menu.products.url').'/{slug}', ['uses' => 'ProductController@show', 'as' => 'product.show']);
            Route::get(trans('front/interface.menu.products.url').'/nutritive/{id}', ['uses' => 'ProductController@showNutritive', 'as' => 'product.show-nutritive']);

            Route::get(trans('front/interface.menu.recipe.url').'/{cat?}', ['uses' => 'RecipeController@index', 'as' => 'kuhinjica.index']);
            // Route::get('/kuhinjica/paginate', ['uses' => 'RecipeController@paginate', 'as' => 'kuhinjica.paginate']);
            Route::get(trans('front/interface.menu.recipe.url').'/{cat}/{slug}', ['uses' => 'RecipeController@show', 'as' => 'kuhinjica.show']);

            Route::get(trans('front/interface.menu.reports.url'), ['uses' => 'PageController@reports', 'as' => 'page.reports']);
            Route::get(trans('front/interface.menu.certificates.url'), ['uses' => 'PageController@certificates', 'as' => 'page.certificates']);
            Route::get(trans('front/interface.menu.faq.url'), ['uses' => 'PageController@faq', 'as' => 'page.faq']);
            Route::get(trans('front/interface.menu.contact.url'), ['uses' => 'PageController@showContact', 'as' => 'page.contact']);
            Route::post('/kontakt', ['uses' => 'PageController@contactUs', 'as' => 'page.contact-us']);

            Route::get('/junior', ['uses' => 'PageController@junior', 'as' => 'page.junior']);
            Route::get('/maslac', ['uses' => 'PageController@maslac', 'as' => 'page.maslac']);
            Route::get('/jogood-page', ['uses' => 'PageController@jogood', 'as' => 'page.jogood']);

            Route::get('/404', ['uses' => 'PageController@error404', 'as' => 'page.error404']);

            Route::get(trans('front/interface.menu.company.url').'/{slug}', ['uses' => 'PageController@showCompany', 'as' => 'company.show']);

            // Route::get('/fetchig/ew4we0-32_ewqnFDSfewroi3', 'SocialController@fetchInstagram');
            // Route::get('/fetchfb/ew4we0-32_ewqnFDSfewroi3', 'SocialController@fetchFacebook');
        });
    });

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

        Route::get('/login', ['uses' => 'AuthController@login', 'as' => 'admin.auth.login']);
        Route::post('/login', ['uses' => 'AuthController@postLogin', 'as' => 'admin.auth.post-login']);
        Route::get('/logout', ['uses' => 'AuthController@logout', 'as' => 'admin.auth.logout']);

        Route::group(['middleware' => 'auth'], function () {
            Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'admin.dashboard.index']);

            Route::get('/update-profile', ['uses' => 'AdministratorController@editProfile', 'as' => 'admin.administrator.edit-profile']);
            Route::put('/update-profile', ['uses' => 'AdministratorController@updateProfile', 'as' => 'admin.administrator.update-profile']);

            Route::post('/administrator/change-status', ['uses' => 'AdministratorController@changeState', 'as' => 'admin.administrator.change-status']);
            Route::resource('administrator', 'AdministratorController');



            // Route::post('/recipe-author/change-status', ['uses' => 'RecipeAuthorController@changeState', 'as' => 'admin.recipe-author.change-status']);
            // Route::resource('recipe-author', 'RecipeAuthorController');

            Route::post('/recipe/change-status', ['uses' => 'RecipeController@changeState', 'as' => 'admin.recipe.change-status']);
            Route::put('/recipe/reorder', ['uses' => 'RecipeController@reorder', 'as' => 'admin.recipe.reorder']);
            Route::resource('recipe', 'RecipeController');

            Route::post('/post/change-status', ['uses' => 'PostController@changeState', 'as' => 'admin.post.change-status']);
            Route::post('/post/slugify', ['uses' => 'PostController@slugify', 'as' => 'admin.post.slugify']);
            Route::put('/post/reorder', ['uses' => 'PostController@reorder', 'as' => 'admin.post.reorder']);
            Route::resource('post', 'PostController');

            // Route::get('/social-post/{type}', ['uses' => 'SocialPostController@index', 'as' => 'admin.social-post.index']);
            // Route::post('/social-post/change-status', ['uses' => 'SocialPostController@changeState', 'as' => 'admin.social-post.change-status']);
            // Route::delete('/social-post/{id}', ['uses' => 'SocialPostController@destroy', 'as' => 'admin.social-post.destroy']);
            Route::post('/certificate/change-status', ['uses' => 'CertificateController@changeState', 'as' => 'admin.certificate.change-status']);
            Route::put('/certificate/reorder', ['uses' => 'CertificateController@reorder', 'as' => 'admin.certificate.reorder']);
            Route::resource('certificate', 'CertificateController');


            Route::post('/post-photo/{id}', ['uses' => 'PostPhotoController@store', 'as' => 'admin.post-photo.store']);
            // Route::put('/post-photo/{id}/cover', ['uses' => 'PostPhotoController@updateCover', 'as' => 'admin.post-photo.cover']);
            Route::put('/post-photo/change-status', ['uses' => 'PostPhotoController@changeState', 'as' => 'admin.post-photo.change-status']);
            Route::put('/post-photo/reorder', ['uses' => 'PostPhotoController@reorder', 'as' => 'admin.post-photo.reorder']);
            Route::delete('/post-photo/{id}', ['uses' => 'PostPhotoController@destroy', 'as' => 'admin.post-photo.destroy']);

            Route::get('/brand', ['uses' => 'BrandController@index', 'as' => 'admin.brand.index']);
            Route::get('/brand/create', ['uses' => 'BrandController@create', 'as' => 'admin.brand.create']);
            Route::post('/brand/store', ['uses' => 'BrandController@store', 'as' => 'admin.brand.store']);
            Route::post('/brand/change-status', ['uses' => 'BrandController@changeState', 'as' => 'admin.brand.change-status']);
            Route::put('/brand/reorder', ['uses' => 'BrandController@reorder', 'as' => 'admin.brand.reorder']);
            Route::get('/brand/{brand}/edit', ['uses' => 'BrandController@edit', 'as' => 'admin.brand.edit']);
            Route::put('/brand/{brand}', ['uses' => 'BrandController@update', 'as' => 'admin.brand.update']);
            Route::delete('/brand/{brand}', ['uses' => 'BrandController@destroy', 'as' => 'admin.brand.destroy']);

            Route::post('/product/change-status', ['uses' => 'ProductController@changeState', 'as' => 'admin.product.change-status']);
            Route::put('/product/reorder', ['uses' => 'ProductController@reorder', 'as' => 'admin.product.reorder']);
            Route::post('/product/slugify', ['uses' => 'ProductController@slugify', 'as' => 'admin.product.slugify']);
            Route::resource('product', 'ProductController');

            Route::resource('product/nutritive', 'Product\NutritiveController');

            Route::put('/product/packshot/change-status', ['uses' => 'Product\PackshotController@changeState', 'as' => 'admin.product.packshot.change-status']);
            Route::post('/product/packshot/{id}', ['uses' => 'Product\PackshotController@store', 'as' => 'admin.product.packshot.store']);
            Route::put('/product/packshot/{id}', ['uses' => 'Product\PackshotController@update', 'as' => 'admin.product.packshot.update']);
            Route::put('/product/packshot/reorder', ['uses' => 'Product\PackshotController@reorder', 'as' => 'admin.product.packshot.reorder']);
            Route::delete('/product/packshot/{id}', ['uses' => 'Product\PackshotController@destroy', 'as' => 'admin.product.packshot.destroy']);
            Route::post('/packshot/slugify', ['uses' => 'Product\PackshotController@slugify', 'as' => 'admin.packshot.slugify']);

            Route::post('/package/change-status', ['uses' => 'PackageController@changeState', 'as' => 'admin.package.change-status']);
            Route::put('/package/reorder', ['uses' => 'PackageController@reorder', 'as' => 'admin.package.reorder']);
            Route::resource('package', 'PackageController');

            Route::post('/report/change-status', ['uses' => 'ReportController@changeState', 'as' => 'admin.report.change-status']);
            Route::put('/report/reorder', ['uses' => 'ReportController@reorder', 'as' => 'admin.report.reorder']);
            Route::resource('report', 'ReportController');

            Route::post('/job/change-status', ['uses' => 'JobController@changeState', 'as' => 'admin.job.change-status']);
            Route::resource('job', 'JobController');

            Route::post('/slide/change-status', ['uses' => 'SlideController@changeState', 'as' => 'admin.slide.change-status']);
            Route::put('/slide/reorder', ['uses' => 'SlideController@reorder', 'as' => 'admin.slide.reorder']);
            Route::resource('slide', 'SlideController');

            Route::post('/history/change-status', ['uses' => 'HistoryController@changeState', 'as' => 'admin.history.change-status']);
            Route::resource('history', 'HistoryController');

            Route::post('/faq/change-status', ['uses' => 'FaqController@changeState', 'as' => 'admin.faq.change-status']);
            Route::put('/faq/reorder', ['uses' => 'FaqController@reorder', 'as' => 'admin.faq.reorder']);
            Route::resource('faq', 'FaqController');

            Route::post('/recipe/change-status', ['uses' => 'RecipeController@changeState', 'as' => 'admin.recipe.change-status']);
            Route::post('/recipe/slugify', ['uses' => 'RecipeController@slugify', 'as' => 'admin.recipe.slugify']);
            Route::resource('recipe', 'RecipeController');

            Route::post('/recipe-category/change-status', ['uses' => 'RecipeCategoryController@changeState', 'as' => 'admin.recipe-category.change-status']);
            Route::resource('recipe-category', 'RecipeCategoryController');

            Route::post('/recipe-packshot/{id}', ['uses' => 'RecipePackshotController@store', 'as' => 'admin.recipe-packshot.store']);
            // Route::put('/recipe-packshot/{id}/cover', ['uses' => 'RecipePackshotController@updateCover', 'as' => 'admin.recipe-packshot.cover']);
            Route::put('/recipe-packshot/change-status', ['uses' => 'RecipePackshotController@changeState', 'as' => 'admin.recipe-packshot.change-status']);
            Route::put('/recipe-packshot/reorder', ['uses' => 'RecipePackshotController@reorder', 'as' => 'admin.recipe-packshot.reorder']);
            Route::delete('/recipe-packshot/{id}', ['uses' => 'RecipePackshotController@destroy', 'as' => 'admin.recipe-packshot.destroy']);

            Route::resource('company', 'CompanyController');

             Route::post('/company-packshot/{id}', ['uses' => 'CompanyPackshotController@store', 'as' => 'admin.company-packshot.store']);
            // Route::put('/company-packshot/{id}/cover', ['uses' => 'CompanyPackshotController@updateCover', 'as' => 'admin.company-packshot.cover']);
            Route::put('/company-packshot/change-status', ['uses' => 'CompanyPackshotController@changeState', 'as' => 'admin.company-packshot.change-status']);
            Route::put('/company-packshot/reorder', ['uses' => 'CompanyPackshotController@reorder', 'as' => 'admin.company-packshot.reorder']);
            Route::delete('/company-packshot/{id}', ['uses' => 'CompanyPackshotController@destroy', 'as' => 'admin.company-packshot.destroy']);


            // Route::get('/newsletter', ['uses' => 'NewsletterController@index', 'as' => 'admin.newsletter.index']);
            // Route::get('/newsletter/export', ['uses' => 'NewsletterController@export', 'as' => 'admin.newsletter.export']);

            // Route::get('/static/catalogue', ['uses' => 'StaticController@catalogue', 'as' => 'admin.static.catalogue']);
            // Route::put('/static/catalogue', ['uses' => 'StaticController@updateCatalogue', 'as' => 'admin.static.update-catalogue']);
            // Route::put('/static-page/{id}/edit', ['uses' => 'StaticPageController@update', 'as' => 'admin.static-page.update']);
        });
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::get('laravel-filemanager', '\Tsawler\Laravelfilemanager\controllers\LfmController@show');
        Route::post('laravel-filemanager/upload', '\Tsawler\Laravelfilemanager\controllers\UploadController@upload');
        Route::get('laravel-filemanager/jsonimages', '\Tsawler\Laravelfilemanager\controllers\ItemsController@getImages');
        Route::get('laravel-filemanager/folders', '\Tsawler\Laravelfilemanager\controllers\FolderController@getFolders');
        Route::get('laravel-filemanager/newfolder', '\Tsawler\Laravelfilemanager\controllers\FolderController@getAddfolder');
        Route::get('laravel-filemanager/crop', '\Tsawler\Laravelfilemanager\controllers\CropController@getCrop');
        Route::get('laravel-filemanager/cropimage', '\Tsawler\Laravelfilemanager\controllers\CropController@getCropImage');
        Route::get('laravel-filemanager/resize', '\Tsawler\Laravelfilemanager\controllers\ResizeController@getResize');
        Route::get('laravel-filemanager/doresize', '\Tsawler\Laravelfilemanager\controllers\ResizeController@performResize');
        Route::get('laravel-filemanager/download', '\Tsawler\Laravelfilemanager\controllers\DownloadController@getDownload');
        Route::get('laravel-filemanager/delete', '\Tsawler\Laravelfilemanager\controllers\DeleteController@getDelete');
        Route::get('laravel-filemanager/rename', '\Tsawler\Laravelfilemanager\controllers\RenameController@getRename');
    });
});
