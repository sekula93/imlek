<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $table = 'recipes';

    protected $fillable = ['category_id', 'video', 'image', 'grid_image', 'preparation_time', 'active', 'youtube', 'order', 'created_at'];

    public $timestamps = true;


    public function translations()
    {
        return $this->belongsToMany(Language::class, 'recipe_translations')
            ->withPivot('title', 'slug', 'excerpt', 'preparation', 'ingredients');
    }

    public function photos()
    {
        return $this->hasMany(RecipePackshot::class);
    }

    public function frontTranslations()
    {
        return $this->belongsToMany(Language::class, 'recipe_translations')
            ->where('language_id', languageId(\App::getLocale()))
            ->withPivot('title', 'slug', 'excerpt', 'preparation', 'ingredients');
    }
}
