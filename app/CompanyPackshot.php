<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPackshot extends Model
{
    protected $table = 'company_packshots';

    protected $fillable = ['company_id', 'filename', 'active', 'order', 'orientation'];

    public function getOrientationAttribute($value)
    {
        switch ($value) {
            case 0:
                return 'square';
                break;
            case 1:
                return 'portrait';
                break;
            case 2:
                return 'landscape';
                break;
            default:
                return '';
                break;
        }
        return ucfirst($value);
    }   

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
