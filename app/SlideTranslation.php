<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlideTranslation extends Model
{
    protected $table = 'slide_translations';

    protected $fillable = ['slide_id', 'language_id', 'title', 'subtitle', 'button_label', 'url'];

    public function slide()
    {
        return $this->belongsTo(Slide::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }
}
