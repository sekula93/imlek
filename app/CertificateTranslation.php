<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificateTranslation extends Model
{
    protected $table = 'certificate_translations';

    protected $fillable = ['certificate_id', 'language_id', 'attachment_label'];

    public $timestamps = true;

    public function certificate()
    {
        return $this->belongsTo(Certificate::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }
}
