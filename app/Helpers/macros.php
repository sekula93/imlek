<?php

/**
 * Displays icon according to 0 or 1
 * @param  integer $value
 * @param  string $size
 * @return string         formated date
 */
Html::macro('recipe_category', function ($value) {
    switch ($value) {
        case 1:
            $category = 'Video recepti';
            break;
        case 2:
            $category = 'Trikovi';
            break;
        case 3:
            $category = '';
            break;
        default:
            $category = '-';
            break;
    }

    return $category;
});
