<?php

if (!function_exists('clean_filename')) {

    /**
     * Cleaning file names from non alphabet characters
     * @param  string $date   date from database
     * @param  string $format format type
     * @return string         formated date
     */
    function clean_filename($string)
    {
        // Remove special accented characters - ie. sí.
        $clean_name = strtr($string, array('Š' => 'S','Ž' => 'Z','š' => 's','ž' => 'z','Ÿ' => 'Y','À' => 'A','Á' => 'A','Â' => 'A','Ã' => 'A','Ä' => 'A','Å' => 'A','Ç' => 'C','È' => 'E','É' => 'E','Ê' => 'E','Ë' => 'E','Ì' => 'I','Í' => 'I','Î' => 'I','Ï' => 'I','Ñ' => 'N','Ò' => 'O','Ó' => 'O','Ô' => 'O','Õ' => 'O','Ö' => 'O','Ø' => 'O','Ù' => 'U','Ú' => 'U','Û' => 'U','Ü' => 'U','Ý' => 'Y','à' => 'a','á' => 'a','â' => 'a','ã' => 'a','ä' => 'a','å' => 'a','ç' => 'c','è' => 'e','é' => 'e','ê' => 'e','ë' => 'e','ì' => 'i','í' => 'i','î' => 'i','ï' => 'i','ñ' => 'n','ò' => 'o','ó' => 'o','ô' => 'o','õ' => 'o','ö' => 'o','ø' => 'o','ù' => 'u','ú' => 'u','û' => 'u','ü' => 'u','ý' => 'y','ÿ' => 'y'));
        $clean_name = strtr($clean_name, array('Þ' => 'TH', 'þ' => 'th', 'Ð' => 'Dj', 'ð' => 'dj','đ' => 'dj', 'ß' => 'ss', 'Œ' => 'OE', 'œ' => 'oe', 'Æ' => 'AE', 'æ' => 'ae', 'µ' => 'u'));

        $clean_name = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), $clean_name);
        return $clean_name;
    }
}

if (!function_exists('cache_buster')) {
    /**
     * Get last commit hash for cache busting.
     *
     * @return string
     */
    function cache_buster()
    {
        $hash = '';
        $file = getcwd() . '/.git-ftp.log';

        if (file_exists($file)) {
            $content = trim(file_get_contents($file));

            if (!empty($content)) {
                $hash = '?' . $content;
            }
        }

        return $hash;
    }
}

if (!function_exists('category_slug')) {
    /**
     * Generate slug according to category ID
     * @param  int $value
     * @return string
     */
    function category_slug($value)
    {
        switch ($value) {
            case 1:
                $category = 'novosti';
                break;
            case 2:
                $category = 'blog';
                break;
            case 3:
                $category = 'fun-zona';
                break;
            case 4:
                $category = 'muzika';
                break;

            default:
                $category = '-';
                break;
        }

        return $category;
    }
}


if (!function_exists('eov')) {
    /**
     * eov - empty or value
     * @param  boolean $isEmpty
     * @param  string $value
     * @return string
     */
    function eov($value)
    {
        die('dd');
        return isset($value) ? '' : $value;
    }

}
