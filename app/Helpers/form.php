<?php

if (!function_exists('ml_tab_input')) {
    function ml_tab_input($label, $type, $languages, $name, $errorsObj, $value = null, $fieldClass = "")
    {
        $tabs = $fields = $inputErrors = "";
        foreach ($languages as $index => $language) {

            $activeClass = $language['id'] == 1 ? 'active' : '';
            $arrayFieldName = "trans[$language->id][$name]";
            $dotFieldName = "trans.$language->id.$name";

            $fieldValue = null;
            if ($value && $value->translations->find($language->id)) {
                $fieldValue = $value->translations->find($language->id)->pivot->{$name};
            }

            if ($type == 'text') {
                $input = sprintf('<input type="text" class="form-control %s" name="%s" value="%s">', $fieldClass, $arrayFieldName, old($dotFieldName, $fieldValue));
            } else {
                $input = sprintf('<textarea class="form-control %s" rows="5" name="%s">%s</textarea>', $fieldClass, $arrayFieldName, old($dotFieldName, $fieldValue));
            }
            $tabs .= sprintf('<li class="%s"><a href="#tab-desc-%s" data-toggle="tab" tabindex="-1">%s</a></li>', $activeClass, $language->id, $language->name);
            $fields .= sprintf('<div class="tab-pane %s error_class" id="tab-desc-%s"><label class="control-class">%s</label>%s</div>', $activeClass, $language['id'], $label, $input);
            if ($errorsObj->first($dotFieldName)) {
                $inputErrors .= sprintf('<p class="help-block error"><i class="fa fa-times-circle"></i> %s - %s</p>', $language->name, $errorsObj->first($dotFieldName));
            }
        }
        return sprintf('<div class="tab-container"><ul class="nav nav-tabs lang-tab"> %s </ul> <div class="tab-content"> %s </div></div>%s', $tabs, $fields, $inputErrors);
    }
}

if (!function_exists('ml_input')) {
    function ml_input($type, $languages, $name, $errorsObj, $value = null, $fieldClass = "")
    {
        $fields = $inputErrors = "";
        foreach ($languages as $index => $language) {
            $activeClass = $language['id'] == 1 ? 'active' : '';
            $arrayFieldName = "trans[$language->id][$name]";
            $dotFieldName = "trans.$language->id.$name";

            $fieldValue = null;
            if ($value && $value->translations->find($language->id)) {
                $fieldValue = $value->translations->find($language->id)->pivot->{$name};
            }

            if ($type == 'text') {
                $input = sprintf('<input type="text" class="form-control %s" name="%s" value="%s">', $fieldClass, $arrayFieldName, old($dotFieldName, $fieldValue));
            } else {
                $input = sprintf('<textarea class="form-control %s" rows="5" name="%s">%s</textarea>', $fieldClass, $arrayFieldName, old($dotFieldName, $fieldValue));
            }
            $fields .= sprintf('<li class="lang-input %s">%s<span class="lang-input-switch">%s</span></li>', $activeClass, $input, strtoupper($language->code));
            if ($errorsObj->first($dotFieldName)) {
                $inputErrors .= sprintf('<p class="help-block error"><i class="fa fa-times-circle"></i> %s - %s</p>', $language->name, $errorsObj->first($dotFieldName));
            }
        }
        return sprintf('<ul class="lang-input-wrap"> %s </ul>%s', $fields, $inputErrors);
    }
}

if (!function_exists('jasnyupload')) {
    function jasnyupload($name, $errorsObj, $accept = null, $currentFile = null, $type = 1)
    {
        $inputErrors = '';
        $fileinputClass = $currentFile ? 'fileinput-exists' : 'fileinput-new';
        if ($type == 2) {
            $mediaTag = '<video src="'.$currentFile.'" class="img-responsive" controls></video>';
        } else if ($type == 3) {
            $pathArray = explode('/', $currentFile);
            $mediaTag =  end($pathArray);
        } else {
            $mediaTag = '<img src="'.$currentFile.'" class="img-responsive">';
        }

        if ($errorsObj->first($name)) {
            $inputErrors .= sprintf('<p class="help-block error"><i class="fa fa-times-circle"></i>%s</p>', $errorsObj->first($name));
        }

        return sprintf('<div class="fileinput %s" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 300px"></div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px;max-height:300px">
                    %s
                    </div>
                    <div>
                        <span class="btn btn-default btn-file btn-full-width"><span class="fileinput-new">Odaberi fajl</span>
                        <span class="fileinput-exists">Izmeni</span><input type="file" name="%s" accept="%s"></span>
                    </div>
                </div> %s', $fileinputClass, $mediaTag, $name, $accept, $inputErrors);
    }
}
