<?php

if (!function_exists('active_menu')) {

    /**
     * Formating mysql date to predefined format
     * @param  string $date   date from database
     * @param  string $format format type
     * @return string         formated date
     */
    function active_menu($route, $parent = false, $boolean = false)
    {
        $routeSegments = explode('.', $route);
        $curenRouteSegments = explode('.', Route::current()->getName());
        // var_dump(Route::current()); die();
        $outputClass = ($boolean) ? true : ($parent) ? ' class="active hasChild"' : ' class="active"';

        // var_dump($routeSegments, $curenRouteSegments, $outputClass,
            // ($route == implode('.', array($curenRouteSegments[0], $curenRouteSegments[1]))));

        if (count($routeSegments) == 2) {
            if ($route == implode('.', array($curenRouteSegments[0], $curenRouteSegments[1]))) {
                return $outputClass;
            }
        } else {
            if ($route == Route::current()->getName()) {
                return $outputClass;
            }
        }

        return '';
    }
}

if (!function_exists('error_class')) {

    /**
     * Formating mysql date to predefined format
     * @param  string $date   date from database
     * @param  string $format format type
     * @return string         formated date
     */
    function error_class($errors, $field)
    {
        return $errors->first($field) ? 'has-error' : '';
    }
}


if (!function_exists('validation_error')) {

    /**
     * Formating mysql date to predefined format
     * @param  string $date   date from database
     * @param  string $format format type
     * @return string         formated date
     */
    function validation_error($errors, $field)
    {
        return $errors->first($field) ? '<p class="help-block"><i class="fa fa-times-circle"></i> '.$errors->first($field).'</p>' : '';
    }
}

if (!function_exists('show_status')) {
    /**
     * Showing status icon according to input parameter
     * @param  string $status
     * @return string           formated html
     */
    function show_status($status)
    {
        if ($status) {
            return '<button class="btn btn-mini btn-warning" disabled><i class="icon-star"></i></button>';
        } else {
            return '<button class="btn btn-mini" disabled><i class="icon-star-empty"></i></button>';
        }
    }
}

if (!function_exists('profile_photo_status')) {
    /**
     * Returns string with photo status
     * @param  string $status
     * @return string
     */
    function profile_photo_status($status)
    {
        switch ($status) {
            case 1:
                $output = 'Pending';
                break;
            case 2:
                $output = 'Rejected';
                break;
            case 3:
                $output = 'Approved';
                break;

            default:
                $output = 'Not uploaded';
                break;
        }

        return '<span class="status-'.str_replace(' ', '-', strtolower($output)).'">'.$output.'</span>';
    }
}

if (!function_exists('format_date')) {

    /**
    * Formating mysql date to predefined format
    * @param  string $date   date from database
    * @param  string $format format type
    * @return string         formated date
    */
    function format_date($date, $format = 'sr-date')
    {
        switch ($format) {
            case 'sr-datetime':
                $strFormat = 'd.m.Y. H:i:s';
                break;
            case 'sr-date':
            default:
                $strFormat = 'd.m.Y.';
                break;
        }
        if (strtotime($date) > 0) {
            return date($strFormat, strtotime($date));
        } else {
            return '-';
        }
    }
}

if (!function_exists('languageId')) {

    function languageId($langCode)
    {
        if ($langCode == 'sr') {
            return 1;
        } else {
            return 2;
        } 

    }

}
