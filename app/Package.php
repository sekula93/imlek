<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'packages';

    protected $fillable = ['featured_image', 'web_address', 'order', 'active', 'image_orientation'];

    public $timestamps = true;

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'package_translations')
            ->withPivot('title', 'text', 'link');
    }

}
