<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackshotTranslation extends Model
{
    protected $table = 'packshot_translations';

    protected $fillable = ['packshot_id', 'language_id', 'label', 'text', 'slug'];

    public $timestamps = true;

    public function packshot()
    {
        return $this->belongsTo(Packshot::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }
}
