<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
	protected $table = 'reports';

    protected $fillable = ['attachment', 'active', 'order', 'created_at'];

	public $timestamps = true;

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'report_translations')
            ->withPivot('attachment_label');
    }
}
