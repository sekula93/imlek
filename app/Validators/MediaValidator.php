<?php namespace App\Validators;

class MediaValidator extends \Illuminate\Validation\Validator {

    public function validateMedia($attribute, $value, $parameters)
    {
        // var_dump($attribute, $value, $parameters); die();
        $mimeTypes = ['video/quicktime','video/mp4', 'video/mpeg', 'video/x-msvideo', 'video/x-ms-wmv', 'video/x-ms-asf', 'image/jpeg', 'application/ogg', 'image/png'];
        if(in_array($value->getMimeType(), $mimeTypes)) {
            return true;
        } else {
            return false;
        }
    }

}