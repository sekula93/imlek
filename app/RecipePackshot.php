<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipePackshot extends Model
{
    protected $table = 'recipe_packshots';

    protected $fillable = ['recipe_id', 'filename', 'active', 'order', 'orientation'];

    public function getOrientationAttribute($value)
    {
        switch ($value) {
            case 0:
                return 'square';
                break;
            case 1:
                return 'portrait';
                break;
            case 2:
                return 'landscape';
                break;
            default:
                return '';
                break;
        }
        return ucfirst($value);
    }   

    public function recipe()
    {
        return $this->belongsTo(Recipe::class);
    }
}
