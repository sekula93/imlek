<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nutritive extends Model
{
    protected $table = 'nutritives';

    protected $fillable = ['name'];

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'nutritive_translations')
            ->withPivot('table', 'legend');
    }
}
