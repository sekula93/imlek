<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'languages';

    protected $fillable = ['name', 'code', 'active'];

    public $timestamps = false;

    public function slides()
    {
        return $this->belongsToMany(Slide::class, 'slide_translations')
            ->withPivot('title', 'subtitle', 'button_label', 'url');
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_translations')
            ->withPivot('title', 'slug', 'excerpt', 'text', 'attachment_label');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_translations')
            ->withPivot('title', 'slug', 'text', 'excerpt');
    }

    public function history()
    {
        return $this->belongsToMany(History::class, 'history_translations')
            ->withPivot('title', 'text');
    }

    public function faq()
    {
        return $this->belongsToMany(Faq::class, 'faq_translations')
            ->withPivot('title', 'text');
    }

    public function nutritives()
    {
        return $this->belongsToMany(Nutritive::class, 'nutritive_translations')
            ->withPivot('table', 'legend');
    }

    public function packshots()
    {
        return $this->belongsToMany(Packshot::class, 'packshot_translations')
            ->withPivot('label', 'text');
    }
}
