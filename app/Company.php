<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    protected $fillable = ['featured_image', 'video', 'poster', 'web_address', 'order', 'active', 'image_orientation'];

    public $timestamps = true;

    public function translations()
    {
        return $this->belongsToMany(Language::class, 'company_translations')
            ->withPivot('title', 'text', 'slug', 'excerpt');
    }

    public function photos()
    {
        return $this->hasMany(CompanyPackshot::class);
    }
}
