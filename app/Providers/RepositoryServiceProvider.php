<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register all application repositories.
     **
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\User\UserInterface', 'App\Repositories\User\UserRepository');
        $this->app->bind('App\Repositories\UserToken\UserTokenInterface', 'App\Repositories\UserToken\UserTokenRepository');
        $this->app->bind('App\Repositories\Post\PostInterface', 'App\Repositories\Post\PostRepository');
        $this->app->bind('App\Repositories\PostPhoto\PostPhotoInterface', 'App\Repositories\PostPhoto\PostPhotoRepository');
        $this->app->bind('App\Repositories\Product\ProductInterface', 'App\Repositories\Product\ProductRepository');
        $this->app->bind('App\Repositories\Product\NutritiveInterface', 'App\Repositories\Product\NutritiveRepository');
        $this->app->bind('App\Repositories\Product\PackshotInterface', 'App\Repositories\Product\PackshotRepository');
        $this->app->bind('App\Repositories\Recipe\RecipeInterface', 'App\Repositories\Recipe\RecipeRepository');
        $this->app->bind('App\Repositories\RecipeCategory\RecipeCategoryInterface', 'App\Repositories\RecipeCategory\RecipeCategoryRepository');
        $this->app->bind('App\Repositories\Slide\SlideInterface', 'App\Repositories\Slide\SlideRepository');
        $this->app->bind('App\Repositories\History\HistoryInterface', 'App\Repositories\History\HistoryRepository');
        $this->app->bind('App\Repositories\StaticPage\StaticPageInterface', 'App\Repositories\StaticPage\StaticPageRepository');
        $this->app->bind('App\Repositories\Brand\BrandInterface', 'App\Repositories\Brand\BrandRepository');
        $this->app->bind('App\Repositories\Faq\FaqInterface', 'App\Repositories\Faq\FaqRepository');
        $this->app->bind('App\Repositories\Report\ReportInterface', 'App\Repositories\Report\ReportRepository');
        $this->app->bind('App\Repositories\Job\JobInterface', 'App\Repositories\Job\JobRepository');
        $this->app->bind('App\Repositories\Package\PackageInterface', 'App\Repositories\Package\PackageRepository');
        $this->app->bind('App\Repositories\PostCategory\PostCategoryInterface', 'App\Repositories\PostCategory\PostCategoryRepository');
        $this->app->bind('App\Repositories\RecipePackshot\RecipePackshotInterface', 'App\Repositories\RecipePackshot\RecipePackshotRepository');
        $this->app->bind('App\Repositories\Certificate\CertificateInterface', 'App\Repositories\Certificate\CertificateRepository');
        $this->app->bind('App\Repositories\Company\CompanyInterface', 'App\Repositories\Company\CompanyRepository');
        $this->app->bind('App\Repositories\CompanyPackshot\CompanyPackshotInterface', 'App\Repositories\CompanyPackshot\CompanyPackshotRepository');
    }
}
