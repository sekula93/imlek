<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        \View::composer('admin.*', 'App\Http\ViewComposers\AppLanguagesComposer');
        \View::composer('front.layout._partials.footer', 'App\Http\ViewComposers\ProductsMenuComposer');
        \View::composer('front.layout._partials.footer', 'App\Http\ViewComposers\CatalogueComposer');
        // \View::composer('layout._partials.top-menu', 'App\Http\ViewComposers\ProductsMenuComposer');
        // \View::composer('user._partials.profile-menu', 'App\Http\ViewComposers\UserMenuComposer');
        // \View::composer('front.*', 'App\Http\ViewComposers\SelectedProfileComposer');

        // \View::composer('front.errors.404', 'App\Http\ViewComposers\FundsStructureComposer');
        // \View::composer('front.funds.*', 'App\Http\ViewComposers\FundsStructureComposer');

    }

    /**
     * Register all application repositories.
     **
     * @return void
     */
    public function register()
    {
        //
    }
}
