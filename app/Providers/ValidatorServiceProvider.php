<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Validators\MediaValidator;

class ValidatorServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		 \Validator::resolver(function($translator, $data, $rules, $messages) {
	        return new MediaValidator($translator, $data, $rules, $messages);
	    });
	}

	/**
	 * Register all application repositories.
	 **
	 * @return void
	 */
	public function register()
	{
	}

}
