<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // App\InstagramPost::class => App\Policies\InstagramPostPolicy::class,
        // App\User::class => App\Policies\UserPolicy::class
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);


        $gate->before(function ($user) {
            if ($user->isSuperAdmin()) {
                return true;
            }
        });

        $gate->define('access', function ($user) {
            return $user->role === 0;
        });

        $gate->define('update-profile', function ($user, $id) {
            return true;
            // dd($user, $id);
            // return $user->id === (int)$id;
        });

        $gate->define('update-post', function ($user, $post) {
            return $user->id === $post->user_id;
        });
    }
}
