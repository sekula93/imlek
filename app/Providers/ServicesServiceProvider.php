<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ServicesServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register all application repositories.
	 **
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('App\Services\File\DocumentInterface', 'App\Services\File\Document');
		// $this->app->bind('App\Services\File\ImageInterface', 'App\Services\File\Image');
	}

}
