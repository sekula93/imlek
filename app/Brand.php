<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    protected $fillable = ['name', 'order', 'active', 'featured_image'];
    
    public function products()
    {
        return $this->hasMany(Product::class)->orderBy('order');
    }
}
