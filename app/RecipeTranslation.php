<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeTranslation extends Model
{
    protected $table = "recipe_translation";

    protected $guard = [];

    public $timestamps = false;

    public function recipe()
    {
        return $this->belongsTo(Recipe::class);
    }
}
