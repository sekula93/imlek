var gulp = require('gulp');
    sass = require('gulp-sass');
    minifyCSS = require('gulp-minify-css');
    autoprefixer = require('gulp-autoprefixer');
    svgmin = require('gulp-svgmin');
    svgstore = require('gulp-svgstore');
    rename = require("gulp-rename");
    concat = require('gulp-concat');
    uglify = require('gulp-uglify');
    pump = require('pump');
    webserver = require('gulp-webserver');

gulp.task('sass', function() {
    gulp.src('./src/scss/*.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest('./src/css'))
        .pipe(minifyCSS({
            keepBreaks: false
        }))
        .pipe(gulp.dest('./css/'));
});

gulp.task('default', ['sass']);

gulp.task('javascript', function() {
    return gulp.src('./src/js/*.js')
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js/'));
});

gulp.task('svgstore', function () {
    return gulp
        .src('./images/svg/*.svg')
        .pipe(svgmin())
        .pipe(svgstore())
        .pipe(rename({basename: 'sprite'}))
        .pipe(gulp.dest('./images/svg/store'));
});

gulp.task('watch', function() {
    gulp.watch('./src/scss/**/*.scss', ['sass']);
    gulp.watch('./src/js/*.js', ['javascript']);
    gulp.watch('./images/svg/*.svg', ['svgstore']);
});

gulp.task('webserver', function() {
  gulp.src('./../../public_html/')
    .pipe(webserver({
      livereload: true,
      open: true
    }));
});