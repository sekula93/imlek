var frizhiderParams;
var frizhiderSvg;

var astalParams;
var astalSvg;

var originalNamazParams;
var originalNamazSvg;

var slaniNamazParams;
var slaniNamazSvg;

var smearBtnParams;
var smearBtnSvg;

var animatedFrizhider = false;
var animatedAstal = false;
var animatedNamaz = false;

var currentPackshot = 'original';
var packshotTimeout;

function frizhiderAnimacija()
{
	frizhiderParams = {
		container: document.getElementById('frizhider'),
		renderer: 'svg',
		loop: false,
		autoplay: false,
		animationData: frizhiderAnimData
	};

	frizhiderSvg = bodymovin.loadAnimation(frizhiderParams);
}

function astalAnimacija()
{
	astalParams = {
		container: document.getElementById('astal'),
		renderer: 'svg',
		loop: false,
		autoplay: false,
		animationData: astalAnimData
	};

	astalSvg = bodymovin.loadAnimation(astalParams);
}

function originalNamazAnimacija()
{
	originalNamazParams = {
		container: document.getElementById('originalNamaz'),
		renderer: 'svg',
		loop: false,
		autoplay: false,
		animationData: originalNamazAnimData
	};

	originalNamazSvg = bodymovin.loadAnimation(originalNamazParams);
}

function slaniNamazAnimacija()
{
	slaniNamazParams = {
		container: document.getElementById('slaniNamaz'),
		renderer: 'svg',
		loop: false,
		autoplay: false,
		animationData: slaniNamazAnimData
	};

	slaniNamazSvg = bodymovin.loadAnimation(slaniNamazParams);
}

function smearBtnAnimacija()
{
	smearBtnParams = {
		container: document.getElementById('smearBtn'),
		renderer: 'svg',
		loop: false,
		autoplay: false,
		animationData: smearBtnAnimData
	};

	smearBtnSvg = bodymovin.loadAnimation(smearBtnParams);
}

function positionSmear()
{
	var posY;
	var height;
		
	posY = $('#packshots').position().top + $('#packshots').height() / 2;
	if(window.innerWidth >= mobileBreakPoint)
	{
		height = $('#packshots').outerHeight() / 2 + $('#frizhider-anim').outerHeight() - $('#frizhider-anim').outerHeight() / 5;
	}
	else
	{
		height = $('#packshots').outerHeight() / 2 + $('#frizhider-anim').outerHeight() - $('#frizhider-anim').outerHeight() / 5;
	}

	$('.smear-parallax').css('top', posY + 'px');
	$('.smear-parallax').css('height', height + 'px');
}

$(document).on('click', '.maslac-arrows', function(){
	if(window.innerWidth >= mobileBreakPoint)
	{
		changeNamaz();
	}
});

$(document).on('touchstart', '.maslac-arrows', function(){
	if(window.innerWidth < mobileBreakPoint)
	{
		changeNamaz();
	}
});

function changeNamaz()
{
	if(packshotTimeout)
	{
		clearTimeout(packshotTimeout);
		packshotTimeout = null;
	}
	if(currentPackshot == 'original')
	{	
		originalNamazSvg.setDirection(-1);
		originalNamazSvg.goToAndPlay(10, true);

		packshotTimeout = setTimeout(function(){
			$('#originalNamaz').css('display','none');
			if(window.innerWidth < mobileBreakPoint)
			{
				$('#slaniNamaz').css('display','block');
			}
			else
			{
				$('#slaniNamaz').css('display','flex');
			}
			$('.namaz-name').text('SLANI');
			$('.namaz-name').css('color', '#d90d15');
			$('#smearBtn').attr('href','https://www.imlek.rs/proizvodi/maslac#moja-kravica-namaz-od-maslaca-slani-200g');
			slaniNamazSvg.setDirection(1);
			slaniNamazSvg.goToAndPlay(1, true);
		}, 250);
		currentPackshot = 'slani';
	}
	else
	{
		slaniNamazSvg.setDirection(-1);
		slaniNamazSvg.goToAndPlay(10, true);

		packshotTimeout = setTimeout(function(){
			$('#slaniNamaz').css('display','none');
			if(window.innerWidth < mobileBreakPoint)
			{
				$('#originalNamaz').css('display','block');
			}
			else
			{
				$('#originalNamaz').css('display','flex');
			}
			$('.namaz-name').text('ORIGINAL');
			$('.namaz-name').css('color', '#2e9dd9');
			$('#smearBtn').attr('href','https://www.imlek.rs/proizvodi/maslac#moja-kravica-namaz-od-maslaca-original-200g');
			originalNamazSvg.setDirection(1);
			originalNamazSvg.goToAndPlay(1, true);
		}, 250);
		currentPackshot = 'original';
	}
}

$(document).on('mouseenter', '#smearBtn', function(){
	if(window.innerWidth >= mobileBreakPoint)
	{
		smearBtnSvg.setDirection(1);
		smearBtnSvg.goToAndPlay(1, true);
	}
});

$(document).on('mouseleave', '#smearBtn', function(){
	if(window.innerWidth >= mobileBreakPoint)
	{
		smearBtnSvg.setDirection(-1);
		smearBtnSvg.goToAndPlay(20, true);
	}
});

$(document).on('touchstart', '#smearBtn', function(){
	if(window.innerWidth < mobileBreakPoint)
	{
		smearBtnSvg.setDirection(1);
		smearBtnSvg.goToAndPlay(1, true);
	}
});

$(document).on('touchend', '#smearBtn', function(){
	if(window.innerWidth < mobileBreakPoint)
	{
		smearBtnSvg.setDirection(-1);
		smearBtnSvg.goToAndPlay(20, true);
	}
});

// $( window ).scroll(function() {
// 	if(window.innerWidth < mobileBreakPoint && $('#maslac-page')[0] != undefined)
// 	{
// 		if( $( '#packshots' ).offset().top - window.innerWidth / 2 <= $( "html" ).scrollTop() && !animatedAstal)
// 		{
// 			animatedAstal = true;
// 			astalSvg.goToAndPlay(1, true);
// 		}
// 		if( $( '#packshots' ).offset().top - window.innerWidth / 2 <= $( "html" ).scrollTop() && !animatedNamaz)
// 		{
// 			animatedNamaz = true;
// 			setTimeout(function(){
// 				originalNamazSvg.goToAndPlay(1, true);
// 			},1000);
// 		}
// 		if($( '#frizhider-anim' ).offset().top - window.innerWidth / 2 <= $( "html" ).scrollTop() && !animatedFrizhider)
// 		{
// 			animatedFrizhider = true;
// 			frizhiderSvg.goToAndPlay(1, true);
// 			$('.frizhider-text').addClass('active');
// 		}
// 	}
// });

$(document.body).on({
    'touchmove': function(e) { 
        mobileScroll();
    }
});

$(window).on(
    'scroll', function(e) { 
        mobileScroll();
    }
);

function mobileScroll()
{
	if(window.innerWidth < mobileBreakPoint && $('#maslac-page')[0] != undefined)
	{
		if( $( '#packshots' ).offset().top + $( '#packshots' ).height() + 50 <= window.pageYOffset + window.innerHeight && !animatedAstal)
		{
			animatedAstal = true;
			astalSvg.goToAndPlay(1, true);
		}
		if( $( '#packshots' ).offset().top + $( '#packshots' ).height() + 50 <= window.pageYOffset + window.innerHeight && !animatedNamaz)
		{
			animatedNamaz = true;
			setTimeout(function(){
				originalNamazSvg.goToAndPlay(1, true);
			},1000);
		}
		if($( '#frizhider-anim' ).offset().top + $( '#frizhider-anim' ).height() + 50 <= window.pageYOffset + window.innerHeight && !animatedFrizhider)
		{
			animatedFrizhider = true;
			$('#frizhider').css('visibility','visible');
			frizhiderSvg.goToAndPlay(1, true);
			$('.frizhider-text').addClass('active');
		}
	}
}