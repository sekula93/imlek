var currentJelo = 1;
var prevJelo;

function positionPavlakaParallax()
{
	var posY;
	var height;
		
	if(window.innerWidth >= mobileBreakPoint)
	{
		posY = $('#jela').position().top + $('#jela').height() / 2 + 80 + scrollbar.scrollTop - $('.jela-text').height() / 2;
		height = $('#jela').height() / 2 + $('#video').outerHeight() / 2;
	}
	else
	{
		posY = $('#jela').position().top + $('#jela .wrap').height() / 2;
		height = $('#jela .wrap').height() / 2 + $('#video').outerHeight();
	}

	$('.pavlaka-parallax').css('top', posY + 'px');
	$('.pavlaka-parallax').css('height', height + 'px');
}

$(document).on('mousemove', '.jela-slider .poklopac', function(e) {
	if(checkPoklopacCircleRadius($(this).parent(), e.clientX, e.clientY) <= 43)
	{
		if(!$(this).hasClass('hover'))
		{
			$(this).addClass('hover');
		}
	}
	else
	{
		$(this).removeClass('hover');
	}
});

$(document).on('touchstart', '.jela-slider .poklopac', function(e) {
	if(checkPoklopacCircleRadius($(this).parent(), e.originalEvent.touches[0].pageX, e.originalEvent.touches[0].pageY) <= 43)
	{
		if(!$(this).hasClass('hover'))
		{
			$(this).addClass('hover');
		}
	}
	else
	{
		$(this).removeClass('hover');
	}
});
$(document).on('touchend', '.jela-slider .poklopac', function(e) {
	if($(this).hasClass('hover'))
	{
		$(this).removeClass('hover');
	}
});

$(document).on('mouseleave', '.jela-slider .poklopac', function() {
	if($(this).hasClass('hover'))
	{
		$(this).removeClass('hover');
	}
});

$('#jela .prev-arrow').on('click', function() {
	prevJelo = currentJelo;

	currentJelo--;

	if(currentJelo < 1)
		currentJelo = 3;

	$('.jela-sa-pavlakom .jelo00' + prevJelo).css('opacity', '0');
	$('.jela-sa-pavlakom .jelo00' + currentJelo).css('opacity', '1');
});

$('#jela .next-arrow').on('click', function() {
	prevJelo = currentJelo;

	currentJelo++;

	if(currentJelo > 3)
		currentJelo = 1;

	$('.jela-sa-pavlakom .jelo00' + prevJelo).css('opacity', '0');
	$('.jela-sa-pavlakom .jelo00' + currentJelo).css('opacity', '1');
});

function checkPoklopacCircleRadius(_this, _mouseX, _mouseY)
{
	var circleWidth		= _this.outerWidth( true );
	var circleHeight	= _this.outerHeight( true );
	var circleLeft		= _this.offset().left;
	var circleTop		= _this.offset().top;
	var circlePos		= {
		x		: circleLeft + circleWidth / 2,
		y		: circleTop + circleHeight / 2,
		radius: circleWidth / 2
	};

	var distance = Math.sqrt( Math.pow( _mouseX - circlePos.x, 2 ) + Math.pow( _mouseY - circlePos.y, 2 ) );
	var distancePercent = Math.round((distance / circleWidth) * 100);
	
	return distancePercent;
}

// $(document).ready(function() {
Barba.Dispatcher.on('transitionCompleted', function(currentStatus, prevStatus) {
	if($('#pavlaka-page')[0] != undefined)
	{
		// $('.pavlaka-sku-slider').slick({
		// 	prevArrow:$('.pavlaka-sku-prev'),
		// 	nextArrow:$('.pavlaka-sku-next'),
		// 	centerMode: true,
		// 	centerPadding: '0px',
		// 	slidesToShow: 1
		// });

		svg4everybody();

		$('.pavlaka-sku-slider').slick({
			slidesToShow: 4,
			prevArrow:$('.pavlaka-sku-prev'),
			nextArrow:$('.pavlaka-sku-next'),
			responsive: [
				{
					breakpoint: mobileBreakPoint,
					settings: {
						prevArrow:$('.pavlaka-sku-prev'),
						nextArrow:$('.pavlaka-sku-next'),
						slidesToShow: 1,
						centerMode: true,
						centerPadding: '0px'
					}
				}
			]
		});

		if(window.innerWidth >= mobileBreakPoint)
		{
			setTimeout(function() {
				brokenHiddenText('.hidden-text');
			}, 1500);
		}

		resizePavlakaVideo();
	}
	else
	{
		if($('body').hasClass('pavlaka-bg'));
		{
			$('body').removeClass('pavlaka-bg');
		}
	}
});

$('.pavlaka-sku-slider').on('afterChange', function(event, slick, currentSlide){
	var currentHref = $( ".pavlaka-sku-slider .slick-list .slick-track .item[data-slick-index="+(3-currentSlide)+"]" ).attr('href');
	$('.call-to-action--mobile-btn').attr('href', currentHref);

	console.log(3-currentSlide);
});

// $(document).on('click', '.pavlaka-sku-slider .slick-list .slick-track .item', function(e) {
// 	// e.preventDefault();
// 	console.log('!!!', e);
// });

$(window).resize(function() {
	if($('#pavlaka-page')[0] != undefined)
	{
		resizePavlakaVideo();
	}
});

function resizePavlakaVideo()
{
	var newHeight = Math.ceil($('#pavlaka-video').width() * 0.562);
	$('#pavlaka-video').css('height', newHeight + 'px');
	positionPavlakaParallax();
}