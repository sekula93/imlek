
$(document).ready(function() {
	/*
		SMOOTH SCROLLER
	*/

	if(window.innerWidth >= mobileBreakPoint)
	{
		scrollbar = Scrollbar.init($('scrollbar')[0], { speed: 0.5, syncCallbacks: true });

		scrollbar.addListener(scrollHandler);
	}

	if(window.innerWidth > window.innerHeight)
	{
		$('#navigation-mobile').addClass('landscape');
	}
	else
	{
		$('#navigation-mobile').removeClass('landscape');
	}

	barbaOrRefresh = 'refresh';

	var randomVar;

	/*
		BARBAJS
	*/
	Barba.Pjax.start();

	var animationData = {"v":"4.11.1","fr":30,"ip":0,"op":100,"w":600,"h":600,"nm":"loader lines","ddd":0,"assets":[],"layers":[{"ddd":0,"ind":1,"ty":1,"nm":"mask 2","td":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[300,300,0],"ix":2},"a":{"a":0,"k":[960,540,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"hasMask":true,"masksProperties":[{"inv":false,"mode":"a","pt":{"a":0,"k":{"i":[[17,136.864],[0.52,-4.131],[0,-95.121],[-58.475,0],[0.009,58.773]],"o":[[-0.505,-4.131],[-17,136.864],[0,58.758],[58.475,0],[-0.015,-95.121]],"v":[[964.073,370.884],[956.256,370.884],[852.338,631.058],[960.164,737.473],[1067.991,631.058]],"c":true},"ix":1},"o":{"a":0,"k":100,"ix":3},"x":{"a":0,"k":0,"ix":4},"nm":"Mask 1"}],"sw":1920,"sh":1080,"sc":"#000000","ip":24,"op":100,"st":30,"bm":0},{"ddd":0,"ind":2,"ty":1,"nm":"line 2","tt":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[300,300,0],"ix":2},"a":{"a":0,"k":[960,540,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"hasMask":true,"masksProperties":[{"inv":false,"mode":"f","pt":{"a":0,"k":{"i":[[17,136.864],[0.52,-4.131],[0,-95.121],[-58.475,0],[0.009,58.773]],"o":[[-0.505,-4.131],[-17,136.864],[0,58.758],[58.475,0],[-0.015,-95.121]],"v":[[964.073,370.884],[956.256,370.884],[852.338,631.058],[960.164,737.473],[1067.991,631.058]],"c":true},"ix":1},"o":{"a":0,"k":100,"ix":3},"x":{"a":0,"k":0,"ix":4},"nm":"Mask 1"}],"ef":[{"ty":22,"nm":"Stroke","np":13,"mn":"ADBE Stroke","ix":1,"en":1,"ef":[{"ty":10,"nm":"Path","mn":"ADBE Stroke-0001","ix":1,"v":{"a":0,"k":1,"ix":1}},{"ty":7,"nm":"All Masks","mn":"ADBE Stroke-0010","ix":2,"v":{"a":0,"k":0,"ix":2}},{"ty":7,"nm":"Stroke Sequentially","mn":"ADBE Stroke-0011","ix":3,"v":{"a":0,"k":1,"ix":3}},{"ty":2,"nm":"Color","mn":"ADBE Stroke-0002","ix":4,"v":{"a":0,"k":[0.635294139385,0.635294139385,0.635294139385,1],"ix":4}},{"ty":0,"nm":"Brush Size","mn":"ADBE Stroke-0003","ix":5,"v":{"a":0,"k":7,"ix":5}},{"ty":0,"nm":"Brush Hardness","mn":"ADBE Stroke-0004","ix":6,"v":{"a":0,"k":0,"ix":6}},{"ty":0,"nm":"Opacity","mn":"ADBE Stroke-0005","ix":7,"v":{"a":0,"k":1,"ix":7}},{"ty":0,"nm":"Start","mn":"ADBE Stroke-0008","ix":8,"v":{"a":0,"k":0,"ix":8}},{"ty":0,"nm":"End","mn":"ADBE Stroke-0009","ix":9,"v":{"a":1,"k":[{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"n":["0p833_0p833_0p167_0p167"],"t":24,"s":[0],"e":[76]},{"t":99}],"ix":9}},{"ty":7,"nm":"Spacing","mn":"ADBE Stroke-0006","ix":10,"v":{"a":0,"k":0,"ix":10}},{"ty":7,"nm":"Paint Style","mn":"ADBE Stroke-0007","ix":11,"v":{"a":0,"k":2,"ix":11}}]}],"sw":1920,"sh":1080,"sc":"#000000","ip":24,"op":100,"st":30,"bm":0},{"ddd":0,"ind":3,"ty":1,"nm":"mask 1","td":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[300,300,0],"ix":2},"a":{"a":0,"k":[960,540,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"hasMask":true,"masksProperties":[{"inv":false,"mode":"f","pt":{"a":0,"k":{"i":[[17,136.864],[0.52,-4.131],[0,-95.121],[-58.475,0],[0.009,58.773]],"o":[[-0.505,-4.131],[-17,136.864],[0,58.758],[58.475,0],[-0.015,-95.121]],"v":[[964.073,370.884],[956.256,370.884],[852.338,631.058],[960.164,737.473],[1067.991,631.058]],"c":true},"ix":1},"o":{"a":0,"k":100,"ix":3},"x":{"a":0,"k":0,"ix":4},"nm":"Mask 1"}],"sw":1920,"sh":1080,"sc":"#000000","ip":0,"op":100,"st":0,"bm":0},{"ddd":0,"ind":4,"ty":1,"nm":"line 1","tt":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[300,300,0],"ix":2},"a":{"a":0,"k":[960,540,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"hasMask":true,"masksProperties":[{"inv":false,"mode":"f","pt":{"a":0,"k":{"i":[[17,136.864],[0.52,-4.131],[0,-95.121],[-58.475,0],[0.009,58.773]],"o":[[-0.505,-4.131],[-17,136.864],[0,58.758],[58.475,0],[-0.015,-95.121]],"v":[[964.073,370.884],[956.256,370.884],[852.338,631.058],[960.164,737.473],[1067.991,631.058]],"c":true},"ix":1},"o":{"a":0,"k":100,"ix":3},"x":{"a":0,"k":0,"ix":4},"nm":"Mask 1"}],"ef":[{"ty":22,"nm":"Stroke","np":13,"mn":"ADBE Stroke","ix":1,"en":1,"ef":[{"ty":10,"nm":"Path","mn":"ADBE Stroke-0001","ix":1,"v":{"a":0,"k":1,"ix":1}},{"ty":7,"nm":"All Masks","mn":"ADBE Stroke-0010","ix":2,"v":{"a":0,"k":0,"ix":2}},{"ty":7,"nm":"Stroke Sequentially","mn":"ADBE Stroke-0011","ix":3,"v":{"a":0,"k":1,"ix":3}},{"ty":2,"nm":"Color","mn":"ADBE Stroke-0002","ix":4,"v":{"a":0,"k":[0.635294139385,0.635294139385,0.635294139385,1],"ix":4}},{"ty":0,"nm":"Brush Size","mn":"ADBE Stroke-0003","ix":5,"v":{"a":0,"k":7,"ix":5}},{"ty":0,"nm":"Brush Hardness","mn":"ADBE Stroke-0004","ix":6,"v":{"a":0,"k":0,"ix":6}},{"ty":0,"nm":"Opacity","mn":"ADBE Stroke-0005","ix":7,"v":{"a":0,"k":1,"ix":7}},{"ty":0,"nm":"Start","mn":"ADBE Stroke-0008","ix":8,"v":{"a":1,"k":[{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"n":["0p833_0p833_0p167_0p167"],"t":0,"s":[0],"e":[24]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"n":["0p833_0p833_0p167_0p167"],"t":24,"s":[24],"e":[100]},{"t":99}],"ix":8}},{"ty":0,"nm":"End","mn":"ADBE Stroke-0009","ix":9,"v":{"a":1,"k":[{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"n":["0p833_0p833_0p167_0p167"],"t":0,"s":[76],"e":[100]},{"t":24}],"ix":9}},{"ty":7,"nm":"Spacing","mn":"ADBE Stroke-0006","ix":10,"v":{"a":0,"k":0,"ix":10}},{"ty":7,"nm":"Paint Style","mn":"ADBE Stroke-0007","ix":11,"v":{"a":0,"k":2,"ix":11}}]}],"sw":1920,"sh":1080,"sc":"#000000","ip":0,"op":100,"st":0,"bm":0}]};
	var params = {
		container: document.getElementById('page-between'),
		renderer: 'svg',
		loop: true,
		autoplay: true,
		animationData: animationData
	};

	var anim;

	anim = bodymovin.loadAnimation(params);

	var preloadDropParams = {
		container: document.getElementById('preload-drop'),
		renderer: 'svg',
		loop: true,
		autoplay: true,
		animationData: animationData
	};

	var preloadDropAnim;

	preloadDropAnim = bodymovin.loadAnimation(preloadDropParams);

	setJuniorHeadingWidth();

});

var barbaOrRefresh;
var goToSlideHelper;

Barba.Dispatcher.on('transitionCompleted', function(currentStatus, prevStatus) {
	headerShadowSetup();
	homeHeroSliderSetup();
	playCurrentSliderVideo();
	playJuniorHeroVideo();
	removeMilkBlob();
	setActiveNavItem();
	productZoomOverlay();
	if($('#maslac-page')[0] != undefined)
	{
		$('#footer').css('display','none');
		$('body').addClass('maslac-bg')

		if(window.innerWidth >= mobileBreakPoint)
		{
			updateHomeParallax();
		} else {
			$('.mobile-burger-icon.dark').hide();
			$('.mobile-burger-icon.light').show();
		}
		frizhiderAnimacija();
		astalAnimacija();
		originalNamazAnimacija();
		slaniNamazAnimacija();
		smearBtnAnimacija();
	}
	else
	{
		$('#footer').css('display','flex');
		if($('body').hasClass('maslac-bg'));
		{
			$('body').removeClass('maslac-bg');
		}
	}
	if($('main#home-page')[0] != undefined )
	{
		if(window.innerWidth >= mobileBreakPoint)
		{
			updateHomeParallax();
		} else {
			$('.mobile-burger-icon.dark').hide();
			$('.mobile-burger-icon.light').show();
		}
		homeProductsSetup();
		sliderMilkWavesSetup();
		homePageMilkAnimations();
		$('.mobile-burger-icon.dark').show();
		$('.mobile-burger-icon.light').hide();
	} else {

		$('.mobile-burger-icon.dark').hide();
		$('.mobile-burger-icon.light').show();

		if($('#hamburger').hasClass( 'closed' ))
		{
			$('#hamburger').removeClass('closed');
			$('#hamburger').addClass('opened');
			$('.logo').addClass('opened');
			$('.blob').addClass('opened');
			$('.nav').addClass('opened');
			$('#lng').addClass('opened');
		}
	}
	if ($('#junior-page').length < 1) {
		$('body').removeClass('junior-parallax');
		$('footer').removeClass('junior-footer');
	} else {
		$('.mobile-burger-icon.light').show();
		$('.mobile-burger-icon.dark').hide();
		$('body').addClass('junior-parallax');
		// $('footer').addClass('junior-footer');
		setJuniorHeadingWidth();
		setTimeout(function() {
			initJuniorSlider();
		}, 10);
		initJuniorSliderMobile();
		if(window.innerWidth < mobileBreakPoint)
		{
			juniorSkuSliderInit()
		}
	}
	// if ($('main#sku-proizvodi-page').length > 0) {
	// 	hashBack = false;
	// }
	var windowWidth = $(window).outerWidth();
	// setJuniorOverlayWidth(windowWidth);
	setRadialWrapperHeight();
	vestiSliderSetup();
	historySliderSetup();
	skuSliderSetup();
	skuTabsSetup();
	preloaderSetup();
	if(window.innerWidth >= mobileBreakPoint)
	{
		moveParallax();
	}
	if(barbaOrRefresh == 'barba')
	{
		setTimeout(function () {
			afterglow.prepareLightboxVideos();
			afterglow.initVideoElements();
		},50);
		googleMapSetup();
		initPage();
	}
	else
	{
		barbaOrRefresh = 'barba';
		if(scrollbar)
		{
			scrollbar.update();
		}
	}
	// removeBetweenPage();
});

// Barba.Dispatcher.on('linkClicked', function(HTMLElement, MouseEvent) {
// 	addBetweenPage();
// });

var FadeTransition = Barba.BaseTransition.extend({
  start: function() {
	/**
	 * This function is automatically called as soon the Transition starts
	 * this.newContainerLoading is a Promise for the loading of the new container
	 * (Barba.js also comes with an handy Promise polyfill!)
	 */
	 $('#page-between').fadeIn();
	 if(window.innerWidth >= mobileBreakPoint)
	 {
		scrollbar.scrollTo(0,0, 1000);
	 }
	 else
	 {
		$('html, body').scrollTop(0);
	 }
	// As soon the loading is finished and the old page is faded out, let's fade the new page
	Promise
	  .all([this.newContainerLoading, this.fadeOut()])
	  .then(this.fadeIn.bind(this));
  },

  fadeOut: function() {
	/**
	 * this.oldContainer is the HTMLElement of the old Container
	 */

	return $(this.oldContainer).animate({ opacity: 0 }).promise();
  },

  fadeIn: function() {
	/**
	 * this.newContainer is the HTMLElement of the new Container
	 * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
	 * Please note, newContainer is available just after newContainerLoading is resolved!
	 */

	var _this = this;
	var $el = $(this.newContainer);

	$(this.oldContainer).hide();

	$el.css({
	  visibility : 'visible',
	  opacity : 0
	});

	$el.animate({ opacity: 1 }, 400, function() {

	});
	$('#page-between').fadeOut();
	_this.done();
  }
});

/**
 * Next step, you have to tell Barba to use the new Transition
 */

Barba.Pjax.getTransition = function() {
  /**
   * Here you can use your own logic!
   * For example you can use different Transition based on the current page or link...
   */

  return FadeTransition;
};

Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;

Barba.Pjax.preventCheck = function(evt, element) {
	if($('#pavlaka-page')[0] == undefined)
	{
		if ($(element).attr('href') && $(element).attr('href').indexOf('#') > -1)
		{
			return true;
		}
		else
		{
			return Barba.Pjax.originalPreventCheck(evt, element)
		}
	}
};

function mapReady()
{
	googleMapSetup();
}

function googleMapSetup()
{
	if($('main#kompanija-page')[0] != undefined || $('main#kontakt-page')[0] != undefined)
	{
		initMap();
	}
}

function headerShadowSetup()
{
	if(!$('main#home-page')[0])
	{
		$('#header').addClass('header-shadow');
		$('#header-mobile').addClass('header-shadow');
	}
	else
	{
		$('#header').removeClass('header-shadow');
		$('#header-mobile').removeClass('header-shadow');
	}
}

var scrollbar;
var mobileBreakPoint = 769;

/*
	HERO SLIDER MILK WAVES
*/

var canvas,ctx;
var vertexes;
var diffPt;
var autoDiff;
var verNum;
var canvasW;
var canvasH;

var xx;
var dd;

var timer;

var color = "#ffffff";

function sliderMilkWavesSetup()
{
	vertexes = [];
	diffPt = [];
	autoDiff = 0;
	verNum = 400;
	canvasW = $('.hero-slider').width();
	canvasH = $('.hero-slider').height();

	xx = 200;
	dd = 13;

	window.clearInterval(timer);

	initWave();
}

var addListener = function( e, str, func )
{
	if( e.addEventListener )
	{
		e.addEventListener( str, func, false );
	}
	else if( e.attachEvent )
	{
		e.attachEvent( "on" + str, func );
	}
	else
	{

	}
};

function resize()
{
	canvasW = document.getElementById('wave-container').offsetWidth + 40;
	canvasH = document.getElementById('wave-container').offsetHeight + 40;

	initCanvas(canvasW, canvasH);
	var cW = canvas.width;
	var cH = canvas.height;
	for(var i = 0;i < verNum;i++)
		vertexes[i] = new Vertex(cW / (verNum -1) * i , cH / 2,cH/2);
	initDiffPt();

}
function initWave()
{
	resize();
	var FPS = 30;
	var interval = 1050 / FPS >> 0;
	timer = setInterval( update, interval );
	addListener(window, "resize",resize);
}

function initDiffPt()
{
	for(var i = 0; i < verNum; i++)
	   diffPt[i]= 0;
}

function update()
{
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	autoDiff -= autoDiff*0.9;
	diffPt[xx] = autoDiff;

		for(var i = xx - 1; i > 0; i--)
		{
			var d = xx-i;
			if(d > dd)d=dd;
			diffPt[i] -= (diffPt[i]-diffPt[i+1])*(1-0.01*d);
		}

		for(var i = xx + 1; i < verNum; i++)
		{
			var d = i-xx;
			if(d > dd)d=dd;
			diffPt[i] -= (diffPt[i]-diffPt[i-1])*(1-0.01*d);
		}

	for(var i = 0; i < vertexes.length; i++)
	{
		vertexes[i].updateY(diffPt[i]);
	}

	draw();

}

function draw()
{
	ctx.beginPath();
	ctx.moveTo(0,window.innerHeight);
	ctx.fillStyle = color;
	ctx.lineTo(vertexes[0].x, vertexes[0].y);
	for(var i = 1; i < vertexes.length; i++)
	{
		ctx.lineTo(vertexes[i].x,vertexes[i].y);
	}
	ctx.lineTo(canvas.width, window.innerHeight);
	ctx.lineTo(0, window.innerHeight);
	ctx.fill();
}

function initCanvas(width,height)
{
	canvas = document.getElementById("canvas");
	canvas.width = width;
	canvas.height = height;
	ctx = canvas.getContext("2d");
}

function Vertex(x,y,baseY)
{
	this.baseY = baseY;
	this.x = x;
	this.y = y;
	this.vy = 0;
	this.targetY = 0;
	this.friction = 0.15;
	this.deceleration = 0.95;
}

Vertex.prototype.updateY = function(diffVal)
{
	this.targetY = diffVal + this.baseY;
	this.vy += this.targetY - this.y
	this.y += this.vy * this.friction;
	this.vy *= this.deceleration;
}

function waveIt(_dir)
{
	autoDiff = 800;

	switch(_dir) {
		case 'left':
			xx = 1 + Math.floor((verNum - 2) * 0 / canvas.width);
			break;
		case 'right':
			xx = 1 + Math.floor((verNum - 2) * canvas.width / canvas.width);
			break;
		default:
			xx = 1 + Math.floor((verNum - 2) * (canvas.width / 2) / canvas.width);
	}

	diffPt[xx] = autoDiff;
}

function playCurrentSliderVideo()
{
	var vid = $('.slick-current').find('video')[0];
	if(vid != null && vid != undefined && ($('main#home-page')[0] != undefined || $('main#maslac-page')[0] != undefined))
	{
		if(vid.paused)
		{
			vid.play();
		}
	}
}

function startSliderCicrcleAnim() {
	if ($('.radial-wrapper #container').length > 0) {
		$('.slider-content .slider-buttons').each(function(index, el) {
			if( scrollbar.isVisible( $( this )[0] ) ) {
				$('.radial-wrapper #container').addClass('active');
			}
		});
	}
}

function playJuniorHeroVideo()
{
	var vid = $('#junior-page #hero .video').find('video')[0];
	if(vid != null && vid != undefined && $('main#junior-page')[0] != undefined)
	{
		if(vid.paused)
		{
			vid.play();
		}
	}
}

function openMenu(_this)
{
	_this.removeClass('closed');
	_this.addClass('opened');

	$('.blob').addClass('opened');
	$('.logo').addClass('opened');
	$('.nav').addClass('opened');
	$('#lng').addClass('opened');

	if($('main#home-page')[0] != undefined)
	{
		milkBlobSvg.goToAndPlay(1);

		if(this.dropItLikeItsHot) clearTimeout(this.dropItLikeItsHot);
		this.dropItLikeItsHot = setTimeout(function() {
			$('#milk-drop').addClass('opened');
			milkDropSvg.goToAndPlay(1);
		}, 400);

		if(this.itIsDropped) clearTimeout(this.itIsDropped);
		this.itIsDropped = setTimeout(function() {
			waveIt();
			milkSplashSvg.goToAndPlay(1);
		}, 1000);
	}
}

var prevMobileScroll;

function openMobileMenu(_this)
{
	$('#milk-splash').css('display', 'block');
	// _this.removeClass('hover-reverse');
	_this.addClass('hover');

	if(this.openSessame)
	{
		clearTimeout(this.openSessame);
		_this.removeClass('hover-reverse');
	}

	$('.mobile-blob').addClass('opened');
	$('.logo').addClass('opened');
	$('.nav').addClass('opened');
	$('#lng-mobile').addClass('opened');
	// $('#header-mobile').addClass('opened');

	// console.log($('html').scrollTop());
	prevMobileScroll = $('html').scrollTop();

	if($('main#home-page')[0] != undefined && $('html').scrollTop() < window.innerHeight / 2)
	{
		milkBlobMobSvg.goToAndPlay(1);

		if(this.dropItLikeItsHot) clearTimeout(this.dropItLikeItsHot);
		this.dropItLikeItsHot = setTimeout(function() {
			$('#milk-drop').addClass('opened');
			milkDropSvg.goToAndPlay(1);
		}, 400);

		if(this.itIsDropped) clearTimeout(this.itIsDropped);
		this.itIsDropped = setTimeout(function() {
			waveIt();
			milkSplashSvg.goToAndPlay(1);
			$('#header-mobile').addClass('opened');
		}, 1000);

		this.openSessame = setTimeout(function() {
			_this.removeClass('hover');
		}, 4500);

		if(this.openSessame2) clearTimeout(this.openSessame2);
		this.openSessame2 = setTimeout(function() {
			_this.removeClass('closed');
			_this.addClass('opened');	
			$('scrollbar').addClass('opened');		
		}, 3500);
	}
	else
	{
		$('#header-mobile').addClass('opened');

		this.openSessame = setTimeout(function() {
			_this.removeClass('hover');
		}, 3000);

		if(this.openSessame2) clearTimeout(this.openSessame2);
		this.openSessame2 = setTimeout(function() {
			_this.removeClass('closed');
			_this.addClass('opened');
			$('scrollbar').addClass('opened');
		}, 2000);
	}
}

function closeMenu(_this)
{
	_this.removeClass('opened');
	_this.addClass('closed');

	$('.blob').removeClass('opened');
	$('.logo').removeClass('opened');
	$('.nav').removeClass('opened');
	$('#lng').removeClass('opened');
	$('#header-mobile').removeClass('opened');

	if($('main#home-page')[0] != undefined)
	{
		$('#milk-drop').removeClass('opened');
		milkDropSvg.stop();

		if(this.dropItLikeItsHot) clearTimeout(this.dropItLikeItsHot);
		if(this.itIsDropped) clearTimeout(this.itIsDropped);
	}
}

function closeMobileMenu(_this, _justClosing)
{
	$('#milk-splash').css('display', 'none');

	_this.addClass('hover-reverse');

	if(this.openSessame)
	{
		clearTimeout(this.openSessame);
		_this.removeClass('hover');
	}
	this.openSessame = setTimeout(function() {
		_this.removeClass('hover-reverse');
	}, 3000);

	if(this.openSessame2) clearTimeout(this.openSessame2);
	this.openSessame2 = setTimeout(function() {
		_this.addClass('closed');
		_this.removeClass('opened');
	}, 2000);

	$('.mobile-blob').removeClass('opened');
	$('.logo').removeClass('opened');
	$('.nav').removeClass('opened');
	$('#lng-mobile').removeClass('opened');
	$('#header-mobile').removeClass('opened');
	$('scrollbar').removeClass('opened');

	if(_justClosing)
	{
		$('html').scrollTop(prevMobileScroll);
	}

	if($('main#home-page')[0] != undefined)
	{
		$('#milk-drop').removeClass('opened');
		milkDropSvg.stop();

		if(this.dropItLikeItsHot) clearTimeout(this.dropItLikeItsHot);
		if(this.itIsDropped) clearTimeout(this.itIsDropped);
	}
}

function initHomeHeroSlider()
{
	var heroNumPages = $('.hero-slider').children().length;
	var heroPages = zeroPad(heroNumPages, 2);

	$('.hero-arrows .pages .current-page').text('01');
	$('.hero-arrows .pages .total-pages').text(heroPages);

	if(heroNumPages > 1)
	{
		$('.hero-slider').slick({
			prevArrow:$('.hero-prev'),
			nextArrow:$('.hero-next'),
			centerMode: true,
			centerPadding: '0px',
			slidesToShow: 1
		});
	}
	else
	{
		if(window.innerWidth < mobileBreakPoint)
		{
			$('.hero-slider .item').css('width', '100%');
			$('.hero-slider .item').addClass('slick-slide');
		}
		$('.hero-slider .item').addClass('slick-center');
		$('.hero-slider .item').addClass('slick-current');
	}

	if(heroNumPages == 1)
	{
		$('.hero-wrap').find('.hero-arrows').css('display', 'none');
	}
}

function zeroPad(n, width, z)
{
	z = z || '0';
	n = n + '';
	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function spacePad(n, width, z)
{
	z = z || ' ';
	n = n + '';
	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function trace(_label, _value)
{
	var num = _value;
	var str = num.toString();

	// console.log('|', _label, _value, spacePad('|', 28 - str.length - _label.length));
}

function scrollHandler(status)
{
	// $('body').css('background-position-y', - (status.offset.y - Math.round(status.offset.y * 0.5)) + 'px');
	$('body').css('background-position-y', - (status.offset.y - status.offset.y * 0.75) + 'px');
	moveParallax();
	showHiddenText();
	showHiddenButtons();

	if($('#home-page')[0] != undefined)
	{
		moveHomeParallax();
	}

	if($('#maslac-page')[0] != undefined)
	{
		moveHomeParallax();
		brokenHiddenText('.hidden-text');

		// if( scrollbar.isVisible( $( '#frizhider' )[0] ))
		// {
		if($( '#frizhider' ).offset().top + $( '#frizhider' ).height() <= scrollbar.scrollTop && !animatedFrizhider)
		{
			animatedFrizhider = true;
			setTimeout(function(){
				$('#frizhider').css('visibility','visible');
				frizhiderSvg.goToAndPlay(1, true);
				$('.frizhider-text').addClass('active');
			},500);
		}

		if( $( '#packshots' ).offset().top + $( '#packshots' ).height() / 2 <= scrollbar.scrollTop && !animatedAstal)
		{
			animatedAstal = true;
			setTimeout(function(){
				astalSvg.goToAndPlay(1, true);
			},500);
		}

		if( $( '#packshots' ).offset().top + $( '#packshots' ).height() / 2 <= scrollbar.scrollTop && !animatedNamaz)
		{
			animatedNamaz = true;
			setTimeout(function(){
				originalNamazSvg.goToAndPlay(1, true);
			},1500);
		}
	}

	if($('#pavlaka-page')[0] != undefined)
	{
		brokenHiddenText('.hidden-text');
	}
	if($('#jogood-page')[0] != undefined)
	{
		brokenHiddenText('.hidden-text');
		fruitsTransformY();
	}

	if($('#junior-page')[0] != undefined) {
		$('body').css('background-position-y', - (status.offset.y - status.offset.y * 0.85) + 'px');
		juniorProductsAnimation();
		showJuniorHeading();
		startSliderCicrcleAnim();
	} else {
		$('body').css('background-position-y', - (status.offset.y - status.offset.y * 0.6) + 'px');
	}

	if($('main#kompanija-page')[0] != undefined)
	{
		slickHiddenTextVisible('.history-text-slider .slick-active');
	}
}

function showHiddenText()
{
	$( ".cover" ).each(function( index ) {
		if( scrollbar.isVisible( $( this )[0] ) )
		{
			// console.log(index);
			if($(this) != undefined && !$(this).hasClass('active'))
			{
				$(this).addClass('active');

				$(this).parent().find('.line').addClass('active');
			}
		}
	});
}

function setJuniorHeadingWidth() {
	var containerWidth = $('.parallax-wrap').outerWidth();
	if ($('.junior-heading').length > 0) {
		$('.junior-heading').css('width', containerWidth * 0.8 + 'px');
	}
}

function juniorProductsAnimation() {
	if ($('#proizvodi.junior').length > 0) {
		$('#proizvodi.junior').each(function(index, el) {
			if ( scrollbar.isVisible( $( this )[0] ) ) {
				$('.proizvod').removeClass('hidden-product');
			}
		});
	}
}

function showJuniorHeading() {
	if ($('.junior-heading').length > 0) {
		$('.junior-heading').each(function(index, el) {
			if ( scrollbar.isVisible( $( this )[0] ) ) {
				$(el).parent('.junior-heading-cover').addClass('active');
				// console.log($(el));
			}
		});
	}
}

var buttonToShow;
function showHiddenButtons()
{
	$( ".reveal-wrap" ).each(function( index ) {

		if( scrollbar.isVisible( $( this )[0] ) )
		{
			buttonToShow = $( this ).find('.reveal');
			if(buttonToShow != undefined && !buttonToShow.hasClass('active'))
			{
				buttonToShow.addClass('active');
			}
		}
	});

	var arrows = $( "#vesti" ).find('.arrows-vesti');

	if(arrows[0] != undefined && !arrows.hasClass('active'))
	{
		if( scrollbar.isVisible( arrows[0] ) )
		{
			arrows.addClass('active');
		}
	}
}

/*
	FILTER DROP DOWN
*/

$( document ).on("click", "#filter-drop-down .droper", function(e){
	e.preventDefault();
	// $(this).parent().parent().children(".dropbtn").html($(this).text());
	$(this).parent().toggleClass('active');
});

$( document ).on("click", "#filter-drop-down .dropdown-content a", function(e){
	// e.preventDefault();
	$(this).parent().parent().find(".dropbtn").html($(this).text());
	$(this).parent().parent().removeClass('active');
});

/*
	NAVIGATION
*/
$(document).on('click', '#navigation .nav-item', function(){
	$('#navigation .nav-item').removeClass('active');
	$(this).addClass('active');
});

$(document).on('click', '#navigation-mobile .nav-item', function(){
	$('#navigation-mobile .nav-item').removeClass('active');
	$(this).addClass('active');
	closeMobileMenu($( "#hamburger-mobile" ),false);
});

// function sliderBackBtnBehavior() {
//     var hash = location.hash;
//     var activeItemClassName = hash.substring(1);
//     var slickActiveIndex = $('.sku-slider .item.' + activeItemClassName).attr('data-index');
//     console.log('hash cahnge');
//     console.log('hash -> ', location.hash);
//     console.log('class name -> ', activeItemClassName);
//     console.log('index -> ', slickActiveIndex);
//     // $('.sku-slider').slick('slickGoTo', 0);
// }
// $(window).on('hashchange', function() {
// 	sliderBackBtnBehavior();
// });

/*
	LANGUAGE
*/
$("#lng button").each(function( index ) {
	$(this).bind("click", function() {
		$("#lng button").removeClass("active");
		$(this).addClass("active");
	});
});

$( document ).on("click", "#lng .dropdown-content a", function(e){
	// e.preventDefault();
	$(this).parent().parent().children(".dropbtn").html($(this).text());

	if(window.innerWidth < mobileBreakPoint)
	{
		closeMobileMenu($('#hamburger-mobile'),false);
	}
});

/*
	LOGO
*/
$(document).on('click', '.logo', function(e){
	e.preventDefault();
	if($('main#home-page')[0] != undefined)
	{
		if($('#hamburger').hasClass( 'closed' ))
		{
			openMenu($('#hamburger'));
		}
		else
		{
			closeMenu($('#hamburger'));
		}
	}
	else
	{
		// window.location.href = '/';
		$('#navigation .nav-item').removeClass('active');
		$('.home-nav').addClass('active');
		$('.logo').trigger('click');
	}
});

/*
	HAMBURGER
*/
$(document).on('click', '#hamburger', function(e){
	if($(this).hasClass( 'closed' ))
	{
		openMenu($(this));
	}
	else
	{
		closeMenu($(this));
	}
	maloMajoneza($(this));
});

$(document).on('click', '#hamburger-mobile', function(e){
	if($( "#hamburger-mobile" ).hasClass( 'closed' ))
	{
		openMobileMenu($(this));
	}
	else
	{
		closeMobileMenu($(this),true);
	}
	// maloMajoneza($(this));
});

$('#hamburger').on('mouseenter', function() {
	if(!$( "#hamburger" ).hasClass( 'hover' ))
	{
		maloMajoneza($(this));
	}
});

function maloMajoneza(_this)
{
	_this.addClass( 'hover' );

	if(this.hamburgerHoverTimeout) clearTimeout(this.hamburgerHoverTimeout);
	this.hamburgerHoverTimeout = setTimeout(function(){
		_this.removeClass( 'hover' );
	},1150);
}

/*
	PARALLAX
*/

var winHeight = window.innerHeight;
var parallaxPosY;
var parallaxHeight;

var textCover, parallaxBox, percent;

function moveParallax()
{
	$( ".parallax-wrap" ).each(function( index ) {

		if( scrollbar.isVisible( $( this )[0] ) )
		{
			parallaxPosY = $( this ).offset().top;
			parallaxHeight = $( this ).height();

			textCover = $( this ).find('.cover');
			parallaxBox = $( this ).find('.parallax-box');

			$( this ).find('.window').css('background-position-y', (winHeight - parallaxPosY) / (winHeight + parallaxHeight) * 100 + '%');

			if(parallaxBox[0] != undefined)
			{
				percent = (parallaxBox.parent().offset().top + parallaxBox.height() / 2) / window.innerHeight;

				parallaxBox.css('transform', 'translateY(' + (percent * 40 - 20) + '%)');
				// trace('pos', parseFloat(percent).toFixed(2));
				// trace('transform', percent * 40 - 20);
				// console.log('+------------------------------+');
			}
		}

	});
}

function moveHomeParallax()
{
	$( ".hero-slider .slick-current" ).each(function( index ) {

		parallaxPosY = $( this ).offset().top;
		parallaxHeight = $( this ).height();

		if($( this ).find('.window')[0] != undefined)
		{
			if( scrollbar.isVisible( $( this ).find('.window')[0] ) )
			{
				homeParallaxImg($( this ));
			}
		}
		else
		{
			if( scrollbar.isVisible( $( this ).find('.video')[0] ) )
			{
				homeParallaxVideo($( this ));
			}
		}

	});

	var arrows = $( "#proizvodi" ).find('.arrows3d');

	if(arrows[0] != undefined && !arrows.hasClass('active'))
	{
		if( scrollbar.isVisible( arrows[0] ) )
		{
			arrows.addClass('active');
		}
	}
}

function updateHomeParallax()
{
	$( ".hero-slider .item" ).each(function( index ) {

		parallaxPosY = $( this ).offset().top;
		parallaxHeight = $( this ).height();

		if($( this ).find('.window')[0])
		{
			// if( scrollbar.isVisible( $( this ).find('.window')[0] ) )
			// {
				homeParallaxImg($( this ));
			// }
		}
		else
		{
			// if( scrollbar.isVisible( $( this ).find('.video')[0] ) )
			// {
				homeParallaxVideo($( this ));
			// }
		}

	});
}

function homeParallaxImg(_img)
{
	var textPosY = _img.find('.mobile-link')[0] && window.innerWidth < mobileBreakPoint ? 50 : 100;

	_img.find('.window').css('background-position-y', ((winHeight - parallaxPosY) / (winHeight + parallaxHeight) * 100 - 25) + '%');

	_img.find('.text').css('transform', 'translateY(' + (-textPosY - 0.75 * ((winHeight - parallaxPosY) / (winHeight + parallaxHeight) * 100 - 50) * 4) + '%)');
}

function homeParallaxVideo(_vid)
{
	var textPosY = _vid.find('.mobile-link')[0] && window.innerWidth < mobileBreakPoint ? 50 : 100;
	var videoPosY = -((winHeight - parallaxPosY) / (winHeight + parallaxHeight) * 100 - 50) / 8;

	if(videoPosY > 0)
	{
		videoPosY = 0;
	}
	if($('#maslac-page')[0] == undefined)
	{
		_vid.find('video').css('transform', 'translateX(-50%) translateY(' + videoPosY + '%)');
	}

	_vid.find('.text').css('transform', 'translateY(' + (-textPosY - 0.75 * ((winHeight - parallaxPosY) / (winHeight + parallaxHeight) * 100 - 50) * 4) + '%)');
}

function slickHiddenText(_slider)
{
	var hiddenText = $( _slider ).find('.text');
	if(hiddenText[0] != undefined && !hiddenText.hasClass('active'))
	{
		hiddenText.addClass('active');
	}
}

function slickHiddenTextVisible(_slider)
{
	var hiddenText = $( _slider ).find('.text');

	if( scrollbar.isVisible( hiddenText[0] ) )
	{
		if(hiddenText[0] != undefined && !hiddenText.hasClass('active'))
		{
			hiddenText.addClass('active');
		}
	}
}

function brokenHiddenText(_item)
{
	if (scrollbar !== undefined)
	{
		$( _item ).each(function( index ) {
			if( scrollbar.isVisible( $( this )[0]) && !$(this).hasClass('active') )
			{
				$(this).addClass('active');
				$(this).find('.vLine').addClass('active');
			}
		});
	}
}

var milkDropParams;
var milkDropSvg;

var milkSplashParams;
var milkSplashSvg;

var milkBlobParams;
var milkBlobSvg, milkBlobMobSvg;

function removeMilkBlob()
{
	$('#milk-blob').empty();
	$('#milk-blob-mob').empty();
}

function homePageMilkAnimations()
{
	milkDropParams = null;
	milkDropSvg = null;

	milkSplashParams = null;
	milkSplashSvg = null;

	milkBlobParams = null;
	milkBlobSvg = null;
	milkBlobMobSvg = null;

	milkDropParams = {
		container: document.getElementById('milk-drop'),
		renderer: 'svg',
		loop: false,
		autoplay: false,
		animationData: dropAnimData
	};

	milkDropSvg = bodymovin.loadAnimation(milkDropParams);

	milkDropSvg.setSpeed(1.75);

	milkSplashParams = {
		container: document.getElementById('milk-splash'),
		renderer: 'svg',
		loop: false,
		autoplay: false,
		animationData: splashAnimData
	};

	milkSplashSvg = bodymovin.loadAnimation(milkSplashParams);

	milkBlobParams = {
		container: document.getElementById('milk-blob'),
		renderer: 'svg',
		loop: false,
		autoplay: false,
		animationData: blobAnimData
	};

	milkBlobSvg = bodymovin.loadAnimation(milkBlobParams);

	milkBlobMobParams = {
		container: document.getElementById('milk-blob-mob'),
		renderer: 'svg',
		loop: false,
		autoplay: false,
		animationData: blobAnimData
	};

	milkBlobMobSvg = bodymovin.loadAnimation(milkBlobMobParams);

	if($('.blob').hasClass('opened'))
	{
		milkBlobSvg.goToAndStop(1000);
	}
}

/*
	HOME HERO SLIDER
*/
function homeHeroSliderSetup()
{
	if($('.hero-slider')[0] != undefined)
	{
		// if(window.innerWidth < mobileBreakPoint)
		// {
		// 	$('#hero').css('height', window.innerHeight + 'px');
		// }
		initHomeHeroSlider();
	}
}

$(document).on('click', '.hero-prev', function(e){
	e.preventDefault();
	if($('main#home-page')[0] != undefined)
	{
		waveIt('left');
	}
});

$(document).on('click', '.hero-next', function(e){
	e.preventDefault();
	if($('main#home-page')[0] != undefined)
	{
		waveIt('right');
	}
});

$(document).on('beforeChange', '.hero-slider', function(event, slick, currentSlide, nextSlide){
	if(window.innerWidth >= mobileBreakPoint)
	{
		updateHomeParallax();
	}
});

$(document).on('afterChange', '.hero-slider', function(event, slick, currentSlide){
	var currentPage = zeroPad(currentSlide + 1, 2);
	$('.hero-arrows .pages .current-page').text(currentPage);

	playCurrentSliderVideo();
	slickHiddenText('.hero-slider .slick-center');
});

/*
	HOME PRODUCTS 3D PACKAGING
*/
var spin_interval, current_package, spin_dir, package3d;
var current_slide;
function homeProductsSetup()
{
	if($('main#home-page')[0] != undefined)
	{
		current_slide = 12;
		current_package = null;
		package3d = null;
		window.clearInterval(spin_interval);

		$( "#proizvodi:not(.junior) .wrap .item" ).each(function( index ) {

			package3d = $( this ).find(".package3d");

			package3d.css("background-position-x", (current_slide * - package3d.width()) + "px");

		});

		var product3dPages = zeroPad($('#proizvodi:not(.junior) .wrap').children().length, 2);

		$('.arrows3d .pages .current-page').text('01');
		$('.arrows3d .pages .total-pages').text(product3dPages);

		$('#proizvodi:not(.junior) .wrap').slick({
			slidesToShow: 5,
			responsive: [
				{
					breakpoint: mobileBreakPoint,
					settings: {
						prevArrow:$('.prev3d'),
						nextArrow:$('.next3d'),
						slidesToShow: 1,
						centerMode: true,
						centerPadding: '0px'
					}
				}
			]
		});
	}
}

$(document).on('mouseover', '#home-page #proizvodi .item', function(){
	if(window.innerWidth >= mobileBreakPoint)
	{
		current_package = $(this);
		package3d = current_package.find(".package3d");

		current_package.mousemove(function(e){
			var x = (e.pageX - current_package.offset().left) / current_package.width();

			if(x <= 0.5)
				spin_dir = "left";
			else
				spin_dir = "right";
		});

		spin_interval = setInterval(spinPackage, 50);
	}
});

$(document).on('mouseout', '#home-page #proizvodi .item', function(){
	if(window.innerWidth >= mobileBreakPoint)
	{
		window.clearInterval(spin_interval);
		current_slide = 12;
		package3d.css("background-position-x", (current_slide * - package3d.width()) + "px");
		current_package = null;
		package3d = null;
	}
});

$(document).on('afterChange', '#proizvodi .wrap', function(event, slick, currentSlide){
	var currentPage = zeroPad(currentSlide + 1, 2);
	$('.arrows3d .pages .current-page').text(currentPage);
});

function spinPackage()
{
	package3d.css("background-position-x", (current_slide * - package3d.width()) + "px");

	if(spin_dir == 'left')
	{
		if(current_slide < 23)
			current_slide++;
		else
			current_slide = 23;
	}
	else
	{
		if(current_slide > 0)
			current_slide--;
		else
			current_slide = 0;
	}
}

/*
	VESTI SLIDER
*/

var vesti_slow = 500;
var vesti_fast = 250;

function vestiSliderSetup()
{
	if($('.vesti-slider')[0] != undefined)
	{
		var vestiPages = zeroPad($('.vesti-slider').children().length, 2);

		$('.arrows-vesti .pages .current-page').text('01');
		$('.arrows-vesti .pages .total-pages').text(vestiPages);

		$('.vesti-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			draggable: false,
			prevArrow:$('.vesti-prev'),
			nextArrow:$('.vesti-next'),
			responsive: [
				{
					breakpoint: mobileBreakPoint,
					settings: {
						prevArrow:$('.vesti-mob-prev'),
						nextArrow:$('.vesti-mob-next'),
						slidesToShow: 1
					}
				}
			]
		});
	}
}

$(document).on('afterChange', '.vesti-slider', function(event, slick, currentSlide){
	var currentPage = zeroPad(currentSlide + 1, 2);
	$('.arrows-vesti .pages .current-page').text(currentPage);
});

$(document).on('mouseenter', '.vesti-slider .item', function(){
	// $(this).find('.dark-overlay').fadeOut(vesti_fast);
	// $(this).find('.light-overlay').fadeIn(vesti_fast);
	// $(this).find('.text').fadeIn(vesti_fast);
	$(this).find('.dark-overlay').addClass('active');
	$(this).find('.light-overlay').addClass('active');
	$(this).find('.text').addClass('active');
	$(this).find('.current-blob').children('svg').addClass('show');
});

$(document).on('mouseleave', '.vesti-slider .item', function(){
	// $(this).find('.dark-overlay').fadeIn(vesti_slow);
	// $(this).find('.light-overlay').fadeOut(vesti_slow);
	// $(this).find('.text').fadeOut(vesti_fast);
	$(this).find('.dark-overlay').removeClass('active');
	$(this).find('.light-overlay').removeClass('active');
	$(this).find('.text').removeClass('active');
	$(this).find('.current-blob svg').removeClass('show');
});

/*
	HISTORY SLIDERS
*/
function historySliderSetup()
{
	if($('main#kompanija-page')[0] != undefined)
	{
		$('.history-image-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			draggable: false,
			prevArrow:$('.history-prev'),
			nextArrow:$('.history-next'),
			asNavFor: '.history-text-slider'
		});

		$('.history-text-slider').slick({
			slidesToShow: 1,
			fade: true,
			arrows: false,
			asNavFor: '.history-image-slider'
		});
	}
}

$(document).on('afterChange', '.history-text-slider', function(event, slick, currentSlide){
			slickHiddenText('.history-text-slider .slick-active');
});
/*
	PROIZVODI SUB-MENU
*/
$(document).on('click', '.sub', function(e){
	e.preventDefault();
	$(this).find('.pod-lista').slideToggle('fast');
});

/*
	FAQ
*/
$(document).on('click', '.item', function(){
	$(this).toggleClass('active');
	$(this).find('.answer').slideToggle(function(){
		if(scrollbar)
		{
			scrollbar.update();
		}
	});
});

/*
	KONTAKT FILTER
*/
$(document).on('click', '#kontakt-filter button', function(e){
	e.preventDefault();
	$('#kontakt-filter button').removeClass('active');
	$(this).addClass('active');

	$('.kontakt-forma').css('display', 'none');

	$('#kontakt-forma-' + $(this).data('uid')).css('display', 'flex');
});

/*
	SARADNJA / POSAO DROP DOWN
*/
$( document ).on("click", "#positions .dropdown-content a", function(e){
	e.preventDefault();
	$(this).parent().parent().children(".dropbtn").html($(this).text());
});
$( document ).on("click", "#positions .dropbtn", function(e){
	e.preventDefault();
});

/*
	FOOTER
*/
$(document).on('click', '.footer-kompanija', function(e) {
	$('#navigation .nav-item').removeClass('active');
	$('.kompanija-nav').addClass('active');
});

$(document).on('click', '.footer-proizvod li a', function(e) {
	$('#navigation .nav-item').removeClass('active');
	$('.proizvodi-nav').addClass('active');
});


$(document).on('click', '.all-certs', function(e) {
	$('#navigation .nav-item').removeClass('active');
	$('.sertifikati-nav').addClass('active');
});

/*
	JUNIOR SKUs PRODUCT SLIDER
*/

// $(document).on('click', '.open-junior-overlay', function(event) {
// 	$('article.junior-overlay').show();
// 	setTimeout(function() {
// 		juniorSkuSliderInit();
// 	}, 10);
// 	$('.junior-overlay .overlay-wrapper').animate({width: '100%'}, 1000, function() {
// 		$('.junior-overlay .arrows').animate({width: '100%'}, 500);
// 	});
// });

// $(document).on('click', '.close-junior-overlay', function(event) {
// 	if ($('.junior-sku-slider').length > 0) {
// 		$('.junior-overlay .overlay-wrapper').animate({ width: '0%' }, 1000, function() {
// 			$('article.junior-overlay').hide();
// 			$('.junior-overlay .arrows').css('width', '0%');
// 			$('.junior-sku-slider').slick('unslick');
// 		});
// 	}
// });

$(document).on('click', '.junior-sku-arrows .sku-prev', function(event) {
	$('.junior-sku-slider .slide-transition').removeClass('start end');
	$('.junior-sku-slider .slide-transition').addClass('start');
	setTimeout(function(){
		$('.junior-sku-slider').slick('slickPrev');
	}, 200);
});

$(document).on('click', '.junior-sku-arrows .sku-next', function(event) {
	$('.junior-sku-slider .slide-transition').removeClass('start end');
	$('.junior-sku-slider .slide-transition').addClass('start');
	setTimeout(function(){
		$('.junior-sku-slider').slick('slickNext');
	}, 200);
});

$('.junior-sku-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	if (nextSlide == 1) {
		$('.junior-overlay .current-page, .junior-sku-arrows .current-page').text('02');
	} else {
		$('.junior-overlay .current-page, .junior-sku-arrows .current-page').text('01');
	}
});

$('.junior-sku-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
	setTimeout(function(){
		$('.junior-sku-slider .slide-transition').addClass('end');
	}, 200);
});

function juniorSkuSliderInit() {
	if ($('.junior-sku-slider').length > 0) {
		$('.junior-sku-slider').slick({
			slideToShow: 1,
			slideToScroll: 1,
			arrows: false,
			dots: false,
			fade: true,
			speed: 300,
			draggable: false,
			swipe: false
		});
		$('.junior-sku-slider').slick('slickGoTo', 0);
	}
}
// function setJuniorOverlayWidth(width) {
// 	if ($('.junior-overlay').length > 0) {
// 		$('.junior-overlay').css('width', width + 'px');
// 		$('.junior-overlay .overlay-content').css('width', width + 'px');
// 	}
// }

/*
	SKUs PRODUCT SLIDER
*/

function skuSliderSetup()
{
	if($('.sku-slider')[0] != undefined)
	{

		var currentURL = window.location.href;

		var splitedURL = currentURL.split("#");

		var slugClass = splitedURL[1];

		if (slugClass) {
			var goToSlide = $('.sku-slider').children('.'+slugClass).data('index');
			var videoDiv = $('.info-slider').children().eq(goToSlide).find('.video-campaign');
			// console.log(goToSlide);
		} else {
			var goToSlide = 0;
			var firstSlideSlug = $('.sku-slider').children(':first').data('slug');
			var videoDiv = $('.info-slider').children().first().find('.video-campaign');
			window.location.href = "#"+firstSlideSlug;
		}

		if (!videoDiv[0]) {
			$('.videocamp').hide();
		}

		var skuPages;

		if($('.double-item')[0] == undefined)
		{
			skuPages = zeroPad($('.sku-slider').children().length, 2);
		}
		else
		{
			skuPages = zeroPad($('.sku-slider').children().length / 2, 2);
		}

		$('.sku-arrows .pages .current-page').text('01');
		$('.sku-arrows .pages .total-pages').text(skuPages);

		if($('.sku-slider').children().length > 1)
		{
			goToSlideHelper = goToSlide;
			$('.sku-slider').slick({
				prevArrow:$('.sku-prev'),
				nextArrow:$('.sku-next'),
				centerMode: true,
				centerPadding: '0px',
				slidesToShow: $('.sku-slider').children().length <= 3 ? $('.sku-slider').children().length - 1 : 3,
				focusOnSelect: true,
				initialSlide: goToSlide,
				asNavFor: '.info-slider',
				responsive: [
					{
						breakpoint: 641,
						settings: {
							slidesToShow: 1
						}
					}
				]
			});
		}
		else
		{
			$('.sku-slider .item').addClass('slick-center');
		}
	}
}

var hashBack = false;
var djura = false;

$(document).on('click', '.sku-next', function(){
	hashBack = false;
});

$(document).on('click', '.sku-prev', function(){
	hashBack = false;
});

$(document).on('click', '.sku-slider .item', function(){
	hashBack = false;
});

$(document).on('swipe', '.sku-slider', function(){
	hashBack = false;
	console.log('hashBack4 -> ', hashBack)
});

$(document).on('init', '.sku-slider', function() {
	hashBack = false;

	var help = goToSlideHelper;
	help += 1;

	if (help < 10) {
		help = '0' + help;
	}

	$('.sku-arrows .pages .current-page').text( help );
});

$(document).on('beforeChange', '.sku-slider', function(event, slick, currentSlide){
	// console.log('///////////////////////')

	if (hashBack) {
		return;
	}
	var videoDiv = $('.info-slider .slick-current').find('.video-campaign');

	if (videoDiv[0])
	{
		$('.info-slider .slick-current').find('.video-campaign').removeClass('active');
	}

	// if(!$('.info-slider .slick-current').find('.product-info').hasClass('active'));
	// {
	// 	$('.info-slider .slick-current').find('.product-info').addClass('active');
	// }

	// $('.nutri').addClass('active');
	// $('.videocamp').removeClass('active');
});

$(document).on('afterChange', '.sku-slider', function(event, slick, currentSlide){
	var currentPage = zeroPad(parseInt( $('.sku-slider .slick-center').attr('data-index') ) + 1, 2);
	$('.sku-arrows .pages .current-page').text( currentPage );

	var slideDiv = $('.sku-slider .slick-slide[data-slick-index="' + currentSlide + '"]');
	// var slug = slideDiv.data('slug');
	var slug = $(slideDiv).attr('data-slug');

	if (!hashBack) {
		window.location.href = '#'+slug;
	}

	$('.info-slider .item').find('.product-info').addClass('active');

	$('.nutri').addClass('active');
	$('.videocamp').removeClass('active');

	var videoDiv = $('.info-slider .slick-current').find('.video-campaign');
	if (!videoDiv[0]) {
		$('.videocamp').hide();
	} else {
		$('.videocamp').show();
	}
	// console.log('hashBack1 -> ', hashBack)
	setTimeout(function(){
		hashBack = true;
		console.log('hashBack2 -> ', hashBack)
	}, 50);
});

window.onhashchange = locationHashChanged;


function locationHashChanged() {
	// console.log('locationHashChanged()');
    if (window.location.hash) {
    	// console.log('hashBack3 -> ', hashBack)
    	if (hashBack) {
    		console.log('hashback true if');
	        var currentURL = window.location.href;
	        // console.log('currentUrl->' +currentURL);

	        // dodatno
	        var splitedURL = currentURL.split('/en/');



	        if (splitedURL.length > 1) {
	        	djura = false;
	        	window.location.href = '/en/products';
	        } else {
	        	djura = false;
	        	window.location.href = '/proizvodi';
	        }


	        return;
	        // dodatno kraj 27.10.



			var splitedURL = currentURL.split("#");
			// console.log('splitedURL ->' +splitedURL);
			var slugClass = splitedURL[1];
			// console.log('slugClass->' +slugClass);
			var goToSlide = $('.sku-slider').find('.'+slugClass+':not(.slick-cloned)').attr('data-slick-index');
			// console.log('goToSlide->' +goToSlide);
			$('.sku-slider').slick('slickGoTo', goToSlide);

    	}

    } else {
    	var currentURL = window.location.href;
        // console.log('currentUrl->' +currentURL);

        // dodatno
        var splitedURL = currentURL.split('/en/');



        if (splitedURL.length > 1) {
        	djura = false;
        	window.location.href = '/en/products';
        } else {
        	djura = false;
        	window.location.href = '/proizvodi';
        }
    }
}

/*
	SKUs TABS
*/
var currentVideo, currentProduct;
function skuTabsSetup()
{
	if($('#sku-proizvodi-page')[0] != undefined)
	{
		$('.info-slider').slick({
			slidesToShow: 1,
			fade: true,
			arrows: false,
			swipe: false,
			initialSlide: goToSlideHelper
		});

		var video = $('.info-slider').children(':first').find('.video-campaign');

		if(!video.length) {
			$('.videocamp').hide();
			$('.nutri').addClass('active');
		}
	}
}

$( document ).on('click', '.tab', function(e){
	$('.tab').removeClass('active');
	$(this).addClass('active');

	$(this).parent().parent().find(".box").removeClass('active');
	$( $(this).data('uid') ).addClass('active');
	if(currentVideo == null || currentVideo == undefined)
	{
		currentVideo = afterglow.getPlayer('video-0');
	}
	else
	{
		currentVideo = afterglow.getPlayer('video-' + (currentProduct));
	}
	if(currentVideo != null && currentVideo != undefined && currentVideo != false)
	{
		if(!currentVideo.paused())
		{
			currentVideo.pause();
		}
	}
});

$(document).on('beforeChange', '.info-slider', function(event, slick, currentSlide, nextSlide){
	currentProduct = nextSlide;
	currentVideo = afterglow.getPlayer('video-' + (currentSlide));
	if(currentVideo != null && currentVideo != undefined && currentVideo != false)
	{
		if(!currentVideo.paused())
		{
			currentVideo.pause();
		}
	}
});

/*
	PRELOADER
*/

// var container;
// var width;
// var height;
// var wave;
// var percentLoaded;

// var waveWidth;
// var waveHeight;
// var waveDelta;
// var speed;
// var wavePoints;

// var points;

// var lastUpdate;
// var totalTime;

// var realPercentLoaded = 1; // od 0 - 1
// var fakePreloadInterval;

// function preloaderSetup()
// {
// 	if($('div#preload-page')[0] != undefined)
// 	{
// 		container = document.body;
// 		width = container.offsetWidth;
// 		height = container.offsetHeight;
// 		wave = document.getElementById('wave');
// 		percentLoaded = 0;

// 		waveWidth = container.offsetWidth;  		// Wave SVG width (usually container width)
// 		waveHeight = height * (1 - percentLoaded);  // Position from the top of container
// 		waveDelta = 15;                     		// Wave amplitude
// 		speed = .5;                         		// Wave animation speed
// 		wavePoints = 3;                     		// How many point will be used to compute our wave

// 		points = [];

// 		lastUpdate;
// 		totalTime = 0;

// 		fakePreloadInterval = setInterval(fakeLoading,5);

// 		tick();
// 	}
// }

// function calculateWavePoints(factor) {
// 	var points = [];

// 	for (var i = 0; i <= wavePoints; i++) {
// 		var x = i/wavePoints * waveWidth;
// 		var sinSeed = (factor + (i + i % wavePoints)) * speed * 100;
// 		var sinHeight = Math.sin(sinSeed / 100) * waveDelta;
// 		var yPos = Math.sin(sinSeed / 100) * sinHeight  + waveHeight;
// 		points.push({x: x, y: yPos});
// 	}

// 	return points;
// }

// function buildPath(points) {
// 	var SVGString = 'M ' + points[0].x + ' ' + points[0].y;

// 	var cp0 = {
// 		x: (points[1].x - points[0].x) / 2,
// 		y: (points[1].y - points[0].y) + points[0].y + (points[1].y - points[0].y)
// 	};

// 	SVGString += ' C ' + cp0.x + ' ' + cp0.y + ' ' + cp0.x + ' ' + cp0.y + ' ' + points[1].x + ' ' + points[1].y;

// 	var prevCp = cp0;
// 	var inverted = -1;

// 	for (var i = 1; i < points.length-1; i++) {
// 		var cpLength = Math.sqrt(prevCp.x * prevCp.x + prevCp.y * prevCp.y);
// 		var cp1 = {
// 			x: (points[i].x - prevCp.x) + points[i].x,
// 			y: (points[i].y - prevCp.y) + points[i].y
// 		};

// 		SVGString += ' C ' + cp1.x + ' ' + cp1.y + ' ' + cp1.x + ' ' + cp1.y + ' ' + points[i+1].x + ' ' + points[i+1].y;
// 		prevCp = cp1;
// 		inverted = -inverted;
// 	};

// 	SVGString += ' L ' + width + ' ' + height;
// 	SVGString += ' L 0 ' + height + ' Z';
// 	return SVGString;
// }

// function tick() {
// 	var now = window.Date.now();

// 	if (lastUpdate) {
// 		var elapsed = (now-lastUpdate) / 1000;
// 		lastUpdate = now;

// 		totalTime += elapsed;

// 		var factor = totalTime*Math.PI;
// 		wave.setAttribute('d', buildPath(calculateWavePoints(factor)));
// 	} else {
// 		lastUpdate = now;
// 	}

// 	window.requestAnimationFrame(tick);
// };

// function updateLoadEvent()
// {
// 	waveHeight = height * (1 - percentLoaded);
// }

// function fakeLoading()
// {
// 	if(percentLoaded < realPercentLoaded + .1)
// 	{
// 		percentLoaded += .0025;
// 	}
// 	else
// 	{
// 		percentLoaded = realPercentLoaded + .1;
// 		window.clearInterval(fakePreloadInterval);
// 		$('#preload-page').addClass('active');
// 		setTimeout(function() {
// 			$('#preload-page').remove();
// 			initPage();
// 		}, 1600);
// 	}

// 	updateLoadEvent();
// }

function preloaderSetup()
{
	$('body').imagesLoaded( function() {
		$('#preload-page').addClass('active');
		$('#preload-drop').addClass('active');
		setTimeout(function() {
			$('#preload-page').remove();
			initPage();
		}, 1750);
	});
}


function initPage()
{
	if(window.innerWidth >= mobileBreakPoint)
	{
		showHiddenText();
		showHiddenButtons();
	}

	if($('#home-page')[0] != undefined || $('#maslac-page')[0] != undefined)
	{
		slickHiddenText('.hero-slider .slick-center');
	}

	var arrows = $( ".hero-wrap" ).find('.hero-arrows');

	if(arrows[0] != undefined && !arrows.hasClass('active'))
	{
		arrows.addClass('active');
	}
	$('#junior-page #hero').find('.text').addClass('active');

	if($('#maslac-page')[0] != undefined)
	{
		positionSmear();
	}

	if(scrollbar)
	{
		scrollbar.update();
	}
}

function setActiveNavItem() {
	if ( $('#home-page').length > 0 ) {
		$('.nav-item').removeClass('active');
		$('.nav-item[data-class="home-page"]').addClass('active');
	}
	if ( $('#kompanija-page').length > 0 ) {
		$('.nav-item').removeClass('active');
		$('.nav-item[data-class="kompanija-page"]').addClass('active');
	}
	if ( $('#proizvodi-page').length > 0 ) {
		$('.nav-item').removeClass('active');
		$('.nav-item[data-class="proizvodi-page"]').addClass('active');
	}
	if ( $('#vesti-page').length > 0 ) {
		$('.nav-item').removeClass('active');
		$('.nav-item[data-class="vesti-page"]').addClass('active');
	}
	if ( $('#izvestaji-page').length > 0 ) {
		$('.nav-item').removeClass('active');
		$('.nav-item[data-class="izvestaji-page"]').addClass('active');
	}
	if ( $('#faq-page').length > 0 ) {
		$('.nav-item').removeClass('active');
		$('.nav-item[data-class="faq-page"]').addClass('active');
	}
	if ( $('#kontakt-page').length > 0 ) {
		$('.nav-item').removeClass('active');
		$('.nav-item[data-class="kontakt-page"]').addClass('active');
	}
	if ( $('#kuhinjica-page').length > 0 ) {
		$('.nav-item').removeClass('active');
		$('.nav-item[data-class="kuhinjica-page"]').addClass('active');
	}
	if ( $('#junior-page').length > 0 ) {
		$('.nav-item').removeClass('active');
		$('.nav-item[data-class="junior-page"]').addClass('active');
	}
}
function calculatePercent(a, b) {
	return a/b*100;
}
function productZoomOverlay() {
	if ($('.product-zoom-overlay').length > 0) {
		$(document).on('click', '.product-zoom-overlay .image-box', function(event) {
			var x = event.clientX;
			var y = event.clientY;
			var imgBoxHeight = $(this).outerHeight();
			var imgBoxWidth = $(this).outerWidth();
			var imgBoxOffsetTop = $(this).offset().top;
			var imgBoxOffsetLeft = $(this).offset().left;
			var imgTop = calculatePercent(y-imgBoxOffsetTop,imgBoxHeight);
			var imgLeft = calculatePercent(x-imgBoxOffsetLeft,imgBoxWidth);
			if (!$(this).hasClass('zoomed')) {
				$(this).find('img').css({
					top: 100 - imgTop + '%',
					left: 100 - imgLeft + '%'
				});
			} else {
				$(this).find('img').css({
					top: '50%',
					left: '50%'
				});
			}
			$(this).toggleClass('zoomed');
			// console.log('x -> ', x);
			// console.log('y -> ', y);
			// console.log('imgBoxHeight -> ', imgBoxHeight);
			// console.log('imgBoxWidth -> ', imgBoxWidth);
			// console.log('imgBoxOffsetTop -> ', imgBoxOffsetTop);
			// console.log('imgBoxOffsetLeft -> ', imgBoxOffsetLeft);
			// console.log('img top -> ', imgTop);
			// console.log('img left -> ', imgLeft);
		});
		$(document).on('mousemove', function(event) {
			if (event.target == $('.product-zoom-overlay .image-box').get(0)) {
				event.preventDefault();
				var x = event.clientX;
				var y = event.clientY;
				var imgBoxHeight = $('.product-zoom-overlay .image-box').outerHeight();
				var imgBoxWidth = $('.product-zoom-overlay .image-box').outerWidth();
				var imgBoxOffsetTop = $('.product-zoom-overlay .image-box').offset().top;
				var imgBoxOffsetLeft = $('.product-zoom-overlay .image-box').offset().left;
				var imgTop = calculatePercent(y-imgBoxOffsetTop,imgBoxHeight);
				var imgLeft = calculatePercent(x-imgBoxOffsetLeft,imgBoxWidth);
				if ($('.product-zoom-overlay .image-box').hasClass('zoomed')) {
					$('.product-zoom-overlay .image-box img').css({
						top: 100 - imgTop + '%',
						left: 100 - imgLeft + '%'
					});
				}
			} else {
				// $('.product-zoom-overlay .image-box img').css({
				// 	top: 50 + '%',
				// 	left: 50 + '%'
				// });
				// $('.product-zoom-overlay .image-box').removeClass('zoomed');
			}
		});
	}
}

$(document).on('click', '.junior-item', function(event) {
	event.preventDefault();
	var clickedSrc = $(this).find('.proizvod').attr('data-src');
	$('.product-zoom-overlay .image-box img').attr('src', clickedSrc);
	$('.product-zoom-overlay').fadeIn();
	// console.log('clickedSrc -> ', clickedSrc);
});
$(document).on('click', '.close-zoom-overlay', function(event) {
	event.preventDefault();
	$('.product-zoom-overlay').fadeOut(function() {
		$('.product-zoom-overlay .image-box img').css({
			top: 50 + '%',
			left: 50 + '%'
		});
		$('.product-zoom-overlay .image-box').removeClass('zoomed');
	});
});

$(window).resize(function() {
	if(this.resizeTO) clearTimeout(this.resizeTO);
	this.resizeTO = setTimeout(function() {
		// $(this).trigger('resizeEnd');
	}, 500);

	if($('#maslac-page')[0] != undefined)
	{
		positionSmear();
	}

	var windowWidth = $(window).outerWidth();
	// setJuniorOverlayWidth(windowWidth);
	setJuniorHeadingWidth();

	if(window.innerWidth > window.innerHeight)
	{
		$('#navigation-mobile').addClass('landscape');
	}
	else
	{
		$('#navigation-mobile').removeClass('landscape');
	}
});

$('.scroll-preventer').on('touchmove', function(e) {
    e.preventDefault();
    // e.stopPropagation();
});
