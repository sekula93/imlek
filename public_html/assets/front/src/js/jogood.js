var anim;
var viewportHeight = $(window).outerHeight();
var shapes = [
    {
        name: 'jagoda',
        color: '#991b20',
        link: '/proizvodi/jogood#jogood-jagoda-330g'
    },
    {
        name: 'kajsija',
        color: '#fda341',
        link: '/proizvodi/jogood#jogood-kajsija-330g-2'
    },
    {
        name: 'svoce',
        color: '#92278f',
        link: '/proizvodi/jogood#jogood-sumsko-voce-330g'
    },
    {
        name: 'visnja',
        color: '#92278f',
        link: '/proizvodi/jogood#jogood-visnja-330g'
    },
    {
        name: 'nar',
        color: '#92278f',
        link: '/proizvodi/jogood#jogood-breskva-nar-330g'
    },
    {
        name: 'ananas',
        color: '#92278f',
        link: '/proizvodi/jogood#jogood-ananas-330g-2'
    },
    {
        name: 'mango',
        color: '#92278f',
        link: '/proizvodi/jogood#jogood-tropic-mix-330g'
    },
];
var greenBg = $('img.green-bg');

    Barba.Dispatcher.on('transitionCompleted', function (currentStatus, prevStatus) {
        
        if ($('#jogood-page')[0] != undefined) {
            jogoodHeroParallax();
            anim = bodymovin.loadAnimation({
                container: document.getElementById('shape'),
                renderer: 'svg',
                loop: false,
                autoplay: false,
                path: basePath + '/assets/front/jogood-shape.json'
            });
            
            $('.jogood-slider').slick({
                dots: false,
                infinite: true,
                speed: 700,
                fade: true,
                cssEase: 'linear',
                autoplay: false,
                autoplaySpeed: 3000,
                nextArrow: $('.arrows-jogood--next'),
                prevArrow: $('.arrows-jogood--prev')
            });
            svg4everybody();
            brokenHiddenText('.hidden-text');

            $('.splash-space').css('height', greenBg.outerHeight() );
            
            if (window.innerWidth >= mobileBreakPoint) {
                setTimeout(function () {
                    brokenHiddenText('.hidden-text');
                }, 1500);
            }

            resizeJogoodVideo();
        } else {
            if ($('body').hasClass('jogood-bg')) 
            {
                $('body').removeClass('jogood-bg');
            }
        }
    });
var sf = 114/7;
var max = 114;
var to = 0;
var from = 0;
var direction;

$('.jogood-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    if (shapes[nextSlide] !== null && shapes[nextSlide] !== undefined) {
        var decl = currentSlide - nextSlide;
        direction = decl == 1 || decl == (slick.slideCount - 1) * (-1);
        if (!direction) {
            to = to + sf;
            from = to - sf;
            if (to > max) {
                to = sf;
                from = to - sf;
            }
        } else {
            to = to - sf;
            from = to + sf;
            if (to < 0) {
                from = max;
                to = from - sf;
            }
        }
        $('#product-hero').attr('data-product', '');
        $('#product-hero').attr('data-product', shapes[nextSlide].name);
        $('#product-link').attr('href', '');
        $('#product-link').attr('href', shapes[nextSlide].link);
        anim.setSpeed(0.85);
        anim.playSegments([from, to], true);
    }
    console.log('from -> ', from);
    console.log('to -> ', to);
    console.log('currentSlide -> ', currentSlide);
    console.log('nextSlide -> ', nextSlide);
    console.log('event -> ', event);
    console.log('slick -> ', slick);
    console.log('direction -> ', direction);
        
});

function jogoodHeroParallax()
{
    var scene = document.getElementsByClassName('parallax-span');
    var scene2 = document.getElementsByClassName('parallax-bg');
    var fruits = document.getElementsByClassName('parallax-fruit');

    var parallaxInstance = new Parallax(scene[0], {
        relativeInput: true,
    });
    var parallaxInstance2 = new Parallax(scene2[0], {
        relativeInput: true,
        invertY: false,
        invertX: false,
    });
    for (var index = 0; index < fruits.length; index++) {
        var parallaxInstanceFruits = new Parallax(fruits[index], {
            relativeInput: true,
            calibrateX: true,
            calibrateY: true,
            scalarX: 9,
            scalarY: 9

        });
    }
}

function fruitsTransformY() {
    if ($('#fruit-prlx-wrap').length > 0 && scrollbar !== undefined) {
        var contentWrap = document.getElementById('fruit-prlx-wrap');
        var scrollPos = scrollbar.scrollTop;
        var x = scrollPos + viewportHeight;
        if (scrollbar.isVisible(contentWrap)) {
            $('img.jagoda-prlx').css('transform', 'translateY(-' + scrollPos * 0.2 + 'px)');
            $('img.ananas-prlx').css('transform', 'translateY(' + scrollPos * 0.15 + 'px) rotate(-' + scrollPos / 20 * 0.1 + 'deg)');
            $('img.kupina-prlx').css('transform', 'translateY(-' + scrollPos * 0.1 + 'px)');
            $('img.kajsija-prlx').css('transform', 'translateY(' + scrollPos * 0.3 + 'px) rotate(-' + scrollPos / 4 * 0.1 + 'deg)');
            $('img.visnja-prlx').css('transform', 'translateY(' + scrollPos * 0.15 + 'px)');
            // console.log('USAOO !!! -> ', scrollPos);
        }
    }
}


$(window).resize(function () {
    if ($('#jogood-page')[0] != undefined) {
        resizeJogoodVideo();
        $('.splash-space').css('height', greenBg.outerHeight());
    }
});

function resizeJogoodVideo() {
    var videoWidth = $('#jogood-video').width();
    var newHeight = Math.ceil(videoWidth * 0.562);
    $('#jogood-video').css('height', newHeight + 'px');
}