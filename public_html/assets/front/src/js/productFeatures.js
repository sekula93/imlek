function setRadialWrapperHeight() {
    if ($('.radial-wrapper').length > 0) {
        var elementWidth = $('.radial-wrapper').outerWidth();
        $('.radial-wrapper').css('height', elementWidth + 'px');
    }
}

function setJuniorAboutSectionWidth() {
    var windowWidth = $(window).outerWidth();
    if ($('#junior-page section#about').length > 0) {
        $('#junior-page section#about').css('width', windowWidth + 'px');
    }
}

function initJuniorSlider() {
    if ($('.junior-slider').length > 0) {
        $('.junior-slider').slick({
            slideToShow: 1,
            slideToScroll: 1,
            infinite: true,
            fade: true,
            speed: 600,
            cssEase: 'linear',
            dots: false,
            arrows: false,
            draggable: false,
            swipe: false
        });

        $('.junior-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
            $('.slide-transition').addClass('end');
        });

        $('.junior-slider-buttons .prev').click(function(event) {
            var prevSlide;
            var currentSlide = $('.junior-slider').slick('slickCurrentSlide');
            if (currentSlide <= 0) {
                prevSlide = 7;
            } else {
                prevSlide = currentSlide - 1;
            }
            $('.slide-transition').removeClass('start end');
            $('.slide-transition').addClass('start');
            setTimeout(function() {
                $('.junior-slider').slick('slickPrev');
                $('.junior-icons li').removeClass('active');
                $('.junior-icons li[data-slide="' + prevSlide + '"]').addClass('active');
            }, 200);
        });

        $('.junior-slider-buttons .next').click(function(event) {
            var nextSlide;
            var currentSlide = $('.junior-slider').slick('slickCurrentSlide');
            if (currentSlide > 6) {
                nextSlide = 0;
            } else {
                nextSlide = currentSlide + 1;
            }
            $('.slide-transition').removeClass('start end');
            $('.slide-transition').addClass('start');
            setTimeout(function() {
                $('.junior-slider').slick('slickNext');
                $('.junior-icons li').removeClass('active');
                $('.junior-icons li[data-slide="' + nextSlide + '"]').addClass('active');
            }, 200);
        });
        $('.junior-icons li').click(function(event) {
            var iconIndex = $(this).attr('data-slide');
            $('.slide-transition').removeClass('start end');
            $('.slide-transition').addClass('start');
            $('.junior-icons li').removeClass('active');
            $(this).addClass('active');
            setTimeout(function() {
                $('.junior-slider').slick('slickGoTo', iconIndex);
            }, 200);
        });
    }
}

function initJuniorSliderMobile() {
    if ($('.junior-slider-mobile .junior-icons').length > 0) {
        $('.junior-slider-mobile .junior-icons').slick({
            arrows: false,
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 3,
            touchMove: true,
            // infinite: false,
            asNavFor: '.junior-slider-mobile .content-slider',
        });
        $('.junior-slider-mobile .content-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            fade: true,
            speed: 500,
            arrows: false,
            dots: false,
            asNavFor: '.junior-slider-mobile .junior-icons',
        });
        $('.junior-arrows-mobile .sku-prev').click(function(event) {
            $('.junior-slider-mobile .slide-transition').removeClass('start end');
            $('.junior-slider-mobile .slide-transition').addClass('start');
            console.log($(this));
            setTimeout(function() {
                $('.junior-slider-mobile .junior-icons').slick('slickPrev');
            }, 100);
        });
        $('.junior-arrows-mobile .sku-next').click(function(event) {
            $('.junior-slider-mobile .slide-transition').removeClass('start end');
            $('.junior-slider-mobile .slide-transition').addClass('start');
            console.log($(this));
            setTimeout(function() {
                $('.junior-slider-mobile .junior-icons').slick('slickNext');
            }, 100);
        });
        $('.junior-slider-mobile .junior-icons').on('afterChange', function(event, slick, currentSlide, nextSlide){
            setTimeout(function(){
                $('.junior-slider-mobile .slide-transition').addClass('end');
            }, 100);
        });
    }
}


$(document).ready(function() {
    setJuniorAboutSectionWidth();
    setRadialWrapperHeight();
    // initJuniorSlider();
    // initJuniorSliderMobile();


});

$(window).resize(function() {
    setRadialWrapperHeight();
    setJuniorAboutSectionWidth();
});