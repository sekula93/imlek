function initMap() {
	var beograd = {lat: 44.9283795, lng: 20.4373813};
	var subotica = {lat: 46.065748, lng: 19.6840899};
	var nis = {lat: 43.3120543, lng: 21.8651748};
	var bosna = {lat: 45.1931108, lng: 16.764494};
	var makedonija = {lat: 41.0424593, lng: 21.3548469};
	var kotor = {lat: 42.382426, lng: 18.748233};
	var zajecar = {lat: 43.8955285, lng: 22.2907501};


	var map = new google.maps.Map(document.getElementById('map'), {
	  center: beograd,
	  disableDefaultUI: true,
	  zoom: 6,
	  gestureHandling: 'cooperative',
	  styles: [
		  {
			"elementType": "geometry",
			"stylers": [
			  {
				"color": "#1e232c"
			  }
			]
		  },
		  {
			"elementType": "labels.icon",
			"stylers": [
			  {
				"visibility": "off"
			  }
			]
		  },
		  {
			"elementType": "labels.text.fill",
			"stylers": [
			  {
				"color": "#757575"
			  }
			]
		  },
		  {
			"elementType": "labels.text.stroke",
			"stylers": [
			  {
				"color": "#212121"
			  }
			]
		  },
		  {
			"featureType": "administrative",
			"elementType": "geometry",
			"stylers": [
			  {
				"color": "#757575"
			  }
			]
		  },
		  {
			"featureType": "administrative",
			"elementType": "labels.text.stroke",
			"stylers": [
			  {
				"visibility": "off"
			  }
			]
		  },
		  {
			"featureType": "administrative.country",
			"elementType": "geometry.stroke",
			"stylers": [
			  {
				"color": "#53575f"
			  }
			]
		  },
		  {
			"featureType": "administrative.country",
			"elementType": "labels.text.fill",
			"stylers": [
			  {
				"color": "#5a5f69"
			  }
			]
		  },
		  {
			"featureType": "administrative.land_parcel",
			"stylers": [
			  {
				"visibility": "off"
			  }
			]
		  },
		  {
			"featureType": "administrative.locality",
			"elementType": "geometry.fill",
			"stylers": [
			  {
				"visibility": "on"
			  }
			]
		  },
		  {
			"featureType": "administrative.locality",
			"elementType": "labels.icon",
			"stylers": [
			  {
				"color": "#53575f"
			  },
			  {
				"visibility": "on"
			  },
			  {
				"weight": 8
			  }
			]
		  },
		  {
			"featureType": "administrative.locality",
			"elementType": "labels.text.fill",
			"stylers": [
			  {
				"color": "#53575f"
			  }
			]
		  },
		  {
			"featureType": "administrative.province",
			"elementType": "geometry.stroke",
			"stylers": [
			  {
				"visibility": "off"
			  }
			]
		  },
		  {
			"featureType": "landscape.natural.terrain",
			"elementType": "geometry.fill",
			"stylers": [
			  {
				"color": "#23272d"
			  }
			]
		  },
		  {
			"featureType": "poi",
			"elementType": "labels.text.fill",
			"stylers": [
			  {
				"color": "#757575"
			  }
			]
		  },
		  {
			"featureType": "poi.park",
			"elementType": "geometry",
			"stylers": [
			  {
				"color": "#181818"
			  }
			]
		  },
		  {
			"featureType": "poi.park",
			"elementType": "labels.text.fill",
			"stylers": [
			  {
				"color": "#53575f"
			  },
			  {
				"visibility": "on"
			  }
			]
		  },
		  {
			"featureType": "poi.park",
			"elementType": "labels.text.stroke",
			"stylers": [
			  {
				"color": "#1b1b1b"
			  },
			  {
				"visibility": "off"
			  }
			]
		  },
		  {
			"featureType": "road",
			"stylers": [
			  {
				"visibility": "off"
			  }
			]
		  },
		  {
			"featureType": "road",
			"elementType": "geometry.fill",
			"stylers": [
			  {
				"color": "#2c2c2c"
			  }
			]
		  },
		  {
			"featureType": "road",
			"elementType": "labels.text.fill",
			"stylers": [
			  {
				"color": "#8a8a8a"
			  }
			]
		  },
		  {
			"featureType": "road.arterial",
			"elementType": "geometry",
			"stylers": [
			  {
				"color": "#373737"
			  }
			]
		  },
		  {
			"featureType": "road.highway",
			"elementType": "geometry",
			"stylers": [
			  {
				"color": "#000000"
			  }
			]
		  },
		  {
			"featureType": "road.highway",
			"elementType": "geometry.fill",
			"stylers": [
			  {
				"color": "#000000"
			  }
			]
		  },
		  {
			"featureType": "road.local",
			"elementType": "labels.text.fill",
			"stylers": [
			  {
				"color": "#616161"
			  }
			]
		  },
		  {
			"featureType": "transit",
			"stylers": [
			  {
				"visibility": "on"
			  }
			]
		  },
		  {
			"featureType": "transit",
			"elementType": "labels.icon",
			"stylers": [
			  {
				"color": "#53575f"
			  },
			  {
				"visibility": "off"
			  }
			]
		  },
		  {
			"featureType": "transit",
			"elementType": "labels.text.fill",
			"stylers": [
			  {
				"color": "#53575f"
			  }
			]
		  },
		  {
			"featureType": "transit",
			"elementType": "labels.text.stroke",
			"stylers": [
			  {
				"visibility": "off"
			  }
			]
		  },
		  {
			"featureType": "transit.station.airport",
			"stylers": [
			  {
				"visibility": "on"
			  }
			]
		  },
		  {
			"featureType": "transit.station.airport",
			"elementType": "labels.text",
			"stylers": [
			  {
				"visibility": "on"
			  }
			]
		  },
		  {
			"featureType": "transit.station.airport",
			"elementType": "labels.text.fill",
			"stylers": [
			  {
				"color": "#53575f"
			  },
			  {
				"visibility": "on"
			  }
			]
		  },
		  {
			"featureType": "transit.station.airport",
			"elementType": "labels.text.stroke",
			"stylers": [
			  {
				"visibility": "on"
			  }
			]
		  },
		  {
			"featureType": "water",
			"elementType": "geometry",
			"stylers": [
			  {
				"color": "#000000"
			  }
			]
		  },
		  {
			"featureType": "water",
			"elementType": "labels.text.fill",
			"stylers": [
			  {
				"color": "#3d3d3d"
			  }
			]
		  }
		]
	});

	var marker_001 = new google.maps.Marker({
		position: beograd,
		map: map
	});

	var contentString_001 = '<div class="map-info-content">'+
		'<h1 class="firstHeading">A.D. IMLEK BEOGRAD</h1>'+
		'<div class="bodyContent">'+
		'<p>Industrijsko naselje bb</p>'+
		'<p>11213 Padinska Skela</p>'+
		'</div>'+
		'</div>';

	var infowindow_001 = new google.maps.InfoWindow({
		content: contentString_001
	});

	marker_001.addListener('mouseover', function() {
		infowindow_001.open(map, marker_001);
	});

	marker_001.addListener('mouseout', function() {
		infowindow_001.close();
	});

	var marker_002 = new google.maps.Marker({
		position: subotica,
		map: map
	});

	var contentString_002 = '<div class="map-info-content">'+
		'<h1 class="firstHeading">MLEKARA SUBOTICA</h1>'+
		'<div class="bodyContent">'+
		'<p>Tolminska 10</p>'+
		'<p>24000 Subotica</p>'+
		'</div>'+
		'</div>';

	var infowindow_002 = new google.maps.InfoWindow({
		content: contentString_002
	});

	marker_002.addListener('mouseover', function() {
		infowindow_002.open(map, marker_002);
	});

	marker_002.addListener('mouseout', function() {
		infowindow_002.close();
	});

	var marker_003 = new google.maps.Marker({
		position: nis,
		map: map
	});

	var contentString_003 = '<div class="map-info-content">'+
		'<h1 class="firstHeading">NIŠKA MLEKARA</h1>'+
		'<div class="bodyContent">'+
		'<p>Nikodija Stojanovića Tatka 28b</p>'+
		'<p>18000 Niš</p>'+
		'</div>'+
		'</div>';

	var infowindow_003 = new google.maps.InfoWindow({
		content: contentString_003
	});

	marker_003.addListener('mouseover', function() {
		infowindow_003.open(map, marker_003);
	});

	marker_003.addListener('mouseout', function() {
		infowindow_003.close();
	});

	var marker_004 = new google.maps.Marker({
		position: bosna,
		map: map
	});

	var contentString_004 = '<div class="map-info-content">'+
		'<h1 class="firstHeading">MLIJEKOPRODUKT d.o.o</h1>'+
		'<div class="bodyContent">'+
		'<p>Vrioci bb</p>'+
		'<p>79240 Kozarska Dubica</p>'+
		'</div>'+
		'</div>';

	var infowindow_004 = new google.maps.InfoWindow({
		content: contentString_004
	});

	marker_004.addListener('mouseover', function() {
		infowindow_004.open(map, marker_004);
	});

	marker_004.addListener('mouseout', function() {
		infowindow_004.close();
	});

	var marker_005 = new google.maps.Marker({
		position: makedonija,
		map: map
	});

	var contentString_005 = '<div class="map-info-content">'+
		'<h1 class="firstHeading">MLEKARA AD BITOLA</h1>'+
		'<div class="bodyContent">'+
		'<p>Ул. Гурчин Наумов Пљакот бр.1</p>'+
		'<p>7000 Битола</p>'+
		'</div>'+
		'</div>';

	var infowindow_005 = new google.maps.InfoWindow({
		content: contentString_005
	});

	marker_005.addListener('mouseover', function() {
		infowindow_005.open(map, marker_005);
	});

	marker_005.addListener('mouseout', function() {
		infowindow_005.close();
	});

	// location start
	var marker_006 = new google.maps.Marker({
		position: kotor,
		map: map
	});

	var contentString_006 = '<div class="map-info-content">'+
		'<h1 class="firstHeading">IMLEK BOKA d.o.o.</h1>'+
		'<div class="bodyContent">'+
		'<p>Industrijska zona bb</p>'+
		'<p>85318 Radanovići, Kotor </p>'+
		'</div>'+
		'</div>';

	var infowindow_006 = new google.maps.InfoWindow({
		content: contentString_006
	});

	marker_006.addListener('mouseover', function() {
		infowindow_006.open(map, marker_006);
	});

	marker_006.addListener('mouseout', function() {
		infowindow_006.close();
	});
	// location end


	// location start
	var marker_007 = new google.maps.Marker({
		position: zajecar,
		map: map
	});

	var contentString_007 = '<div class="map-info-content">'+
		'<h1 class="firstHeading">POGON ZAJEČAR</h1>'+
		'<div class="bodyContent">'+
		'<p>Rasadnička bb,</p>'+
		'<p>19000 Zaječar</p>'+
		'</div>'+
		'</div>';

	var infowindow_007 = new google.maps.InfoWindow({
		content: contentString_007
	});

	marker_007.addListener('mouseover', function() {
		infowindow_007.open(map, marker_007);
	});

	marker_007.addListener('mouseout', function() {
		infowindow_007.close();
	});
	// location end


  }