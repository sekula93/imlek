function popShare(url, winWidth, winHeight) {
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
}

$(document).on('click', '#tw', function(e) {
    e.preventDefault();

    // var title =  $('.recipe-title').text();
    // var picture = $(this).parent().attr('data-image');
    // var shareUrl = $(this).parent().attr('data-url');


    // var twShareText = ''
    // twShareText = encodeURIComponent('Imlek junior: '+title+' '+shareUrl);

    popShare("http://twitter.com/intent/tweet?text=Saznaj sve o Moja Kravica Junior mleku! http://imlek.rs/junior", 400, 300)
});

$(document).on('click', '#tw_maslac', function(e) {
    e.preventDefault();

    // var title =  $('.recipe-title').text();
    // var picture = $(this).parent().attr('data-image');
    // var shareUrl = $(this).parent().attr('data-url');


    // var twShareText = ''
    // twShareText = encodeURIComponent('Imlek junior: '+title+' '+shareUrl);

    popShare("http://twitter.com/intent/tweet?text=Saznaj sve o Nazmazu od Maslaca! http://imlek.rs/maslac", 400, 300)
});

$(document).on('click', '#tw_pavlaka', function(e) {
    e.preventDefault();

    popShare("http://twitter.com/intent/tweet?text=Saznaj sve o Moja Kravica Kiseloj Pavlaci! http://imlek.rs/pavlaka", 400, 300)
});

$(document).on('click', '#tw_jogood', function(e) {
    e.preventDefault();

    popShare("http://twitter.com/intent/tweet?text=Osetite nezaboravnu senzaciju koja puca od voća! http://imlek.rs/jogood-page", 400, 300)
});

$(document).on('click', '#in', function(event) {
    event.preventDefault();

    popShare("https://www.linkedin.com/shareArticle?mini=true&url=http://imlek.rs/junior&title=Imlek Junior%20&summary=Saznaj sve o Moja Kravica Junior mleku! http://imlek.rs/junior &source=", 400, 300);
    
}); 

$(document).on('click', '#in_maslac', function(event) {
    event.preventDefault();

    popShare("https://www.linkedin.com/shareArticle?mini=true&url=http://imlek.rs/junior&title=Imlek%20Namaz%20od%20Maslaca%20&summary=Saznaj sve o Namazu od Maslaca! http://imlek.rs/maslac &source=", 400, 300);
    
}); 

$(document).on('click', '#in_pavlaka', function(event) {
	event.preventDefault();

	popShare("https://www.linkedin.com/shareArticle?mini=true&url=http://imlek.rs/pavlaka&title=Imlek%20Kisela%20Pavlaka%20&summary=Saznaj sve o Moja Kravica Kiseloj Pavlaci! http://imlek.rs/pavlaka &source=", 400, 300);
	
}); 

$(document).on('click', '#in_jogood', function(event) {
	event.preventDefault();

    popShare("https://www.linkedin.com/shareArticle?mini=true&url=http://imlek.rs/jogood-page&title=Imlek%20Jogood%20&summary=Osetite nezaboravnu senzaciju koja puca od voća! http://imlek.rs/jogood-page &source=", 400, 300);
	
}); 

$(document).on('click', '#fb', function(event) {
    FB.ui({
      method: 'feed',
      link: 'http://imlek.rs/junior',
      caption: '',
    }, function(response){});

});     

$(document).on('click', '#fb_maslac', function(event) {
    event.preventDefault();
    FB.ui({
      method: 'feed',
      link: 'http://imlek.rs/maslac',
      caption: '',
    }, function(response){});

});  	

$(document).on('click', '#fb_pavlaka', function(event) {
    event.preventDefault();
    FB.ui({
      method: 'feed',
      link: 'http://imlek.rs/pavlaka',
      caption: '',
    }, function(response){});

}); 

$(document).on('click', '#fb_jogood', function (event) {
    event.preventDefault();
    FB.ui({
        method: 'feed',
        link: 'http://imlek.rs/jogood-page',
        caption: '',
    }, function (response) { });

}); 



// kompanija 
$(document).on('click', '.company-fb', function(event) {
    var link = window.location.href;
    FB.ui({
      method: 'feed',
      link: link,
      caption: 'An example caption',
    }, function(response){});

}); 

$(document).on('click', '.company-in', function(event) {
    event.preventDefault();
    var link = window.location.href;
    popShare("https://www.linkedin.com/shareArticle?mini=true&url="+link+"&title=Imlek%20&summary=Imlek je regionalna kompanija koja posluje na teritorijama Srbije, Crne Gore, Bosne i Hercegovine i Makedonije. "+ link +" &source=", 400, 300);
    
}); 


$(document).on('click', '.company-tw', function(e) {
    e.preventDefault();
    var link = window.location.href;
    // var title =  $('.recipe-title').text();
    // var picture = $(this).parent().attr('data-image');
    // var shareUrl = $(this).parent().attr('data-url');


    // var twShareText = ''
    // twShareText = encodeURIComponent('Imlek junior: '+title+' '+shareUrl);

    popShare("http://twitter.com/intent/tweet?text=Imlek je regionalna kompanija koja posluje na teritorijama Srbije, Crne Gore, Bosne i Hercegovine i Makedonije. "+ link +"", 400, 300)
});

// vesti share
$(document).on('click', '.fb-news', function(event) {
    var link = window.location.href;
    FB.ui({
      method: 'feed',
      link: link,
      caption: 'An example caption',
    }, function(response){});

}); 

$(document).on('click', '.in-news', function(event) {
    event.preventDefault();
    var link = window.location.href;
    var title = $('.title').text();

    // popShare("https://www.linkedin.com/shareArticle?mini=true&url="+link+"&title="+title+"%20&summary=Pročitaj "+ link +" &source=", 400, 300);
    popShare("https://www.linkedin.com/shareArticle?mini=true&url="+link+"&title="+title+"%20&"+ link +"&source=", 400, 300);
    
}); 
$(document).on('click', '.tw-news', function(e) {
    e.preventDefault();
    var link = window.location.href;
    var title = $('.title').text();
    // var picture = $(this).parent().attr('data-image');
    // var shareUrl = $(this).parent().attr('data-url');


    // var twShareText = ''
    // twShareText = encodeURIComponent('Imlek junior: '+title+' '+shareUrl);

    popShare("http://twitter.com/intent/tweet?text="+title+" - "+ link +"", 400, 300)
});

$(document).on('click', '.fb-recipe', function(event) {

    var link = window.location.href;
    FB.ui({
      method: 'feed',
      link: link,
      caption: 'An example caption',
    }, function(response){});
});

$(document).on('click', '.in-recipe', function(event) {

    var link = window.location.href;
    var title = $('.title').text();

    // popShare("https://www.linkedin.com/shareArticle?mini=true&url="+link+"&title="+title+"%20&summary=Pročitaj "+ link +" &source=", 400, 300);
    popShare("https://www.linkedin.com/shareArticle?mini=true&url="+link+"&title="+title+"%20&"+ link +"&source=", 400, 300);
    
}); 
$(document).on('click', '.tw-recipe', function(e) {
    var link = window.location.href;
    var title = $('.title').text();
    // var picture = $(this).parent().attr('data-image');
    // var shareUrl = $(this).parent().attr('data-url');


    // var twShareText = ''
    // twShareText = encodeURIComponent('Imlek junior: '+title+' '+shareUrl);

    popShare("http://twitter.com/intent/tweet?text="+title+" - "+ link +"", 400, 300)
});


