/**** FILES MANAGEMENT ****/
    // Disable defaultl drag'n'drop
    $(document).bind('drop dragover', function (e) {
        e.preventDefault();
    });


    // Uploader callbacks
    function afterUpload (e, data) {
        if(data.result.status == 'success') {

            $('.gallery').load(window.location.href+' .gallery', function (data) {
                // console.log(data);
                $('.toggle').toggles({on:false});
                $('.toggle.active').toggles({on:true});
                makeSortable();
            });
        } else {
            //print error
        }
        $('.fileinput-button').removeClass('dragover');
        $('.progress-bar').css('display', 'none');
        $('.progress-bar').css('width', '0%');
    }

    function uploadProgress(e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        // console.log(progress,e, data);
        $('.progress-bar').css('display', 'block');
        $('.progress-bar').css('width', progress + '%');
    }

    function uploadDragover(e, data) {
        e.preventDefault();
        var uploadElement = $(e.target).children();
        // console.log(uploadElement);
        uploadElement.addClass('dragover');
    }

    function uploadFail(e, data) {
        console.log(e, data);
    }

    $('.fileinput-button').on('dragleave', function (e) {
        $(this).removeClass('dragover');
    });

    // Uploaders setup
    $('.fileupload-photo').fileupload({
        dropZone: $('.photo-upload-button'),
        formData: { _token: csrf },
        dataType: 'json',
        dragover: function(e, data) {
            uploadDragover(e, data);
        },
        done: function(e, data){
            afterUpload(e, data);
        },
        fail: function(e, data) {
            uploadFail(e, data);
        },
        progressall: function(e, data) {
            uploadProgress(e, data);
        },
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');


    $(document).on('click', '.file-active', function() {
        var element = $(this);
        var id = element.parents('li').attr('data-id');
        var imageElement = element.parents('li').find('.file-thumb');
        $.ajax({
            type: 'PUT',
            url: basePath+'/admin/company-packshot/change-status',
            dataType: 'json',
            data: { _token : csrf, id: id},
            success: function(response) {
                if (response.status = 'success') {
                    if (response.state == 1) {
                        imageElement.removeClass('inactive');
                        imageElement.parent().find('.set-cover').show();
                        // console.log(response.state ,imageElement, imageElement.find('.set-cover'));
                        // element.children('i').removeClass('fa-square-o');
                        // element.children('i').addClass('fa-check-square-o');
                    } else {
                        imageElement.addClass('inactive');
                        imageElement.parent().find('.set-cover').hide();
                        // console.log(response.state ,imageElement, imageElement.find('.set-cover'));
                        // element.children('i').removeClass('fa-check-square-o');
                        // element.children('i').addClass('fa-square-o');
                    }
                }
            }
        });
    });

    $(document).on('click', '.set-cover:not(.image-cover)', function(){
        var element = $(this);
        var wrapperElement = element.parents('li');
        var id = wrapperElement.attr('data-id');
        $.ajax({
            type: 'PUT',
            url: basePath+'/admin/company-packshot/'+id+'/cover',
            dataType: 'json',
            data: { _token: csrf },
            success: function(response){
                if(response.status = 'success') {
                    wrapperElement.parents('ul').children().find('.file-active').show();
                    wrapperElement.parents('ul').find('button.remove-file').show();
                    $('.gallery .set-cover i').removeClass('fa-check-circle');
                    $('.gallery .set-cover').removeClass('image-cover');
                    $('.gallery .set-cover i').addClass('fa-circle-o');
                    $('.gallery li').removeClass('cover');

                    element.children('i').removeClass('fa-circle-o');
                    element.children('i').addClass('fa-check-circle');
                    element.addClass('image-cover');
                    wrapperElement.addClass('cover');
                    wrapperElement.find('.file-active').hide();
                    wrapperElement.find('button.remove-file').hide();
                    var src = wrapperElement.find('.file-thumb img').attr('src');
                    $('#cover-image').attr('src', src);
                }
            }
        });
    });

    $(document).on('click', '.remove-file', function(){
        var element = $(this);
        var wrapperElement = element.parents('li');
        var id = wrapperElement.attr('data-id');
        bootbox.confirm('Da li ste sigurni da želite da obrišete ovaj fajl?', function(confirmed) {
            if(confirmed) {
                $.ajax({
                    type: 'DELETE',
                    url: basePath+'/admin/company-packshot/'+id,
                    data: { _token: csrf },
                    dataType: 'json',
                    success: function(response){
                        if(response.status == 'success') {
                            wrapperElement.hide(200, function(){
                                wrapperElement.remove();
                            });
                        }
                    }
                });
            }
        });
    });

     /** Sortable **/
    function makeSortable() {
        $('.gallery.post-gallery').sortable({
            placeholder: "image sortable-placeholder",
            stop: function(e, ui){
                var data = $(this).sortable('serialize');
                $.ajax({
                    method: 'PUT',
                    url: basePath+'/admin/company-packshot/reorder',
                    dataType: 'json',
                    data: data+'&_token='+csrf,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
        });
        $('.gallery').disableSelection();
    }

    makeSortable();