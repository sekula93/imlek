$(document).on('click', '.new-row', function(e) {
    e.preventDefault();
    var row = $('.nutritive-table').find('tr:last()').clone();
    $('.nutritive-table tbody').append(processRow(row));
});

$(document).on('click', '.new-column', function(e) {
    e.preventDefault();
    var rows = $('.nutritive-table').find('tr');
    rows.each(function (index, row) {
        var cell = $(row).children().last().clone();
        $(row).append(processCell(cell, index));
    });
});

function processRow(row)
{
    var rowsCount = $('.nutritive-table tr').length;
    var inputs = row.find('input');
    inputs.val('');
    inputs.each(function(index, element) {
        var languageId = $(element).data('lang');
        var col = $(element).data('col');
        var newName = 'trans['+languageId+'][table]['+rowsCount+']['+col+']';
        $(element).attr('name', newName);
    });

    return row;
}

function processCell(cell, rowIndex)
{
    var inputs = cell.find('input')
    var colIndex = parseInt(inputs.attr('data-col'))+1;
    inputs.val('');
    inputs.attr('data-col', colIndex);
    inputs.each(function(index, input) {
        var languageId = $(input).data('lang');
        var newName = 'trans['+languageId+'][table]['+rowIndex+']['+colIndex+']';
        $(input).attr('name', newName);
    });

    return cell;
}

$(document).on('click', '.delete-nutri-row', function(event) {
  
    var element = $(this);
    var tr = $('.nutritive-table tbody tr:last-child');
    tr.remove();
    
}); 

$(document).on('click', '.delete-nutri-column', function(event) {
  
    var element = $(this);
    var tbody = $('.nutritive-table tbody');
    tbody.find('tr').each(function(index, el) {
        var td = $(this).children('td:last-child');
        var th = $(this).children('th:last-child');
        td.remove();
        th.remove();
    });
    
}); 
