function showPackshotDialog(e, data = null, update = false) {
    var packshotForm = $('.packshot-form').clone();
    if(update) {
        var packshotId = e.target.dataset.id;
        var nutritiveId = e.target.dataset.nutritiveId;
        packshotForm.find('input[name="trans[1][label]"]').val(e.target.dataset.label);
        packshotForm.find('input[name="trans[2][label]"]').val(e.target.dataset.label_en);
        packshotForm.find('input[name="trans[1][slug]"]').val(e.target.dataset.slug);
        packshotForm.find('input[name="trans[2][slug]"]').val(e.target.dataset.slug_en);
        packshotForm.find('input[name="trans[1][text]"]').val(e.target.dataset.text);
        packshotForm.find('input[name="trans[2][text]"]').val(e.target.dataset.text_en);
        packshotForm.find('input[name="link"]').val(e.target.dataset.link);
        packshotForm.find('select[name="nutritive_id"]').val(nutritiveId);
        packshotForm.find('select[name="nutritive_id"] option').each(function(index, element) {
            console.log($(element).val(), nutritiveId);
            if($(element).val() == nutritiveId) {
                console.log('true');
                $(element).prop('selected', true);
            }
        });

    }
    bootbox.dialog({
        message: packshotForm,
        title: 'Packshot',
        className: "packshot-form-modal",
        buttons: {
            cancel: {
                label: 'Odustani',
                className: "btn-default",
                callback: function(e) {
                    $('.fileinput-button').removeClass('dragover');
                }
            },
            main: {
                label: 'Potvrdi',
                className: "btn-primary",
                callback: function(e) {
                    var modalForEvent = $(e.currentTarget).parents('.modal-content');
                    var nutritive_id = modalForEvent.find('.packshot-form').find('select[name="nutritive_id"]').val();
                    var serializedForm = packshotForm.serialize();
                    var label_sr = modalForEvent.find('.packshot-form').find('input[name="trans[1][label]"]').val();
                    var label_en = modalForEvent.find('.packshot-form').find('input[name="trans[2][label]"]').val();
                    var text_sr = modalForEvent.find('.packshot-form').find('input[name="trans[1][text]"]').val();
                    var text_en = modalForEvent.find('.packshot-form').find('input[name="trans[2][text]"]').val();

                    var slug_sr = modalForEvent.find('.packshot-form').find('input[name="trans[1][slug]"]').val();
                    var slug_en = modalForEvent.find('.packshot-form').find('input[name="trans[2][slug]"]').val();
                    // var text = modalForEvent.find('.packshot-form').find('input[name="trans[2][text]"]').val();
                    var link = modalForEvent.find('.packshot-form').find('input[name="link"]').val();
                    // var dataJson = 
                    //     +'"trans[1][label]" : 'label_sr','
                    //     +'"trans[2][label]" : 'label_en','
                    //     +'"trans[1][text]" : 'text_sr','
                    //     +'"trans[2][text]" : 'text_en','
                    // +']';

                    // var dataJson = {
                    //     'trans[1][label]' : label_sr,
                    //     'trans[2][label]' : label_en,
                    // };

                    var dataJson = [
                            {"label": label_sr, "text": text_sr, "slug": slug_sr},
                            {"label": label_en, "text": text_en, "slug": slug_en}
                        ];

                    if(update) {
                        $.ajax({
                            type: 'put',
                            url: basePath+'/admin/product/packshot/'+packshotId,
                            data: {nutritive_id: nutritive_id, trans: JSON.stringify(dataJson), link: link, _token: csrf},
                            dataType: 'json',
                            success: function() {
                                $('span.wrap-gallery').load( window.location.href+ ' ul.gallery');
                                return true;

                            }
                        })
                    } else {
                        data.formData = {
                            _token: csrf,
                            // label: label,
                            nutritive_id: nutritive_id,
                            link: link,
                            trans: JSON.stringify(dataJson),

                            // serializedForm,
                        }
                        data.submit();
                        return true;
                    }
                }
            }
        }
    });// End bootbox
}

$(document).on('click', '.packshot-item', function(e) {
    showPackshotDialog(e, null, true);
});

$(document).on('submit', '.packshot-form', function(e) {
    e.preventDefault();
});

/**** FILES MANAGEMENT ****/
    // Disable defaultl drag'n'drop
    $(document).bind('drop dragover', function (e) {
        e.preventDefault();
    });


    // Uploader callbacks
    function afterUpload (e, data) {
        if(data.result.status == 'success') {

            $('.gallery').load(window.location.href+' .gallery', function (data) {
                // console.log(data);
                $('.toggle').toggles({on:false});
                $('.toggle.active').toggles({on:true});
                makeSortable();
            });
        } else {
            //print error
        }
        $('.fileinput-button').removeClass('dragover');
        $('.progress-bar').css('display', 'none');
        $('.progress-bar').css('width', '0%');
    }

    function uploadProgress(e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        // console.log(progress,e, data);
        $('.progress-bar').css('display', 'block');
        $('.progress-bar').css('width', progress + '%');
    }

    function uploadDragover(e, data) {
        e.preventDefault();
        var uploadElement = $(e.target).children();
        // console.log(uploadElement);
        uploadElement.addClass('dragover');
    }

    function uploadFail(e, data) {
        // console.log(e, data);
    }

    $('.fileinput-button').on('dragleave', function (e) {
        $(this).removeClass('dragover');
    });

    // Uploaders setup
    $('.fileupload-photo').fileupload({
        dropZone: $('.photo-upload-button'),
        formData: { _token: csrf },
        dataType: 'json',
        dragover: function(e, data) {
            uploadDragover(e, data);
        },
        drop: function(e, data) {
            if(data.files.length > 1) {
                bootbox.alert("Nije dozvoljen upload više fajlova istovremeno");
                return false;
            }
        },
        add: function(e, data) {
            showPackshotDialog(e, data);
        },
        done: function(e, data){
            afterUpload(e, data);
        },
        fail: function(e, data) {
            uploadFail(e, data);
        },
        progressall: function(e, data) {
            uploadProgress(e, data);
        },
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

    $(document).on('click', '.file-active', function() {
        var element = $(this);
        var id = element.parents('li').attr('data-id');
        var imageElement = element.parents('li').find('.file-thumb');
        $.ajax({
            type: 'PUT',
            url: basePath+'/admin/product/packshot/change-status',
            dataType: 'json',
            data: { _token : csrf, id: id},
            success: function(response) {
                if (response.status = 'success') {
                    if (response.state == 1) {
                        imageElement.removeClass('inactive');
                    } else {
                        imageElement.addClass('inactive');
                    }
                }
            }
        });
    });

    $(document).on('click', '.remove-file', function(){
        var element = $(this);
        var wrapperElement = element.parents('li');
        var id = wrapperElement.attr('data-id');
        bootbox.confirm('Da li ste sigurni da želite da obrišete ovaj packshot?', function(confirmed) {
            if(confirmed) {
                $.ajax({
                    type: 'DELETE',
                    url: basePath+'/admin/product/packshot/'+id,
                    data: { _token: csrf },
                    dataType: 'json',
                    success: function(response){
                        if(response.status == 'success') {
                            wrapperElement.hide(200, function(){
                                wrapperElement.remove();
                            });
                        }
                    }
                });
            }
        });
    });

     /** Sortable **/
    function makeSortable() {
        $('.gallery.packshot').sortable({
            placeholder: "image sortable-placeholder",
            stop: function(e, ui){
                var data = $(this).sortable('serialize');
                $.ajax({
                    method: 'PUT',
                    url: basePath+'/admin/product/packshot/reorder',
                    dataType: 'json',
                    data: data+'&_token='+csrf,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
        });
        $('.gallery').disableSelection();
    }

    makeSortable();