var i18n_save = 'Save';
var i18n_add = 'Add';
var i18n_cancel = 'Cancel';
var i18n_delete = 'Delete';
var i18n_check = 'Check';
var i18n_change = 'Change';

var i18n_title_required = "Title field is required";
var i18n_two_options_required = "At least two options are required";
var i18n_incorrect_required = "At least one option must be incorrect";
var i18n_correct_required = "At least one option must be marked as correct";