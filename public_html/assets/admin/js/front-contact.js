$(document).on('click', '#kontakt-forma-informacije .submit', function(event) {

	var element = $(this);
	var form = $('#kontakt-forma-informacije');
	form.find('.error').text('');
	
	var name = form.find('input[name="name"]').val();
	var email = form.find('input[name="email"]').val();
	var phone = form.find('input[name="phone"]').val();
	var message = form.find('textarea[name="message"]').val();

	if (validationInfo()) {
		form.find('.error').text('Proverite da li ste ispravno popunili sva polja!');
		return false;
	} else {
		$.ajax({
			url: '/kontakt',
			type: 'POST',
			dataType: 'json',
			data: {name: name, email: email, phone: phone, message: message, _token: csrf},
			success: function(response) {
				if (response.status == 'success') {
					console.log('success');
					form.find('.status').text('Uspešno ste poslali email.');
				}
			}
		})
	}


	
});	

$(document).on('click', '#kontakt-forma-posao .submit', function(event) {
	var element = $(this);
	var form = $('#kontakt-forma-posao');
	form.find('.error').text('');

	var name = form.find('input[name="name"]').val();
	var email = form.find('input[name="email"]').val();
	var phone = form.find('input[name="phone"]').val();
	var message = form.find('textarea[name="message"]').val();
	var position = form.find('.dropbtn').text();
	var file = form.find('input[name="upload-file"]')[0].files[0];
	var form_data = new FormData();

	if (validationJob()) {
		form.find('.error').text('Proverite da li ste ispravno popunili sva polja!');
	} else {
		form_data.append('_token', csrf);
		form_data.append('file', file);
		form_data.append('name', name);
		form_data.append('phone', phone);
		form_data.append('email', email);
		form_data.append('message', message);
		form_data.append('position', position);

		$.ajax({
			url: '/kontakt',
			type: 'POST',
			dataType: 'json',
			data:  form_data,
			contentType: false,       // The content type used when sending data to the server.
			processData: false, 
			success: function(response) {
				if (response.status == 'success') {
					form.find('.status').text('Uspešno ste poslali email.');
				}
			}
		});
	}
		
});


function changeIcon(condition, element) {
	$(element).siblings('label').find('.icon').attr('src', '/assets/front/images/kontakt/cv-ico-'+condition+'.png');
}
$(document).on('change', '#upload-cv', function() {
	
	var file = $(this)[0].files[0];

	var element = $(this);
	if (file) {
		$('.upload-percent').css({
			transition: '0.5s all',
			width: '100%',
		});
		setTimeout(function() {
			element.siblings('label').find('.icon').attr('src', '/assets/front/images/kontakt/cv-ico-success.png');
			element.siblings('label').children('span').text(file['name']);
		}, 550);
	} else {
		$('.upload-percent').css({
			transition: '0.5s all',
			width: '0%',
		});
		setTimeout(function() {
			element.siblings('label').find('.icon').attr('src', '/assets/front/images/kontakt/cv-ico-add.png');
			element.siblings('label').children('span').text('Dodaj CV*');
		}, 700)
	}
});




function validationInfo() {
	var form = $('#kontakt-forma-informacije');

	var name = form.find('input[name="name"]').val();
	var email = form.find('input[name="email"]').val();
	var message = form.find('textarea[name="message"]').val();
	var error = 0;

	if (!name && name == '') {
		error++
	}
	if (!email && email == '') {
		error++
	}
	
	if (!message && message == '') {
		error++
	}

	return error;
}

function validationJob() {

	var form = $('#kontakt-forma-posao');

	var name = form.find('input[name="name"]').val();
	var email = form.find('input[name="email"]').val();
	var message = form.find('textarea[name="message"]').val();
	var position = form.find('.dropbtn').text();
	var file = form.find('input[name="upload-file"]')[0].files[0];
	var form_data = new FormData();

	var error = 0;

	if (!name && name == '') {
		error++
	}
	if (!email && email == '') {
		error++
	}
	if (!message && message == '') {
		error++
	}
	if (position == 'Pozicije') {
		error++
	}

	if (!file) {
		error++
	}

	return error;
}































