$(document).ready(function() {

	if ($.cookie("menuCookie")) {
		openMenu($('#hamburger'));
	} else {
		closeMenu($('#hamburger'));
	}
});

$(document).on('click', '#hamburger', function(event) {
	 var date = new Date();
	 var minutes = 120;
	 date.setTime(date.getTime() + (minutes * 60 * 1000));
	$.cookie('menuCookie', 'open', {expires: date});

});

$(document).on('click', '#hamburger.opened', function(event) {
	deleteCookie();
});

function deleteCookie() {
	$.removeCookie('menuCookie'); 
}

$(document).on('click', '.vesti-slider .item', function(event) {

	$('.nav-item').each(function() {
		$(this).removeClass('active');
	});
	
	var vesti = $('nav').find('[data-class="vesti-page"]')
	vesti.addClass('active');
});

$(document).on('click', '#proizvodi .item', function(event) {

	$('.nav-item').each(function() {
		$(this).removeClass('active');
	});

	var proizvodi = $('nav').find('[data-class="proizvodi-page"]')
	proizvodi.addClass('active');

});	

$(document).on('click', '#proizvodi-page .read-more', function(event) {

	$('.nav-item').each(function() {
		$(this).removeClass('active');
	});

	var kuhinjica = $('nav').find('[data-class="kuhinjica-page"]')
	kuhinjica.addClass('active');

});	






