$(document).on('click', '.lang-input-switch', function(e) {
    e.preventDefault();

    var langInputs = $(this).parents('.lang-input-wrap').children('.lang-input').length;
    var currentChild = $(this).parent().index()+1;
    var nextChild;
    currentChild + 1 <= langInputs ? nextChild = currentChild + 1 : nextChild = 1;

    $('.lang-input').removeClass('active');
    $('.lang-input-wrap li:nth-child('+nextChild+')').addClass('active');
});

// $(document).on('keyup', '.cloneme', function(e) {
//     e.preventDefault();
//     $(this).parents('.lang-input-wrap').find('.lang-input:not(.active) .cloneme').val($(this).val());
// });

$(function(){

    $(document).on('click', '.delete-item', function(e){
        e.preventDefault();
        var button = $(this);
        deleteItem(button, 'tr');
    });

    $(document).on('click', '.delete-collection-item', function(e){
        e.preventDefault();
        var button = $(this);
        deleteItem(button, '.list-item');
    });

    function deleteItem(button, parent) {
        var resource = button.parents('.resource-container').data('resource');
        var modalText = button.parents('.resource-container').data('modal');
        var itemId = button.parents(parent).data('id');
        var URL = basePath+'/admin/'+resource+'/'+itemId;
        var row = button.parents(parent);
        bootbox.confirm(modalText, function(confirmed){
            if(confirmed) {
                $.ajax({
                    type: 'DELETE',
                    url: URL,
                    data: {_token: csrf},
                    dataType: 'json',
                    success: function(response) {
                        if(response.status == 'success') {
                            row.hide(400, function(){
                                row.remove();
                            });
                        }
                    }
                });
            }
        });
    }

     /** Change all active tabs on one click **/
    $(document).on('click', '.lang-tab li', function() {
        console.log('tab click');
        var tabIndex = $(this).index()+1;
        $('.lang-tab li').removeClass('active');
        $('.lang-tab li:nth-child('+tabIndex+')').addClass('active');
        $('.lang-tab').siblings('.tab-content').children('.tab-pane').removeClass('active');
        $('.lang-tab').siblings('.tab-content').children('.tab-pane:nth-child('+tabIndex+')').addClass('active');
    });

    // $(document).on('click', '.delete-collection-item', function(e){
    //     e.preventDefault();
    //     var button = $(this);
    //     var resource = button.parents('.resource-container').data('resource');
    //     var modalText = button.parents('.resource-container').data('modal');
    //     var itemId = button.parents('tr').data('id');
    //     var URL = basePath+'/admin/'+resource+'/'+itemId;
    //     var row = button.parents('.list-item');
    //     bootbox.confirm(modalText, function(confirmed){
    //         if(confirmed) {
    //             $.ajax({
    //                 type: 'DELETE',
    //                 url: URL,
    //                 data: {_token: csrf},
    //                 dataType: 'json',
    //                 success: function(response) {
    //                     if(response.status == 'success') {
    //                         row.hide(400, function(){
    //                             row.remove();
    //                         });
    //                     }
    //                 }
    //             });
    //         }
    //     });
    // });

    /** Togles **/
    function assignToggle(){
        $('.toggle').toggles({on:false});
        $('.toggle.active').toggles({on:true});
    }

    assignToggle();


    $(document).on('toggle', '.toggle.table-ajax', function(e, active) {
        var button = $(this);
        var resource = button.parents('.resource-container').data('resource');
        var itemId = button.data('id');
        var URL = basePath+'/admin/'+resource+'/change-status';
        $.ajax({
            type: 'POST',
            url: URL,
            dataType: 'json',
            data: { id : itemId, _token: csrf },
            success: function(response) {
                if(response.state == 1) {
                    button.parents('tr').children('td:not(.table-options)').removeClass('inactive');
                } else {
                    button.parents('tr').children('td:not(.table-options)').addClass('inactive');
                }
            }
        });
    });

    $(document).on('toggle', '.toggle.collection-ajax', function(e, active) {
        var button = $(this);
        var resource = button.parents('.resource-container').data('resource');
        var itemId = button.data('id');
        var URL = basePath+'/admin/'+resource+'/change-status';
        $.ajax({
            type: 'POST',
            url: URL,
            dataType: 'json',
            data: { id : itemId, _token: csrf },
            success: function(response) {
                if(response.state == 1) {
                    button.parents('.list-item').find('img').removeClass('inactive');
                } else {
                    button.parents('.list-item').find('img').addClass('inactive');
                }
            }
        });
    });

    $(document).on('toggle', '.toggle.form-active', function(e, active) {
        var button = $(this);
        var activeInput = button.siblings('input[name="active"]');
        if (active) {
            activeInput.val(1);
        } else {
            activeInput.val(0);
        }
        console.log('form toggle', activeInput, activeInput.val());
    });

    $(document).on('keyup', '.slugify', function(e) {
        var postId = $(this).data('id');

        var element = $(this);
        var resource = element.parents('form').data('resource');
        console.log(resource);
        $.ajax({
            type: 'POST',
            url: basePath+'/admin/'+resource+'/slugify',
            dataType: 'json',
            data: {title: $(this).val(), id: postId, _token: csrf},
            success: function(response) {
                element.parents('form').find('.active input[name*=slug]').val(response.slug);
            },
            error: function(response) {
                element.parents('form').find('.active input[name*=slug]').val('');
            }
        });
    });

    // Reorder
    $('.gallery.slide-gallery').sortable({
        placeholder: 'sortable-placeholder list-item',
        stop: function(e, ui) {
            var data = $(this).sortable('serialize', {key: "sort_item[]"});
            $.ajax({
                method: 'put',
                url: basePath+'/admin/slide/reorder',
                dataType: 'json',
                data: data+"&_token="+csrf,
                success: function(response) {
                    console.log(response);
                }
            });
        }
    });
    $('.gallery').disableSelection();

    // Reorder
    $('table.sortable tbody').sortable({
        placeholder: '',
        helper: fixWidthHelper,
        handle: ".bars",
        stop: function(e, ui) {
            var data = $(this).sortable('serialize', {key: "sort_item[]"});
            var resource = $('.resource-container').data('resource');
            $.ajax({
                method: 'put',
                url: basePath+'/admin/'+resource+'/reorder',
                dataType: 'json',
                data: data+"&_token="+csrf,
                success: function(response) {
                    console.log(response);
                }
            });
        }
    }).disableSelection();
    // $('table.sortable tbody').disableSelection();

    function fixWidthHelper(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    }

    FB.ui({
      method: 'share',
      href: 'https://developers.facebook.com/docs/',
    }, function(response){});

});

$(window).on('scroll', function(event) {
    event.preventDefault();
    var scrollTop = $('html, body').scrollTop();
    console.log(scrollTop);
    $('#page-content').css('background-position-y', - (scrollTop - scrollTop * 0.75) + 'px');
});