$(document).on('click', '#new-recipe-step', function(e) {
    e.preventDefault();

    var newStep = $('.recipe-steps').find('.recipe-step-wrapper').first().clone();



    $('.recipe-steps').append(processStepFields(newStep));

});


function processStepFields(step)
{
    var inputs = step.find('input');
    inputs.each(function(index, element) {
        var name = $(element).attr('name').replace('[0]', '['+$('.recipe-step-wrapper').length+']');
        $(element).val('');
        $(element).attr('name', name);
    });

    var textareas = step.find('textarea');
    textareas.each(function(index, element) {
        var name = $(element).attr('name').replace('[0]', '['+$('.recipe-step-wrapper').length+']');
        $(element).val('');
        $(element).attr('name', name);
    });

    return step
}

$(function(){

});

$(document).on('click', '.save-recipe', function(event) {

    console.log('klik');
    var gridImage = $('input[name="grid_image"]');
    var image = $('input[name="image"]');

    if (gridImage.get(0).files.length == 0) {
        gridImage.parents('.fileinput').parent().append('<p class="help-block error"><i class="fa fa-times-circle"></i>The image field is required.</p>');
        return false;
    } else {
        gridImage.parents('.fileinput').parent().find('.help-block').remove();
    }

    if (image.get(0).files.length == 0) {
        image.parents('.fileinput').parent().append('<p class="help-block error"><i class="fa fa-times-circle"></i>The image field is required.</p>');
        return false;
    } else {
        image.parents('.fileinput').parent().find('.help-block').remove();
    }


}); 

$(document).on('click', '#delete-recipe-step', function(event) {


    $('.recipe-steps').each(function(index, el) {
       var lastStep = $(this).children('.recipe-step-wrapper:last-child'); 
       lastStep.remove();
    });

});