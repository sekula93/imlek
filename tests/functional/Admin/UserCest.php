<?php
namespace Admin;

use \FunctionalTester;

use Illuminate\Support\Facades\Hash;

class UserCest
{
    protected $adminUserAttributes;
    protected $adminUserAttributes1;
    protected $regularUserAttributes;

    public function _before(FunctionalTester $I)
    {
        $this->adminUserAttributes = [
            'first_name' => 'NFirst',
            'last_name' => 'NLast',
            'email' => 'admin@test.com',
            'password' => Hash::make('password'),
            'role' => 0,
            'active' => 1,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ];

        $this->adminUserAttributes1 = [
            'first_name' => 'NFirst1',
            'last_name' => 'NLast1',
            'email' => 'admin1@test.com',
            'password' => Hash::make('password'),
            'role' => 0,
            'active' => 1,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ];


        $this->regularUserAttributes = [
            'first_name' => 'RFirst',
            'last_name' => 'RLast',
            'email' => 'regular@test.com',
            'password' => Hash::make('password'),
            'role' => 1,
            'active' => 1,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ];

        $I->haveRecord('users', $this->adminUserAttributes);
        $I->amLoggedAs(['email' => 'admin@test.com', 'password' => 'password']);

    }

    public function _after(FunctionalTester $I)
    {
        // $I->seeAuthentication();
        // $I->logout();
        // $I->dontSeeAuthentication();
    }

    public function seeTheListOfAdmins(FunctionalTester $I)
    {
        $I->haveRecord('users', $this->adminUserAttributes1);
        $I->amOnRoute('admin.user.index', 'admin');
        $I->see('admin1@test.com', '//td/a');
    }

    public function doNotSeeMySelfInTheListOfAdmins(FunctionalTester $I)
    {
        $I->haveRecord('users', $this->adminUserAttributes1);
        $I->amOnRoute('admin.user.index', 'admin');
        $I->dontSee('admin@test.com', '//td/a');
    }

    public function doNotSeeRegularUserInTheListOfAdmins(FunctionalTester $I)
    {
        $I->haveRecord('users', $this->regularUserAttributes);
        $I->amOnRoute('admin.user.index', 'admin');
        $I->dontSee('regular@test.com', '//td/a');
    }

    public function canEditMySelf(FunctionalTester $I)
    {
        $I->amOnPage('/admin/user/1/edit');
        $I->see('Edit profile', 'h4');
        $I->dontSee('active', 'label');
    }

    public function canSeeRegularUsersList(FunctionalTester $I)
    {
        $I->haveRecord('users', $this->regularUserAttributes);
        $I->amOnRoute('admin.user.index', 'member');
        $I->see('regular@test.com', '//td/a');
    }

    public function dontSeeAdminInRegularUsersList(FunctionalTester $I)
    {

    }




}
