<?php
namespace Admin;
use \FunctionalTester;

use App\User;
use Illuminate\Support\Facades\Hash;

class AuthCest
{
    private $adminUserAttributes;

    public function _before(FunctionalTester $I)
    {
        $this->adminUserAttributes = [
            'first_name' => 'NFirst',
            'last_name' => 'NLast',
            'email' => 'some@test.com',
            'password' => Hash::make('password'),
            'role' => 0,
            'active' => 1,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ];
    }

    public function _after(FunctionalTester $I)
    {
        // $I->seeAuthentication();
        $I->logout();
        $I->dontSeeAuthentication();
    }

    public function loginUsingUserRecord(FunctionalTester $I)
    {
        $I->dontSeeAuthentication();
        $I->amLoggedAs(User::firstOrNew($this->adminUserAttributes));
    }

    // tests
    public function loginUsingCredentials(FunctionalTester $I)
    {
        $I->dontSeeAuthentication();
        $I->haveRecord('users', $this->adminUserAttributes);
        $I->amLoggedAs(['email' => 'some@test.com', 'password' => 'password']);
    }

    public function requireAuthenticationForAdminRoute(FunctionalTester $I)
    {
        $I->dontSeeAuthentication();
        $I->amOnPage('/admin');
        $I->seeCurrentUrlEquals('/admin/login');
        $I->see('Log in to get started', 'h4');

        $I->amLoggedAs(User::firstOrNew($this->adminUserAttributes));
        $I->amOnPage('/admin');
        $I->seeResponseCodeIs(200);
        $I->see('NFirst NLast');
    }

    public function loginErrorMessageAfterLoginFailed(FunctionalTester $I)
    {
        $I->amOnPage('admin/login');
        $I->submitForm('#login', ['email' => 'wrong@test.com', 'password' => 'wrong']);
        $I->dontSeeAuthentication();
        $I->see('Combination email/password is wrong. Please try again', '.error');
    }

    public function logoutByVisitingLogoutLink(FunctionalTester $I)
    {
        $I->haveRecord('users', $this->adminUserAttributes);
        $I->amLoggedAs(['email' => 'some@test.com', 'password' => 'password']);
        $I->amOnPage('/admin/logout');
        $I->dontSeeAuthentication();
        $I->seeCurrentRouteIs('admin.auth.login');
    }
}
