<?php
    return [
        'job_position'      => 'Job Position|Job positions',
        'positions_list'    => 'Positions list',
        'add_job_position'  => 'Add job position',
        'new_job_position'  => 'New job position',
        'edit_job_position' => 'Edit job position',
        'name'              => 'Job Position name',

        'msg_delete'        => 'Are you sure you want to delete this job position?',
        'msg_create_success'=> 'New job position has been created',
        'msg_create_error'  => 'There was error during job position creation',
        'msg_update_success'=> 'Job Position has been updated successfully',
        'msg_update_error'  => 'There was error during job position update'
    ];