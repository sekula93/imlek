<?php
    return [
        'game'          => 'Game|Games',
        'edit_game'     => 'Edit game',
        'name'          => 'Game name',
        'description'   => 'Description',
        'timer_type'    => 'Timer type',
        'time'          => 'Countdown time',
        'add_question'  => 'Add question',
        'question_title'    => 'Question',
        'question_type' => 'Type',
        'option'        => 'Option',
        'correct'       => 'Correct',

        'msg_update_success' => 'Game has been updated successfully',
        'msg_update_error' => 'There was error during game update',
        'msg_trivia_delete' => 'Are you sure you want to delete this trivia question?'
    ];