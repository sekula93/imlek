<?php
    return [
        'company'       => 'Company|Companies',
        'cities_list'   => 'Companies list',
        'add_company'   => 'Add company',
        'new_company'   => 'New company',
        'edit_company'  => 'Edit company',
        'name'          => 'Company name',

        'msg_delete' => 'Are you sure you want to delete this company?',
        'msg_create_success' => 'New company has been created',
        'msg_create_error' => 'There was error during company creation',
        'msg_update_success' => 'Company has been updated successfully',
        'msg_update_error' => 'There was error during company update'
    ];