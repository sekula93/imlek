<?php

return [
    'forgot_password'   => "Forgot password",
    'login_title'       => "Log in to get started",
    'remember_me'       => "Remember me",
    'btn_login'         => "Log in",
    'sign_out'          => "Sign out",
    'email'             => "Email",
    'password'          => "Password",
    'confirm_password'  => "Confirm password",
];