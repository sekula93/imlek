<?php
    return [
        'event'       => 'Event|Events',
        'events_list' => 'Events list',
        'add_event'   => 'Add event',
        'new_event'   => 'New event',
        'edit_event'  => 'Edit event',
        'type'        => 'Event type',
        'title'       => 'Event name',
        'photo'       => 'Feature photo',
        'start_date'  => 'Start date',
        'end_date'    => 'End date',
        'agenda'      => 'Agenda',
        'map'         => 'Pin on map',
        'address'     => 'Venue address',
        'speakers'    => 'Speakers',
        'featured'    => 'Featured',
        'venue'       => 'Venue',

        'msg_delete' => 'Are you sure you want to delete this event?',
        'msg_create_success' => 'New event has been created',
        'msg_create_error' => 'There was error during event creation',
        'msg_update_success' => 'Event has been updated successfully',
        'msg_update_error' => 'There was error during event update'
    ];
