<?php
    return [
        'btn_back'      => "Back",
        'btn_submit'    => "Submit",
        'btn_send'      => "Send",
        'btn_save'      => "Save",
        'btn_cancel'    => "Cancel",
        'btn_reset'     => "Reset",
        'active'        => "Active",
        'select_file'   => "Select file for upload",
        'btn_select'    => "Select",
        'btn_change'    => "Change",
        'btn_remove'    => "Remove",

        'btn_drop_files'    => 'Drop files to upload',
        'btn_or_click'      => '(or click)',

        'password'      => "Password",
        'loyalty_points'    => 'Loyalty points',

        'please_select' => "-- Please select --"
    ];