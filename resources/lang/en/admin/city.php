<?php
    return [
        'city'          => 'City|Cities',
        'cities_list'   => 'Cities list',
        'add_city'      => 'Add city',
        'new_city'      => 'New city',
        'edit_city'     => 'Edit city',
        'name'          => 'City name',
        'photo'         => 'Photo',
        'guide'         => 'City guide',

        'msg_delete' => 'Are you sure you want to delete this city?',
        'msg_create_success' => 'New city has been created',
        'msg_create_error' => 'There was error during city creation',
        'msg_update_success' => 'City has been updated successfully',
        'msg_update_error' => 'There was error during city update'
    ];