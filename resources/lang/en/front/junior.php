<?php


return [
	'slider' => 'Unique milk product on our market enriched with <br> Optivita vitamin complex for children’s healthy growth and development.',
	'title' => 'Moja Kravica Junior',
	'subtitle' => 'milk for our youngest ones',
	'pasus1' => 'Moja Kravica, who has been supporting a happy growing up experience for children for years, now presents its Junior milk, enriched with 10 vitamins from the Optivita complex.',
	'pasus2' => 'Due to a combination of milk fat and calcium from the milk, vitamins from the Optivita complex are exceptionally easily absorbed by the body, which is very important for the proper growth and development of children. Thanks to numerous vitamins, Junior mleko helps teeth grow, favorably affects the metabolism, blood picture, the immune system, the children’s nervous system and brings numerous other benefits. This milk is primarily intended for the little ones, but also for all those who want to boost their immune systems.',
	'pasus3' => 'More details about the vitamins and benefits of the Junior milk can be found bellow',

	'krugOrange' => [
		'title' => 'Vitamin D3',
		'subtitle' => 'and calcium from the milk',
		'text' => 'Contribute to proper growth and development'
	],
	'krugViolet' => [
		'title' => 'Vitamins A, B6, D3, B12 ',
		'subtitle' => 'and calcium from the milk',
		'text' => 'Contribute in the strengthening of the immune system'
	],
	'krugDarkGreen' => [
		'title' => 'Vitamins A, B5, B6, D3 ',
		'subtitle' => 'and calcium from the milk',
		'text' => 'Contribute to the blood picture and prevention of anemia'
	],
	'krugLightGreen' => [
		'title' => 'Vitamins B3, B5, B6, B7, B9 ',
		'subtitle' => 'and calcium from the milk',
		'text' => 'Contribute to the development and functioning of the nervous system'
	],
	'krugPlin' => [
		'title' => 'Vitamins D3, A, B6',
		'subtitle' => 'and calcium from the milk',
		'text' => 'Contribute to the development of strong bones and teeth'
	],
	'krugRed' => [
		'title' => 'Vitamins A, D3',
		'subtitle' => 'and calcium from the milk',
		'text' => 'Contribute to the development and preservation of vision'
	],
	'krugGreenSpiral' => [
		'title' => 'Vitamin B3',
		'subtitle' => 'and calcium from the milk',
		'text' => 'Contribute to the normal functioning of the metabolism'
	],
	'krugBlue' => [
		'title' => 'Vitamins A, B5, B7, E',
		'subtitle' => 'and calcium from the milk',
		'text' => 'Contribute to the hair and skin development and function'
	],
	'efsa' => 'The function and health claims regarding the product are approved under EU regulations.',
	'share' => 'Share online and tell everyone'

];