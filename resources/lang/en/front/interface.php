<?php

    return [
        'menu' => [
            'home' => [
                'label' => 'Home',
                'url'   => '/'
            ],
            'company' => [
                'label' => 'Company',
                'url'   => '/company'
            ],
            'products' => [
                'label' => 'Products',
                'url'   => '/products'
            ],
            'news' => [
                'label' => 'News',
                'url'   => '/news'
            ],
            'reports' => [
                'label' => 'Reports',
                'url'   => '/reports'
            ],
            'certificates' => [
                'label' => 'Certificates',
                'url'   => '/certificates'
            ],
            'faq' => [
                'label' => 'FAQ',
                'url'   => '/faq'
            ],
            'contact' => [
                'label' => 'Contact',
                'url'   => '/contact'
            ],
            'recipe' => [
                'label' => 'Recipes',
                'url' => '/recipes'
            ]
        ],
        'link' => [
            'about' => [
                'label' => 'About us',
                'slug'   => 'about-us'
            ],
            'mission' => [
                'label' => 'Mission and Vision',
                'slug' => 'misija-i-vizija'
            ],
            'responsibility' => [
                'label' => 'Social Responsibility',
                'slug' => 'social-responsibility',
            ],
            'protection' => [
                'label' => 'Environmental Protection', 
                'slug' => 'environmental-protection'
            ]
        ],

        'product' => [
            'balans' => [
                'label' => 'Balans',
                'slug' => 'yogurt-1'
            ],
            'jogood' => [
                'label' => 'Jogood',
                'slug' => 'jogood'
            ],
            'flert' => [
                'label' => 'Flert',
                'slug' => 'flert'
            ],
            'milk' => [
                'label' => 'Milk',
                'slug' => 'milk',
            ],
            'joghurt' => [
                'label' => 'Yogurt',
                'slug' => 'yogurt'
            ],
            'sour-milk' => [
                'label' => 'Sour milk',
                'slug' => 'sour-milk'
            ],
            'butter' => [
                'label' => 'Butter',
                'slug' => 'butter'
            ],
            'cream-cheese' => [
                'label' => 'Cream cheese',
                'slug' => 'cream-cheese'
            ],
            'sour-cream' => [
                'label' => 'Sour cream',
                'slug' => 'sour-cream'
            ],
            'bello' => [
                'label' => 'bello',
                'slug' => 'bello-1'
            ],
            'cheese' => [
                'label' => 'Cheese',
                'slug' => 'Cheese'
            ],
            'subotica' => [
                'label' => 'Mlekara Subotica',
                'slug' => 'milk-1'
            ],
            'nis' => [
                'label' => 'Niška mlekara',
                'slug' => 'niska-mlekara-1'
            ],

        ],
        'contact' => [
            'contact_us' => 'Contact us',
            'tab' => [
                'informations' => 'Information', 
                'career' => 'Career',
            ],
            'name' => 'Name and Last name',
            'email' => 'Email',
            'phone' => 'Telephone (optional)',
            'message' => 'Your message', 
            'send' => 'Send',
            'info_line' => 'Info line',
            'fax' => 'Fax',
            'headquarters' => 'Headquarters',
            'positions' => 'Positions',
            'add_cv' => 'Add CV',
        ],


        'category' => 'category', 
        'btn_send' => 'SEND',
        'read_more' => 'Read more',
        'history' => 'Our history',
        'previous' => 'Previous',
        'next' => 'Next',
        'look' => 'See more',
        'view_product' => 'View product',
        'all_recipes' => 'All recipes',
        'see_website' => 'See website', 
        'product_info' => 'Product informations',
        'video_campaign' => 'Video campaign',
        'date' => 'Date',
        'share' => 'Share',
        'ingredients' => 'Ingredients',
        'instructions' => 'Instructions',
        'recipes_and_tricks' => 'Recipes and Tricks',
        'recipes_sub' => 'Moja Kravica Kuhinjica products are for those who like to enjoy food, rather than counting calories :)',


    ];