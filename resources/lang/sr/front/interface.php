<?php

    return [
        'menu' => [
            'home' => [
                'label' => 'Naslovna',
                'url'   => '/'
            ],
            'company' => [
                'label' => 'Kompanija',
                'url'   => '/kompanija'
            ],
            'products' => [
                'label' => 'Proizvodi',
                'url'   => '/proizvodi'
            ],
            'news' => [
                'label' => 'Vesti',
                'url'   => '/vesti'
            ],
            'reports' => [
                'label' => 'Izveštaji',
                'url'   => '/izvestaji'
            ],
            'certificates' => [
                'label' => 'Sertifikati',
                'url'   => '/sertifikati'
            ],
            'faq' => [
                'label' => 'FAQ',
                'url'   => '/faq'
            ],
            'contact' => [
                'label' => 'Kontakt',
                'url'   => '/kontakt'
            ],
            'recipe' => [
                'label' => 'Kuhinjica',
                'url' => '/kuhinjica'
            ]
        ],
        'link' => [
            'about' => [
                'label' => 'O nama',
                'slug'   => 'o-nama'
            ],
            'mission' => [
                'label' => 'Misija i vizija',
                'slug' => 'misija-i-vizija'
            ],
            'responsibility' => [
                'label' => 'Društvena odgovornost',
                'slug' => 'drustvena-odgovornost',
            ],
        ],

        'product' => [
            'balans' => [
                'label' => 'Balans',
                'slug' => 'jogurt-1'
            ],
            'jogood' => [
                'label' => 'Jogood',
                'slug' => 'jogood'
            ],
            'flert' => [
                'label' => 'Flert',
                'slug' => 'flert'
            ],
            'milk' => [
                'label' => 'Mleko',
                'slug' => 'mleko',
            ],
            'joghurt' => [
                'label' => 'Jogurt',
                'slug' => 'jogurt'
            ],
            'sour-milk' => [
                'label' => 'Kiselo mleko',
                'slug' => 'kiselo-mleko'
            ],
            'butter' => [
                'label' => 'Maslac',
                'slug' => 'maslac'
            ],
            'cream-cheese' => [
                'label' => 'Krem sir',
                'slug' => 'krem-sir'
            ],
            'sour-cream' => [
                'label' => 'Kisela pavlaka',
                'slug' => 'kisela-pavlaka'
            ],
            'bello' => [
                'label' => 'bello',
                'slug' => 'bello'
            ],
            'cheese' => [
                'label' => 'Sirevi',
                'slug' => 'sirevi'
            ],
            'subotica' => [
                'label' => 'Mlekara Subotica',
                'slug' => 'mleko-1'
            ],
            'nis' => [
                'label' => 'Niška mlekara',
                'slug' => 'niska-mlekara'
            ],

        ],
        'contact' => [
            'contact_us' => 'Kontaktirajte nas',
            'tab' => [
                'informations' => 'Informacije', 
                'career' => 'Posao',
            ],
            'name' => 'Ime i prezime',
            'email' => 'Email',
            'phone' => 'Broj telefona (opciono)',
            'message' => 'Vaša poruka', 
            'send' => 'Pošalji',
            'info_line' => 'Info tel',
            'fax' => 'Fax',
            'headquarters' => 'Centrala',
            'positions' => 'Pozicije',
            'add_cv' => 'Dodaj CV'
        ],
        'category' => 'kategorija',
        'btn_send' => 'POŠALJI',
        'read_more' => 'Pročitaj više',
        'history' => 'Istorijat',
        'previous' => 'Prethodna vest',
        'next' => 'Sledeća vest',
        'look' => 'Pogledaj',
        'view_product' => 'Pogledaj proizvod',
        'all_recipes' => 'Svi recepti',
        'see_website' => 'Pogledaj sajt',
        'product_info' => 'Informacije o proizvodu',
        'video_campaign' => 'Video kampanja',
        'date' => 'Datum objave',
        'share' => 'Podeli na mreži',
        'ingredients' => 'Sastojci',
        'instructions' => 'Priprema',
        'recipes_and_tricks' => 'Recepti i trikovi',
        'recipes_sub' => 'Moja Kravica Kuhinjica proizvodi su proizvodi za ljude koji vole da uživaju u hrani, a ne u brojanju kalorija :)',


    ];