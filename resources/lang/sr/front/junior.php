<?php


return [

	'slider' => 'Jedinstveno mleko na tržištu obogaćeno <br> Optivita kompleksom za rast i razvoj dece.',
	'title' => 'Moja Kravica Junior',
	'subtitle' => 'mleko za naše najmlađe',
	'pasus1' => 'Moja Kravica koja je dugi niz godina podrška u srećnom odrastanju dece, sada predstavlja Junior mleko obogaćeno sa 10 vitamina u okviru Optivita kompleksa.',
	'pasus2' => 'Zahvaljujući kombinaciji mlečnih masti i kalcijuma iz mleka,  vitamini iz Optivita kompleksa se izuzetno dobro apsorbuju u organizmu, što je veoma važno za pravilan rast i razvoj dece. Zahvaljujući brojnim vitaminima, Junior mleko pospešuje rast zubića, povoljno utiče na metabolizam, krvnu sliku, imuni sistem, nervni sistem dece i donosi brojne druge pogodnosti. Ovo mleko namenjeno je prvenstveno mališanima, ali i svima onima koji žele da ojačaju svoj imuni sistem.',
	'pasus3' => 'Više detalja o vitaminima koje sadrži i prednostima Junior mleka možete videti u nastavku',

	'krugOrange' => [
		'title' => 'Vitamin D3 i',
		'subtitle' => 'kalcijum iz mleka',
		'text' => 'Doprinose pravilnom rastu<br>i razvoju'
	],
	'krugViolet' => [
		'title' => 'Vitamini A, B6, D3, B12 i',
		'subtitle' => 'kalcijum iz mleka',
		'text' => 'Doprinose izgradnji<br>imuniteta'
	],
	'krugDarkGreen' => [
		'title' => 'Vitamini A, B5, B6, D3 i',
		'subtitle' => 'kalcijum iz mleka',
		'text' => 'Doprinose očuvanju krvne<br>slike i prevenciji anemije'
	],
	'krugLightGreen' => [
		'title' => 'Vitamini B3, B5, B6, B7',
		'subtitle' => 'B9 i kalcijum iz mleka',
		'text' => 'Doprinose razvoju i funkciji<br>nervnog sistema'
	],
	'krugPlin' => [
		'title' => 'Vitamini D3, A, B6 i',
		'subtitle' => 'kalcijum iz mleka',
		'text' => 'Doprinose razvoju jakih<br>kostiju i zubića'
	],
	'krugRed' => [
		'title' => 'Vitamini A, D3',
		'subtitle' => 'kalcijum iz mleka',
		'text' => 'Doprinose razvoju i<br>očuvanju vida'
	],
	'krugGreenSpiral' => [
		'title' => 'Vitamin B3 i',
		'subtitle' => 'kalcijum iz mleka',
		'text' => 'Doprinose normalnom<br>funkcionisanju metabolizma'
	],
	'krugBlue' => [
		'title' => 'Vitamini A, B5, B7, E i',
		'subtitle' => 'kalcijum iz mleka',
		'text' => 'Doprinose razvoju i<br>funkciji kose i kože'
	],
	'efsa' => 'Funkcionalnost proizvoda i zdravstvene <br>tvrdnje su odobrene po EU regulativi',
	'share' => 'Podeli na mreži i javi svima'

];