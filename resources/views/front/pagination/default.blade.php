<!-- @if ($paginator->lastPage() > 1)
<ul class="pagination">
    <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
        <a href="{{ $paginator->url(1) }}">Previous</a>
    </li>
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
            <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
        </li>
    @endfor
    <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
        <a href="{{ $paginator->url($paginator->currentPage()+1) }}" >Next</a>
    </li>
</ul>
@endif -->

@if ($paginator->lastPage() > 1)
<section id="pagination">

    <div class="box">

        <div class="holder">
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                <a href="{{ $paginator->url($i) }}" class="{{ ($paginator->currentPage() == $i) ? ' jp-current' : '' }}"><span class="line"></span>{{ $i }}</a>
            @endfor
        </div>

    </div>
    <div class="arrows">

        <a href="{{ $paginator->url($paginator->currentPage()-1) }}">
            <button type="button" class="arrow hero-prev">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                    <polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
                </svg>
            </button>
        </a>

        <div class="delimiter">|</div>
        
        <a href="{{ $paginator->url($paginator->currentPage()+1) }}">
            <button type="button" class="arrow hero-next">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                    <polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
                </svg>
            </button>
        </a>
        
    </div>
</section>
@endif