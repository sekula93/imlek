<section id="vesti">
            
            <button type="button" class="arrow vesti-prev">
                <!-- <label>&lt;</label> -->
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15px" height="21px" viewBox="19.563 0.938 35.91 51.535">
                    <polygon fill="#001c44" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
                </svg>
                <span>{{ trans('front/interface.next') }}</span>
                <div class="line"></div>
            </button>

            <div class="vesti-slider">
                @foreach($recentPosts as $post)
                <a href="{{ route('front.page.show-post', ['slug' => $post->slug, 'cat' => $post->category->translations[0]->pivot->slug]) }}" class="item">

                    @if(BrowserDetect::isMobile()) 
                    <img src="{{ config('settings.image.post.landscape.upload_dir').$post->landscape_image }}">
                    @else
                    <img src="{{ config('settings.image.post.featured.upload_dir').$post->image }}">
                    @endif
                    <div class="dark-overlay"></div>
                    <div class="light-overlay"></div>
                    <div class="text">
                        <h4>{!! nl2br($post->title) !!}</h4>
                        <p>{!! nl2br(str_limit($post->excerpt, 130)) !!}</p>
                        <span>{{ $post->created_at->format('d.m.Y.') }}</span>
                    </div>
                    <!-- <div class="current-blob"><img src="/assets/front/images/home/vesti-current-blob.png"></div> -->
                    <div class="current-blob">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 433 40">
                        <path fill="#FFFFFF" d="M332,20.892c-34.634,11.794-72.967,18.157-115,19.091
                            c-42.036-0.946-80.377-7.318-115.025-19.116C67.324,9.065,33.333,2.11,0,0h433C399.988,2.217,366.321,9.181,332,20.892z"/>
                        </svg>
                    </div>
                </a>
                @endforeach
            </div>
            
            <button type="button" class="arrow vesti-next">
                <!-- <label>&gt;</label> -->
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15px" height="21px" viewBox="19.563 0.938 35.91 51.535">
                    <polygon fill="#001c44" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
                </svg>
                <span>{{ trans('front/interface.previous') }}</span>
                <div class="line"></div>
            </button>
            
            <div class="shadow-box"></div>
            <div class="arrows-vesti">
                <div class="pages"><span class="current-page">00</span> / <span class="total-pages">00</span></div>

                <div class="arrows-wrap">

                    <div class="arrows">

                        <button type="button" class="arrow vesti-mob-prev">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                <polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
                            </svg>
                        </button>

                        <div class="delimiter">|</div>

                        <button type="button" class="arrow vesti-mob-next">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                <polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
                            </svg>
                        </button>

                    </div>

                </div>

            </div>
        </section>