<section id="hero">

    <div class="hero-wrap">

        <div class="slider-wrap">

            <div class="hero-slider">
                
            @foreach($slides as $slide)

                {{-- @if($slide['type'] == 'video') --}}
                <div class="item">
                    <!-- <img src="images/temp/home-slider-image.jpg"> -->
                    <!-- <div class="image" style="background-image: url('images/temp/home-slider-image.jpg')"></div> -->
                    <div class="parallax-wrap">
                        @if($slide['type'] == 'video')
                        <!-- <div class="window" style="background-image: url('images/temp/home-slider-image-parallax.jpg');"></div> -->
                            <div class="video">
                                <video width="1280" height="720" muted playsinline loop>
                                    <source src="{{ config('settings.video.slide.upload_dir').$slide['filename'] }}" type="video/mp4">
                                </video>
                            </div>
                        @else
                            @if(BrowserDetect::isMobile() && $slide['fallback_image']) 
                                <div class="window" style="background-image: url('{{ config('settings.image.slide.upload_dir').$slide['fallback_image'] }}');"></div>
                            @else
                                <div class="window" style="background-image: url('{{ config('settings.image.slide.upload_dir').$slide['filename'] }}');"></div>
                            @endif

                        @endif
                    </div>
                    <div class="text">
                        <h4><a href="{{ $slide['url'] }}" title="">{!! nl2br($slide['title']) !!}</a></h4>
                        <p>{!! nl2br($slide['subtitle']) !!}</p>
                        <a href="{{ $slide['url'] }}" class="mobile-link">{{trans('front/interface.look')}}</a>

                    </div>
                </div>
                {{-- @else --}}
                {{-- <div class="item"> --}}
                    {{-- <img src="/assets/front/images/temp/home-slider-image.jpg">  --}}
                    {{-- <div class="image" style="background-image: url('/assets/front/images/temp/home-slider-image.jpg')"></div> --}}
                    {{-- <div class="parallax-wrap">
                        @if(BrowserDetect::isMobile() && $slide['fallback_image']) 
                            <div class="window" style="background-image: url('{{ config('settings.image.slide.upload_dir').$slide['fallback_image'] }}');"></div>
                        @else
                            <div class="window" style="background-image: url('{{ config('settings.image.slide.upload_dir').$slide['filename'] }}');"></div>
                        @endif

                    </div>

                    <div class="text">
                        <h4><a href="{{ $slide['url'] }}" title="">{!! nl2br($slide['title']) !!}</a></h4>
                        <p>{!! nl2br($slide['subtitle']) !!}</p>
                        <a href="{{ $slide['url'] }}" class="mobile-link">{{trans('front/interface.look')}}</a>
                    </div> --}}
                {{-- </div> --}}
                {{-- @endif --}}

            @endforeach

            </div>

            <div id="milk-drop"></div>

            <div id='wave-container'>
                <canvas id="canvas"></canvas>
            </div>

            <div id="milk-splash"></div>

            <div class="hero-arrows">

                <div class="pages"><span class="current-page">00</span> / <span class="total-pages">00</span></div>
                
                <div class="arrows-wrap">

                    <div class="arrows">

                        <button type="button" class="arrow hero-prev">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                <polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
                            </svg>
                        </button>

                        <div class="delimiter">|</div>

                        <button type="button" class="arrow hero-next">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                <polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
                            </svg>
                        </button>

                    </div>

                </div>

            </div>

        </div>

        <div class="shadow-box"></div>

        <div class="scroll-arrow">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="6px" viewBox="5.57 18.93 24.766 13.688">
                <polygon fill="#FFFFFF" points="17.955,32.614 5.572,21.841 8.116,18.926 17.955,27.477 27.794,18.926 30.338,21.841"/>
            </svg>
        </div>

        <div class="scroll-dots">
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
        </div>

    </div>

</section>



