@extends('front.layout.template')

@section('top_styles')
    @parent
    {{-- <link rel="stylesheet" href="/assets/front/stylesheets/vendor/jquery.fullPage.css"> --}}
@stop

@section('content')
<main id="kontakt-page">
				<div id="kontakt-title">
					<h2 class="main-title">{{ trans('front/interface.contact.contact_us') }}</h2>
				</div>

				<section id="kontakt-filter">
					<div class="wrap">
						<button type="button" class="filter-btn active" data-uid="informacije"><span class="line"></span>{{ trans('front/interface.contact.tab.informations') }}</button>
						<!-- <button type="button" class="filter-btn" data-uid="saradnja">Saradnja</button> -->
						@if(count($jobs))
						<button type="button" class="filter-btn" data-uid="posao"><span class="line"></span>{{ trans('front/interface.contact.tab.career') }}</button>
						@endif
					</div>
				</section>
				
				<section id="kontakt-forma-informacije" class="kontakt-forma">
					
					<div class="wrap">
					
						<div class="forma">

							<form id="forma-informacije">
								<input type="text" name="name" placeholder="{{trans('front/interface.contact.name')}}*">
								<input type="email" name="email" placeholder="{{trans('front/interface.contact.email')}}*">
								<input type="text" name="phone" placeholder="{{trans('front/interface.contact.phone')}}">
								<!-- <div class="g-recaptcha" data-sitekey="6LdNtDEUAAAAAEvdqgMxQtFLtMYTxt9y2dKQhPVE"></div> -->

							</form>
							
							<textarea form="forma-informacije" name="message" placeholder="{{trans('front/interface.contact.message')}}*"></textarea>
							
							<div class="button-wrap">
								<div class="msg">
									<span class="status"></span>
									<span class="error"></span>
								</div>
								<button type="button" class="submit">{{trans('front/interface.contact.send')}}</button>
							</div>

						</div>

					</div>

				</section>
				
				<!-- <section id="kontakt-forma-saradnja" class="kontakt-forma" style="display: none;">
					
					<div class="wrap">
					
						<div class="forma">

							<form id="forma-saradnja">

								<div id="positions" class="dropdown">
									<button class="dropbtn">Pozicije</button>
									<div class="arrow"><span>></span></div>
									<div class="dropdown-content">
										@foreach($jobs as $job)
										<a href="#">$job->translations[0]->pivot->title</a>
										@endforeach
									</div>
								</div>

								<input type="text" placeholder="Ime i Prezime">
								<input type="email" placeholder="Email">
								<input type="text" placeholder="Broj telefona (opciono)">
							</form>
							
							<textarea form="forma-informacije" placeholder="Vaša poruka"></textarea>

							<form id="forma-saradnja-file">
								<div class="upload-percent" style="width: 20%;"></div>
								<input type="file" name="upload-file" value="" id="upload-cv" class="input-file">
								<label for="upload-cv" class="input-file-label"><img class="icon" src="/assets/front/images/kontakt/cv-ico-add.png"><span>Dodaj CV</span></label>
							</form>

							<span class="status">Ovde se pojavljuje text kada je poruka uspešno poslata</span>
							<span class="error">Ovde se pojavljuje text ako ima neka greška</span>
							
							<div class="button-wrap">
								<button type="button" class="submit">Pošalji</button>
							</div>

						</div>

					</div>

				</section> -->
				
				<section id="kontakt-forma-posao" class="kontakt-forma" style="display: none;">
					
					<div class="wrap">
					
						<div class="forma">

							<form id="forma-posao">
								<div id="positions" class="dropdown">
									<button class="dropbtn">{{ trans('front/interface.contact.positions') }}*</button>
									<div class="arrow">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="6px" viewBox="5.57 18.93 24.766 13.688">
											<polygon fill="#001c44" points="17.955,32.614 5.572,21.841 8.116,18.926 17.955,27.477 27.794,18.926 30.338,21.841"/>
										</svg>
									</div>
									<div class="dropdown-content">
										@foreach($jobs as $job)
										<a href="#">{{ $job->translations[0]->pivot->job_label }}</a>
										@endforeach
									</div>
								</div>

								<input type="email" name="email" placeholder="{{ trans('front/interface.contact.email') }}*">
								<input type="text" name="name" placeholder="{{ trans('front/interface.contact.name') }}*">
								<input type="text" name="phone" placeholder="{{ trans('front/interface.contact.phone') }}">
							</form>
							<textarea form="forma-informacije" name="message" placeholder="{{ trans('front/interface.contact.message') }}*"></textarea>
							

							<form id="forma-posao-file">
								<div class="upload-percent" style="width: 0;"></div>
								<input type="file" name="upload-file" value="" id="upload-cv" class="input-file" accept=".doc, .docx, .pdf">
								<label for="upload-cv" class="input-file-label"><img class="icon" src="/assets/front/images/kontakt/cv-ico-add.png"><span>{{ trans('front/interface.contact.add_cv') }}*</span></label>
							</form>

							<span class="status"></span>
							<span class="error"></span>
							
							<div class="button-wrap">
								<button type="button" class="submit">{{ trans('front/interface.contact.send') }}</button>
							</div>

						</div>

					</div>

				</section>

				<section id="kontakt-info">
					<div class="wrap">
						<h4 class="title">AD Imlek</h4>
						<p class="address">Industrijsko naselje bb, Padinska skela, Beograd</p>
						<p class="contact"><span class="label">{{ trans('front/interface.contact.info_line') }}:</span> +381 11 30 50 505</p>
						<p class="contact"><span class="label">{{ trans('front/interface.contact.fax') }}:</span> +381 11 37 14 515 </p>
						<p class="contact"><span class="label">{{ trans('front/interface.contact.headquarters') }}:</span> +381 11 37 72 473</p>
						<p class="contact"><span class="label">{{ trans('front/interface.contact.email') }}:</span> info@imlek.rs</p>
					</div>
				</section>

				<section id="google-map">

					<div id="map"></div>
					
				</section>

			</main>

@stop