@extends('front.layout.template')



@section('content')

    <main id="home-page">
    @include('front.page._partials.slider')

        <section id="proizvodi">

            <div class="wrap">
            @foreach($packages as $package)
                

                <a href="{{ $package->link }}" class="item">
                    <div class="proizvod">
                        <div class="package3d" style="background: url({{ config('settings.image.package.featured.upload_dir').$package['featured_image'] }});"></div>
                        <div class="rings">
                            <div class="ring prvi"></div>
                            <div class="ring drugi"></div>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="info">
                            <h5>{!! nl2br($package->title) !!}</h5>
                            <span>{!! nl2br($package->text) !!}</span>
                            <div class="mobile-info">{{ trans('front/interface.view_product') }}</div>
                            <div class="cover"></div>
                    </div>
                </a>

            @endforeach
            </div>
            <div class="arrows3d">
                <div class="pages"><span class="current-page">00</span> / <span class="total-pages">00</span></div>
                
                <div class="arrows-wrap">

                    <div class="arrows">

                        <button type="button" class="arrow prev3d">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                <polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
                            </svg>
                        </button>

                        <div class="delimiter">|</div>

                        <button type="button" class="arrow next3d">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                <polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
                            </svg>
                        </button>

                    </div>

                </div>

            </div>


        </section>

        @include('front.page._partials.vesti')

    </main>

    


@stop