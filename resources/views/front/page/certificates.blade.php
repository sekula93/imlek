@extends('front.layout.template')

@section('heading')
@stop

@section('content')
<main id="sertifikati-page">

	<section id="sertifikati-content">
		
		<div class="back-btn-wrap">
			<a href="{{ route('front.page.history') }}">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="43px" height="43px" viewBox="0 0 43 43">
				<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="3" d="M21.5,1C27.166,1,32,3,36,7s6,8.833,6,14.5S40,32,36,36s-8.834,6-14.5,6C15.833,42,11,40,7,36s-6-8.834-6-14.5S3,11,7,7 S15.833,1,21.5,1z M25.05,29.4l-11.2-7.8l11.2-8.6"/>
				</svg>
				<span>Nazad</span>
			</a>
		</div>

		<div class="wrap">
			@foreach($certificates as $certificate)
			<a target="_blank" href="{{ config('settings.file.certificate.upload_dir').$certificate->attachment }}" class="item">
				<div class="image">
					<img src="{{ config('settings.image.certificate.upload_dir').$certificate->featured_image }}">
					<div class="light-overlay"></div>
					<div class="dark-overlay">
						<img src="/assets/front/images/sertifikati/inspect.png" class="inspect">
					</div>
				</div>
				<div class="text">
					<h4>{{ $certificate->attachment_label }}</h4>
				</div> 
			</a>
			@endforeach
		
		</div>

	</section>

	@include('front.pagination.default', ['paginator' => $certificates])

</main>
@stop
