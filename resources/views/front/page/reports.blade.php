@extends('front.layout.template')

@section('heading')
@stop

@section('content')

<main id="izvestaji-page">
	<section id="content">
		<div class="wrap">
			@foreach($reports as $report)
			<?php 
				$ext = explode('.', $report->attachment);	
			?>
			<a href="{{ config('settings.file.report.upload_dir').$report->attachment }}" download="{{ $report->translations[0]->pivot->attachment_label }}.{{ $ext[1] }}" class="document-button item">
				<div class="date">{{ $report->created_at->format('d.m.Y.') }}</div>
				<div class="label">
					{{ $report->attachment_label }}
					<img src="/assets/front/images/izvestaji/download-arrow.png" class="arrow">
				</div>
			</a>
			@endforeach
		</div>
	</section>

	@include('front.pagination.default', ['paginator' => $reports])
	
</main>
@stop