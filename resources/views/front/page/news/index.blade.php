@extends('front.layout.template')

@section('heading')
@stop

@section('content')
<main id="vesti-page">

				<section id="filter">
					<div class="wrap">
						@foreach($categories as $category)
						@if ($category->translations[0]->pivot->slug == 'sve-vesti' || $category->translations[0]->pivot->slug == 'all-news' )
							<a href="{{ route('front.page.news')}}">
								<button type="button" class="filter-btn {{ Request::segment(2) == '' || Request::segment(2) == 'sve-vesti' || Request::segment(3) == 'all-news' ? 'active' : '' }}">{{ $category->translations[0]->pivot->title }} </button>
							</a>
						@else
							<a href="{{ route('front.page.news', $category->translations[0]->pivot->slug) }}">
								<button type="button" class="filter-btn {{ Request::segment(2) == $category->translations[0]->pivot->slug ? 'active' : '' }}">{{ $category->translations[0]->pivot->title }} </button>
							</a>
						@endif
						<!-- <button type="button" class="filter-btn">Kampanje</button> -->
						<!-- <button type="button" class="filter-btn">PR</button> -->
						@endforeach
					</div>
				</section>
				<section id="filter-drop-down">
				<div class="droper">
					<button class="dropbtn">{{ Request::segment(2) ? $category->translations[0]->pivot->title : 'Sve vesti' }}</button>
					<div class="arrow">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="6px" viewBox="5.57 18.93 24.766 13.688">
							<polygon fill="#FFFFFF" points="17.955,32.614 5.572,21.841 8.116,18.926 17.955,27.477 27.794,18.926 30.338,21.841"/>
						</svg>
					</div>
				</div>
					<div class="dropdown-content">
						<!-- <a href="#">Sve vesti</a>
						<a href="#">Kampanje</a>
						<a href="#">PR</a> -->
						@foreach($categories as $category)
						@if ($category->translations[0]->pivot->slug == 'sve-vesti')
						<a href="{{ route('front.page.news')}}">
							{{ $category->translations[0]->pivot->title }} 
						</a>
						@else
						<a href="{{ route('front.page.news', $category->translations[0]->pivot->slug) }}">
							{{ $category->translations[0]->pivot->title }}
						</a>
						@endif
						<!-- <button type="button" class="filter-btn">Kampanje</button> -->
						<!-- <button type="button" class="filter-btn">PR</button> -->
						@endforeach
					</div>
				</section>

				<section id="content">
					
					<div class="wrap">
						@foreach($posts as $post)

						<a data-class="vesti-single-page" data-link="{{route('front.page.show-post', ['cat' => $post->cat_slug, 'slug' => $post->frontTranslations[0]->pivot->slug]) }}" href="{{ route('front.page.show-post', ['cat' => $post->cat_slug, 'slug' => $post->frontTranslations[0]->pivot->slug]) }}" class="item vesti-item">
							<div class="image">
								<img src="{{ config('settings.image.post.landscape.upload_dir').$post->landscape_image }}">
								<div class="light-overlay"></div>
							</div>
							<div class="text">
								<h4>{!! nl2br($post->frontTranslations[0]->pivot->title) !!}</h4>
								<p>{!! nl2br(str_limit($post->frontTranslations[0]->pivot->excerpt, 130)) !!}</p>
								<span>{{ $post->created_at->format('d.m.Y.') }}</span>
							</div>
							<div class="current-blob">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 433 40">
								<path fill="#FFFFFF" d="M332,20.892c-34.634,11.794-72.967,18.157-115,19.091
									c-42.036-0.946-80.377-7.318-115.025-19.116C67.324,9.065,33.333,2.11,0,0h433C399.988,2.217,366.321,9.181,332,20.892z"/>
								</svg>
							</div>
						</a>
						@endforeach
					
					</div>

				</section>
				@include('front.pagination.default', ['paginator' => $posts])
			</main>

@stop