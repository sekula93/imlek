@extends('front.layout.template')

@section('top_styles')
    @parent
    {{-- <link rel="stylesheet" href="/assets/front/stylesheets/vendor/jquery.fullPage.css"> --}}
@stop



@section('content')




    <main id="kompanija-page">

				<section id="about">
				
					<div class="parallax-wrap">
						
						<!-- <img src="images/kompanija/about.jpg"> -->
						@foreach($listArray as $index => $item)
						@if ($index == 0)
						<div class="window" style="background-image: url({{ config('settings.image.company.featured.upload_dir').$item->featured_image }});"></div>
						
						<div class="parallax-box reveal-wrap">

							<h3 class="title"><span class="line"></span>{{ $item->title }}</h3>
							
							<div class="text">
									{!! nl2br(str_limit($item->excerpt, 1000)) !!}
							</div>

							<div class="cover"></div>
							
							<div class="reveal">
								<a href="{{ route('front.company.show', $item->slug) }}" class="read-more">{{trans('front/interface.read_more')}}</a>
							</div>
						</div>
						@endif
						@endforeach

					</div>

				</section>

				<section id="history">
				
					<div class="parallax-wrap">
						
						<!-- <img src="/assets/front/images/kompanija/history.jpg"> -->

						<div class="history-image-slider">
							@foreach($yearsArray as $item)
							<div class="image-item">
								<img src="{{ config('settings.image.history.left.upload_dir').$item->image_left }}">
							</div>

							@endforeach

						</div>
						
						<div class="parallax-box">

							<h3 class="title"><span class="line"></span>{{ trans('front/interface.history') }}</h3>
							<div class="cover"></div>
							<div class="history-text-slider">
								@foreach($yearsArray as $item)
								<div class="item">
									<div class="text">
										<p>
										{!! nl2br($item->text) !!}
											
										</p>
										<span class="year">{{ $item->year }}.</span>
										<div class="history-cover"></div>
									</div>
								</div>
								@endforeach

							</div>
							<div class="history-arrows reveal-wrap">

								<div class="reveal">
									
									<div class="arrows-wrap">
										
										<button type="button" class="arrow history-prev">
												<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
													<polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
												</svg>
											</button>

											<div class="delimiter">|</div>

											<button type="button" class="arrow history-next">
												<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
													<polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
												</svg>
											</button>

									</div>

								</div>

							</div>

						</div>

					</div>

				</section>

				<section id="parallax">
					
					<div class="parallax-wrap">
						
						@foreach($listArray as $index => $item)
						@if ($index == 1)
						<div class="box">
							<h3 class="title"><span class="line"></span>{{ $item->title }}</h3>
							
							<div class="text">
								{!! nl2br(str_limit($item->excerpt, 1000)) !!}
							</div>
							<a href="{{ route('front.company.show', $item->slug) }}" class="read-more">{{trans('front/interface.read_more')}}</a>
							<div class="cover"></div>
						</div>

						<!-- <img src="/assets/front/images/kompanija/drustvena-odgovornost.jpg"> -->
						<div class="window" style="background-image: url({{ config('settings.image.company.featured.upload_dir').$item->featured_image }}););"></div>
						@endif
						@endforeach

					</div>
					
				</section>


				<section id="parallax">
					
					<div class="parallax-wrap">
						@foreach($listArray as $index => $item)
						@if ($index == 2)
						
						<div class="box">
							<h3 class="title"><span class="line"></span>{{ $item->title }}</h3>
							
							<div class="text">
								{!! nl2br(str_limit($item->excerpt, 1000)) !!}
							</div>
							<a href="{{ route('front.company.show', $item->slug) }}" class="read-more">{{trans('front/interface.read_more')}}</a>

							<div class="cover"></div>

						</div>

						<!-- <img src="images/kompanija/drustvena-odgovornost.jpg"> -->
						<div class="window" style="background-image: url({{ config('settings.image.company.featured.upload_dir').$item->featured_image }}););"></div>
						@endif
						@endforeach

					</div>
					
				</section>

				<section id="parallax">
			
					@foreach($listArray as $index => $item)
					@if ($index == 3)
					<div class="parallax-wrap">
						
						<div class="box">
							<h3 class="title"><span class="line"></span>{{ $item->title }}</h3>
							
							<div class="text misija-text">
								{!! nl2br(str_limit($item->excerpt, 1000)) !!}
							</div>
							<a href="{{ route('front.company.show', $item->slug) }}" class="read-more">{{trans('front/interface.read_more')}}</a>
							<div class="cover"></div>

						</div>

						<!-- <img src="images/kompanija/drustvena-odgovornost.jpg"> -->
						<div class="window" style="background-image: url({{ config('settings.image.company.featured.upload_dir').$item->featured_image }}););"></div>
					@endif
					@endforeach

					</div>
					
				</section>
				
				<section id="button">
					<div class="reveal-wrap">
						<a href="{{ route('front.page.certificates') }}" class="all-certs reveal"><img src="/assets/front/images/kompanija/sertifikati-ico.svg">{{ trans('front/interface.menu.certificates.label') }}</a>
					</div>
				</section>
				
				<section id="google-map">

					<div id="map"></div>
					
				</section>

			</main>                                    
    	
        	
@stop