@extends('front.layout.template')

@section('heading')
@stop

@section('content')
<main id="faq-page">

	<section id="content">

		<div class="wrap">

			<ul class="lista">
				@foreach($faqs as $key => $faq)
				<li class="item {{ $key == 0 ? 'active' : '' }}">
					<div class="head">
						<div class="line"></div>
						<p class="question">{{ $faq->title }}</p>
					</div>
					<div class="answer" {{ $key == 0 ? 'style=display:block;' : '' }}>
						{!! nl2br($faq->text) !!}
					</div>
				</li>
				@endforeach
				
			</ul>

		</div>

	</section>

</main>
@stop
