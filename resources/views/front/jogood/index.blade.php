<?php $routeName = Route::currentRouteName(); ?>
<!DOCTYPE html>
<html lang="sr" class="{{ BrowserDetect::isMobile() ? 'mobile-device' : '' }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108928473-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-108928473-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <meta property="og:url"                content="{{ route('front.page.jogood') }}" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="Sve puca od voća! " />
    <meta property="og:description"        content="Osetite nezaboravnu senzaciju koja puca od voća!" />
    <meta property="og:image"              content="{{ url("") }}/assets/front/images/jogood/jogood-visual.jpg" />
    <meta property="fb:app_id"             content="162961914259939" />

    <link rel="shortcut icon" type="image/png" href="/assets/front/favicon.ico"/>
    <title>Imlek</title>

    @section('top_styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">

    @show
    <link rel="stylesheet" href="/assets/front/css/main.css?asdf">

    <script>
        var csrf = '{{ csrf_token() }}';
        var basePath = '{{ url("") }}';
    </script>
</head>
<body class="jogood-bg">
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '162961914259939',
          xfbml      : true,
          version    : 'v2.10'
        });
        FB.AppEvents.logPageView();
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>

    {{-- @include('front.layout._partials.mobile-header') --}}
    <scrollbar>

        <div id="barba-wrapper">

            <div class="barba-container">

                <header id="header" class="transparent-header">
                    <nav id="navigation">
                        <div class="pavlaka-logo">
                            <a href="{{ route('front.page.index') }}" class="no-barba">
                                <img src="/assets/front/images/logo.svg">
                            </a>
                        </div>
                    </nav>
                </header>

                <main id="jogood-page">
                    <section id="product-hero" class="product-jogood" data-product="jagoda">
                        <div class="wrap">
                            <div class="jogood-slider">
                                <div class="slider--item">
                                    <img src="/assets/front/images/jogood/sku/SKU-jagoda.png" alt="jogood">
                                    <span class="parallax-fruit">
                                        <img src="/assets/front/images/jogood/sku/jagode.png" data-depth="-0.1" class="fruit">
                                    </span>
                                </div>
                                <div class="slider--item">
                                    <img src="/assets/front/images/jogood/sku/SKU-kajsija.png" alt="jogood">
                                    <span class="parallax-fruit">
                                        <img src="/assets/front/images/jogood/sku/kajsije.png" data-depth="0.1" class="fruit">
                                    </span>
                                </div>
                                <div class="slider--item">
                                    <img src="/assets/front/images/jogood/sku/SKU-svoce.png" alt="jogood">
                                    <span class="parallax-fruit">
                                        <img src="/assets/front/images/jogood/sku/kupina.png" data-depth="-0.1" class="fruit">
                                    </span>
                                </div>
                                <div class="slider--item">
                                    <img src="/assets/front/images/jogood/sku/SKU-visnja.png" alt="jogood">
                                    <span class="parallax-fruit">
                                        <img src="/assets/front/images/jogood/sku/visnja.png" data-depth="0.1" class="fruit">
                                    </span>
                                </div>
                                <div class="slider--item">
                                    <img src="/assets/front/images/jogood/sku/SKU-nar.png" alt="jogood">
                                    <span class="parallax-fruit">
                                        <img src="/assets/front/images/jogood/sku/nar.png" data-depth="-0.1" class="fruit">
                                    </span>
                                </div>
                                <div class="slider--item">
                                    <img src="/assets/front/images/jogood/sku/SKU-ananas.png" alt="jogood">
                                    <span class="parallax-fruit">
                                        <img src="/assets/front/images/jogood/sku/ananas.png" data-depth="0.1" class="fruit">
                                    </span>
                                </div>
                                <div class="slider--item">
                                    <img src="/assets/front/images/jogood/sku/SKU-mango.png" alt="jogood">
                                    <span class="parallax-fruit">
                                        <img src="/assets/front/images/jogood/sku/mango.png" data-depth="0.1" class="fruit">
                                    </span>
                                </div>
                            </div>
                            <!--./slider-->
                            <div class="info-box">
                                <a href="/proizvodi/jogood#jogood-jagoda-330g" id="product-link" class="btn product-btn no-barba">Pogledaj više</a>
                                <div class="logo-hero">
                                    <img src="/assets/front/images/jogood/JGD-logo.png" alt="jogood">
                                </div>
                            </div>
                            <!--  ./info-box  -->
                            <div class="jogood-slider--buttons">
                                <button class="arrows-jogood arrows-jogood--next"></button>
                                <button class="arrows-jogood arrows-jogood--prev"></button>
                            </div>
                            <!--  ./jogood-slider--buttons  -->
                            <span class="parallax-span">
                                <img src="/assets/front/images/jogood/milk-hero-middle.png" class="slider-milk-splash" data-depth="0.1">
                                <div id="shape" data-depth="-0.1"></div>
                            </span>
                            <div class="parallax-bg">
                                <img src="/assets/front/images/jogood/milk-splash-hero.png" class="slider-milk-bg" data-depth="0.04">
                            </div>
                        </div>
                    </section>
                    <div id="fruit-prlx-wrap">
                        <section id="video">
                            <img src="/assets/front/images/jogood/wawe-yellow.png" alt="" class="wawe-yellow">
                            <img src="/assets/front/images/jogood/milk-splash.png" class="milk-splash">
                            <img src="/assets/front/images/jogood/green-bg.png" class="green-bg">
                            <div class="wrap">
                                <div class="hidden-text">
                                    <!--                                <h3>Sve puca od voća</h3>-->
                                    <img src="/assets/front/images/jogood/sve-puca.png" alt="">
                                    <p>Jogood je voćni jogurt bogat komadićima pažljivo biranih <br class="break">plodova voća koji pruža pun užitak u savršenom ukusu.</p> 
                                    <p>Savršena slatkoća komadića voća, koji se tope u svakom zalogaju,<br class="break"> izvor je čistog zadovoljstva. </p>
                                    <br>
                                    <p>Probajte i vi neodoljiv spoj jogurta I voća, idealnog <br class="break">kao osvežavajuća užina, zdraviji doručak</p>
                                    <p>ili dobro ohlađen desert. Osetite nezaboravnu <br class="break">senzaciju koja puca od voća!</p>
                                </div>
                                <video class="afterglow" id="jogood-video" width="1280" height="728" data-youtube-id="UMN6HfRtAss" data-volume=".5"></video>
                            </div>
                            <img src="/assets/front/images/jogood/jagoda-prlx.png" class="jagoda-prlx fruit-prlx">
                            <img src="/assets/front/images/jogood/ananas-prlx.png" class="ananas-prlx fruit-prlx">
                        </section>
                        <div class="splash-space">
                            <img src="/assets/front/images/jogood/kupina-prlx.png" class="kupina-prlx fruit-prlx">
                            <img src="/assets/front/images/jogood/visnja-prlx.png" class="visnja-prlx fruit-prlx">
                            <img src="/assets/front/images/jogood/kajsija-prlx.png" class="kajsija-prlx fruit-prlx">
                        </div>
                        <section id="jogood-footer">
                            
                            <img src="/assets/front/images/jogood/nar-prlx.png" class="nar-prlx fruit-prlx">
                            <div class="wrap">

                                <img src="/assets/front/images/jogood/JGD-logo.png">

                                <h3>Neka net puca<br>od voća</h3>

                                <div class="social">
                                    <a href="#" id="fb_jogood" class="icon"><svg class="fb"><use xlink:href="/assets/front/images/svg/store/sprite.svg#fb"></use></svg></a>
                                    <a href="#" id="in_jogood" class="icon"><svg class="in"><use xlink:href="/assets/front/images/svg/store/sprite.svg#in"></use></svg></a>
                                    <a href="#" id="tw_jogood" class="icon"><svg class="tw"><use xlink:href="/assets/front/images/svg/store/sprite.svg#tw"></use></svg></a>
                                </div>

                            </div>

                        </section>
                    </div>
                </main>

            </div>

        </div>

    </scrollbar>

    <div id="preload-page">
        <div id="preload-drop"></div>
    </div>
    <div id="page-between" style="display:none"></div>

    @section('bottom_scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="/assets/front/js/afterglow.min.js"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>

    @show
    <script src="/assets/front/js/svg4everybody.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/4.13.0/bodymovin.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>

    <script src="/assets/admin/js/jquery.cookie.js"></script>
    <script src="/assets/front/js/barba.min.js"></script>
    <script src="/assets/front/js/app.min.js"></script>
    <script src="/assets/admin/js/cookie.js"></script>
    <script src="/assets/admin/js/front-contact.js"></script>
    <script src="/assets/front/js/share.js"></script>

</body>
</html>