@extends('front.layout.template')

@section('bottom_scripts')
	@parent
	@if($product->yt_link)
		<!-- <script  src="https://cdn.jsdelivr.net/afterglow/latest/afterglow.min.js"></script> -->
	@endif
@stop
@section('content')
	<main id="sku-proizvodi-page">

				<section id="sku-proizvodi-slider">
					
					<div class="sku-wrap">
						
						<div class="slider-wrap">
							@if(count($product->packshots) > 1)
							<div class="sku-arrows">

								<div class="arrows">
									<button type="button" class="arrow sku-prev">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
											<polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
										</svg>
									</button>

									<div class="delimiter">|</div>

									<button type="button" class="arrow sku-next">
										<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
											<polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
										</svg>
									</button>
								</div>

								<div class="pages"><span class="current-page">00</span> / <span class="total-pages">00</span></div>

							</div>
							@endif

							<div class="line left"></div>
							
							<div class="sku-slider {{ count($product->packshots) == 2 ? 'double-item' : '' || count($product->packshots) == 3 ? 'double-item' : '' }}">
								@if(count($product->packshots) == 2 || count($product->packshots) == 3)
									@foreach($product->packshots as $index => $packshot)
										<div class="item {{ $packshot->slug }}" data-slug="{{ $packshot->slug }}" data-index="{{ $index }}">
											<img src="{{ config('settings.image.product.packshot.upload_dir').$packshot->filename }}">
											<div class="text">
												<h4>{!! nl2br ($packshot->label) !!}</h4>
												<p>{!! nl2br($packshot->text) !!}</p>
											</div>
										</div>
									@endforeach
									@foreach($product->packshots as $index => $packshot)
										<div class="item {{ $packshot->slug }}" data-slug="{{ $packshot->slug }}" data-index="{{ $index }}">
											<img src="{{ config('settings.image.product.packshot.upload_dir').$packshot->filename }}">
											<div class="text">
												<h4>{!! nl2br ($packshot->label) !!}</h4>
												<p>{!! nl2br($packshot->text) !!}</p>
											</div>
										</div>
									@endforeach
								@else
									@foreach($product->packshots as $index => $packshot)
										<div class="item {{ $packshot->slug }}" data-slug="{{ $packshot->slug }}" data-index="{{ $index }}">
											<img src="{{ config('settings.image.product.packshot.upload_dir').$packshot->filename }}">
											<div class="text">
												<h4>{!! nl2br ($packshot->label) !!}</h4>
												<p>{!! nl2br($packshot->text) !!}</p>
											</div>
										</div>
									@endforeach
								@endif
							</div>

							<div class="line right"></div>
							@if($product->ms_link)
								<div class="microsite">
									<a target="_blank" href="{{ $product->ms_link }}" title="">{{ $product->text }}</a>
								</div>
							@endif
							<div class="social">
								{{-- @if($product->fb_link) --}}
								<a target="_blank" href="{{ $product->fb_link }}">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
										<path class="circle" d="M97.861,49.059c0,26.953-21.85,48.802-48.803,48.802c-26.952,0-48.801-21.85-48.801-48.802
											S22.106,0.257,49.058,0.257C76.012,0.257,97.861,22.106,97.861,49.059"/>
										<path class="icon" d="M61.421,52.915l1.529-10.706H52.244v-6.885c0-3.058,0.765-5.352,5.353-5.352h5.735v-9.561
											c-1.148,0-4.206-0.382-8.412-0.382c-8.03,0-13.766,4.971-13.766,14.149v8.03h-9.177v10.706h9.177v27.532h11.09V52.915H61.421z"/>
									</svg>
								</a>
								{{-- @endif --}}
								@if($product->in_link)
								<a target="_blank" href="{{ $product->in_link }}">
									{{-- <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
										<path class="circle" d="M97.618,49.059c0,26.819-21.74,48.559-48.559,48.559C22.241,97.618,0.5,75.878,0.5,49.059
											C0.5,22.241,22.241,0.5,49.059,0.5C75.878,0.5,97.618,22.241,97.618,49.059"/>
										<path class="icon" d="M78.731,70.868H66.597V53.023c0-4.641-1.787-7.853-6.068-7.853c-3.212,0-4.997,2.141-5.713,4.283
											c-0.354,0.713-0.354,1.785-0.354,2.855v18.56H42.324V36.602h12.137v5.355c0.714-2.499,4.641-5.711,10.707-5.711
											c7.853,0,13.92,4.998,13.92,15.705v18.917H78.731z M29.833,32.32L29.833,32.32c-3.927,0-6.425-2.499-6.425-6.067
											c0-3.57,2.498-6.07,6.781-6.07c3.926,0,6.424,2.5,6.424,6.07C36.257,29.822,33.759,32.32,29.833,32.32 M24.479,36.604h10.707v34.267
											H24.479V36.604z"/>
									</svg> --}}
								  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="1920px" height="1080px" viewBox="400 0 1098 1098" enable-background="new 0 0 1920 1080" xml:space="preserve">
										<path class="circle" d="M1498.468,540.65c0,297.033-240.796,537.818-537.829,537.818c-297.022,0-537.807-240.796-537.807-537.818
										S663.617,2.832,960.639,2.832C1257.683,2.832,1498.468,243.617,1498.468,540.65"></path>
									   
											<path class="icon" d="M1084.268,221.014H805.966c-105.633,0-190.951,85.319-190.951,190.951v278.302c0,105.633,85.319,190.951,190.951,190.951
												h278.302c105.633,0,190.951-85.319,190.951-190.951V411.966C1275.22,306.333,1189.901,221.014,1084.268,221.014 M1214.278,694.331
												c0,69.067-56.879,125.947-125.947,125.947H801.903c-69.068,0-125.947-56.879-125.947-125.947V407.903
												c0-69.067,56.879-125.947,125.947-125.947h286.427c69.067,0,125.947,56.879,125.947,125.947V694.331z"></path>
											<path class="icon" d="M946.133,381.495c-95.476,0-170.637,77.193-170.637,170.637c0,95.476,77.193,170.637,170.637,170.637
												c95.476,0,170.637-77.193,170.637-170.637C1116.77,456.657,1039.577,381.495,946.133,381.495 M946.133,663.859
												c-60.942,0-111.727-48.753-111.727-111.727c0-60.942,48.753-111.727,111.727-111.727c60.942,0,111.727,48.753,111.727,111.727
												C1055.828,613.075,1007.075,663.859,946.133,663.859"></path>
											<path class="icon" d="M1122.865,334.773c-22.345,0-40.628,18.282-40.628,40.628s18.282,40.628,40.628,40.628
												c22.346,0,40.628-18.282,40.628-40.628S1145.21,334.773,1122.865,334.773"></path>

									</svg>
									   
											<path class="icon" d="M1084.268,221.014H805.966c-105.633,0-190.951,85.319-190.951,190.951v278.302c0,105.633,85.319,190.951,190.951,190.951
												h278.302c105.633,0,190.951-85.319,190.951-190.951V411.966C1275.22,306.333,1189.901,221.014,1084.268,221.014 M1214.278,694.331
												c0,69.067-56.879,125.947-125.947,125.947H801.903c-69.068,0-125.947-56.879-125.947-125.947V407.903
												c0-69.067,56.879-125.947,125.947-125.947h286.427c69.067,0,125.947,56.879,125.947,125.947V694.331z"/>
											<path class="icon" d="M946.133,381.495c-95.476,0-170.637,77.193-170.637,170.637c0,95.476,77.193,170.637,170.637,170.637
												c95.476,0,170.637-77.193,170.637-170.637C1116.77,456.657,1039.577,381.495,946.133,381.495 M946.133,663.859
												c-60.942,0-111.727-48.753-111.727-111.727c0-60.942,48.753-111.727,111.727-111.727c60.942,0,111.727,48.753,111.727,111.727
												C1055.828,613.075,1007.075,663.859,946.133,663.859"/>
											<path class="icon" d="M1122.865,334.773c-22.345,0-40.628,18.282-40.628,40.628s18.282,40.628,40.628,40.628
												c22.346,0,40.628-18.282,40.628-40.628S1145.21,334.773,1122.865,334.773"/>

									</svg>

									{{-- <svg class="icon"><use xlink:href="/assets/front/images/svg/store/sprite.svg#djuricon"></use></svg> --}}
								</a>
								@endif
								{{-- @if($product->yt_link) --}}
								<a target="_blank" href="{{ $product->yt_link }}">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
										<path class="circle" d="M97.709,49.059C97.709,75.962,75.9,97.769,49,97.769c-26.9,0-48.708-21.807-48.708-48.709
											C0.291,22.158,22.099,0.35,49,0.35C75.9,0.35,97.709,22.158,97.709,49.059"/>
										<path class="icon" d="M43.029,60.353L43.027,40.25l17.495,10.079L43.029,60.353z M77.336,39.283c0,0-0.564-4.013-2.297-5.78
											c-2.201-2.321-4.666-2.333-5.797-2.467c-8.09-0.589-20.23-0.589-20.23-0.589h-0.025c0,0-12.139,0-20.232,0.589
											c-1.13,0.134-3.594,0.146-5.794,2.467c-1.734,1.768-2.298,5.78-2.298,5.78s-0.579,4.714-0.579,9.426v4.418
											c0,4.715,0.579,9.427,0.579,9.427s0.564,4.013,2.298,5.78c2.2,2.321,5.089,2.247,6.378,2.489C33.964,71.273,49,71.41,49,71.41
											s12.153-0.019,20.243-0.606c1.131-0.137,3.596-0.147,5.797-2.469c1.732-1.768,2.297-5.78,2.297-5.78s0.578-4.712,0.578-9.427v-4.418
											C77.914,43.997,77.336,39.283,77.336,39.283"/>
									</svg>
								</a>
								{{-- @endif --}}
							</div>
						</div>

						<div class="info">

							<div class="tabs">

								<div class="tab active nutri" data-uid=".product-info">{{ trans('front/interface.product_info') }}</div>

								<div class="tab videocamp" data-uid=".video-campaign">{{ trans('front/interface.video_campaign') }}</div>

							</div>
							
							<div class="info-slider">
							@foreach($product->packshots as $index => $packshot)
							<?php
								$table = json_decode($packshot->table);
								$rows = count($table);
								$columns = count($table[0]);
							?>
								<div class="item">
									<div class="box product-info active">
											@for($r = 0; $r < $rows; $r++)
												@if($r == 0)
													<div class="th">
														@for($c = 1; $c < $columns; $c++)
															<div class="item">{{ str_replace('#', '- ', $table[$r][$c]) }}</div>
														@endfor
													</div>
													@else
													<div class="tr">
														@for($c = 1; $c < $columns; $c++)
															<div class="item">{{ str_replace('#', '- ', $table[$r][$c]) }}</div>
														@endfor
													</div>     
												@endif
											@endfor
									</div>
									@if($packshot->link)
										<div class="box video-campaign">
											<video class="afterglow" id="video-{{ $index }}" width="1280" height="720" data-autoresize="fit" preload="none" data-overscale="false" poster="https://img.youtube.com/vi/{{ $packshot->link }}/hqdefault.jpg" data-youtube-id="{{ $packshot->link }}"></video>
										</div>
									@endif

								</div>
							@endforeach
							
							@if(count($product->packshots) == 2 || count($product->packshots) == 3)
								@foreach($product->packshots as $index => $packshot)
								<?php
									$table = json_decode($packshot->table);
									$rows = count($table);
									$columns = count($table[0]);
								?>
									<div class="item">
										<div class="box product-info active">
												@for($r = 0; $r < $rows; $r++)
													@if($r == 0)
														<div class="th">
															@for($c = 1; $c < $columns; $c++)
																<div class="item">{{ str_replace('#', '- ', $table[$r][$c]) }}</div>
															@endfor
														</div>
														@else
														<div class="tr">
															@for($c = 1; $c < $columns; $c++)
																<div class="item">{{ str_replace('#', '- ', $table[$r][$c]) }}</div>
															@endfor
														</div>     
													@endif
												@endfor
										</div>
										@if($packshot->link)
											<div class="box video-campaign">
												<video class="afterglow" id="video-{{ $index }}" width="1280" height="720" data-autoresize="fit" preload="none" data-overscale="false" poster="https://img.youtube.com/vi/{{ $packshot->link }}/hqdefault.jpg" data-youtube-id="{{ $packshot->link }}"></video>
											</div>
										@endif

									</div>
								@endforeach
							@endif

							</div>

						</div>

					</div>

				</section>

			</main>

@stop