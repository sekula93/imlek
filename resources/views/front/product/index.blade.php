@extends('front.layout.template')

@section('content')
	<main id="proizvodi-page">
				<section id="menu">
					<div class="lista">
						<div class="item sub">
							<a class="sub-menu">
								@foreach($brands as $brand)
									@if($brand->name == 'Moja Kravica')
										<img src="{{ config('settings.image.brand.featured.upload_dir').$brand->featured_image }}">
									@endif
								@endforeach
							</a>
							<div class="pod-lista" style="display: none;">
								@foreach($productsArray as $item)
									@if($item['brand_name'] == 'Moja Kravica')
										<div class="pod-item"><a href="{{ route('front.product.show', $item['slug']) }}">{{ $item['title'] }}</a></div>
									@endif

								@endforeach
							</div>
						</div>
						<div class="item sub">
							<a class="sub-menu">
								@foreach($brands as $brand)
									@if($brand->name == 'Balans')
										<img src="{{ config('settings.image.brand.featured.upload_dir').$brand->featured_image }}">
									@endif
								@endforeach
							</a>
							<div class="pod-lista" style="display: none;">
								@foreach($productsArray as $item)
									@if($item['brand_name'] == 'Balans')
										<div class="pod-item"><a href="{{ route('front.product.show', $item['slug']) }}">{{ $item['title'] }}</a></div>
									@endif

								@endforeach
							</div>
						</div>

							@foreach($productsArray as $item)
								@if($item['brand_name'] != 'Moja Kravica' && $item['brand_name'] != 'Balans' && $item['brand_name'] != 'Mlekara Subotica')
									<div class="item solo">
										<a href="{{ route('front.product.show', $item['slug']) }}">
											@if(\App::getLocale() == 'en') 
												@if($item['poster'])
													<img src="{{ config('settings.image.product.featured.upload_dir').$item['poster'] }}">
												@else
													<img src="{{ config('settings.image.product.featured.upload_dir').$item['featured_image'] }}">
												@endif
											@else
												<img src="{{ config('settings.image.product.featured.upload_dir').$item['featured_image'] }}">
											@endif
										</a>
									</div>
								@endif
							
							@endforeach

						<div class="item sub">
							<a class="sub-menu">
								@foreach($brands as $brand)
									@if($brand->name == 'Mlekara Subotica')
										<img src="{{ config('settings.image.brand.featured.upload_dir').$brand->featured_image }}">
									@endif
								@endforeach
							</a>
							<div class="pod-lista" style="display: none;">
								@foreach($productsArray as $item)
									@if($item['brand_name'] == 'Mlekara Subotica')
										<div class="pod-item"><a href="{{ route('front.product.show', $item['slug']) }}">{{ $item['title'] }}</a></div>
									@endif
								@endforeach
							</div>
						</div>

				   

					</div>
				</section>

				<section id="parallax">
					<div class="parallax-wrap">
						<div class="window"></div>
						<div class="parallax-box">
							<div class="wrap">
								<div class="line"></div>
								<img src="/assets/front/images/proizvodi/moja-kravica-kuhinjica.png">
								<h4 class="title">{{ trans('front/interface.recipes_and_tricks') }}</h4>
								<p class="text">{{trans('front/interface.recipes_sub')}}</p>
								<div class="cover"></div>
								<div class="reveal-wrap">
									<div class="reveal">
										<a href="{{ route('front.kuhinjica.index') }}" class="read-more">{{ trans('front/interface.read_more') }}</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</main>
@stop