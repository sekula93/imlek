<?php
    $rows = count($table);
    $columns = count($table[0]);
?>
<table class="table nutritive-table">
    <thead>
        <tr><th colspan="{{ $columns }}">TABELA NUTRITIVNIH VREDNOSTI</th></tr>
    </thead>
    <tbody>
        @for($r = 0; $r < $rows; $r++)
        <tr>
            @for($c = 0; $c < $columns; $c++)
                <td class="{{ $r == 0 || $c == 0 ? 'heading' : '' }} {{ strpos($table[$r][$c], '#') !== false ? 'text-indent' : '' }}">
                     {{ str_replace('#', '- ', $table[$r][$c]) }}
                </td>
            @endfor
        </tr>
        @endfor
    </tbody>
</table>

<p>
    {!! nl2br($legend) !!}
</p>