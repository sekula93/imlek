<?php $routeName = Route::currentRouteName(); ?>
<!DOCTYPE html>
<html lang="sr" class="{{ BrowserDetect::isMobile() ? 'mobile-device' : '' }}">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108928473-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-108928473-1');
	</script>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<meta property="og:url"                content="{{ route('front.page.pavlaka') }}" />
	<meta property="og:type"               content="article" />
	<meta property="og:title"              content="Voli je svako jelo! " />
	<meta property="og:description"        content="Ona je uvek tu za nas. Svojom pojavom uvek privlačna, otvorena za svako porodično okupljanje, savršeno se uklapa u bilo koje društvo" />
	<meta property="og:image"              content="{{ url("") }}/assets/front/images/pavlaka-visual.jpg" />
	<meta property="fb:app_id"             content="162961914259939" />

	<link rel="shortcut icon" type="image/png" href="/assets/front/favicon.ico"/>
	<title>Imlek</title>

	@section('top_styles')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">

	@show
	<link rel="stylesheet" href="/assets/front/css/main.css?asdf">

	<script>
		var csrf = '{{ csrf_token() }}';
		var basePath = '{{ url("") }}';
	</script>
</head>
<body class="pavlaka-bg">
	<script>
	  window.fbAsyncInit = function() {
		FB.init({
		  appId      : '162961914259939',
		  xfbml      : true,
		  version    : 'v2.10'
		});
		FB.AppEvents.logPageView();
	  };

	  (function(d, s, id){
		 var js, fjs = d.getElementsByTagName(s)[0];
		 if (d.getElementById(id)) {return;}
		 js = d.createElement(s); js.id = id;
		 js.src = "//connect.facebook.net/en_US/sdk.js";
		 fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>

	{{-- @include('front.layout._partials.mobile-header') --}}
	<scrollbar>

		<div id="barba-wrapper">

			<div class="barba-container">

				<header id="header">
					<nav id="navigation">
						<div class="pavlaka-logo">
							<a href="{{ route('front.page.index') }}" class="no-barba">
								<img src="/assets/front/images/logo.svg">
							</a>
						</div>
					</nav>
				</header>

				<main id="pavlaka-page">
					
					<section id="packshots">

						<div class="wrap">

							<div class="sku-mobile">
								
								<button class="pavlaka-sku-prev pavlaka-sku-arrows">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 25 35">
										<polygon class="stroke" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="3" points="25,0 0,18 25,35"/>
									</svg>
								</button>

								<div class="pavlaka-sku-slider">
									
									<a href="{{ route('front.page.index') }}/proizvodi/kisela-pavlaka#moja-kravica-kisela-pavlaka-20-mm-700g" class="item no-barba">

										<span class="call-to-action-btn">
											Pogledaj više
										</span>
										
										<img src="/assets/front/images/pavlaka/pavlaka-sku-004.png">

									</a>
									
									<a href="{{ route('front.page.index') }}/proizvodi/kisela-pavlaka#moja-kravica-kisela-pavlaka-20-mm-1400g" class="item no-barba">

										<span class="call-to-action-btn">
											Pogledaj više
										</span>
										
										<img src="/assets/front/images/pavlaka/pavlaka-sku-003.png">

									</a>
									
									<a href="{{ route('front.page.index') }}/proizvodi/kisela-pavlaka#moja-kravica-kisela-pavlaka-12-mm-180g" class="item no-barba">

										<span class="call-to-action-btn">
											Pogledaj više
										</span>
										
										<img src="/assets/front/images/pavlaka/pavlaka-sku-002.png">

									</a>
									
									<a href="{{ route('front.page.index') }}/proizvodi/kisela-pavlaka#moja-kravica-kisela-pavlaka-20-mm-180g" class="item no-barba">

										<span class="call-to-action-btn">
											Pogledaj više
										</span>
										
										<img src="/assets/front/images/pavlaka/pavlaka-sku-001.png">

									</a>

								</div>

								<button class="pavlaka-sku-next pavlaka-sku-arrows">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 25 35">
										<polygon class="stroke" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="3" points="0,0 25,18 0,35"/>
									</svg>
								</button>

								<img src="/assets/front/images/pavlaka/pavlaka-skus-table.png" class="mobile-table">

							</div>

							<div class="hidden-text">
								<h3>Moja Kravica kisela pavlaka</h3>
								<p>Poznajete je jako dugo i ona već decenijama  ostaje neizostavni deo svakog<br>Vašeg porodičnog obroka. Moja Kravica kisela pavlaka – Voli je svako jelo.</p>
							</div>

							<a href="{{ route('front.page.index') }}/proizvodi/kisela-pavlaka#moja-kravica-kisela-pavlaka-20-mm-180g" class="call-to-action--mobile-btn no-barba">
								Pogledaj više
							</a>

						</div>

					</section>

					<section id="parallax" class="pavlaka-parallax">
						<div class="parallax-wrap">
							<div class="window"></div>
						</div>
					</section>

					<section id="jela">
						
						<div class="wrap">

							<button class="prev-arrow pavlaka-arrows">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 25 35">
									<polygon class="stroke" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="3" points="25,0 0,18 25,35"/>
								</svg>
							</button>
						
							<div class="jela-slider">
								
								<div class="poklopac"></div>
								
								<div class="jela-sa-pavlakom">
									
									<img src="/assets/front/images/pavlaka/pavlaka-jela-001.png" class="jelo-sa-pavlakom jelo001">
									<img src="/assets/front/images/pavlaka/pavlaka-jela-002.png" class="jelo-sa-pavlakom jelo002">
									<img src="/assets/front/images/pavlaka/pavlaka-jela-003.png" class="jelo-sa-pavlakom jelo003">

								</div>

								<img src="/assets/front/images/pavlaka/pavlaka-jela-tanjir.png" class="tanjir">

							</div>

							<button class="next-arrow pavlaka-arrows">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 25 35">
									<polygon class="stroke" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="3" points="0,0 25,18 0,35"/>
								</svg>
							</button>

						</div>

						<div class="jela-text cover"><span class="vLine"></span><p>Savršeno se uklapa<br>u svako društvo.</p></div>

					</section>

					<section id="video">

						<div class="wrap">

							<img class="title" src="/assets/front/images/pavlaka/pavlaka-video-title.png" alt="Voli je svako jelo">

							<video class="afterglow" id="pavlaka-video" width="1280" height="728" data-youtube-id="9XvE2c0XG2o" data-volume=".5"></video>

						</div>

					</section>

					<section id="pavlaka-footer">

						<div class="wrap">
						
							<img src="assets/front/images/pavlaka/pavlaka-logo.png">

							<h3>Razmaži ovu priču<br>po mrežama</h3>

							<div class="social">
								<a href="#" id="fb_pavlaka" class="icon"><svg class="fb"><use xlink:href="/assets/front/images/svg/store/sprite.svg#fb"></use></svg></a>
								<a href="#" id="in_pavlaka" class="icon"><svg class="in"><use xlink:href="/assets/front/images/svg/store/sprite.svg#in"></use></svg></a>
								<a href="#" id="tw_pavlaka" class="icon"><svg class="tw"><use xlink:href="/assets/front/images/svg/store/sprite.svg#tw"></use></svg></a>
							</div>
						
						</div>

					</section>

				</main>

			</div>

		</div>

	</scrollbar>    

	<div id="preload-page">
		<div id="preload-drop"></div>
	</div>
	<div id="page-between" style="display:none"></div>

	@section('bottom_scripts')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
	<script src="/assets/front/js/afterglow.min.js"></script>
	<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>

	@show
	<script src="/assets/front/js/svg4everybody.min.js"></script>

	<script src="/assets/admin/js/jquery.cookie.js"></script>
	<script src="/assets/front/js/barba.min.js"></script>
	<script src="/assets/front/js/app.min.js"></script>
	<script src="/assets/admin/js/cookie.js"></script>
	<script src="/assets/admin/js/front-contact.js"></script>
	<script src="/assets/front/js/share.js"></script>

</body>
</html>