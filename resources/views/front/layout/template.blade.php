<?php $routeName = Route::currentRouteName(); ?>
<!DOCTYPE html>
<html lang="sr" class="{{ BrowserDetect::isMobile() ? 'mobile-device' : '' }}">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108928473-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-108928473-1');
	</script>

		<!-- Google Analytics -->
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-XXXXX-Y', 'auto');
	ga('send', 'pageview');
	</script>
	<!-- End Google Analytics -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

  	@if ($routeName == 'front.page.junior')
	  	{{-- junior og tags --}}
	  	<meta property="og:url"                content="{{ route('front.page.junior') }}" />
	    <meta property="og:type"               content="article" />
	    <meta property="og:title"              content="Imlek Junior!" />
	    <meta property="og:description"        content="Saznaj sve o Moja Kravica Junior mleku!" />
	    <meta property="og:image"              content="{{ url("") }}/assets/front/images/fb-junior-visual.jpg" />
	    <meta property="fb:app_id"             content="162961914259939" />

    @elseif ($routeName == 'front.page.show-post')
    	<meta property="og:url"                content="{{ url()->current() }}" />
	    <meta property="og:type"               content="article" />
	    <meta property="og:title"              content="{{ $post->title }}" />
	    <meta property="og:description"        content="" />
	    <meta property="og:image"              content="{{ url("") }}{{ config('settings.image.post.landscape.upload_dir').$post->landscape_image }}" />
	    <meta property="fb:app_id"             content="162961914259939" />

	@elseif($routeName == 'front.kuhinjica.show')
		<meta property="og:url"                content="{{ url()->current() }}" />
	    <meta property="og:type"               content="article" />
	    <meta property="og:title"              content="{{ $recipe->title }}" />
	    <meta property="og:description"        content="" />
	    <meta property="og:image"              content="" />
	    <meta property="fb:app_id"             content="162961914259939" />

    @else
	   	{{-- default og tags --}}
	   	@if($routeName == 'front.company.show')
		    {{-- <meta property="og:url"                content="{{ route('front.company.show', $item->slug) }}" /> --}}
		    <meta property="og:url"                content="{{ url()->current() }}" />
		    @if(Request::segment(2) == 'o-nama' || Request::segment(3) == 'about-us')
			    <meta property="og:image"              content="{{ url("") }}/assets/front/images/onama-visual.png" />
		    @elseif(Request::segment(2) == 'drustvena-odgovornost' || Request::segment(3) == 'social-responsibility')
			    <meta property="og:image"              content="{{ url("") }}/assets/front/images/do-visual.png" />
		    @elseif(Request::segment(2) == 'zastita-zivotne-sredine' || Request::segment(3) == 'environmental-protection')
			    <meta property="og:image"              content="{{ url("") }}/assets/front/images/zzs-visual.png" />
		    @elseif(Request::segment(2) == 'misija-i-vizija' || Request::segment(3) == 'mission-and-vision')
			    <meta property="og:image"              content="{{ url("") }}/assets/front/images/miv-visual.png" />
		    @endif
	   	@else
		    <meta property="og:url"                content="{{ url("") }}" />
	    <meta property="og:image"              content="{{ url("") }}/assets/front/images/fb-visual.png" />
	   	@endif
	    <meta property="og:type"               content="article" />
	    <meta property="og:title"              content="Novi Imlek.rs sajt je stigao!" />
	    <meta property="og:description"        content="Imlek je regionalna kompanija koja posluje na teritorijama Srbije, Crne Gore, Bosne i Hercegovine i Makedonije." />
	    <meta property="fb:app_id"             content="162961914259939" />
    @endif

	@if ($routeName == 'front.page.maslac')
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 936589400;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/936589400/?guid=ON&amp;script=0"/>
		</div>
		</noscript>
	@endif


    <link rel="shortcut icon" type="image/png" href="/assets/front/favicon.ico"/>
    <title>Imlek</title>



	@section('top_styles')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">

	@show
	<link rel="stylesheet" href="/assets/front/css/main.css">

	<script>
		var csrf = '{{ csrf_token() }}';
		var basePath = '{{ url("") }}';
	</script>
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '746612328834666');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=746612328834666&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Adform Tracking Code BEGIN -->
	<script type="text/javascript">
	    window._adftrack = Array.isArray(window._adftrack) ? window._adftrack : (window._adftrack ? [window._adftrack] : []);
	    window._adftrack.push({
	        pm: 1218298
	    });
	    (function () { var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://track.adform.net/serving/scripts/trackpoint/async/'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); })();

	</script>
	<noscript>
	    <p style="margin:0;padding:0;border:0;">
	        <img src="https://track.adform.net/Serving/TrackPoint/?pm=1218298" width="1" height="1" alt="" />
	    </p>
	</noscript>
	<!-- Adform Tracking Code END -->
</head>
<body {!! isset($bodyClass) ? 'class="'.$bodyClass.'"' : '' !!}>
	<script>
	  window.fbAsyncInit = function() {
		FB.init({
		  appId      : '162961914259939',
		  xfbml      : true,
		  version    : 'v2.10'
		});
		FB.AppEvents.logPageView();
	  };

	  (function(d, s, id){
		 var js, fjs = d.getElementsByTagName(s)[0];
		 if (d.getElementById(id)) {return;}
		 js = d.createElement(s); js.id = id;
		 js.src = "//connect.facebook.net/en_US/sdk.js";
		 fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>

	@include('front.layout._partials.mobile-header')
	<scrollbar>

		<div id="barba-wrapper">
			@include('front.layout._partials.header')

			<div class="barba-container">

				@yield('content')

			</div>
		</div>

		<footer id="footer">

			<div class="sitemap">

				<h3>Moja Kravica</h3>

				<ul class="bordered">
					<li><a href="{{ route('front.company.show', trans('front/interface.link.about.slug')) }}" class="footer-kompanija">{{ trans('front/interface.link.about.label') }}</a></li>
					<li><a href="{{ route('front.company.show', trans('front/interface.link.mission.slug')) }}" class="footer-kompanija">{{ trans('front/interface.link.mission.label') }}</a></li>
					<li><a href="{{ route('front.company.show', trans('front/interface.link.responsibility.slug')) }}" class="footer-kompanija">{{ trans('front/interface.link.responsibility.label') }}</a></li>
				</ul>

				<ul class="bordered-right footer-proizvod">
					<li><a href="{{ route('front.product.show', trans('front/interface.product.balans.slug')) }}">{{ trans('front/interface.product.balans.label') }}</a></li>
					<li><a href="{{ route('front.product.show', trans('front/interface.product.jogood.slug')) }}">{{ trans('front/interface.product.jogood.label') }}</a></li>
					<li><a href="{{ route('front.product.show', trans('front/interface.product.flert.slug')) }}">{{ trans('front/interface.product.flert.label') }}</a></li>
				</ul>

				<ul class="right footer-proizvod">
					<li><a href="{{ route('front.product.show', trans('front/interface.product.milk.slug')) }}">{{ trans('front/interface.product.milk.label') }}</a></li>
					<li><a href="{{ route('front.product.show', trans('front/interface.product.joghurt.slug')) }}">{{ trans('front/interface.product.joghurt.label') }}</a></li>
					<li><a href="{{ route('front.product.show', trans('front/interface.product.sour-milk.slug')) }}">{{ trans('front/interface.product.sour-milk.label') }}</a></li>

				</ul>

				<ul class="left footer-proizvod">
					<li><a href="{{ route('front.product.show', trans('front/interface.product.butter.slug')) }}">{{ trans('front/interface.product.butter.label') }}</a></li>
					<li><a href="{{ route('front.product.show', trans('front/interface.product.cream-cheese.slug')) }}">{{ trans('front/interface.product.cream-cheese.label') }}</a></li>
					<li><a href="{{ route('front.product.show', trans('front/interface.product.sour-cream.slug')) }}">{{ trans('front/interface.product.sour-cream.label') }}</a></li>
				</ul>

				<ul class="bordered-left footer-proizvod">
					<li><a href="{{ route('front.product.show', trans('front/interface.product.bello.slug')) }}">{{ trans('front/interface.product.bello.label') }}</a></li>
					<li><a href="{{ route('front.product.show', trans('front/interface.product.subotica.slug')) }}">{{ trans('front/interface.product.subotica.label')}}</a></li>
					{{-- <li><a href="{{ route('front.product.show', trans('front/interface.product.cheese.slug')) }}">{{ trans('front/interface.product.cheese.label') }}</a></li> --}}
					<li><a href="{{ route('front.product.show', trans('front/interface.product.nis.slug')) }}">{{ trans('front/interface.product.nis.label')}}</a></li>
				</ul>

				<ul class="bordered">
					<li><a href="{{ route('front.page.news') }}">{{ trans('front/interface.menu.news.label') }}</a></li>
					<li><a href="{{ route('front.page.reports') }}">{{ trans('front/interface.menu.reports.label') }}</a></li>
					<li><a href="{{ route('front.page.contact') }}">{{trans('front/interface.menu.contact.label')}}</a></li>
				</ul>

			</div>

			<div class="sitemap-mobile">

				<ul>
					<li><a href="{{ route('front.product.index') }}">{{ trans('front/interface.menu.products.label') }}</a></li>
					<li><a href="{{ route('front.page.history') }}">{{ trans('front/interface.menu.company.label') }}</a></li>
				</ul>

				<ul>
					<li><a href="{{ route('front.page.news') }}">{{ trans('front/interface.menu.news.label') }}</a></li>
					<li><a href="{{ route('front.kuhinjica.index') }}">{{ trans('front/interface.menu.recipe.label') }}</a></li>
					<li><a href="{{ route('front.page.contact') }}">{{ trans('front/interface.menu.contact.label') }}</a></li>
				</ul>

			</div>

			<div class="social">
				<a target="_blank" href="https://www.facebook.com/mojakravica/">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
						<path class="circle" d="M97.861,49.059c0,26.953-21.85,48.802-48.803,48.802c-26.952,0-48.801-21.85-48.801-48.802
							S22.106,0.257,49.058,0.257C76.012,0.257,97.861,22.106,97.861,49.059"/>
						<path class="icon" d="M61.421,52.915l1.529-10.706H52.244v-6.885c0-3.058,0.765-5.352,5.353-5.352h5.735v-9.561
							c-1.148,0-4.206-0.382-8.412-0.382c-8.03,0-13.766,4.971-13.766,14.149v8.03h-9.177v10.706h9.177v27.532h11.09V52.915H61.421z"/>
					</svg>
				</a>
				<a target="_blank" href="https://www.linkedin.com/company/139620/">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
						<path class="circle" d="M97.618,49.059c0,26.819-21.74,48.559-48.559,48.559C22.241,97.618,0.5,75.878,0.5,49.059
							C0.5,22.241,22.241,0.5,49.059,0.5C75.878,0.5,97.618,22.241,97.618,49.059"/>
						<path class="icon" d="M78.731,70.868H66.597V53.023c0-4.641-1.787-7.853-6.068-7.853c-3.212,0-4.997,2.141-5.713,4.283
							c-0.354,0.713-0.354,1.785-0.354,2.855v18.56H42.324V36.602h12.137v5.355c0.714-2.499,4.641-5.711,10.707-5.711
							c7.853,0,13.92,4.998,13.92,15.705v18.917H78.731z M29.833,32.32L29.833,32.32c-3.927,0-6.425-2.499-6.425-6.067
							c0-3.57,2.498-6.07,6.781-6.07c3.926,0,6.424,2.5,6.424,6.07C36.257,29.822,33.759,32.32,29.833,32.32 M24.479,36.604h10.707v34.267
							H24.479V36.604z"/>
					</svg>
				</a>
				<a target="_blank" href="https://www.youtube.com/channel/UCETXyDRNmiVa-UVtxdYc4OQ">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
						<path class="circle" d="M97.709,49.059C97.709,75.962,75.9,97.769,49,97.769c-26.9,0-48.708-21.807-48.708-48.709
							C0.291,22.158,22.099,0.35,49,0.35C75.9,0.35,97.709,22.158,97.709,49.059"/>
						<path class="icon" d="M43.029,60.353L43.027,40.25l17.495,10.079L43.029,60.353z M77.336,39.283c0,0-0.564-4.013-2.297-5.78
							c-2.201-2.321-4.666-2.333-5.797-2.467c-8.09-0.589-20.23-0.589-20.23-0.589h-0.025c0,0-12.139,0-20.232,0.589
							c-1.13,0.134-3.594,0.146-5.794,2.467c-1.734,1.768-2.298,5.78-2.298,5.78s-0.579,4.714-0.579,9.426v4.418
							c0,4.715,0.579,9.427,0.579,9.427s0.564,4.013,2.298,5.78c2.2,2.321,5.089,2.247,6.378,2.489C33.964,71.273,49,71.41,49,71.41
							s12.153-0.019,20.243-0.606c1.131-0.137,3.596-0.147,5.797-2.469c1.732-1.768,2.297-5.78,2.297-5.78s0.578-4.712,0.578-9.427v-4.418
							C77.914,43.997,77.336,39.283,77.336,39.283"/>
					</svg>
				</a>
			</div>

			<div class="copy">
				<p>© {{ date("Y")}} Copyright  |  All rights reserved {{ date("Y")}} ®</p>
			</div>
			<img src="/assets/front/images/junior/footer-grass.png" class="grass" alt="">
			<img src="/assets/front/images/junior/tree.png" class="tree" alt="">
			<img src="/assets/front/images/junior/cow.png" class="cow" alt="">
		</footer>

	</scrollbar>

	<article class="junior-overlay">
		<div class="overlay-wrapper">
	        <div class="overlay-content">
	        	<div class="back-btn-wrap">
	                <div class="back-btn close-junior-overlay">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="43px" height="43px" viewBox="0 0 43 43">
						<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="3" d="M21.5,1C27.166,1,32,3,36,7s6,8.833,6,14.5S40,32,36,36s-8.834,6-14.5,6C15.833,42,11,40,7,36s-6-8.834-6-14.5S3,11,7,7 S15.833,1,21.5,1z M25.05,29.4l-11.2-7.8l11.2-8.6"></path>
						</svg>
						<span>Nazad</span>
					</div>
					<!-- ./back-btn -->
	            </div>
	            <!-- ./back-btn-wrap -->
	            <div class="sku-wrap">
	                <div class="slider-wrap">
	                    <div class="sku-arrows">
	                        <div class="arrows">
	                            <button type="button" class="arrow sku-prev">
	                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
	                                    <polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
	                                </svg>
	                            </button>
	                            <div class="delimiter">|</div>
	                            <button type="button" class="arrow sku-next">
	                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
	                                    <polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
	                                </svg>
	                            </button>
	                        </div>
	                        <div class="pages"><span class="current-page">01</span> / <span class="total-pages">02</span></div>
	                    </div>

	                    <div class="line left"></div>

	                    <div class="junior-sku-slider">
	                        <div class="item">
	                        	<div class="slide-transition"></div>
	                            <img src="/assets/front/images/junior/malo-pakovanje.png">
	                            <div class="text">
	                                <h4>Moja Kravica Junior 250ml</h4>
	                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex saepe odio animi, dolores impedit architecto fuga quam ut, accusamus totam illo doloremque ipsum, deleniti veritatis numquam corporis commodi repellat quasi.</p>
	                            </div>
	                        </div>
	                        <!-- ./item -->
	                        <div class="item">
	                        	<div class="slide-transition"></div>
	                            <img src="/assets/front/images/junior/veliko-pakovanje.png">
	                            <div class="text">
	                                <h4>Moja Kravica Junior 250ml</h4>
	                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex saepe odio animi, dolores impedit architecto fuga quam ut, accusamus totam illo doloremque ipsum, deleniti veritatis numquam corporis commodi repellat quasi.</p>
	                            </div>
	                            <!-- ./text -->
	                            <div class="info">
	                            	<div class="tab active nutri">Informacije o proizvodu</div>
	                            	<div class="box product-info active">
                                        <div class="th">
                                            <div class="box-item">Proizvod i opis</div>
                                            <div class="box-item">Pakovanje/SKU</div>
                                            <div class="box-item">mlečna mast</div>
                                            <div class="box-item">Rok trajanja i uslovi čuvanja</div>
                                            <div class="box-item">Transportno pakovanje</div>
                                            <div class="box-item">Proteini</div>
                                            <div class="box-item">Ugljeni hidrati  </div>
                                            <div class="box-item">Energetska vrednost</div>
	                                    </div>
                                        <div class="tr">
                                            <div class="box-item">Balans+ jogurt </div>
                                            <div class="box-item">TTOSO 1kg</div>
                                            <div class="box-item">1g</div>
                                            <div class="box-item">25 dana</div>
                                            <div class="box-item">12 komada</div>
                                            <div class="box-item">3,2g</div>
                                            <div class="box-item">5,2g</div>
                                            <div class="box-item">192kJ/46kcal</div>
	                                    </div>
                                    </div>
                                    <!-- ./product-info -->
	                            </div>
	                            <!-- ./info -->
	                        </div>
	                        <!-- ./item -->
	                    </div>

	                    <div class="line right"></div>
	                </div>
	                <!-- ./slider-wrap -->
	            </div>
	            <!-- ./sku-wrap -->
	        </div>
	        <!-- ./overlay-content -->
        </div>
        <!-- ./overlay-wrapper -->
    </article>
    <!-- ./junior-overlay -->

    <article class="product-zoom-overlay">
        <div class="overlay-content-wrap">
            <div class="overlay-content">
                <div class="close-zoom-overlay"><span>✕</span></div>
                <div class="image-box">
                    <img src="/assets/front/images/junior/zoom_malo_pakovanje.jpg" alt="">
                </div>
                <!-- ./image-box -->
            </div>
            <!-- ./overlay-content -->
        </div>
        <!-- ./overlay-content-wrap -->
    </article>

	<div id="preload-page">
		{{-- <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" id="intro">
			<path id="wave" d="" fill="#fff"/>
		</svg> --}}
		<div id="preload-drop"></div>
	</div>
	<div id="page-between" style="display:none"></div>
	@section('bottom_scripts')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
	<script src="/assets/front/js/afterglow.min.js"></script>
	<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0eAslnR2xVwg8Z8pGDmGJGKB_eX1e1_Q&callback=mapReady" async defer></script>
	<script src="/assets/front/js/gMap.js"></script>


		<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCswFck-9vaWwGpqMacchgFsI3iLrsTosc&callback=initMap" async defer></script> -->


	@show
	<!-- <script src="/assets/admin/js/jqueryLoad.js" async defer></script> -->
    <script src="/assets/admin/js/jquery.cookie.js"  ></script>
	<script src="/assets/front/js/barba.min.js"  ></script>
	<script src="/assets/front/js/app.min.js?a"  ></script>
	<script src="/assets/admin/js/cookie.js"  ></script>
	<script src="/assets/admin/js/front-contact.js"  ></script>
	<script src="/assets/front/js/share.js"  ></script>

	<script>
	        
	        $(document).on('click', '.play_btn', function(event) {
	            ga('send', {
	                hitType: 'event',
	                eventCategory: 'Videos',
	                eventAction: 'play',
	                eventLabel: 'Kuhinjica'
	            });
	            console.log('PLAY');
	        });


    </script>

</body>
</html>