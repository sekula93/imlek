<footer id="footer">
				
				<div class="sitemap">
					
					<h3>Moja Kravica</h3>
					
					<ul class="bordered">
						<li><a href="#">O Nama</a></li>
						<li><a href="#">Misija i Vizija</a></li>
						<li><a href="#">Odgovornost</a></li>
					</ul>
					
					<ul class="bordered-right">
						<li><a href="#">Balans</a></li>
						<li><a href="#">Jogood</a></li>
						<li><a href="#">Flert</a></li>
					</ul>
					
					<ul class="right">
						<li><a href="#">Mleko</a></li>
						<li><a href="#">Jogurt</a></li>
						<li><a href="#">Kiselo mleko</a></li>
					</ul>
					
					<ul class="left">
						<li><a href="#">Maslac</a></li>
						<li><a href="#">Krem sir</a></li>
						<li><a href="#">Kisela pavlaka</a></li>
					</ul>
					
					<ul class="bordered-left">
						<li><a href="#">Bello</a></li>
						<li><a href="#">Sirevi</a></li>
						<li><a href="#">Ostalo</a></li>
					</ul>
					
					<ul class="bordered">
						<li><a href="#">Vesti</a></li>
						<li><a href="#">Izveštaji</a></li>
						<li><a href="#">Kontakt</a></li>
					</ul>

				</div>

				<div class="sitemap-mobile">
					
					<ul>
						<li><a href="#">Proizvodi</a></li>
						<li><a href="#">Kompanija</a></li>
					</ul>

					<ul>
						<li><a href="#">Vesti</a></li>
						<li><a href="#">Kuhinjica</a></li>
						<li><a href="#">Kontakt</a></li>
					</ul>

				</div>

				<div class="social">
					<a href="https://www.facebook.com/mojakravica/">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
							<path class="circle" d="M97.861,49.059c0,26.953-21.85,48.802-48.803,48.802c-26.952,0-48.801-21.85-48.801-48.802
								S22.106,0.257,49.058,0.257C76.012,0.257,97.861,22.106,97.861,49.059"/>
							<path class="icon" d="M61.421,52.915l1.529-10.706H52.244v-6.885c0-3.058,0.765-5.352,5.353-5.352h5.735v-9.561
								c-1.148,0-4.206-0.382-8.412-0.382c-8.03,0-13.766,4.971-13.766,14.149v8.03h-9.177v10.706h9.177v27.532h11.09V52.915H61.421z"/>
						</svg>
					</a>
					<a href="https://www.linkedin.com/company/139620/">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
							<path class="circle" d="M97.618,49.059c0,26.819-21.74,48.559-48.559,48.559C22.241,97.618,0.5,75.878,0.5,49.059
								C0.5,22.241,22.241,0.5,49.059,0.5C75.878,0.5,97.618,22.241,97.618,49.059"/>
							<path class="icon" d="M78.731,70.868H66.597V53.023c0-4.641-1.787-7.853-6.068-7.853c-3.212,0-4.997,2.141-5.713,4.283
								c-0.354,0.713-0.354,1.785-0.354,2.855v18.56H42.324V36.602h12.137v5.355c0.714-2.499,4.641-5.711,10.707-5.711
								c7.853,0,13.92,4.998,13.92,15.705v18.917H78.731z M29.833,32.32L29.833,32.32c-3.927,0-6.425-2.499-6.425-6.067
								c0-3.57,2.498-6.07,6.781-6.07c3.926,0,6.424,2.5,6.424,6.07C36.257,29.822,33.759,32.32,29.833,32.32 M24.479,36.604h10.707v34.267
								H24.479V36.604z"/>
						</svg>
					</a>
					<a href="https://www.youtube.com/channel/UCETXyDRNmiVa-UVtxdYc4OQ">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
							<path class="circle" d="M97.709,49.059C97.709,75.962,75.9,97.769,49,97.769c-26.9,0-48.708-21.807-48.708-48.709
								C0.291,22.158,22.099,0.35,49,0.35C75.9,0.35,97.709,22.158,97.709,49.059"/>
							<path class="icon" d="M43.029,60.353L43.027,40.25l17.495,10.079L43.029,60.353z M77.336,39.283c0,0-0.564-4.013-2.297-5.78
								c-2.201-2.321-4.666-2.333-5.797-2.467c-8.09-0.589-20.23-0.589-20.23-0.589h-0.025c0,0-12.139,0-20.232,0.589
								c-1.13,0.134-3.594,0.146-5.794,2.467c-1.734,1.768-2.298,5.78-2.298,5.78s-0.579,4.714-0.579,9.426v4.418
								c0,4.715,0.579,9.427,0.579,9.427s0.564,4.013,2.298,5.78c2.2,2.321,5.089,2.247,6.378,2.489C33.964,71.273,49,71.41,49,71.41
								s12.153-0.019,20.243-0.606c1.131-0.137,3.596-0.147,5.797-2.469c1.732-1.768,2.297-5.78,2.297-5.78s0.578-4.712,0.578-9.427v-4.418
								C77.914,43.997,77.336,39.283,77.336,39.283"/>
						</svg>
					</a>
				</div>
				
				<div class="copy">
					<p>© 2017 Copyright  |  All rights reserved 2017 ®</p>
				</div>

			</footer>