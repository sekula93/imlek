<header id="header" class="{{ $routeName != 'front.page.index' ? 'header-shadow' : '' }}">

    <nav id="navigation">

        <ul class="nav left">
            <li data-class="home-page" data-link="{{ route('front.page.index') }}" class="nav-item home-nav {{ $routeName == 'front.page.index' ? 'active' : '' }}"><a href="{{ route('front.page.index') }}">{{ trans('front/interface.menu.home.label') }}</a></li>
            <li data-class="kompanija-page" data-link="{{ route('front.page.history') }}" class="nav-item kompanija-nav {{ $routeName == 'front.page.history' ? 'active' : '' }}"><a href="{{ route('front.page.history') }}" class="always-on">{{ trans('front/interface.menu.company.label') }}</a></li>
            <li data-class="proizvodi-page" data-link="{{ route('front.product.index') }}" class="nav-item proizvodi-nav {{ stripos($routeName, 'front.product') !== false ? 'active' : '' }}"><a href="{{ route('front.product.index') }}" class="always-on">{{ trans('front/interface.menu.products.label') }}</a></li>
            <li data-class="vesti-page" data-link="{{ route('front.page.news') }}" class="nav-item {{ stripos($routeName, 'front.page.news') !== false ? 'active' : '' }}"><a href="{{ route('front.page.news') }}" class="always-on">{{ trans('front/interface.menu.news.label') }}</a></li>
           
                <li data-class="sertifikati-page" data-link="{{ route('front.page.certificates') }}" class="nav-item sertifikati-nav {{ stripos($routeName, 'front.page.certificates') !== false ? 'active' : '' }}"><a href="{{ route('front.page.certificates') }}" class="always-on">{{ trans('front/interface.menu.certificates.label') }}</a></li>
           
        </ul>

        <div class="logo">
            <a href="{{ route('front.page.index') }}">
                <img src="/assets/front/images/logo.svg">
            </a>
        </div>
        <div id="milk-blob"></div>
        <div class="blob">
            <!-- <img src="/assets/front/images/header-milk-blob.svg"> -->
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="169px" height="58px" viewBox="0 0 169 58">
                <path fill="#FFFFFF" d="M84.5,57.922c-17.731,0.504-31.256-8.026-40.575-25.591 C34.611,14.771,19.969,3.994,0,0h169c-19.967,3.995-34.616,14.78-43.95,32.356C115.75,49.9,102.233,58.421,84.5,57.922z"/>
            </svg>
        </div>

        <ul class="nav right">
            <li data-class="izvestaji-page" data-link="{{ route('front.page.reports') }}" class="nav-item {{ $routeName == 'front.page.reports' ? 'active' : '' }}"><a href="{{ route('front.page.reports') }}">{{ trans('front/interface.menu.reports.label') }}</a></li>
            <li data-class="faq-page" data-link="{{ route('front.page.faq') }}" class="nav-item {{ $routeName == 'front.page.faq' ? 'active' : '' }}"><a href="{{ route('front.page.faq') }}">{{ trans('front/interface.menu.faq.label') }}</a></li>
            <li data-class="kontakt-page" data-link="{{ route('front.page.contact') }}" class="nav-item {{ $routeName == 'front.page.contact' ? 'active' : '' }}"><a href="{{ route('front.page.contact') }}">{{ trans('front/interface.menu.contact.label') }}</a></li>
            <li data-class="kuhinjica-page" data-link="{{ route('front.kuhinjica.index') }}" class="nav-item {{ stripos($routeName, 'front.kuhinjica') !== false ? 'active' : '' }}"><a href="{{ route('front.kuhinjica.index') }}" class="always-on">{{ trans('front/interface.menu.recipe.label') }}</a></li>

                <li data-class="junior-page" data-link="{{ route('front.page.junior') }}" class="nav-item {{ stripos($routeName, 'front.page.junior') !== false ? 'active' : '' }}"><a href="{{ route('front.page.junior') }}" class="always-on">Junior</a></li>

        </ul>

    </nav>

    <div id="lng" class="dropdown">

        <button class="dropbtn">
            <div class="arrow">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="6px" viewBox="5.57 18.93 24.766 13.688">
                    <polygon fill="#FFFFFF" points="17.955,32.614 5.572,21.841 8.116,18.926 17.955,27.477 27.794,18.926 30.338,21.841"></polygon>
                </svg>
            </div>
         {{ \App::getLocale() == 'en' ? 'En' : 'Sr' }}</button>
        <div class="dropdown-content">
            <a class="no-barba" href="/">Sr</a>
            <a class="no-barba" href="/en">En</a>
        </div>
    </div>

    <div id="hamburger" class="closed">
        <span></span>
        <span></span>
        <span>MENU</span>
    </div>

</header>