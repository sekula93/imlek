<?php
    $notClear = count($recentPosts['news']) || count($recentPosts['instagram']) || count($recentPosts['facebook']);
    $i = 0;
?>

@while($notClear)
    @if(count($recentPosts['news']))
        @include('front.layout._partials.post.news', ['post' => $recentPosts['news'][0]])
        <?php $recentPosts['news']->shift(); ?>
    @endif
    @if(count($recentPosts['instagram']))
        @include('front.layout._partials.post.instagram', ['post' => $recentPosts['instagram'][0]])
        <?php $recentPosts['instagram']->shift(); ?>
    @endif
    @if(count($recentRecipes))
        @include('front.layout._partials.post.recipe', ['recipe' => $recentRecipes[0]])
        <?php $recentRecipes->shift(); ?>
    @endif
    @if(count($recentPosts['facebook']))
        @include('front.layout._partials.post.facebook', ['post' => $recentPosts['facebook'][0]])
        <?php $recentPosts['facebook']->shift(); ?>
    @endif
    @if(count($recentPosts['news']))
        @include('front.layout._partials.post.news', ['post' => $recentPosts['news'][0]])
        <?php $recentPosts['news']->shift(); ?>
    @endif
    <?php
        $notClear = count($recentPosts['news']) || count($recentPosts['instagram']) || count($recentPosts['facebook']);
        $i++;
    ?>
@endwhile
