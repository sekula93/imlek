@extends('front.layout.template')

@section('content')
    <main id="junior-page">
        <section id="hero">
            <div class="hero-wrap">
                <div class="slider-wrap">
                    <div class="hero-slider">
                        <div class="item">
                            <div class="parallax-wrap">
                                <a href="javascript:;" title="">
                                    <div class="video">
                                    </div>
                                </a>
                            </div>
                            <!-- ./parallax-wrap -->
                            <div class="text">
                                {{-- <a href="#" title="" target="_blank"> --}}
                                    {{-- <h4>{!! trans('front/junior.title') !!}</h4> --}}
                                    {{-- <p>{!! trans('front/junior.slider') !!}</p> --}}
                                {{-- </a> --}}
                            </div>
                            <!-- ./text -->
                        </div>
                        <!-- ./item -->
                    </div>
                    <!-- ./hero-slider -->
                    <div class="scroll-arrow">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="6px" viewBox="5.57 18.93 24.766 13.688">
                            <polygon fill="#FFFFFF" points="17.955,32.614 5.572,21.841 8.116,18.926 17.955,27.477 27.794,18.926 30.338,21.841"/>
                        </svg>
                    </div>
                    <div class="scroll-dots green">
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                    </div>
                </div>
                <!-- ./slider-wrap -->
            </div>
            <!-- ./hero-wrap -->
        </section>

       <!--  <div class="about-wrap">
            <section id="about">
                <div class="parallax-wrap">
                    <div class="window" style="background-image: url(/assets/front/images/junior/about-photo.jpg);"></div>
                    <div class="parallax-box reveal-wrap">
                        <h3 class="title"><span class="line"></span>Moja Kravica Junior mleko za naše najmlađe</h3>
                        <div class="text">
                            <p>Kao i uvek, želimo samo najbolje za naše najmlađe. Ovde ide neki kratak tekst o proizvodu i glavnim benefitima. Najbolje bi bilo da to budu tri do četiri rečenice propraćene nekim atraktivnim lifestyle fotografijama sa proizvodom. Kao i uvek, želimo samo najbolje za decu</p>
                            <p>Ovde ide neki kratak tekst o proizvodu i glavnim benefitima. Najbolje bi bilo da to budu tri do četiri rečenice propraćevne nekim atraktivnim lifestyle fotografijama sa proizvodom.</p>
                        </div>
                        <div class="cover"></div>
                    </div>
                </div>
            </section>
        </div> -->
        <section id="proizvodi" class="junior">
            <device class="mobile">
                <div class="junior-sku-slider">
                    <div class="item">
                        <div class="slide-transition"></div>
                        <img src="/assets/front/images/junior/malo-pakovanje.png">
                        <!-- <div class="text">
                            <h4>Moja Kravica Junior 250ml</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex saepe odio animi, dolores impedit architecto fuga quam ut, accusamus totam illo doloremque ipsum, deleniti veritatis numquam corporis commodi repellat quasi.</p>
                        </div> -->
                    </div>
                    <!-- ./item -->
                    <div class="item">
                        <div class="slide-transition"></div>
                        <img src="/assets/front/images/junior/veliko-pakovanje.png">
                        <!-- <div class="text">
                            <h4>Moja Kravica Junior 250ml</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex saepe odio animi, dolores impedit architecto fuga quam ut, accusamus totam illo doloremque ipsum, deleniti veritatis numquam corporis commodi repellat quasi.</p>
                        </div> -->
                    </div>
                    <!-- ./item -->
                </div>
                <!-- ./junior-sku-slider -->
                <div class="sku-arrows junior-sku-arrows">
                    <div class="arrows">
                        <button type="button" class="arrow sku-prev">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                <polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
                            </svg>
                        </button>
                        <div class="delimiter">|</div>
                        <button type="button" class="arrow sku-next">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                <polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
                            </svg>
                        </button>
                    </div>
                    <div class="pages"><span class="current-page">01</span> / <span class="total-pages">02</span></div>
                </div>
            </device>
            <!-- ./mobile -->
            <device class="desktop">
                <div class="wrap">
                    <div class="junior-item left open-junior-overlay">
                        <div class="proizvod hidden-product" data-src="/assets/front/images/junior/zoom_veliko_pakovanje.jpg">
                            <img src="/assets/front/images/junior/veliko-pakovanje.png" alt="" class="package">
                        </div>
                        <!-- ./proizvod -->
                    </div>
                    <!-- ./junior-item -->
                    <div class="junior-item right open-junior-overlay">
                        <div class="proizvod hidden-product" data-src="/assets/front/images/junior/zoom_malo_pakovanje.jpg">
                            <img src="/assets/front/images/junior/malo-pakovanje.png" alt="" class="package">
                        </div>
                        <!-- ./proizvod -->
                    </div>
                    <!-- ./junior-item -->
                </div>
            </device>
            <!-- ./desktop -->
        </section>

        <section id="product-features">
            <div class="parallax-wrap">
                <div class="junior-heading-wrap">
                    <div class="junior-heading-cover">
                        <div class="section-header junior-heading">
                            <h3>{!! trans('front/junior.title') !!}<br>{!! trans('front/junior.subtitle') !!}</h3>
                            <p>{!! trans('front/junior.pasus1') !!}</p><br>
                            <p>{!! trans('front/junior.pasus2') !!}</p><br>
                            <p>{!! trans('front/junior.pasus3') !!}</p>
                        </div>
                        <!-- ./section-header -->
                    </div>
                    <!-- ./junior-heading-cover -->
                </div>
                <!-- ./junior-heading-wrap -->
                <div class="section-content">
                    <device class="mobile">
                        <div class="junior-slider-mobile">
                            <img src="/assets/front/images/junior/junior-logo.png" class="logo" alt="">
                            <div class="icons-slider">
                                <div class="icons junior-icons">
                                    <div class="item">
                                        <div class="icon icon-1 active" data-slide="0">
                                            <svg><use xlink:href="/assets/front/images/svg/store/sprite.svg#deca-icon"></use></svg>
                                            <div class="bg"></div>
                                            <div class="first"></div>
                                            <div class="second"></div>
                                            <div class="third"></div>
                                        </div>
                                    </div>
                                    <!-- ./item -->
                                    <div class="item">
                                        <div class="icon icon-2" data-slide="1">
                                            <svg><use xlink:href="/assets/front/images/svg/store/sprite.svg#stit-icon"></use></svg>
                                            <div class="bg"></div>
                                            <div class="first"></div>
                                            <div class="second"></div>
                                            <div class="third"></div>
                                        </div>
                                    </div>
                                    <!-- ./item -->
                                    <div class="item">
                                        <div class="icon icon-3" data-slide="2">
                                            <svg><use xlink:href="/assets/front/images/svg/store/sprite.svg#srce-icon"></use></svg>
                                            <div class="bg"></div>
                                            <div class="first"></div>
                                            <div class="second"></div>
                                            <div class="third"></div>
                                        </div>
                                    </div>
                                    <!-- ./item -->
                                    <div class="item">
                                        <div class="icon icon-4" data-slide="3">
                                            <svg><use xlink:href="/assets/front/images/svg/store/sprite.svg#sijalica-icon"></use></svg>
                                            <div class="bg"></div>
                                            <div class="first"></div>
                                            <div class="second"></div>
                                            <div class="third"></div>
                                        </div>
                                    </div>
                                    <!-- ./item -->
                                    <div class="item">
                                        <div class="icon icon-5" data-slide="4">
                                            <svg><use xlink:href="/assets/front/images/svg/store/sprite.svg#zub-icon"></use></svg>
                                            <div class="bg"></div>
                                            <div class="first"></div>
                                            <div class="second"></div>
                                            <div class="third"></div>
                                        </div>
                                    </div>
                                    <!-- ./item -->
                                    <div class="item">
                                        <div class="icon icon-6" data-slide="5">
                                            <svg><use xlink:href="/assets/front/images/svg/store/sprite.svg#oko-icon"></use></svg>
                                            <div class="bg"></div>
                                            <div class="first"></div>
                                            <div class="second"></div>
                                            <div class="third"></div>
                                        </div>
                                    </div>
                                    <!-- ./item -->
                                    <div class="item">
                                        <div class="icon icon-7" data-slide="6">
                                            <svg><use xlink:href="/assets/front/images/svg/store/sprite.svg#burek-icon"></use></svg>
                                            <div class="bg"></div>
                                            <div class="first"></div>
                                            <div class="second"></div>
                                            <div class="third"></div>
                                        </div>
                                    </div>
                                    <!-- ./item -->
                                    <div class="item">
                                        <div class="icon icon-8" data-slide="7">
                                            <svg><use xlink:href="/assets/front/images/svg/store/sprite.svg#seka-icon"></use></svg>
                                            <div class="bg"></div>
                                            <div class="first"></div>
                                            <div class="second"></div>
                                            <div class="third"></div>
                                        </div>
                                    </div>
                                    <!-- ./item -->
                                </div>

                            </div>
                            <!-- ./icons-slider -->
                            <div class="content-slider">
                                <div class="item">
                                    <div class="slide-transition"></div>
                                    <div class="heading"> {!! trans('front/junior.krugOrange.title') !!} <br>{!! trans('front/junior.krugOrange.subtitle') !!}</div>
                                    <p>{{ trans('front/junior.krugOrange.text') !!}</p>
                                </div>
                                <div class="item">
                                    <div class="slide-transition"></div>
                                    <div class="heading">{!! trans('front/junior.krugViolet.title') !!}<br>{!! trans('front/junior.krugViolet.subtitle') !!}</div>
                                    <p>{!! trans('front/junior.krugViolet.text') !!}</p>
                                </div>
                                <div class="item">
                                    <div class="slide-transition"></div>
                                    <div class="heading">{!! trans('front/junior.krugDarkGreen.title') !!}<br>{!! trans('front/junior.krugDarkGreen.subtitle') !!}</div>
                                    <p>{!! trans('front/junior.krugDarkGreen.text') !!}</p>
                                </div>
                                <div class="item">
                                    <div class="slide-transition"></div>
                                    <div class="heading">{!! trans('front/junior.krugLightGreen.title') !!}<br>{!! trans('front/junior.krugLightGreen.subtitle') !!}</div>
                                    <p>{!! trans('front/junior.krugLightGreen.text') !!}</p>
                                </div>
                                <div class="item">
                                    <div class="slide-transition"></div>
                                    <div class="heading">{!! trans('front/junior.krugPlin.title') !!}<br>{!! trans('front/junior.krugPlin.subtitle') !!}</div>
                                    <p>{!! trans('front/junior.krugPlin.text') !!}</p>
                                </div>
                                <div class="item">
                                    <div class="slide-transition"></div>
                                    <div class="heading">{!! trans('front/junior.krugRed.title') !!}<br>{!! trans('front/junior.krugRed.subtitle') !!}</div>
                                    <p>{!! trans('front/junior.krugRed.text') !!}</p>
                                </div>
                                <div class="item">
                                    <div class="slide-transition"></div>
                                    <div class="heading">{!! trans('front/junior.krugGreenSpiral.title') !!}<br>{!! trans('front/junior.krugGreenSpiral.subtitle') !!}</div>
                                    <p>{!! trans('front/junior.krugGreenSpiral.text') !!}</p>
                                </div>
                                <div class="item">
                                    <div class="slide-transition"></div>
                                    <div class="heading">{!! trans('front/junior.krugBlue.title') !!}<br>{!! trans('front/junior.krugBlue.subtitle') !!}</div>
                                    <p>{!! trans('front/junior.krugBlue.text') !!}</p>
                                </div>

                            </div>
                            <!-- ./content-slider -->
                            <div class="sku-arrows junior-arrows-mobile">
                                <div class="arrows">
                                    <button type="button" class="arrow sku-prev">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                            <polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
                                        </svg>
                                    </button>
                                    <div class="delimiter">|</div>
                                    <button type="button" class="arrow sku-next">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                            <polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
                                        </svg>
                                    </button>
                                </div>
                                <!-- <div class="pages"><span class="current-page">01</span> / <span class="total-pages">02</span></div> -->
                            </div>
                            <!-- ./sku-arrows -->
                            <img src="/assets/front/images/junior/krug-shadow.png" class="mobile-shadow" alt="">
                        </div>
                        <!-- ./junior-slider-mobile -->
                    </device>
                    <!-- ./mobile -->
                    <device class="desktop">
                        <div class="radial-wrapper">
                            <div id="container">
                                <div id="halfclip">
                                    <div class="halfcircle" id="clipped">
                                    </div>
                                </div>
                                <div class="halfcircle" id="fixed">
                                </div>
                            </div>
                            <ul class="icons junior-icons">
                                <li class="icon icon-1 active" data-slide="0">
                                    <svg class="icon"><use xlink:href="/assets/front/images/svg/store/sprite.svg#deca-icon"></use></svg>
                                    <div class="bg"></div>
                                    <div class="first"></div>
                                    <div class="second"></div>
                                    <div class="third"></div>
                                </li>
                                <li class="icon icon-2" data-slide="1">
                                    <svg class="icon"><use xlink:href="/assets/front/images/svg/store/sprite.svg#stit-icon"></use></svg>
                                    <div class="bg"></div>
                                    <div class="first"></div>
                                    <div class="second"></div>
                                    <div class="third"></div>
                                </li>
                                <li class="icon icon-3" data-slide="2">
                                    <svg class="icon"><use xlink:href="/assets/front/images/svg/store/sprite.svg#srce-icon"></use></svg>
                                    <div class="bg"></div>
                                    <div class="first"></div>
                                    <div class="second"></div>
                                    <div class="third"></div>
                                </li>
                                <li class="icon icon-4" data-slide="3">
                                    <svg class="icon"><use xlink:href="/assets/front/images/svg/store/sprite.svg#sijalica-icon"></use></svg>
                                    <div class="bg"></div>
                                    <div class="first"></div>
                                    <div class="second"></div>
                                    <div class="third"></div>
                                </li>
                                <li class="icon icon-5" data-slide="4">
                                    <svg class="icon"><use xlink:href="/assets/front/images/svg/store/sprite.svg#zub-icon"></use></svg>
                                    <div class="bg"></div>
                                    <div class="first"></div>
                                    <div class="second"></div>
                                    <div class="third"></div>
                                </li>
                                <li class="icon icon-6" data-slide="5">
                                    <svg class="icon"><use xlink:href="/assets/front/images/svg/store/sprite.svg#oko-icon"></use></svg>
                                    <div class="bg"></div>
                                    <div class="first"></div>
                                    <div class="second"></div>
                                    <div class="third"></div>
                                </li>
                                <li class="icon icon-7" data-slide="6">
                                    <svg class="icon"><use xlink:href="/assets/front/images/svg/store/sprite.svg#burek-icon"></use></svg>
                                    <div class="bg"></div>
                                    <div class="first"></div>
                                    <div class="second"></div>
                                    <div class="third"></div>
                                </li>
                                <li class="icon icon-8" data-slide="7">
                                    <svg class="icon"><use xlink:href="/assets/front/images/svg/store/sprite.svg#seka-icon"></use></svg>
                                	<div class="bg"></div>
                                    <div class="first"></div>
                                    <div class="second"></div>
                                    <div class="third"></div>
                                </li>
                            </ul>
                            <div class="slider-content">
                                <img src="/assets/front/images/junior/junior-logo.png" class="logo" alt="">
                                <div class="junior-slider">
                                    <div class="content-item">
                                        <div class="slide-transition"></div>
                                        <div class="heading">{!! trans('front/junior.krugOrange.title') !!}<br>{!! trans('front/junior.krugOrange.subtitle') !!}</div>
                                        <p>{!! trans('front/junior.krugOrange.text') !!}</p>
                                    </div>
                                    <!-- ./content-item -->
                                    <div class="content-item">
                                        <div class="slide-transition"></div>
                                        <div class="heading">{!! trans('front/junior.krugViolet.title') !!}<br>{!! trans('front/junior.krugViolet.subtitle') !!}</div>
                                        <p>{!! trans('front/junior.krugViolet.text') !!}</p>
                                    </div>
                                    <div class="content-item">
                                        <div class="slide-transition"></div>
                                        <div class="heading">{!! trans('front/junior.krugDarkGreen.title') !!}<br>{!! trans('front/junior.krugDarkGreen.subtitle') !!}</div>
                                        <p>{!! trans('front/junior.krugDarkGreen.text') !!}</p>
                                    </div>
                                    <div class="content-item">
                                        <div class="slide-transition"></div>
                                        <div class="heading">{!! trans('front/junior.krugLightGreen.title') !!}<br>{!! trans('front/junior.krugLightGreen.subtitle') !!}</div>
                                        <p>{!! trans('front/junior.krugLightGreen.text') !!}</p>
                                    </div>
                                    <div class="content-item">
                                        <div class="slide-transition"></div>
                                        <div class="heading">{!! trans('front/junior.krugPlin.title') !!}<br>{!! trans('front/junior.krugPlin.subtitle') !!}</div>
                                        <p>{!! trans('front/junior.krugPlin.text') !!}</p>
                                    </div>
                                    <div class="content-item">
                                        <div class="slide-transition"></div>
                                        <div class="heading">{!! trans('front/junior.krugRed.title') !!}<br>{!! trans('front/junior.krugRed.subtitle') !!}</div>
                                        <p>{!! trans('front/junior.krugRed.text') !!}</p>
                                    </div>
                                    <div class="content-item">
                                        <div class="slide-transition"></div>
                                        <div class="heading">{!! trans('front/junior.krugGreenSpiral.title') !!}<br>{!! trans('front/junior.krugGreenSpiral.subtitle') !!}</div>
                                        <p>{!! trans('front/junior.krugGreenSpiral.text') !!}</p>
                                    </div>
                                    <div class="content-item">
                                        <div class="slide-transition"></div>
                                        <div class="heading">{!! trans('front/junior.krugBlue.title') !!}<br>{!! trans('front/junior.krugBlue.subtitle') !!}</div>
                                        <p>{!! trans('front/junior.krugBlue.text') !!}</p>
                                    </div>
                                </div>
                                <!-- ./junior-slider -->
                                <div class="slider-buttons junior-slider-buttons">
                                    <button class="prev">
                                        <span>
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                                <polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "></polygon>
                                            </svg>
                                        </span>
                                    </button>
                                    <button class="next">
                                        <span>
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                                <polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "></polygon>
                                            </svg>
                                        </span>
                                    </button>
                                </div>
                                <!-- ./slider-buttons -->
                            </div>
                            <!-- ./slider-content -->
                        </div>
                        <!-- ./radial-wrapper -->
                    </device>
                    <!-- ./desktop -->
                </div>
                <!-- ./section-content -->
            </div>
            <!-- ./parallax-wrap -->
        </section>

        <img src="/assets/front/images/junior/trava.png" class="trava" alt="">

        <section class="funkcionalnost">
            <div class="parallax-wrap">
                <div class="junior-heading-wrap">
                    <div class="junior-heading-cover">
                        <div class="section-header junior-heading">
                            <img src="/assets/front/images/junior/efsa-logo.png" alt="">
                            <h3>{!! trans('front/junior.efsa') !!}</h3>
                            <!-- <p>U 2016. godini investirano preko 30 miliona evra u unapređenje<br>proizvodnje, inovacije ipodršku farmerima. Prethodnu poslovn. U 2016. godini investirano preko 30 miliona evra u unapređenje proizvodnje, inovacije ipodršku farmerima. Prethodnu poslovn.U 2016. godini investirano preko 30 miliona evra u unapređenje proizvodnje, inovacije ipodršku farmerima. Prethodnu poslovn</p> -->
                            <br>
                            <div class="social">
                                <a href="#" id="fb" class="icon"><svg class="fb"><use xlink:href="/assets/front/images/svg/store/sprite.svg#fb"></use></svg></a>
                                <a href="#" id="in" class="icon"><svg class="in"><use xlink:href="/assets/front/images/svg/store/sprite.svg#in"></use></svg></a>
                                <a href="#" id="tw" class="icon"><svg class="tw"><use xlink:href="/assets/front/images/svg/store/sprite.svg#tw"></use></svg></a>
                            </div>
                            <p>{!! trans('front/junior.share') !!}</p>
                        </div>
                        <!-- ./section-header -->
                    </div>
                    <!-- ./junior-heading-cover -->
                </div>
                <!-- ./junior-heading-wrap -->
            </div>
            <!-- ./parallax-wrap -->
        </section>


    </main>


@stop