@extends('front.layout.template')

@section('content')
    <main id="kuhinjica-page">

                <section id="filter">
                    <div class="wrap">
                        <a href="{{ route('front.kuhinjica.index') }}">
                            <button type="button" class="filter-btn {{ (\App::getLocale() == 'sr' ? Request::segment(2) : Request::segment(3)) == null ? 'active' : '' }}">{{ trans("front/interface.all_recipes") }}</button>
                        </a>

                        @foreach($categoriesArray as $category)
                                <a href="{{ route('front.kuhinjica.index', $category->slug) }}">
                                    <button type="button" class="filter-btn {{ (\App::getLocale() == "sr" ? Request::segment(2) : Request::segment(3)) == $category->slug ? 'active' : '' }}">{{ $category->title }}</button>
                                </a>
                        @endforeach
                    </div>
                </section>
                <section id="filter-drop-down">
                <div class="droper">
                    <button class="dropbtn">Svi recepti</button>
                    <div class="arrow">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="6px" viewBox="5.57 18.93 24.766 13.688">
                            <polygon fill="#FFFFFF" points="17.955,32.614 5.572,21.841 8.116,18.926 17.955,27.477 27.794,18.926 30.338,21.841"/>
                        </svg>
                    </div>
                </div>
                    <div class="dropdown-content">
                        <!-- <a href="#">Svi recepti</a>
                        <a href="#">Video recepti</a>
                        <a href="#">Trikovi</a> -->
                        <a href="{{ route('front.kuhinjica.index') }}">
                            {{ trans('front/interface.all_recipes') }}
                        </a>
                        @foreach($categoriesArray as $category)
                            <a href="{{ route('front.kuhinjica.index', $category->slug) }}">
                                {{ $category->title }}
                            </a>
                        @endforeach
                    </div>
                </section>

                <section id="kuhinjica">
                    
                    <div class="wrap">
                        @foreach($recipesArray as $recipe)

                        <a href="{{ route('front.kuhinjica.show', ['cat' => $recipe->cat_slug, 'slug' => $recipe->frontTranslations[0]->pivot->slug ]) }}" class="item">
                            <div class="image">
                                <img src="{{ config('settings.image.recipe.grid.upload_dir').$recipe->grid_image }}">
                                <div class="dark-overlay">
                                    <img src="/assets/front/images/kuhinjica/clock.png" class="clock">
                                    <span class="time">{{ $recipe->preparation_time }} min.</span>
                                </div>
                            </div>
                            <div class="text">
                                <h4>{!! nl2br($recipe->frontTranslations[0]->pivot->title) !!}</h4>
                                <span>{{ $recipe->created_at->format('d.m.Y.') }}</span>
                                <p>{!! nl2br(str_limit($recipe->frontTranslations[0]->pivot->excerpt, 130)) !!}</p>
                            </div>
                        </a>
                        @endforeach
                    </div>

                </section>
                @include('front.pagination.default', ['paginator' => $recipesArray])

            </main>
@stop