@extends('front.layout.template')

@section('content')
    <?php $steps = json_decode($recipe->ingredients); 
        
    ?>

    <main id="recept-page">

                <section id="hero">

                    <div class="hero-wrap">

                        <div class="slider-wrap">

                            <div class="hero-slider">
                                

                                <div class="item">
                                    @if($recipe->youtube)
                                        <a class="afterglow" href="#video-1" title="">
                                            <img src="{{ config('settings.image.recipe.hero.upload_dir').$recipe->image }}">
                                        </a>
                                            
                                        <a class="afterglow" href="#video-1" title="">
                                            <img style="position: absolute;width: 80px;left: 50%;margin-left: -40px;top: 50%;margin-top: -40px;cursor: pointer;"        class="play_btn" src="/assets/front/images/play_btn.png" alt="">
                                        </a>
                                    @else    
                                    <img src="{{ config('settings.image.recipe.hero.upload_dir').$recipe->image }}">
                                    @endif
                                </div>
                                @foreach($recipe->photos as $photo)
                                <div class="item">
                                    <img src="{{ config('settings.image.recipe.slider.upload_dir').$photo->filename }}">
                                </div>
                                @endforeach


                            </div>

                            <div class="hero-arrows">

                                <div class="pages"><span class="current-page">00</span> / <span class="total-pages">00</span></div>

                                <div class="arrows">

                                    <button type="button" class="arrow hero-prev">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                            <polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
                                        </svg>
                                    </button>

                                    <div class="delimiter">|</div>

                                    <button type="button" class="arrow hero-next">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
                                            <polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
                                        </svg>
                                    </button>

                                </div>

                            </div>
                            
                        </div>
                        @if($recipe->youtube)
                            <div class="item">
                                <video id="video-1" width="1280" height="720" data-autoresize="fit" preload="none" data-overscale="false" poster="https://img.youtube.com/vi/{{ $recipe->youtube }}/hqdefault.jpg" data-youtube-id="{{ $recipe->youtube }}"></video>
                            </div>
                        @endif
                </section>

                <section id="content">

                    <div class="wrap">

                        <h3 class="title">{{ $recipe->title }}</h3>

                        <p class="time"><img src="/assets/front/images/recept/icon-clock.svg" class="ico"> {{ $recipe->preparation_time }} min.</p>
                        
                        <div class="prvi-deo">
                            <p>{!! nl2br($recipe->excerpt) !!}</p>
                        </div>

                        @if(count($componentsArray) > 1)
                        <div class="drugi-deo">

                            <div class="sastojci">

                                <h4 class="sub-title">{{ trans('front/interface.ingredients') }} <img src="/assets/front/images/recept/icon-sastojci.svg" class="ico"></h4>

                                <ul class="lista">
                                    @foreach($componentsArray as $component)
                                        <li class="sastojak">{{ $component }}</li>
                                    @endforeach

                                </ul>
                                
                            </div>

                            <div class="priprema">

                                <h4 class="sub-title">{{ trans('front/interface.instructions') }} <img src="/assets/front/images/recept/icon-priprema.svg" class="ico"></h4>

                                @foreach($steps as $index => $step)
                                <div class="step">
                                        <span class="num">{{ $index+1 }}.</span>
                                        <p>
                                            {!! nl2br($step->ingredient) !!}
                                            
                                        </p>
                                </div>
                                @endforeach
                            </div>

                        </div>
                        @endif

                        <div class="buttons">
                            
                            <p class="date"><span class="label">{{ trans('front/interface.date') }}:</span> {{ $recipe->created_at->format('d.m.Y.') }}</p>

                            <div class="social">

                                <span class="label">{{ trans('front/interface.share') }}</span>

                                <a href="#fb" class="fb fb-recipe">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px" viewBox="0 0 98 98">
                                        <path class="circle" d="M97.861,49.059c0,26.953-21.85,48.802-48.803,48.802c-26.952,0-48.801-21.85-48.801-48.802
                                            S22.106,0.257,49.058,0.257C76.012,0.257,97.861,22.106,97.861,49.059"/>
                                        <path class="icon" d="M61.421,52.915l1.529-10.706H52.244v-6.885c0-3.058,0.765-5.352,5.353-5.352h5.735v-9.561
                                            c-1.148,0-4.206-0.382-8.412-0.382c-8.03,0-13.766,4.971-13.766,14.149v8.03h-9.177v10.706h9.177v27.532h11.09V52.915H61.421z"/>
                                    </svg>
                                </a>
                                
                                <a href="#in" class="in in-recipe">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px" viewBox="0 0 98 98">
                                        <path class="circle" d="M97.618,49.059c0,26.819-21.74,48.559-48.559,48.559C22.241,97.618,0.5,75.878,0.5,49.059
                                            C0.5,22.241,22.241,0.5,49.059,0.5C75.878,0.5,97.618,22.241,97.618,49.059"/>
                                        <path class="icon" d="M78.731,70.868H66.597V53.023c0-4.641-1.787-7.853-6.068-7.853c-3.212,0-4.997,2.141-5.713,4.283
                                            c-0.354,0.713-0.354,1.785-0.354,2.855v18.56H42.324V36.602h12.137v5.355c0.714-2.499,4.641-5.711,10.707-5.711
                                            c7.853,0,13.92,4.998,13.92,15.705v18.917H78.731z M29.833,32.32L29.833,32.32c-3.927,0-6.425-2.499-6.425-6.067
                                            c0-3.57,2.498-6.07,6.781-6.07c3.926,0,6.424,2.5,6.424,6.07C36.257,29.822,33.759,32.32,29.833,32.32 M24.479,36.604h10.707v34.267
                                            H24.479V36.604z"/>
                                    </svg>
                                </a>
                                
                                <a href="#tw" class="tw tw-recipe">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px" viewBox="0 0 98 98">
                                    <path class="circle" d="M97.769,49.116C97.769,76.114,75.883,98,48.885,98C21.886,98,0,76.114,0,49.116
                                        C0,22.118,21.886,0.231,48.885,0.231C75.883,0.231,97.769,22.118,97.769,49.116"/>
                                    <path class="icon" d="M78.123,33.292c-1.964,0.895-4.08,1.501-6.3,1.771c2.266-1.392,4.006-3.596,4.825-6.222
                                        c-2.121,1.286-4.467,2.224-6.971,2.725c-1.999-2.186-4.849-3.55-8.01-3.55c-6.052,0-10.965,5.037-10.965,11.248
                                        c0,0.884,0.095,1.74,0.283,2.563c-9.119-0.469-17.2-4.943-22.615-11.756c-0.945,1.667-1.485,3.6-1.485,5.662
                                        c0,3.9,1.938,7.344,4.882,9.362c-1.795-0.054-3.49-0.566-4.972-1.403v0.137c0,5.453,3.783,10,8.804,11.032
                                        c-0.92,0.263-1.889,0.396-2.891,0.396c-0.705,0-1.396-0.067-2.066-0.2c1.397,4.466,5.446,7.722,10.25,7.808
                                        c-3.754,3.021-8.487,4.82-13.626,4.82c-0.885,0-1.761-0.053-2.618-0.156c4.856,3.188,10.625,5.051,16.817,5.051
                                        c20.183,0,31.215-17.14,31.215-32.005c0-0.49-0.01-0.979-0.027-1.458C74.794,37.528,76.657,35.548,78.123,33.292"/>
                                    </svg>
                                </a>

                            </div>

                        </div>

                    </div>

                </section>

                <section id="kuhinjica">
                    
                    <div class="wrap">
                        @foreach($similarArray as $recipe)

                        <a href="{{ route('front.kuhinjica.show', ['slug' => $recipe->frontTranslations[0]->pivot->slug, 'cat' => $recipe->cat_slug]) }}" class="item">
                            <div class="image">
                                <img src="{{ config('settings.image.recipe.grid.upload_dir').$recipe->grid_image }}">
                                <div class="dark-overlay">
                                    <img src="/assets/front/images/kuhinjica/clock.png" class="clock">
                                    <span class="time">{{ $recipe->preparation_time }} min.</span>
                                </div>
                            </div>
                            <div class="text">
                                <h4>{{ $recipe->frontTranslations[0]->pivot->title }}</h4>
                                <span>{{ $recipe->created_at->format('d.m.Y.') }}</span>
                                <p>{{ str_limit($recipe->frontTranslations[0]->pivot->excerpt, 130) }}...</p>
                            </div>
                        </a>
                        @endforeach
                    
                    </div>

                </section>

            </main>


@stop