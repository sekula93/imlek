@extends('front.layout.template')

@section('content')

 <main id="maslac-page">

 	<section id="hero">

		<div class="hero-wrap">

			<div class="slider-wrap">

				<div class="hero-slider">

					<div class="item">
						<div class="parallax-wrap">
							<div class="video">
								{{-- <video width="1280" height="720" muted playsinline loop>
									<source src="./assets/front/videos/namaz-od-maslaca.mp4" type="video/mp4">
								</video> --}}
								<video width="1280" height="720" muted playsinline loop>
                                    <source src="/assets/front/videos/namaz-od-maslaca.mp4" type="video/mp4">
                                </video>
							</div>
						</div>
						<div class="text">
							<h4>Moja Kravica <br>namaz od maslaca</h4>
						</div>
					</div>

				</div>

				<div class="hero-arrows">

					<div class="pages"><span class="current-page">00</span> / <span class="total-pages">00</span></div>
					
					<div class="arrows-wrap">

						<div class="arrows">

							<button type="button" class="arrow hero-prev">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
									<polygon fill="#FFFFFF" points="55.471,0.938 19.563,26.705 55.471,52.473 55.471,44.051 31.319,26.705 55.471,9.376 "/>
								</svg>
							</button>

							<div class="delimiter">|</div>

							<button type="button" class="arrow hero-next">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="6px" height="10px" viewBox="19.563 0.938 35.91 51.535">
									<polygon fill="#FFFFFF" points="19.563,9.376 43.715,26.705 19.563,44.051 19.563,52.473 55.471,26.705 19.563,0.938 "/>
								</svg>
							</button>

						</div>

					</div>

				</div>

			</div>

			<div class="scroll-arrow">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="10px" height="6px" viewBox="5.57 18.93 24.766 13.688">
					<polygon fill="#FFFFFF" points="17.955,32.614 5.572,21.841 8.116,18.926 17.955,27.477 27.794,18.926 30.338,21.841"/>
				</svg>
			</div>

			<div class="scroll-dots">
				<div class="dot"></div>
				<div class="dot"></div>
				<div class="dot"></div>
				<div class="dot"></div>
				<div class="dot"></div>
			</div>

		</div>

	</section>

	<section id="info">
		<div class="wrap">
			<div class="hidden-text">
				<h3>Moja Kravica namaz od maslaca</h3>
				<p>Maslac je u potpunosti prirodan proizvod, napravljen izdvajanjem mlečne masti iz svežeg mleka.<br>Jedna je od namirnica koje mogu pretvoriti običan obrok u remek delo kulinarstva.</p>
				<p>Moja Kravica maslac je tradicionalani deo svakog obroka I svojim punim I mlečnim ukusom obogati svako<br>jelo/poslasticu. Bilo da je reč o doručku ili pripremi jela, maslac je prirodan, zdrav i ukusan izbor<br>Potrošačima nudimo dva profila ukusa – original (neslan) i blago slan.</p>
			</div>
		</div>
	</section>

	{{-- <div id="smear"></div> --}}
	<section id="parallax" class="smear-parallax">
		<div class="parallax-wrap">
			<div class="window"></div>
		</div>
	</section>

	<section id="packshots">
		
		<button class="prev-arrow maslac-arrows">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 25 35">
				<polygon class="stroke" fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="3" points="25,0 0,18 25,35"/>
			</svg>
		</button>

		<div class="maslac-packshot-slider">
			
			<span class="namaz-name">ORIGINAL</span>

			<div class="vLine" style="top: 91px;"></div>

			<div class="hLine" style="left: 60px;"></div>

			<div class="items">
				
				<div id="originalNamaz"></div>

				<div id="slaniNamaz"></div>

			</div>

			<div id="astal"></div>
			
			<div class="hLine" style="right: 47px;"></div>

			<div class="vLine" style="top: 423px; background: white;"></div>

			<a href="https://www.imlek.rs/proizvodi/maslac#moja-kravica-namaz-od-maslaca-original-200g" id="smearBtn"></a>

		</div>

		<button class="next-arrow maslac-arrows">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 25 35">
				<polygon class="stroke" fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="3" points="0,0 25,18 0,35"/>
			</svg>
		</button>

	</section>

	<section id="frizhider-anim">
		<div class="wrap">
			<div class="frizhider-text"><p>Savršeno spreman<br>za mazanje čim se<br>izvadi iz frižidera<span class="vLine"></span></p></div>
			<div id="frizhider"></div>
		</div>
	</section>

	<section id="maslac-footer">

		<div class="wrap">
		
			<h3>Razmaži ovu priču<br>po mrežama</h3>

			<div class="social">
				{{-- <a id="fb_maslac">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
						<path class="circle" d="M97.861,49.059c0,26.953-21.85,48.802-48.803,48.802c-26.952,0-48.801-21.85-48.801-48.802
							S22.106,0.257,49.058,0.257C76.012,0.257,97.861,22.106,97.861,49.059"/>
						<path class="icon" d="M61.421,52.915l1.529-10.706H52.244v-6.885c0-3.058,0.765-5.352,5.353-5.352h5.735v-9.561
							c-1.148,0-4.206-0.382-8.412-0.382c-8.03,0-13.766,4.971-13.766,14.149v8.03h-9.177v10.706h9.177v27.532h11.09V52.915H61.421z"/>
					</svg>
				</a>
				<a id="in_maslac">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
						<path class="circle" d="M97.618,49.059c0,26.819-21.74,48.559-48.559,48.559C22.241,97.618,0.5,75.878,0.5,49.059
							C0.5,22.241,22.241,0.5,49.059,0.5C75.878,0.5,97.618,22.241,97.618,49.059"/>
						<path class="icon" d="M78.731,70.868H66.597V53.023c0-4.641-1.787-7.853-6.068-7.853c-3.212,0-4.997,2.141-5.713,4.283
							c-0.354,0.713-0.354,1.785-0.354,2.855v18.56H42.324V36.602h12.137v5.355c0.714-2.499,4.641-5.711,10.707-5.711
							c7.853,0,13.92,4.998,13.92,15.705v18.917H78.731z M29.833,32.32L29.833,32.32c-3.927,0-6.425-2.499-6.425-6.067
							c0-3.57,2.498-6.07,6.781-6.07c3.926,0,6.424,2.5,6.424,6.07C36.257,29.822,33.759,32.32,29.833,32.32 M24.479,36.604h10.707v34.267
							H24.479V36.604z"/>
					</svg>
				</a>
				<a id="tw_maslac">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 98 98">
						<path class="circle" d="M97.709,49.059C97.709,75.962,75.9,97.769,49,97.769c-26.9,0-48.708-21.807-48.708-48.709
							C0.291,22.158,22.099,0.35,49,0.35C75.9,0.35,97.709,22.158,97.709,49.059"/>
						<path class="icon" d="M43.029,60.353L43.027,40.25l17.495,10.079L43.029,60.353z M77.336,39.283c0,0-0.564-4.013-2.297-5.78
							c-2.201-2.321-4.666-2.333-5.797-2.467c-8.09-0.589-20.23-0.589-20.23-0.589h-0.025c0,0-12.139,0-20.232,0.589
							c-1.13,0.134-3.594,0.146-5.794,2.467c-1.734,1.768-2.298,5.78-2.298,5.78s-0.579,4.714-0.579,9.426v4.418
							c0,4.715,0.579,9.427,0.579,9.427s0.564,4.013,2.298,5.78c2.2,2.321,5.089,2.247,6.378,2.489C33.964,71.273,49,71.41,49,71.41
							s12.153-0.019,20.243-0.606c1.131-0.137,3.596-0.147,5.797-2.469c1.732-1.768,2.297-5.78,2.297-5.78s0.578-4.712,0.578-9.427v-4.418
							C77.914,43.997,77.336,39.283,77.336,39.283"/>
					</svg>
				</a> --}}
				<a href="#" id="fb_maslac" class="icon"><svg class="fb"><use xlink:href="/assets/front/images/svg/store/sprite.svg#fb"></use></svg></a>
				<a href="#" id="in_maslac" class="icon"><svg class="in"><use xlink:href="/assets/front/images/svg/store/sprite.svg#in"></use></svg></a>
				<a href="#" id="tw_maslac" class="icon"><svg class="tw"><use xlink:href="/assets/front/images/svg/store/sprite.svg#tw"></use></svg></a>
			</div>

			<img src="assets/front/images/maslac/imlek-maslac-namaz-logo.png">
		
		</div>

	</section>

 </main>

@stop