<!DOCTYPE html><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <title>Komentar sa sajta: imlek.rs</title>
</head>

<body style="padding:0; margin:0; font-family: Helvetica, Arial, sans-serif;">
  <table border="0" width="100%" cellpadding="0" cellspacing="0">
    <tr>
     <td style="width:550px; text-align:left" align="left">
        <p><strong>Email adresa:</strong> {{ $email }}</p>
        <p><strong>Tekst komentara:</strong> <br>
        {!! nl2br($text) !!}
        </p>
     </td>
    </tr>
  </table>
</body>
</html>
