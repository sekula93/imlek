@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
@stop
@section('content')
    <div id="page-heading">
        <h1>Dodavanje godine</h1>
        <div class="options">
            <a href="{{ route('admin.history.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.history.store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4 col-lg-2">
                                <label for="year" class="control-label">Godina</label>
                                <input type="text" name="year" class="form-control" value="{{ old('year') }}" autofocus>
                                {!! validation_error($errors, 'year') !!}
                            </div>
                            <!-- <div class="col-lg-10">
                                <label for="" class="control-label">Naslov</label>
                                {!! ml_input('text', $appLanguages, 'title', $errors) !!}
                            </div> -->
                        </div>
                        <div class="row">
                            <!-- <div class="col-sm-4 col-lg-2">
                                <label for="template_id" class="control-label">Templejt</label>
                                <select name="template_id" class="form-control" value="{{ old('template_id') }}">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                                {!! validation_error($errors, 'template_id') !!}
                            </div> -->
                            <div class="col-lg-10">
                                <label for="" class="control-label">Tekst</label>
                                {!! ml_input('textarea', $appLanguages, 'text', $errors) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <label for="image_left" class="upload-label">Fotografija (745x466)</label>
                                {!! jasnyupload('image_left', $errors, 'image/png, image/jpeg') !!}
                            </div>
                            <!-- <div class="col-md-6 col-lg-4">
                                <label for="file_right" class="upload-label">Desni fajl</label>
                                {!! jasnyupload('file_right', $errors, 'image/png, image/jpeg') !!}
                            </div> -->
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="0">
                                    <div class="toggle toggle-success form-active" title="Status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ route('admin.history.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
