<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>{{ Config::get('settings.application_name') }}</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('/assets/admin/img/logo-old.png') }}"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Avant">
	<meta name="author" content="The Red Team">

    <!-- <link href="assets/less/styles.less" rel="stylesheet/less" media="all">  -->
    @section('top_style')
    {!! Html::style('assets/admin/css/styles.css') !!}
    {!! Html::style('assets/admin/plugins/form-toggle/toggles.css') !!}
    {!! Html::style('assets/admin/js/jqueryui.css') !!}
    @show
    {!! Html::style('assets/admin/css/app.css') !!}
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js IE8 support of Html5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
	<!--[if lt IE 9]>
	    {!! Html::style('assets/admin/css/is8.css') !!}
		<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
        <script type="text/javascript" src="assets/plugins/charts-flot/excanvas.min.js"></script>
	<![endif]-->
    <script type="text/javascript">
        var basePath = '{{ url("") }}';
        var csrf = '{{ csrf_token() }}';
        var appLocale = '{{ App::getLocale() }}';
    </script>
</head>

<body class=" ">
    <header class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <a id="leftmenu-trigger" class="tooltips" data-toggle="tooltip" data-placement="right" title="Toggle Sidebar"></a>
        <ul class="nav navbar-nav pull-right toolbar">
        	<li class="dropdown">
        		<a href="#" class="dropdown-toggle username" data-toggle="dropdown"><span class="hidden-xs">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }} <i class="fa fa-caret-down"></i></span></a>
        		<ul class="dropdown-menu userinfo arrow">
        			<li class="userlinks">
        				<ul class="dropdown-menu">
        					<li><a href="{{ route('admin.administrator.update-profile') }}"> Izmena profila<i class="pull-right fa fa-pencil"></i></a></li>
        					<li class="divider"></li>
        					<li><a href="{{ route('admin.auth.logout') }}" class="text-right">Odjava</a></li>
        				</ul>
        			</li>
        		</ul>
        	</li>
		</ul>
    </header>

    <div id="page-container">
  		@include('admin.layout._partials.left_sidebar')
		<div id="page-content" style="background-image:url('/assets/front/images/prallax-bg.jpg')">
			<div id="wrap">

                {!! Notification::showAll() !!}
				@yield('content')

			</div> <!--wrap -->
		</div> <!-- page-content -->

	    <footer role="contentinfo">
	        <div class="clearfix">
	            <ul class="list-unstyled list-inline">
	                <li>{{ Config::get('settings.application_name') }} &copy; {{ date('Y') }}</li>
	                <button class="pull-right btn btn-inverse-alt btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></button>
	            </ul>
	        </div>
	    </footer>

</div> <!-- page-container -->

<!--
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>!window.jQuery && document.write(unescape('%3Cscript src="assets/js/jquery-1.10.2.min.js"%3E%3C/script%3E'))</script>
<script type="text/javascript">!window.jQuery.ui && document.write(unescape('%3Cscript src="assets/js/jqueryui-1.10.3.min.js'))</script>
-->

@section('bottom_scripts')
{!! Html::script('assets/admin/js/jquery-1.10.2.min.js') !!}
{!! Html::script('assets/admin/js/jqueryui-1.10.3.min.js') !!}
{!! Html::script('assets/admin/js/bootstrap.min.js') !!}
{!! Html::script('assets/admin/js/enquire.js') !!}
{!! Html::script('assets/admin/js/jquery.cookie.js') !!}
{!! Html::script('assets/admin/js/jquery.nicescroll.min.js') !!}
{!! Html::script('assets/admin/plugins/codeprettifier/prettify.js') !!}
{!! Html::script('assets/admin/plugins/easypiechart/jquery.easypiechart.min.js') !!}
{!! Html::script('assets/admin/plugins/sparklines/jquery.sparklines.min.js') !!}
{!! Html::script('assets/admin/plugins/form-toggle/toggle.min.js') !!}
{!! Html::script('assets/admin/js/placeholdr.js') !!}
{!! Html::script('assets/admin/js/application.js') !!}
{!! Html::script('assets/admin/js/lang/sr.js') !!}
{!! HTML::script('assets/admin/plugins/bootbox/bootbox.min.js') !!}
@show
{!! Html::script('assets/admin/js/app.js') !!}
</body>
</html>