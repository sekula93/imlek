<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ Config::get('settings.application_name') }}</title>
    <link rel="shortcut icon" type="image/png" href="/assets/front/media/favicon.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Avant">
    <meta name="author" content="The Red Team">

    <!-- <link href="assets/less/styles.less" rel="stylesheet/less" media="all"> -->
    <link rel="stylesheet" type="text/css" href="/assets/admin/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/assets/admin/css/app.css">
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>

</head><body class="focusedform">

    @yield('content')

</body>
</html>