<!-- BEGIN SIDEBAR -->
<nav id="page-leftbar" role="navigation">
    <!-- BEGIN SIDEBAR MENU -->
    <div id="sidebar-logo">
        <a href="{{ route('admin.dashboard.index') }}"><img src="{{ asset('/assets/admin/img/logo-old.png') }}" style="width:100%"></a>
    </div>
    @if(Auth::user()->isSuperAdmin())
    <ul class="acc-menu" id="sidebar">
        <li {{ active_menu('admin.admin') }}><a href="{{ route('admin.dashboard.index') }}"><i class="fa fa-tachometer"></i><span>Kontrolna tabla</span></a></li>
        <li {{ active_menu('admin.admin') }}><a href="javascript:;"><i class="fa fa-institution"></i><span>Administratori</span></a>
            <ul class="acc-menu">
                <li><a href="{{ route('admin.administrator.index') }}">Lista administratora</a></li>
                <li><a href="{{ route('admin.administrator.create') }}">Novi administrator</a></li>
            </ul>
        </li>
        <li {{ active_menu('admin.slider') }}><a href="{{ route('admin.slide.index') }}"><i class="fa fa-image"></i><span>Slajder</span></a></li>
        <li {{ active_menu('admin.recipe') }}><a href="javascript:;"><i class="fa fa-user"></i><span>Recepti</span></a>
            <ul class="acc-menu">
                <li><a href="{{ route('admin.recipe.create') }}">Novi recept</a></li>
                <li><a href="{{ route('admin.recipe.index') }}">Lista recepata</a></li>
                {{--<!-- <li><a href="{{ route('admin.recipe-author.index') }}">Lista autora</a></li> -->--}}
                <li><a href="{{ route('admin.recipe-category.index') }}">Lista kategorija</a></li>
            </ul>
        </li>
        <li {{ active_menu('admin.post') }}><a href="javascript:;"><i class="fa fa-file-text-o"></i><span>Postovi</span></a>
            <ul class="acc-menu">
                <li><a href="{{ route('admin.post.create') }}">Kreiraj post</a></li>
                <li><a href="{{ route('admin.post.index') }}">Svi postovi</a></li>
            </ul>
        </li>
        {{--<!-- <li {{ active_menu('admin.social-post') }}><a href="{{ route('admin.social-post.index', 2) }}"><i class="fa fa-facebook" aria-hidden="true"></i><span>Facebook</span></a></li> -->
        <!-- <li {{ active_menu('admin.social-post') }}><a href="{{ route('admin.social-post.index', 3) }}"><i class="fa fa-instagram" aria-hidden="true"></i><span>Instagram</span></a></li> -->--}}

        <li {{ active_menu('admin.brand') }}><a href="javascript:;"><i class="fa fa-archive"></i><span>Brendovi</span></a>
            <ul class="acc-menu">
                <li><a href="{{ route('admin.brand.create') }}">Kreiraj brend</a></li>
                <li><a href="{{ route('admin.brand.index') }}">Svi brendovi</a></li>
            </ul>
        </li>


        <li {{ active_menu('admin.product') }}><a href="javascript:;"><i class="fa fa-archive"></i><span>Proizvodi</span></a>
            <ul class="acc-menu">
                <li><a href="{{ route('admin.product.create') }}">Kreiraj proizvod</a></li>
                <li><a href="{{ route('admin.product.index') }}">Svi proizvodi</a></li>
                <li><a href="{{ route('admin.product.nutritive.index') }}">Tabele Nutritivnih Vrednosti</a></li>
            </ul>
        </li>
        
        <li {{ active_menu('admin.package') }}><a href="{{ route('admin.package.index')  }}"><i class="fa fa-cube" aria-hidden="true"></i><span>Package3D</span></a></li>

        <li {{ active_menu('admin.history') }}><a href="{{ route('admin.history.index')  }}"><i class="fa fa-history" aria-hidden="true"></i><span>Istorija</span></a></li>
        
        <li {{ active_menu('admin.company') }}><a href="{{ route('admin.company.index')  }}"><i class="fa fa-home" aria-hidden="true"></i><span>Kompanija</span></a></li>
        
        <li {{ active_menu('admin.faq') }}><a href="{{ route('admin.faq.index')  }}"><i class="fa fa-question" aria-hidden="true"></i><span>FAQ</span></a></li>
        

        <li {{ active_menu('admin.report') }}><a href="{{ route('admin.report.index')  }}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span>Izveštaji</span></a></li>
        
        <li {{ active_menu('admin.job') }}><a href="{{ route('admin.job.index')  }}"><i class="fa fa-briefcase" aria-hidden="true"></i><span>Posao</span></a></li>

        <li {{ active_menu('admin.certificate') }}><a href="{{ route('admin.certificate.index')  }}"><i class="fa fa-certificate" aria-hidden="true"></i><span>Sertifikati</span></a></li>

        {{-- <li {{ active_menu('admin.newsletter') }}><a href="{{ route('admin.newsletter.index')  }}"><i class="fa fa-envelope" aria-hidden="true"></i><span>Newsletter</span></a></li> -- }}

        {{-- <li class="divider"></li> --}}
        {{-- <li><a href="{{ route('admin.static.catalogue')  }}"><i class="fa fa-archive" aria-hidden="true"></i><span>Katalog proizvoda</span></a></li> --}}
        {{-- <li {{ active_menu('admin.settings') }}><a href="{{ route('admin.static-page.edit', 3)  }}"><i class="fa fa-wpforms" aria-hidden="true"></i><span>Uslovi korišćenja</span></a></li> --}}
        {{-- <li {{ active_menu('admin.settings') }}><a href="{{ route('admin.static-page.edit', 4)  }}"><i class="fa fa-wpforms" aria-hidden="true"></i><span>Pravila</span></a></li> --}}
        {{-- <li class="divider"></li> --}}
        {{-- <li {{ active_menu('admin.settings') }}><a href="/"><i class="fa fa-cogs"></i><span>Podešavanja</span></a></li> --}}
    </ul>
    @else
    {{-- <ul class="acc-menu" id="sidebar">
        <li {{ active_menu('admin.settings') }}><a href="{{ route('admin.post.create') }}"><i class="fa fa-plus"></i><span>Novi post</span></a></li>
        <li class="divider"></li>
        <li {{ active_menu('admin.settings') }}><a href="{{ route('admin.post.index') }}"><i class="fa fa-file-text-o"></i><span>Lista postova</span></a></li>
    </ul> --}}
    @endif
    <!-- END SIDEBAR MENU -->
</nav>
