@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
    <script type="text/javascript" src="/assets/admin/js/recipe_step.js"></script>
@stop
@section('content')
    <div id="page-heading">
        <h1>Novi recept</h1>
        <div class="options">
            <a href="{{ route('admin.recipe.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.recipe.store') }}" method="post" data-resource="recipe" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel-body">
                        <div class="row">
                        </div>
                        <div class="row">
                            <!-- <div class="col-sm-6 col-md-5">
                                <label for="user_id" class="control-label">Autor:</label>
                                <select class="form-control" name="user_id">
                                        <option value="" class="form-control">-- Odaberite --</option>
                                    @foreach($authorsArray as $author)
                                        <option value="{{ $author->id }}" class="form-control" {{ old('user_id') == $author->id ? 'selected' : '' }}>{{ $author->name }}</option>
                                    @endforeach
                                </select>
                                {!! validation_error($errors, 'user_id') !!}
                            </div> -->
                            <div class="col-sm-6 col-md-5">
                                <label for="category_id" class="control-label">Kategorija:</label>
                                <select class="form-control" name="category_id">
                                        <option value="" class="form-control">-- Odaberite --</option>
                                    @foreach($categoriesArray as $category)
                                        <option value="{{ $category->id }}" class="form-control" {{ old('category_id') == $category->id ? 'selected' : '' }}>{{ $category->translations[0]->pivot->title }}</option>
                                    @endforeach
                                </select>
                                {!! validation_error($errors, 'category_id') !!}
                            </div>
                            <div class="col-sm-6 col-md-2">
                                <label for="preparation_time" class="control-label">Vreme pripreme</label>
                                <input type="text" name="preparation_time" class="form-control" value="{{ old('preparation_time') }}">
                                {!! validation_error($errors, 'preparation_time') !!}
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <label class="control-class">Datum objave</label>
                                <input type="date" name="created_at" value="{{ date('Y-m-d') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                <label class="control-class">Naziv</label>
                                {!! ml_input('textarea', $appLanguages, 'title', $errors, '', 'slugify') !!}
                            </div>
                            <div class="col-md-12 col-lg-6">
                                <label class="control-class">Slug</label>
                                {!! ml_input('text', $appLanguages, 'slug', $errors) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-class">Uvod</label>
                                {!! ml_input('textarea', $appLanguages, 'excerpt', $errors) !!}
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-class">Sastojci</label>
                                {!! ml_input('textarea', $appLanguages, 'preparation', $errors) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="grid_image" class="upload-label">Grid fotografija</label>
                                {!! jasnyupload('grid_image', $errors, 'image/png, image/jpeg') !!}
                            </div>
                            <div class="col-md-4">
                                <label for="image" class="upload-label">Hero fotografija</label>
                                {!! jasnyupload('image', $errors, 'image/png, image/jpeg') !!}
                            </div>
                            <!-- <div class="col-md-4">
                                <label for="image" class="upload-label">Video</label>
                                {!! jasnyupload('video', $errors, 'video/mp4') !!}
                            </div> -->
                            <div class="col-md-3">
                                <label class="control-class">Youtube ID</label>
                                <input class="form-control" type="text" name="yt-id" placeholder="Youtube ID">
                            </div>
                        </div>
                        <h3>Priprema</h3>
                            <div class="recipe-steps">
                                @include('admin.recipe._partials.steps')
                            </div>
                            <div class="row text-center">
                                <button type="button" class="btn btn-warning" id="new-recipe-step">Novi korak</button>
                                <button type="button" class="btn btn-danger" id="delete-recipe-step">Obrisi korak</button>
                            </div>

                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="0">
                                    <div class="toggle toggle-success form-active" title="Status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn save-recipe">Snimi</button>
                                <a href="{{ route('admin.post.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
