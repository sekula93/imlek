<div class="row">
    <div class="col-md-12">
        <!-- The fileinput-button span is used to style the file input field as button -->
        <form class="fileupload-photo" action="{{ route('admin.recipe-packshot.store', $recipe->id) }}" method="POST" enctype="multipart/form-data">
            <span class="btn btn-success fileinput-button photo-upload-button">
                <h2><i class="fa fa-cloud-upload"></i>Spustite fajlove ovde</h2>
                <h4>ili kliknite</h4>
                <!-- The file input field used as target for the file upload widget -->
                <input type="file" name="files[]" multiple>
            </span>
        </form>
        <br>
        <br>
        <!-- The global progress bar -->
        <div class="progress-bar progress-bar-success col-md-12" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="">
            <span class="sr-only"></span>
        </div>
        <div class="col-md-12 errors-wrapper"></div>

        <ul class="post images gallery uploaded list-unstyled col-sm-12" data-type="1">
            @if($recipe->photos)
            @foreach ($recipe->photos as $photo)
            <li class="image" data-name="" data-id="{{ $photo->id }}" id="file-{{ $photo->id }}">
                <a class="file-thumb {{ !$photo['active'] ? 'inactive' : ''}}" href="/uploads/recipes/slides/{{ $photo->filename }}" data-lightbox="photos">
                    <img src="/uploads/recipes/slides/{{ $photo->filename }}" width="230"></a>
                </a>
                <div class="toggle toggle-success {{ $photo->active ? 'active' : '' }} file-active" title="Status"></div>
                <button type="button" class="btn btn-danger-alt remove-file"><i class="fa fa-times"></i></button>
            </li>
            @endforeach
            @endif
        </ul>
    </div>

</div>