<?php $stepsCount = isset($stepsArray) && count($stepsArray[1]) > 0 ? count($stepsArray[1]) : 1; ?>
@for($i = 0; $i < $stepsCount; $i++)
<div class="recipe-step-wrapper">
    <hr>
    <div class="form-group">
        <div class="col-sm-12 col-lg-6">
            <label class="control-class">Korak</label>
            <ul class="lang-input-wrap">
                <?php $inputErrors = ''; ?>
                @foreach ($appLanguages as $index => $language)
                    <li class="lang-input {{ $language->id == 1 ? 'active' : '' }}">
                        <?php $value = isset($stepsArray[$language->id]) ? $stepsArray[$language->id][$i]->title : null; ?>
                        <input type="text" class="form-control " name="trans[{{ $language->id }}][step][{{ $i }}][title]" value="{{ old('trans.'.$language->id.'.step.'.$i.'.title', $value) }}">
                        <span class="lang-input-switch">{{ strtoupper($language->code) }}</span>
                    </li>
                    <?php
                    $dotFieldName = 'trans.'.$language->id.'.step.'.$i.'.title';
                    if ($errors->first($dotFieldName)) {
                        $inputErrors .= sprintf('<p class="help-block error"><i class="fa fa-times-circle"></i> %s - %s</p>', $language->name, $errors->first($dotFieldName));
                    }
                    ?>
                @endforeach
            </ul>
            {!! $inputErrors !!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-6">
            <div class="tab-container">
                <label class="control-class">Priprema</label>
                <ul class="lang-input-wrap">
                    <?php $inputErrors = ''; ?>
                    @foreach ($appLanguages as $index => $language)
                        <li class="lang-input {{ $language->id == 1 ? 'active' : '' }}">
                            <?php $value = isset($stepsArray[$language->id]) ? $stepsArray[$language->id][$i]->ingredient : null; ?>
                            <textarea class="form-control " rows="5" name="trans[{{ $language->id }}][step][{{ $i }}][ingredient]">{{ old('trans.'.$language->id.'.step.'.$i.'.ingredient', $value) }}</textarea>
                            <span class="lang-input-switch">{{ strtoupper($language->code) }}</span>
                        </li>
                    <?php
                    $dotFieldName = 'trans.'.$language->id.'.step.'.$i.'.ingredient';
                    if ($errors->first($dotFieldName)) {
                        $inputErrors .= sprintf('<p class="help-block error"><i class="fa fa-times-circle"></i> %s - %s</p>', $language->name, $errors->first($dotFieldName));
                    }
                    ?>
                    @endforeach
                </ul>
                {!! $inputErrors !!}
            </div>
        </div>
    </div>
</div>
@endfor
