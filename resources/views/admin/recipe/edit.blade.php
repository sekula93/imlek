@extends('admin.layout.inner')

@section('top_style')
    @parent
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/jquery-fileupload/css/jquery.fileupload-ui.css">
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/lightbox/css/lightbox.css">
@stop

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>

    <script type="text/javascript" src="/assets/admin/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.iframe-transport.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.fileupload.js"></script>
    <script type="text/javascript" src="/assets/admin/js/recipe-packshot-upload.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/lightbox/js/lightbox.min.js"></script>
    <script type="text/javascript" src="/assets/admin/js/recipe_step.js"></script>

@stop
@section('content')
    <div id="page-heading">
        <h1>Izmena recepta</h1>
        <div class="options">
            <a href="{{ route('admin.recipe.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="tab-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#form" data-toggle="tab">Sadržaj recepta</a></li>
                            <li><a href="#gallery" data-toggle="tab">Fotografije</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="form">
                                <form action="{{ route('admin.recipe.update', $recipe->id) }}" method="post" data-resource="recipe" enctype="multipart/form-data" class="form-horizontal">
                                    {{ csrf_field() }}
                                    {{ method_field('put') }}
                                    <div class="panel-body">
                                        <div class="row">
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-sm-6 col-md-5">
                                                <label for="user_id" class="control-label">Autor:</label>
                                                <select class="form-control" name="user_id">
                                                        <option value="" class="form-control">-- Odaberite --</option>
                                                    @foreach($authorsArray as $author)
                                                        <option value="{{ $author->id }}" class="form-control" {{ old('user_id', $recipe->user_id) == $author->id ? 'selected' : '' }}>{{ $author->name }}</option>
                                                    @endforeach
                                                </select>
                                                {!! validation_error($errors, 'user_id') !!}
                                            </div> -->
                                            <div class="col-sm-6 col-md-5">
                                                <label for="category_id" class="control-label">Kategorija:</label>
                                                <select class="form-control" name="category_id">
                                                    <option value="" class="form-control">-- Odaberite --</option>
                                                @foreach($categoriesArray as $category)
                                                    <option value="{{ $category->id }}" class="form-control" {{ old('category_id', $recipe->category_id) == $category->id ? 'selected' : '' }}>{{ $category->translations[0]->pivot->title }}</option>
                                                @endforeach
                                                </select>
                                                {!! validation_error($errors, 'category_id') !!}
                                            </div>
                                            <div class="col-sm-6 col-md-2">
                                                <label for="preparation_time" class="control-label">Vreme pripreme</label>
                                                <input type="text" name="preparation_time" class="form-control" value="{{ old('preparation_time', $recipe->preparation_time) }}">
                                                {!! validation_error($errors, 'preparation_time') !!}
                                            </div>
                                            <div class="col-sm-6 col-md-3">
                                                <label class="control-class">Datum objave</label>
                                                <input type="date" name="created_at" value="{{ date('Y-m-d', strtotime($recipe->created_at)) }}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-lg-6">
                                                <label class="control-class">Naziv</label>
                                                {!! ml_input('textarea', $appLanguages, 'title', $errors, $recipe, 'slugify') !!}
                                            </div>
                                            <div class="col-md-12 col-lg-6">
                                                <label class="control-class">Slug</label>
                                                {!! ml_input('text', $appLanguages, 'slug', $errors, $recipe) !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label class="control-class">Uvod</label>
                                                {!! ml_input('textarea', $appLanguages, 'excerpt', $errors, $recipe) !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label class="control-class">Sastojci</label>
                                                {!! ml_input('textarea', $appLanguages, 'preparation', $errors, $recipe) !!}
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="grid_image" class="upload-label">Grid fotografija</label>
                                                {!! jasnyupload('grid_image', $errors, 'image/png, image/jpeg', config('settings.image.recipe.grid.upload_dir').$recipe->grid_image) !!}
                                            </div>
                                            <div class="col-md-4">
                                                <label for="image" class="upload-label">Fotografija</label>
                                                {!! jasnyupload('image', $errors, 'image/png, image/jpeg', config('settings.image.recipe.hero.upload_dir').$recipe->image) !!}
                                            </div>
                                            <!-- <div class="col-md-4">
                                                <label for="image" class="upload-label">Video</label>
                                                {!! jasnyupload('video', $errors, 'video/mp4', config('settings.video.recipe.upload_path').$recipe->video) !!}
                                            </div> -->
                                            <div class="col-md-3">
                                                <label class="control-class">Youtube ID</label>
                                                <input class="form-control" type="text" name="yt-id" placeholder="Youtube ID" value="{{ $recipe->youtube }}">
                                            </div>
                                        </div>

                                        <h3>Sastojci</h3>
                                            <div class="recipe-steps">
                                                @include('admin.recipe._partials.steps')
                                            </div>
                                            <div class="row text-center">

                                                <button type="button" class="btn btn-warning" id="new-recipe-step">Novi korak</button>
                                                <button type="button" class="btn btn-danger" id="delete-recipe-step">Obrisi korak</button>

                                            </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-lg-4">
                                                <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                                <div class="checkbox block">
                                                    <input type="hidden" name="active" value="{{ $recipe->active }}">
                                                    <div class="toggle toggle-success {{ $recipe->active ? 'active' : ''}} form-active" title="Status"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button class="btn-primary btn">Snimi</button>
                                                <a href="{{ route('admin.recipe.index') }}" class="btn btn-default-alt">Odustani</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="gallery">
                                <div class="panel-body">
                                    @include('admin.recipe._partials.photo')
                                </div>
                            </div>
                        </div>
                    </div> <!-- tab-container -->



                    
                </div>
            </div>
        </div>
    </div>
@stop
