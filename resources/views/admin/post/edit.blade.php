@extends('admin.layout.inner')

@section('top_style')
    @parent
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/jquery-fileupload/css/jquery.fileupload-ui.css">
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/lightbox/css/lightbox.css">
@stop

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>

    <script type="text/javascript" src="/assets/admin/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.iframe-transport.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.fileupload.js"></script>
    <script type="text/javascript" src="/assets/admin/js/file-upload.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/lightbox/js/lightbox.min.js"></script>
    <script type="text/javascript">
        <?php foreach ($appLanguages as $language) echo "CKEDITOR.replace('trans[$language->id][text]');";  ?>
    </script>
@stop
@section('content')
    <div id="page-heading">
        <h1>Izmena posta</h1>
        <div class="options">
            <a href="{{ route('admin.post.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="tab-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#form" data-toggle="tab">Sadržaj posta</a></li>
                            <li><a href="#gallery" data-toggle="tab">Fotografije</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="form">
                                <form action="{{ route('admin.post.update', $post) }}" method="post" data-resource="post" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}
                                {{ method_field('put') }}
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-sm-6 col-md-6">
                                            <label class="control-class">Datum objave</label>
                                            <input type="date" name="created_at" value="{{ date('Y-m-d', strtotime($post->created_at)) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-md-6">
                                            <label for="category_id" class="control-label">Kategorija:</label>
                                            <select class="form-control" name="category_id">
                                                    <option value="{{ $post->category->id }}" class="form-control">{{ $post->category->translations[0]->pivot->title }}</option>
                                                @foreach($categories as $category)
                                                    @if($category->id != $post->category->id)
                                                        <option value="{{ $category->id }}" class="form-control" {{ old('category_id') == $category->id ? 'selected' : '' }}>{{ $category->translations[0]->pivot->title }}</option>
                                                    @endif
                                                @endforeach


                                            </select>
                                            <!-- {!! validation_error($errors, 'user_id') !!} -->
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                        <div class="col-sm-12 col-lg-6">
                                            <label class="control-class">Naziv</label>
                                            {!! ml_input('textarea', $appLanguages, 'title', $errors, $post, 'slugify') !!}
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <label class="control-class">Slug</label>
                                            {!! ml_input('text', $appLanguages, 'slug', $errors, $post) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-lg-12">
                                            <label class="control-class">Uvod</label>
                                            {!! ml_input('textarea', $appLanguages, 'excerpt', $errors, $post) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-lg-12">
                                            <label class="control-class">Tekst</label>
                                            {!! ml_input('textarea', $appLanguages, 'text', $errors, $post) !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="image" class="upload-label">Fotografija (433 × 516 preporuceno)</label>
                                            {!! jasnyupload('image', $errors, 'image/png, image/jpeg', config('settings.image.post.featured.upload_dir').$post->image) !!}
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="image" class="upload-label">Landscape fotografija</label>
                                            {!! jasnyupload('landscape_image', $errors, 'image/png, image/jpeg', config('settings.image.post.landscape.upload_dir').$post->landscape_image) !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="attachment_label" class="upload-label">Naziv dokumenta</label>
                                            {!! ml_input('text', $appLanguages, 'attachment_label', $errors, $post) !!}
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="attachment" class="upload-label">Dokument</label>
                                            {!! jasnyupload('attachment', $errors, 'application/pdf, application/zip, application/x-compressed-zip, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document', config('settings.file.post.upload_dir').$post->attachment, 3) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-lg-4">
                                            <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                            <div class="checkbox block">
                                                <input type="hidden" name="active" value="{{ $post->active }}">
                                                <div class="toggle toggle-success {{ $post->active ? 'active' : '' }} form-active" title="Status"></div>
                                            </div>
                                        </div>
                                        <div class="checkbox block"><label><input type="checkbox" name="checkbox" {{ $post->featured ? 'checked' : '' }} value="1" > Istaknuta vest</label></div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <button class="btn-primary btn">Snimi</button>
                                            <a href="{{ route('admin.post.index') }}" class="btn btn-default-alt">Odustani</a>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="gallery">
                                <div class="panel-body">
                                    @include('admin.post._partials.photo')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
