@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>

    <script type="text/javascript" src="/assets/admin/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        <?php foreach ($appLanguages as $language) echo "CKEDITOR.replace('trans[$language->id][text]');";  ?>
    </script>
@stop
@section('content')
    <div id="page-heading">
        <h1>Novi post</h1>
        <div class="options">
            <a href="{{ route('admin.post.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.post.store') }}" method="post" data-resource="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <label for="category_id" class="control-label">Kategorija:</label>
                                <select class="form-control" name="category_id">
                                        <option value="" class="form-control">-- Odaberite --</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" class="form-control" {{ old('category_id') == $category->id ? 'selected' : '' }}>{{ $category->translations[0]->pivot->title }}</option>
                                    @endforeach
                                </select>
                                <!-- {!! validation_error($errors, 'user_id') !!} -->
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <label class="control-class">Datum objave</label>
                                <input type="date" name="created_at" value="{{ date('Y-m-d') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <label class="control-class">Naziv</label>
                                {!! ml_input('textarea', $appLanguages, 'title', $errors, '', 'slugify') !!}
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <label class="control-class">Slug</label>
                                {!! ml_input('text', $appLanguages, 'slug', $errors) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-class">Uvod</label>
                                {!! ml_input('textarea', $appLanguages, 'excerpt', $errors) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="control-class">Tekst</label>
                                {!! ml_input('textarea', $appLanguages, 'text', $errors) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="image" class="upload-label">Fotografija (433 × 516 preporuceno) </label>
                                {!! jasnyupload('image', $errors, 'image/png, image/jpeg') !!}
                            </div>
                            <div class="col-sm-6">
                                <label for="landscape_image" class="upload-label">Landscape fotografija</label>
                                {!! jasnyupload('landscape_image', $errors, 'image/png, image/jpeg') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="attachment_label" class="upload-label">Naziv dokumenta</label>
                                {!! ml_input('text', $appLanguages, 'attachment_label', $errors) !!}
                            </div>
                            <div class="col-sm-6">
                                <label for="attachment" class="upload-label">Dokument</label>
                                {!! jasnyupload('attachment', $errors, 'application/pdf, application/zip, application/x-compressed-zip, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="0">
                                    <div class="toggle toggle-success form-active" title="Status"></div>
                                </div>
                            </div>
                            <!-- <div class="checkbox block"><label><input type="checkbox" name="checkbox"> Istaknuta vest</label></div> -->
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ route('admin.post.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
