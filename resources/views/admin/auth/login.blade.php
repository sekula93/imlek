@extends('admin.layout.public')

@section('content')
    <div class="verticalcenter">
        <img src="/assets/admin/img/logo-old.png" class="login-logo" alt="imlek">
        <div class="panel panel-primary">
            {!! Form::open(['route' => 'admin.auth.post-login', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'login']) !!}
            <div class="panel-body">
                <h4 class="text-center" style="margin-bottom: 25px;">Imlek admin</h4>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Lozinka']) !!}
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="pull-left">
                        <label style="margin-top:0"><input type="checkbox" name="remember" style="margin-bottom: 20px" checked="">Upamti me</label>
                    </div>
                </div>

                @if(Session::has('errors'))
                <div class="error text-center">Kombinacija email/lozinka nije ispravna</div>
                @endif
            </div>
            <div class="panel-footer">
                {{-- <a href="extras-forgotpassword.htm" class="pull-left btn btn-link" style="padding-left:0">{{ Lang::get('admin/auth.forgot_password') }}?</a> --}}

                <div class="pull-left">
                    {!! Form::submit('Prijavi se', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
     </div>

@endsection
