@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
@stop

<?php
if (Auth::user()->role == 2) {
    $backRoute = route('admin.post.index');
} else {
    $backRoute = route('admin.recipe-author.index');
}

?>

@section('content')
    <div id="page-heading">
        <h1>Izmena autora recepta</h1>
        <div class="options">
            <a href="{{ $backRoute }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.recipe-author.update', $user->id) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="panel-body">
                            <div class="row {{ error_class($errors, 'name') }}">
                                <div class="col-sm-6">
                                    <label for="name" class="control-label">Ime</label>
                                    <input type="text" name="name" class="form-control required" autofocus value="{{ old('name', $user->name) }}">
                                    {!! validation_error($errors, 'name') !!}
                                </div>
                            </div>
                            <div class="row {{ error_class($errors, 'email') }}">
                                <div class="col-sm-6">
                                    <label for="email" class="control-label">E-mail</label>
                                    <input type="email" name="email" class="form-control required" value="{{ old('email', $user->email) }}">
                                    {!! validation_error($errors, 'email') !!}
                                </div>
                            </div>
                            <div class="row {{ error_class($errors, 'image') }}">
                                <div class="col-sm-6">
                                    <label for="image" class="upload-label">Profilna slika</label>
                                    {!! jasnyupload('image', $errors, 'image/png, image/jpeg', config('settings.image.author.upload_dir').$user->recipeAuthorProfile->image) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                    <div class="checkbox block">
                                        <input type="hidden" name="active" value="{{ $user->active }}">
                                        <div class="toggle toggle-success {{ $user->active ? 'active' : '' }} form-active" title="{{ trans('admin/form.btn_status') }}"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-6">
                                    <button class="btn-primary btn">Snimi</button>
                                    <a href="{{ $backRoute }}" class="btn btn-default-alt">Odustani</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
