@extends('admin.layout.inner')

@section('content')
    <div id="page-heading">
        <h1>Autori recepata</h1>
        <div class="options">
            <a href="{{ route('admin.recipe-author.create') }}" class="btn btn-success"><i class="fa fa-user"></i> Novi autor</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-body resource-container" data-resource="recipe-author" data-modal="Svi recepti izabranog autora će biti obrisani. <br> Da li ste sigurni da želite da obrišete autora? ">
                        @if(count($listArray))
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Ime</th>
                                    <th class="col-md-1 text-center">Aktivan</th>
                                    <th class="col-md-1 text-center">Opcije</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($listArray as $item)
                                @if($item->id != Auth::user()->id)
                                <tr data-id="{{ $item->id }}">
                                    <td class="{{ $item->active ? '' : 'inactive' }}"><a href="{{ route('admin.recipe-author.edit', $item->id) }}">{{ $item->name }}</a></td>
                                    <td class="text-right table-options">
                                        <div class="toggle toggle-success {{ $item->active ? 'active' : '' }} table-ajax" title="{{ trans('admin/form.btn_status') }}" data-id="{{ $item->id }}"></div>
                                    </td>
                                    <td class="text-center">
                                        <div class="col-sm-6">
                                            <a href="{{ route('admin.recipe-author.edit', $item->id) }}" class="option-edit"><i class="fa fa-pencil"></i></a>
                                        </div>
                                        <div class="col-sm-6">
                                            <a class="delete-item"><i class="fa fa-times"></i> </a>
                                        </div>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">{!! $listArray->render() !!}</div>
                        @else
                        <p class="text-center">Lista je trenutno prazna</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
