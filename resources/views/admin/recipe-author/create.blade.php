@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
@stop


@section('content')
    <div id="page-heading">
        <h1>Novi autor recepta</h1>
        <div class="options">
            <a href="{{ route('admin.recipe-author.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.recipe-author.store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <div class="form-group {{ error_class($errors, 'name') }}">
                                <div class="col-sm-6 col-lg-4">
                                    <label for="name" class="control-label">Ime</label>
                                    <input type="text" name="name" class="form-control" autofocus value="{{ old('name') }}">
                                    {!! validation_error($errors, 'name') !!}
                                </div>
                            </div>
                            <div class="form-group {{ error_class($errors, 'email') }}">
                                <div class="col-sm-6 col-lg-4">
                                    <label for="email" class="control-label">Email</label>
                                    <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                                    {!! validation_error($errors, 'email') !!}
                                </div>
                            </div>
                            <div class="form-group {{ error_class($errors, 'image') }}">
                                <div class="col-sm-6">
                                    <label for="image" class="upload-label">Profilna slika</label>
                                    {!! jasnyupload('image', $errors, 'image/png, image/jpeg') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 col-lg-4">
                                    <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                    <div class="checkbox block">
                                        <input type="hidden" name="active" value="0">
                                        <div class="toggle toggle-success form-active" title="Status"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-6">
                                    <button class="btn-primary btn">Snimi</button>
                                    <a href="{{ route('admin.recipe-author.index') }}" class="btn btn-default-alt">Odustani</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
