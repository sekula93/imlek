@extends('admin.layout.inner')

@section('content')
    <div id="page-heading">
        <h1>Package3D</h1>
        <div class="options">
            <a href="{{ route('admin.package.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Novi Package3D</a>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-body resource-container" data-resource="package" data-modal="Da li ste sigurni da želite da obrišete proizvod?">
                        @if(count($listArray))
                        <table class="table table-striped sortable">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Naziv</th>
                                    <th class="col-lg-2 col-md-3">Datum kreiranja</th>
                                    <th class="col-lg-1 text-center">Aktivan</th>
                                    <th class="col-md-1 text-center">Opcije</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($listArray as $item)
                                <tr data-id="{{ $item->id }}" id="item-{{ $item->id }}">
                                    <td class="bars"><i class="fa fa-bars"></i></td>
                                    <td class="{{ $item->active ? '' : 'inactive' }}"><a href="{{ route('admin.package.edit', $item->id) }}">{{ $item->translations->find(1)->pivot->title }}</a></td>
                                    <td class="{{ $item->active ? '' : 'inactive' }}">{{ $item->created_at->format('d.m.Y H:i:s') }}</td>
                                    <td class="text-right table-options">
                                        <div class="col-sm-12">
                                            <div class="toggle toggle-success {{ $item->active ? 'active' : '' }} table-ajax" title="Status" data-id="{{ $item->id }}"></div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="col-sm-6">
                                            <a href="{{ route('admin.package.edit', $item->id) }}" class="option-edit"><i class="fa fa-pencil"></i></a>
                                        </div>
                                        <div class="col-sm-6">
                                            <a class="delete-item"><i class="fa fa-times"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">{!! $listArray->render() !!}</div>
                        @else
                        <p class="text-center">Lista je trenutno prazna</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
