@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
@stop
@section('content')
    <div id="page-heading">
        <h1>Novi Package3D</h1>
        <div class="options">
            <a href="{{ route('admin.package.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.package.store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                <label for="title" class="control-label">Naslov</label>
                                {!! ml_input('textarea', $appLanguages, 'title', $errors, '', 'slugify') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="" class="control-label">Tekst</label>
                                {!! ml_input('textarea', $appLanguages, 'text', $errors) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="" class="control-label">Link</label>
                                {!! ml_input('text', $appLanguages, 'link', $errors) !!}
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <label for="web_address" class="control-label">Web adresa</label>
                                <input type="text" name="web_address" class="form-control" value="{{ old('web_address') }}">
                                {!! validation_error($errors, 'web_address') !!}
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-sm-12 col-md-4">
                                <label for="featured_image" class="upload-label">Fotografija</label>
                                {!! jasnyupload('featured_image', $errors, 'image/png, image/jpeg') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="0">
                                    <div class="toggle toggle-success form-active" title="Status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ route('admin.package.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
