@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
@stop


@section('content')
    <div id="page-heading">
        <h1>Katalog
            <a href="/uploads/catalogues/{{ $catalogue->value }}" target="_blank" class="btn">Trenutni katalog</a>
        </h1>
        <div class="options">
            <a href="{{ route('admin.dashboard.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.static.update-catalogue') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="file" class="upload-label">Novi katalog</label>
                                    {!! jasnyupload('file', $errors, 'application/pdf') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-6">
                                    <button class="btn-primary btn">Snimi</button>
                                    <a href="{{ route('admin.dashboard.index') }}" class="btn btn-default-alt">Odustani</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
