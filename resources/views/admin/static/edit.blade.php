@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    {!! Html::script('/assets/admin/ckeditor/ckeditor.js') !!}
    <script type="text/javascript">
        CKEDITOR.replace('text', {
            language: 'sr',
            height: 500,
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload'
        });
    </script>
@stop


@section('content')
    <div id="page-heading">
        <h1>Statični sadržaj</h1>
        <div class="options">
            <a href="{{ URL::route('admin.dashboard.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    {!! Form::open(['url' => route('admin.static-page.update', $text->id), 'method' => 'put', 'files' => true, 'class' => 'form-horizontal']) !!}
                    <div class="panel-body">
                        <div class="form-group {{ error_class($errors, 'title') }}">
                            {!! Form::label('title', 'Naziv:', ['class' => 'col-sm-2 col-md-2 col-lg-1 control-label']) !!}
                            <div class="col-sm-11">
                                {!! Form::text('title', old('title', $text->title), ['class' => 'form-control', 'readonly', 'disabled']) !!}
                                {!! validation_error($errors, 'title') !!}
                            </div>
                        </div>
                        <div class="form-group {{ error_class($errors, 'text') }}">
                            {!! Form::label('text', 'Sadržaj:', ['class' => 'col-sm-2 col-md-2 col-lg-1 control-label']) !!}
                            <div class="col-sm-11">
                                {!! Form::textarea('text', old('text', $text->text), ['class' => 'form-control', 'autofocus', 'rows' => 20])  !!}
                                {!! validation_error($errors, 'text') !!}
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ URL::route('admin.dashboard.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
