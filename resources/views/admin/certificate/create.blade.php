@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>

@stop
@section('content')
    <div id="page-heading">
        <h1>Novi izvestaj</h1>
        <div class="options">
            <a href="{{ route('admin.certificate.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.certificate.store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel-body">
                            
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="attachment_label" class="upload-label">Naziv dokumenta</label>
                                {!! ml_input('text', $appLanguages, 'attachment_label', $errors) !!}
                            </div>
                            <div class="col-sm-4">
                                <label for="attachment" class="upload-label">Dokument</label>
                                {!! jasnyupload('attachment', $errors, 'application/pdf, application/zip, application/x-compressed-zip, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document') !!}
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label for="featured_image" class="upload-label">Preview fotografija</label>
                                {!! jasnyupload('featured_image', $errors, 'image/png, image/jpeg') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="0">
                                    <div class="toggle toggle-success form-active" title="Status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ route('admin.certificate.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
