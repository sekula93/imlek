@extends('admin.layout.inner')

@section('content')
    <div id="page-heading">
        <h1>Slajdovi</h1>
        <div class="options">
            <a href="{{ URL::route('admin.slide.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Novi slajd</a>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body resource-container" data-resource="slide" data-modal="Da li ste sigurni da želite da obrišete slajd?">
                        <div class="tab-container">
                            {{-- <ul class="nav nav-tabs">
                                <li class="active"><a href="#image_list" data-toggle="tab">Slike</a></li>
                                <li><a href="#video_list" data-toggle="tab">Video</a></li>
                            </ul> --}}
                            {{-- <div class="tab-content">
                                <div class="tab-pane active" id="image_list"> --}}
                                   {{--  @if(count($imageArray))
                                        <div class="collection">
                                        <ul class="gallery slide-gallery">
                                            @foreach($imageArray as $item)
                                                <li class="panel panel-body list-item" data-id="{{ $item->id }}" id="image-{{ $item->id }}">
                                                    <img src="{{ config('settings.image.slide.upload_dir'). $item->filename }}" class="img-responsive {{ $item->active ? '' : 'inactive' }}">
                                                    <div style="margin-top:10px;">
                                                        <div class="row">
                                                            <div class="col-sm-8">
                                                                <div class="toggle toggle-success {{ $item->active ? 'active' : '' }} collection-ajax" title="status" data-id="{{ $item->id }}" style="margin:0"></div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <a class="delete-collection-item pull-right col-xs-6 text-center"><i class="fa fa-times"></i></a>
                                                                <a href="{{ route('admin.slide.edit', $item->id) }}" class="pull-right col-xs-6 text-center"><i class="fa fa-pencil"></i></a>
                                                            </div>
                                                            <div class="col-sm-4">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        </div>
                                    @endif --}}

                                    @if(count($listArray))
                                        <div class="collection">
                                        <ul class="gallery slide-gallery">
                                            @foreach($listArray as $item)
                                                <li class="panel panel-body list-item" data-id="{{ $item->id }}" id="image-{{ $item->id }}">
                                                    @if($item->type == '1')
                                                        <img src="{{ config('settings.image.slide.upload_dir'). $item->filename }}" class="img-responsive {{ $item->active ? '' : 'inactive' }}">
                                                    @else 
                                                        <video src="{{ config('settings.video.slide.upload_dir'). $item->filename }}" controls style="width:100%"></video>
                                                    @endif
                                                    <div style="margin-top:10px;">
                                                        <div class="row">
                                                            <div class="col-sm-8">
                                                                <div class="toggle toggle-success {{ $item->active ? 'active' : '' }} collection-ajax" title="status" data-id="{{ $item->id }}" style="margin:0"></div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <a class="delete-collection-item pull-right col-xs-6 text-center"><i class="fa fa-times"></i></a>
                                                                <a href="{{ route('admin.slide.edit', $item->id) }}" class="pull-right col-xs-6 text-center"><i class="fa fa-pencil"></i></a>
                                                            </div>
                                                            <div class="col-sm-4">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        </div>
                                    @endif

                                {{-- </div> --}}
                                {{-- <div class="tab-pane " id="video_list"> --}}
                                    {{-- @if(count($videoArray))
                                        <div class="collection">
                                        <ul class="gallery slide-gallery">
                                            @foreach($videoArray as $item)
                                                <li class="panel panel-body list-item" data-id="{{ $item->id }}" id="video-{{ $item->id }}">
                                                    <video src="{{ config('settings.video.slide.upload_dir'). $item->filename }}" controls style="width:100%"></video>
                                                    <div style="margin-top:10px;">
                                                        <div class="row">
                                                            <div class="col-sm-8">
                                                                <div class="toggle toggle-success {{ $item->active ? 'active' : '' }} collection-ajax" title="status" data-id="{{ $item->id }}" style="margin:0"></div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <a class="delete-collection-item pull-right col-xs-6 text-center"><i class="fa fa-times"></i></a>
                                                                <a href="{{ route('admin.slide.edit', $item->id) }}" class="pull-right col-xs-6 text-center"><i class="fa fa-pencil"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        </div>
                                    @else
                                    @endif --}}


                                {{-- </div> --}}
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
