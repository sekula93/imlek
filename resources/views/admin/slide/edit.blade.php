@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
@stop

@section('content')
    <div id="page-heading">
        <h1>Izmena slajda</h1>
        <div class="options">
            <a href="{{ route('admin.slide.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.slide.update', $slide->id) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    <div class="panel-body">
                        <div class="row">
                        </div>
                        <div class="row">
                            <!-- <div class="col-sm-6 col-md-4">
                                <label for="subtitle" class="control-label">Ispis</label>
                                <select name="text_color" class="form-control">
                                    <option value="2" {{ old('text_color', $slide->text_color) == 2 ? 'selected' : '' }}>Beli</option>
                                    <option value="1" {{ old('text_color', $slide->text_color) == 1 ? 'selected' : '' }}>Tamni</option>
                                    <option value="3" {{ old('text_color', $slide->text_color) == 3 ? 'selected' : '' }}>Bez filtera</option>
                                </select>
                            </div> -->
                            <div class="col-sm-12 col-md-8">
                                <label for="title" class="control-label">Naslov</label>
                                {!! ml_input('textarea', $appLanguages, 'title', $errors, $slide) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-8">
                                <label for="subtitle" class="control-label">Podnaslov</label>
                                {!! ml_input('textarea', $appLanguages, 'subtitle', $errors, $slide) !!}
                            </div>
                        </div>
                        <div class="row">
                            <!-- <div class="col-md-3">
                                    <label for="button_label" class="control-label">CTA</label>
                                    {!! ml_input('text', $appLanguages, 'button_label', $errors, $slide) !!}
                            </div>
                            <div class="col-sm-6 col-md-2">
                                <label for="subtitle" class="control-label">Tip URL-a</label>
                                <select name="url_type" class="form-control">
                                    <option value="">-- Odaberite --</option>
                                    <option value="1" {{ old('url_type', $slide->url_type) == 1 ? 'selected' : '' }}>Vest</option>
                                    <option value="2" {{ old('url_type', $slide->url_type) == 2 ? 'selected' : '' }}>Interni link</option>
                                    <option value="3" {{ old('url_type', $slide->url_type) == 3 ? 'selected' : '' }}>Eksterni link</option>
                                </select>
                            </div> -->
                            <div class="col-md-7">
                                <label for="url" class="control-label">URL</label>
                                {!! ml_input('text', $appLanguages, 'url', $errors, $slide) !!}
                            </div>
                        </div>
                        <div class="row {{ error_class($errors, 'file') }}">
                            <div class="col-sm-4">
                                <label for="file" class="upload-label">Fajl</label>
                                {!! jasnyupload('file', $errors, 'image/png, image/jpeg, video/mp4', $uploadPath.$slide->filename, $slide->type) !!}
                            </div>
                            <div class="col-sm-4 fallback-image">
                                <label for="fallback_image" class="upload-label">Mobile foto</label>
                                {!! jasnyupload('fallback_image', $errors, 'image/png, image/jpeg', $slide->fallback_image ? config('settings.image.slide.upload_dir').$slide->fallback_image : null, 1) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="{{ $slide->active }}">
                                    <div class="toggle toggle-success {{ $slide->active ? 'active' : '' }} form-active" title="Status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ route('admin.slide.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
