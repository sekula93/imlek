@extends('admin.layout.inner')
@section('top_style')
    @parent
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/jquery-fileupload/css/jquery.fileupload-ui.css">
@stop

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.iframe-transport.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.fileupload.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/lightbox/js/lightbox.min.js"></script>
@stop
@section('content')
    <div id="page-heading">
        <h1>Izmena proizvoda</h1>
        <div class="options">
            <a href="{{ route('admin.brand.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="tab-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#form" data-toggle="tab">Detalji proizvoda</a></li>

                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="form">
                                <form action="{{ route('admin.brand.update', $brand) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}
                                {{ method_field('put') }}
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <label for="name" class="control-label">Naziv Brenda</label>
                                            {!! Form::text('name', old('name', $brand->name), ['class' => 'form-control', 'autofocus']) !!}
                                            {!! validation_error($errors, 'name') !!}
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label for="featured_image" class="upload-label">Fotografija</label>
                                            {!! jasnyupload('featured_image', $errors, 'image/png, image/jpeg', config('settings.image.brand.featured.upload_dir').$brand->featured_image, 1) !!}
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-lg-4">
                                            <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                            <div class="checkbox block">
                                                <input type="hidden" name="active" value="{{ $brand->active }}">
                                                <div class="toggle toggle-success {{ $brand->active ? 'active' : '' }} form-active" title="Status"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <button class="btn-primary btn">Snimi</button>
                                            <a href="{{ route('admin.brand.index') }}" class="btn btn-default-alt">Odustani</a>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
