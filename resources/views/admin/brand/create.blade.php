@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
@stop

@section('content')
    <div id="page-heading">
        <h1>Novi Brend</h1>
        <div class="options">
            <a href="{{ route('admin.brand.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.brand.store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel-body">

                        
                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                <label for="name" class="control-label">Naziv Brenda</label>
                                {!! Form::text('name', old('name'), ['class' => 'form-control', 'autofocus']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="0">
                                    <div class="toggle toggle-success form-active" title="Status"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-4">
                                <label for="featured_image" class="upload-label">Fotografija</label>
                                {!! jasnyupload('featured_image', $errors, 'image/png, image/jpeg') !!}
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ route('admin.brand.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
