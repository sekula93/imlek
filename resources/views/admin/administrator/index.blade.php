@extends('admin.layout.inner')

@section('content')
    <div id="page-heading">
        <h1>Administratori</h1>
        <div class="options">
            <a href="{{ URL::route('admin.administrator.create') }}" class="btn btn-success"><i class="fa fa-institution"></i> Novi administrator</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-body resource-container" data-resource="administrator" data-modal="Da li ste sigurni da želite da obrišete administratora?">
                        @if(count($listArray))
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Ime</th>
                                    <th>Email</th>
                                    <th class="col-md-1 text-center">Aktivan</th>
                                    <th class="col-md-1 text-center">Opcije</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($listArray as $item)
                                @if($item->id != Auth::user()->id)
                                <tr data-id="{{ $item->id }}">
                                    <td class="{{ $item->active ? '' : 'inactive' }}"><a href="{{ URL::route('admin.administrator.edit', $item->id) }}">{{ $item->name }}</a></td>
                                    <td class="{{ $item->active ? '' : 'inactive' }}"><a href="mailto:{{ $item->email }}" target="_blank">{{ $item->email }}</a></td>
                                    <td class="text-right table-options">
                                        <div class="toggle toggle-success {{ $item->active ? 'active' : '' }} table-ajax" title="{{ Lang::get('admin/form.btn_status') }}" data-id="{{ $item->id }}"></div>
                                    </td>
                                    <td class="text-center">
                                        <div class="col-sm-6">
                                        <a href="{{ URL::route('admin.administrator.edit', $item->id) }}" class="option-edit"><i class="fa fa-pencil"></i></a>
                                        </div>
                                        <div class="col-sm-6">
                                        <a class="delete-item"><i class="fa fa-times"></i> </a>
                                        </div>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">{!! $listArray->render() !!}</div>
                        @else
                        <p class="text-center">Lista je trenutno prazna</p>
                        <p class="text-center">
                            <a href="{{ URL::route('admin.administrator.create') }}" class="btn">{{ trans('admin/user.add_administrator') }}</a>
                        </p>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
