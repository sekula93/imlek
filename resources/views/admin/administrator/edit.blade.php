@extends('admin.layout.inner')

@section('content')
    <div id="page-heading">
        <h1>Administrator</h1>
        <div class="options">
            <a href="{{ URL::route('admin.administrator.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>Izmena podataka</h4>
                    </div>
                    {!! Form::open(['url' => route('admin.administrator.update', $user->id), 'method' => 'put', 'class' => 'form-horizontal']) !!}
                    <div class="panel-body">
                        <div class="form-group {{ error_class($errors, 'name') }}">
                            {!! Form::label('name', 'Ime', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('name', old('name', $user->name), ['class' => 'form-control', 'autofocus']) !!}
                                {!! validation_error($errors, 'name') !!}
                            </div>
                        </div>
                        <div class="form-group {{ error_class($errors, 'email') }}">
                            {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::email('email', old('email', $user->email), ['class' => 'form-control', 'required']) !!}
                                {!! validation_error($errors, 'email') !!}
                            </div>
                        </div>
                        <div class="form-group {{ error_class($errors, 'password') }}">
                            {!! Form::label('password', 'Lozinka', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::password('password', ['class' => 'form-control']) !!}
                                <p class="help-block">Lozinka mora da sadrži minimalno šest karaktera od kojih minimalno jedno veliko slovo, malo slovo i jedan broj</p>
                                {!! validation_error($errors, 'password') !!}
                            </div>
                        </div>
                        <div class="form-group {{ error_class($errors, 'password_confirmation') }}">
                            {!! Form::label('password_confirmation', 'Potvrda lozinke', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                {!! validation_error($errors, 'password_confirmation') !!}
                            </div>
                        </div>
                        @if(Auth::user()->id != $user->id)
                        <div class="form-group">
                            {!! Form::label('active', 'Aktivan', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-6">
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="{{ $user->active }}">
                                    <div class="toggle toggle-success {{ $user->active ? 'active' : '' }} form-active" title="{{ trans('admin/form.btn_status') }}"></div>
                                </div>
                            </div>
                        </div>
                        @else
                            <input type="hidden" name="active" value="1">
                        @endif
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ route('admin.administrator.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
