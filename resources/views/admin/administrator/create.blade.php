@extends('admin.layout.inner')

@section('content')
    <div id="page-heading">
        <h1>Administrator</h1>
        <div class="options">
            <a href="{{ URL::route('admin.administrator.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>Novi administrator</h4>
                    </div>
                    {!! Form::open(['url' => route('admin.administrator.store'), 'method' => 'post', 'class' => 'form-horizontal']) !!}
                    <div class="panel-body">
                        <div class="form-group {{ error_class($errors, 'name') }}">
                            {!! Form::label('name', 'Ime', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-6 col-lg-4">
                                {!! Form::text('name', old('name'), ['class' => 'form-control', 'autofocus']) !!}
                                {!! validation_error($errors, 'name') !!}
                            </div>
                        </div>
                        <div class="form-group {{ error_class($errors, 'email') }}">
                            {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-6 col-lg-4">
                                {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
                                {!! validation_error($errors, 'email') !!}
                            </div>
                        </div>
                        <div class="form-group {{ error_class($errors, 'password') }}">
                            {!! Form::label('password', 'Lozinka', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-6 col-lg-4">
                                {!! Form::password('password', ['class' => 'form-control']) !!}
                                <p class="help-block">Lozinka mora da sadrži minimalno šest karaktera od kojih minimalno jedno veliko slovo, malo slovo i jedan broj</p>
                                {!! validation_error($errors, 'password') !!}
                            </div>
                        </div>
                        <div class="form-group {{ error_class($errors, 'password_confirmation') }}">
                            {!! Form::label('password_confirmation', 'Potvrdi lozinku', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-6 col-lg-4">
                                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                {!! validation_error($errors, 'password_confirmation') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('active', 'Aktivan', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-6 col-lg-4">
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="1">
                                    <div class="toggle toggle-success form-active" title="Status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ URL::route('admin.administrator.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
