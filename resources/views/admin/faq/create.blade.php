@extends('admin.layout.inner')


@section('content')
    <div id="page-heading">
        <h1>Dodavanje FAQ</h1>
        <div class="options">
            <a href="{{ route('admin.faq.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.faq.store') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-10">
                                <label for="" class="control-label">Naslov</label>
                                {!! ml_input('text', $appLanguages, 'title', $errors) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <label for="" class="control-label">Tekst</label>
                                {!! ml_input('textarea', $appLanguages, 'text', $errors) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="0">
                                    <div class="toggle toggle-success form-active" title="Status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ route('admin.faq.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
