@extends('admin.layout.inner')

@section('content')
    <div id="page-heading">
        <h1>Postovi</h1>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-body resource-container" data-resource="social-post" data-modal="Da li ste sigurni da želite da obrišete post?">
                        <div class="tab-content">
                            <div class="tab-pane active" id="image_list">
                                @if(count($postsArray))
                                    <div class="collection">
                                    <ul class="gallery social-post-gallery">
                                        @foreach($postsArray as $item)
                                            <li class="panel panel-body list-item" data-id="{{ $item->id }}" id="image-{{ $item->id }}">
                                                <img src="/uploads/posts/images/{{ $item->image }}" class="img-responsive {{ $item->active ? '' : 'inactive' }}">
                                                <div style="margin-top:10px;">
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="toggle toggle-success {{ $item->active ? 'active' : '' }} collection-ajax" title="status" data-id="{{ $item->id }}" style="margin:0"></div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <a class="delete-collection-item pull-right col-xs-6 text-center"><i class="fa fa-times"></i></a>
                                                        </div>
                                                        <div class="col-sm-4">
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                    </div>
                                @else
                                <p class="text-center">Lista je trenutno prazna</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
