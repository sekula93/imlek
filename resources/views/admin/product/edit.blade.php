@extends('admin.layout.inner')
@section('top_style')
    @parent
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/jquery-fileupload/css/jquery.fileupload-ui.css">
@stop

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.iframe-transport.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.fileupload.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/lightbox/js/lightbox.min.js"></script>
    <script type="text/javascript" src="/assets/admin/js/packshots.js"></script>
@stop
@section('content')
    <div id="page-heading">
        <h1>Izmena proizvoda</h1>
        <div class="options">
            <a href="{{ route('admin.product.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="tab-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#form" data-toggle="tab">Detalji proizvoda</a></li>
                            <li><a href="#packshots" data-toggle="tab">Packshots</a></li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="form">
                                <form action="{{ route('admin.product.update', $product) }}" method="post" data-resource="product" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}
                                {{ method_field('put') }}
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-5">
                                            <label for="brand_id" class="control-label">Brend:</label>
                                            <select class="form-control" name="brand_id">
                                                    <option value="{{ $product->brand_id }}" class="form-control">{{ $product->brand->name }}</option>
                                                    @foreach($brands as $brand)
                                                        @if($product->brand_id != $brand->id)
                                                        <option value="{{ $brand->id }}" class="form-control" {{ old('brand_id') == $brand->id ? 'selected' : '' }}>{{ $brand->name }}</option>
                                                        @endif
                                                    @endforeach
                                            </select>
                                            <!-- {!! validation_error($errors, 'user_id') !!} -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="" class="control-label">Naslov</label>
                                            {!! ml_input('text', $appLanguages, 'title', $errors, $product, 'slugify') !!}
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="" class="control-label">Slug</label>
                                            {!! ml_input('text', $appLanguages, 'slug', $errors, $product) !!}
                                        </div>
                                    </div>
                                   
                                    {{-- <div class="row">
                                        <div class="col-sm-12">
                                            <label for="" class="control-label">Uvod</label>
                                            {!! ml_input('textarea', $appLanguages, 'excerpt', $errors, $product) !!}
                                        </div>
                                    </div> --}}
                                   {{--  <div class="row">
                                        <div class="col-lg-6">
                                            <label for="web_address" class="control-label">Web adresa</label>
                                            <input type="text" name="web_address" class="form-control" value="{{ old('web_address', $product->web_address) }}">
                                            {!! validation_error($errors, 'web_address') !!}
                                        </div>
                                    </div> --}}
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label for="featured_image" class="upload-label">Fotografija</label>
                                            {!! jasnyupload('featured_image', $errors, 'image/png, image/jpeg', config('settings.image.product.featured.upload_dir').$product->featured_image, 1) !!}
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="poster" class="upload-label">Fotografija EN</label>
                                            {!! jasnyupload('poster', $errors, 'image/png, image/jpeg', config('settings.image.product.poster.upload_dir').$product->poster, 1) !!}
                                        </div>
                                        <!-- <div class="col-lg-4">
                                            <label for="video" class="upload-label">Video</label>
                                            {!! jasnyupload('video', $errors, 'video/mp4', config('settings.video.product.upload_dir').$product->video, 2) !!}
                                        </div> -->
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <label for="fb-link">Facebook link</label>
                                            <input type="text" name="fb-link" class="form-control" value="{{ $product->fb_link }}">
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label for="in-link">Instagram link</label>
                                            <input type="text" name="in-link" class="form-control" value="{{ $product->in_link }}">
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label for="yt-link">YouTube link</label>
                                            <input type="text" name="yt-link" class="form-control" value="{{ $product->yt_link }}">
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label for="ms-link">Microsite link</label>
                                            <input type="text" name="ms-link" class="form-control" value="{{ $product->ms_link }}">
                                        </div>

                                        <div class="col-md-offset-9 col-md-3 col-sm-12">
                                            <label for="" class="control-label">Microsite button text</label>
                                            {!! ml_input('text', $appLanguages, 'text', $errors, $product) !!}
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-lg-4">
                                            <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                            <div class="checkbox block">
                                                <input type="hidden" name="active" value="{{ $product->active }}">
                                                <div class="toggle toggle-success {{ $product->active ? 'active' : '' }} form-active" title="Status"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <button class="btn-primary btn">Snimi</button>
                                            <a href="{{ route('admin.product.index') }}" class="btn btn-default-alt">Odustani</a>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="packshots">
                                <div class="panel-body">
                                    @include('admin.product._partials.packshot')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
