@extends('admin.layout.inner')

@section('content')
    <div id="page-heading">
        <h1>Nutritivne tabele</h1>
        <div class="options">
            <a href="{{ route('admin.product.nutritive.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Nova tabela</a>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-body resource-container" data-resource="product/nutritive" data-modal="Da li ste sigurni da želite da obrišete tabelu?">
                        @if(count($listArray))
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Naziv</th>
                                    <th class="col-md-1 text-center">Opcije</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($listArray as $item)
                                <tr data-id="{{ $item->id }}">
                                    <td><a href="{{ route('admin.product.nutritive.edit', $item->id) }}">{{ $item->name }}</a></td>
                                    <td class="text-center">
                                        <div class="col-sm-6">
                                            <a href="{{ route('admin.product.nutritive.edit', $item->id) }}" class="option-edit"><i class="fa fa-pencil"></i></a>
                                        </div>
                                        <div class="col-sm-6">
                                            <a class="delete-item"><i class="fa fa-times"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">{!! $listArray->render() !!}</div>
                        @else
                        <p class="text-center">Lista je trenutno prazna</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
