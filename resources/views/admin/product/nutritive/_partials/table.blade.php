<?php
if (isset($nutritiveTable) && count($nutritiveTable[1]) > 0) {
    $rows = count($nutritiveTable[1]);
    $columns = count($nutritiveTable[1][0]);
} else {
    $rows = 2;
    $columns = 2;
}
?>
<table class="table nutritive-table">
    <tbody>
        @for($r = 0; $r < $rows; $r++)
        <tr>
            @for($c = 0; $c < $columns; $c++)
                @if($r == 0 && $c == 0)
                <th>
                    <button type="button" class="btn new-row">novi red <i class="fa fa-arrow-down"></i></button> <button type="button" class="btn pull-right new-column">nova kolona <i class="fa fa-arrow-right"></i></button>
                @foreach($appLanguages as $index => $language)
                    <input type="hidden" name="trans[{{ $language->id }}][table][0][0]" value="">
                @endforeach
                </th>
                @else
                {!! $r > 0 ? '<td>' : '<th>' !!}
                    <ul class="lang-input-wrap">
                        @foreach($appLanguages as $index => $language)
                        <li class="lang-input {{ $language->id == 1 ? 'active' : '' }}">
                            <?php $value = isset($nutritiveTable) && isset($nutritiveTable[$language->id]) ? $nutritiveTable[$language->id][$r][$c] : null;  ?>
                            <input type="text" data-lang="{{ $language->id }}" data-col="{{ $c }}" class="form-control {{ $r > 0 && $c > 0 ? 'cloneme' : '' }}" name="trans[{{ $language->id }}][table][{{ $r }}][{{ $c }}]" value="{{ old('trans'.$language->id.'.table.'.$r.'.'.$c, $value) }}">
                            {{-- @if($r == 0 || $c == 0) --}}
                            <span class="lang-input-switch">{{ strtoupper($language->code) }}</span>
                            {{-- @endif --}}
                        </li>
                        @endforeach
                    </ul>
                {!! $r > 0 ? '</td>' : '</th>' !!}
                @endif
            @endfor
        </tr>
        @endfor
    </tbody>
</table>