@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script src="/assets/admin/js/nutritive_table.js"></script>
@stop
@section('content')
    <div id="page-heading">
        <h1>Nova Tabela nutritivnih vrednosti</h1>
        <div class="options">
            <a href="{{ route('admin.product.nutritive.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.product.nutritive.store') }}" method="post" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <label for="title" class="control-label">Naziv</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                {!! validation_error($errors, 'name') !!}
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-sm-12">
                                <label for="" class="control-label">Legenda</label>
                                {!! ml_input('textarea', $appLanguages, 'legend', $errors) !!}
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-sm-12 col-lg-12">
                                <section id="nutritive-table">
                                <h3>Tabela</h3>
                                <p class="help-block">Za indentovani tekst tipa "od toga" staviti prefiks <strong>#</strong> npr. "#od toga šećeri"</p>
                                @include('admin.product.nutritive._partials.table')
                                </section>
                            </div>
                        </div>
                    <button class="btn btn-danger delete-nutri-row" type="button">Obriši red</button>
                    <button class="btn btn-danger delete-nutri-column" type="button">Obriši kolonu</button>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ route('admin.post.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
