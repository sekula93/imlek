<div class="row">
    <div class="col-md-12">
                <!-- The fileinput-button span is used to style the file input field as button -->
        <form class="fileupload-photo" action="{{ route('admin.product.packshot.store', $product->id) }}" method="POST" enctype="multipart/form-data">
            <span class="btn btn-success fileinput-button photo-upload-button">
                <h2><i class="fa fa-cloud-upload"></i>Spustite fajl ovde</h2>
                <h4>ili kliknite</h4>
                <!-- The file input field used as target for the file upload widget -->
                <input type="file" name="file">
            </span>
        </form>

        <br>
        <br>
        <!-- The global progress bar -->
        <div class="progress-bar progress-bar-success col-md-12" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="">
            <span class="sr-only"></span>
        </div>
        <div class="col-md-12 errors-wrapper"></div>

        <span class="wrap-gallery">
            <ul class="packshot gallery uploaded images list-unstyled col-sm-12" data-type="1">
                @if($product->packshots)
                @foreach ($product->packshots as $packshot)
                
                <li class="image" data-name="" data-id="{{ $packshot->id }}" id="file-{{ $packshot->id }}">
                    <img src="/uploads/products/images/{{ $packshot->filename }}" class="packshot-item" width="230" data-id="{{ $packshot->id }}" data-label="{{ $packshot->translations[0]->pivot->label }}" data-label_en="{{ $packshot->translations[1]->pivot->label }}" data-text="{{ $packshot->translations[0]->pivot->text }}" data-text_en="{{ $packshot->translations[1]->pivot->text }}" data-slug="{{ $packshot->translations[0]->pivot->slug }}" data-slug_en="{{ $packshot->translations[1]->pivot->slug }}" data-nutritive-id="{{ $packshot->nutritive_id }}" data-link="{{ $packshot->link }}"></a>
                    <div class="toggle toggle-success {{ $packshot->active ? 'active' : '' }} file-active" title="Status"></div>
                    <button type="button" class="btn btn-danger-alt remove-file"><i class="fa fa-times"></i></button>
                </li>
                @endforeach
                @endif
            </ul>
        </span>
    </div>
</div>

<section class="hidden-elements">
    <form action="{{ route('admin.product.packshot.store', $product->id) }}" method="post" data-resource="packshot" class="form-vertical packshot-form">
        <div class="form-group">
            <label for="label">Naziv i gramatura:</label>
            <!-- <input type="text" name="label" class="form-control"> -->
            {!! ml_input('text', $appLanguages, 'label', $errors, '', 'slugify') !!}
        </div>
        <div class="form-group">
            <label for="" class="control-label">Slug</label>
            {!! ml_input('text', $appLanguages, 'slug', $errors) !!}
        </div>
        <div class="form-group">
            <label for="text">Opis</label>
            <!-- <input type="text" name="text" id="text" class="form-control"> -->
            {!! ml_input('text', $appLanguages, 'text', $errors) !!}
        </div>
        <div class="form-group">
            <label for="link">Youtube ID</label>
            <input type="text" name="link" id="link" class="form-control">
        </div>
        <div class="form-group">
        <label for="">Tabela nutritivnih vrednosti:</label>
        <select name="nutritive_id" id="" class="form-control">
            @foreach($nutritiveTables as $table)
            <option value="{{ $table->id }}">{{ $table->name }}</option>
            @endforeach
        </select>
        </div>
    </form>
</section>