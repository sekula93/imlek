@extends('admin.layout.inner')

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
@stop
@section('content')
    <div id="page-heading">
        <h1>Novi Proizvod</h1>
        <div class="options">
            <a href="{{ route('admin.product.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.product.store') }}" method="post" data-resource="product" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-sm-6 col-md-5">
                                <label for="brand_id" class="control-label">Brend:</label>
                                <select class="form-control" name="brand_id">
                                        <option value="" class="form-control">-- Odaberite --</option>
                                    @foreach($brands as $brand)
                                        <option value="{{ $brand->id }}" class="form-control" {{ old('brand_id') == $brand->id ? 'selected' : '' }}>{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                                <!-- {!! validation_error($errors, 'user_id') !!} -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                <label for="title" class="control-label">Naslov</label>
                                {!! ml_input('text', $appLanguages, 'title', $errors, '', 'slugify') !!}
                            </div>
                            <div class="col-md-12 col-lg-6">
                                <label for="" class="control-label">Slug</label>
                                {!! ml_input('text', $appLanguages, 'slug', $errors) !!}
                            </div>
                        </div>
                     {{--    <div class="row">
                            <div class="col-sm-12">
                                <label for="" class="control-label">Uvod</label>
                                {!! ml_input('textarea', $appLanguages, 'excerpt', $errors) !!}
                            </div>
                        </div>  --}}
                     
                        {{-- <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <label for="web_address" class="control-label">Web adresa</label>
                                <input type="text" name="web_address" class="form-control" value="{{ old('web_address') }}">
                                {!! validation_error($errors, 'web_address') !!}
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="col-sm-12 col-md-4">
                                <label for="featured_image" class="upload-label">Fotografija</label>
                                {!! jasnyupload('featured_image', $errors, 'image/png, image/jpeg') !!}
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <label for="poster" class="upload-label">Fotografija EN</label>
                                {!! jasnyupload('poster', $errors, 'image/png, image/jpeg') !!}
                            </div>
                            <!-- <div class="col-sm-12 col-md-4">
                                <label for="video" class="upload-label">Video</label>
                                {!! jasnyupload('video', $errors, 'video/mp4') !!}
                            </div> -->
                            

                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-3">
                                <label for="fb-link">Facebook link</label>
                                <input type="text" name="fb-link" class="form-control">
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <label for="in-link">Instagram link</label>
                                <input type="text" name="in-link" class="form-control">
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <label for="yt-link">YouTube link</label>
                                <input type="text" name="yt-link" class="form-control">
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <label for="ms-link">Microsite link</label>
                                <input type="text" name="ms-link" class="form-control">
                            </div>
                            <div class="col-sm-offset-9 col-md-3 col-sm-12">
                                <label for="" class="control-label">Microsite button text</label>
                                {!! ml_input('text', $appLanguages, 'text', $errors) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-lg-4">
                                <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="0">
                                    <div class="toggle toggle-success form-active" title="Status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ route('admin.post.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
