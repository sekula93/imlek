@extends('admin.layout.inner')


@section('top_style')
    @parent
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/jquery-fileupload/css/jquery.fileupload-ui.css">
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/lightbox/css/lightbox.css">
@stop

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>

    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.iframe-transport.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.fileupload.js"></script>
    <script type="text/javascript" src="/assets/admin/js/file-upload.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/lightbox/js/lightbox.min.js"></script>
@stop
@section('content')
    <div id="page-heading">
        <h1>Izmeni izvestaj</h1>
        <div class="options">
            <a href="{{ route('admin.report.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <form action="{{ route('admin.report.update', $report) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    <div class="panel-body">
                            
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="attachment_label" class="upload-label">Naziv dokumenta</label>
                                {!! ml_input('text', $appLanguages, 'attachment_label', $errors, $report) !!}
                            </div>
                            <div class="col-sm-6">
                                <label for="attachment" class="upload-label">Dokument</label>
                                {!! jasnyupload('attachment', $errors, 'application/pdf, application/zip, application/x-compressed-zip, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document', config('settings.file.report.upload_dir').$report->attachment, 3) !!}
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <label class="control-class">Datum objave</label>
                                <input type="date" name="created_at" value="{{ $report->created_at->format('Y-m-d') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-lg-4">
                                <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                <div class="checkbox block">
                                    <input type="hidden" name="active" value="{{ $report->active }}">
                                    <div class="toggle toggle-success {{ $report->active ? 'active' : '' }} form-active" title="Status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn-primary btn">Snimi</button>
                                <a href="{{ route('admin.report.index') }}" class="btn btn-default-alt">Odustani</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
