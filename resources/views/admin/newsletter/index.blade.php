@extends('admin.layout.inner')

@section('content')
    <div id="page-heading">
        <h1>Newsletter</h1>
        <div class="options">
            <a href="{{ route('admin.newsletter.export') }}" class="btn btn-success"><i class="fa fa-table"></i> Eksportuj listu</a>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-10 col-lg-8">
                <div class="panel panel-primary">
                    <div class="panel-body resource-container">
                        @if(count($listArray))
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Ime i prezime</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($listArray as $item)
                                <tr data-id="{{ $item->id }}">
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->email }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                        <p class="text-center">Lista je trenutno prazna</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
