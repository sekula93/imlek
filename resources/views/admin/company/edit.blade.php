@extends('admin.layout.inner')
@section('top_style')
    @parent
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/jquery-fileupload/css/jquery.fileupload-ui.css">


@stop

@section('bottom_scripts')
    @parent
    <script type="text/javascript" src="/assets/admin/plugins/form-jasnyupload/fileinput.min.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.iframe-transport.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/jquery-fileupload/js/jquery.fileupload.js"></script>
    <script type="text/javascript" src="/assets/admin/plugins/lightbox/js/lightbox.min.js"></script>
    <script type="text/javascript" src="/assets/admin/ckeditor/ckeditor.js"></script>
     <script type="text/javascript" src="/assets/admin/js/company-packshot-upload.js"></script>
    <script type="text/javascript">
        <?php foreach ($appLanguages as $language) echo "CKEDITOR.replace('trans[$language->id][text]');";  ?>
    </script>
@stop
@section('content')
    <div id="page-heading">
        <h1>Izmena</h1>
        <div class="options">
            <a href="{{ route('admin.company.index') }}" class="btn btn-default-alt"><i class="fa fa-chevron-left"></i> Nazad</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="tab-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#form" data-toggle="tab">Detalji</a></li>
                            <li><a href="#gallery" data-toggle="tab">Fotografije</a></li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="form">
                                <form action="{{ route('admin.company.update', $company) }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}
                                {{ method_field('put') }}
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="" class="control-label">Naslov</label>
                                            {!! ml_input('textarea', $appLanguages, 'title', $errors, $company, 'slugify') !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label for="" class="control-label">Tekst</label>
                                            {!! ml_input('textarea', $appLanguages, 'text', $errors, $company) !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label for="" class="control-label">Uvod</label>
                                            {!! ml_input('textarea', $appLanguages, 'excerpt', $errors, $company) !!}
                                        </div>
                                    </div>
                                    <!-- <div class="row">
                                        <div class="col-lg-6">
                                            <label for="web_address" class="control-label">Web adresa</label>
                                            <input type="text" name="web_address" class="form-control" value="{{ old('web_address', $company->web_address) }}">
                                            {!! validation_error($errors, 'web_address') !!}
                                        </div>
                                    </div> -->
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label for="featured_image" class="upload-label">Fotografija</label>
                                            {!! jasnyupload('featured_image', $errors, 'image/png, image/jpeg', config('settings.image.company.featured.upload_dir').$company->featured_image, 1) !!}
                                        </div>
                                    </div>
                                    <!-- <div class="row">
                                        <div class="col-sm-6 col-lg-4">
                                            <label for="active" class="col-sm-2 control-label">Aktivan</label>
                                            <div class="checkbox block">
                                                <input type="hidden" name="active" value="{{ $company->active }}">
                                                <div class="toggle toggle-success {{ $company->active ? 'active' : '' }} form-active" title="Status"></div>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <button class="btn-primary btn">Snimi</button>
                                            <a href="{{ route('admin.company.index') }}" class="btn btn-default-alt">Odustani</a>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>

                            <div class="tab-pane" id="gallery">
                                <div class="panel-body">
                                    @include('admin.company._partials.photo')
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
