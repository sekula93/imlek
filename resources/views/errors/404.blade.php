<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>404</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
    <link rel="stylesheet" href="/assets/front/css/errors.css">
</head>
<body>

    <div class="logo">
        <a href="/">
            <img src="/assets/front/images/logo.svg">
        </a>
    </div>

    <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" id="intro">
        <path id="wave" d="" fill="#fff"/>
    </svg>
    <div class="div404"></div>
    <div class="div404 no-color">404</div>
    <svg width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" id="intro2">
        <path id="wave2" d="" fill="#fff"/>
    </svg>

    <p class="msg">Ova stranica ne postoji ili je trenutno<br>potopljena u mleku. <a href="/">Vrati se na naslovnu stranu.</a></p>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/front/js/errorPageJs.js"></script>
</body>
</html>